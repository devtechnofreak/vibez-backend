<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_invitations', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('from_user')->unsigned();
			$table->foreign('from_user')->references('id')->on('users')->onDelete('cascade');
			$table->integer('to_user')->unsigned();
			$table->foreign('to_user')->references('id')->on('users')->onDelete('cascade');
			$table->boolean('accepted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_invitations');
    }
}
