<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistNewsTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artist_news', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('artist_id')->unsigned();
			$table->foreign('artist_id')->references('id')->on('artists')->onDelete('cascade');
			$table->integer('news_id')->unsigned();
			$table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('artist_news');
    }
}
