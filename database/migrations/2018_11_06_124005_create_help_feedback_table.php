<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('help_feedback', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('help_id')->unsigned();
			$table->foreign('help_id')->references('id')->on('user_help')->onDelete('cascade');
			$table->text('feedback');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('help_feedback');
    }
}
