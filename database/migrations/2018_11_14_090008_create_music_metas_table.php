<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMusicMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('music_meta', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('music_id')->unsigned();
			$table->foreign('music_id')->references('id')->on('musics');
			$table->string('title');
			$table->string('artist');
			$table->string('album_artist');
			$table->string('album');
			$table->string('genre');
			$table->string('track');
			$table->string('account_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('music_metas');
    }
}
