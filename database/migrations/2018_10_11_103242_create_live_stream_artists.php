<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiveStreamArtists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('live_stream_artists', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('artist_id')->unsigned();
			$table->foreign('artist_id')->references('id')->on('artists')->onDelete('cascade');
			$table->integer('live_stream_id')->unsigned();
			$table->foreign('live_stream_id')->references('id')->on('live_streams')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('live_stream_artists');
    }
}
