@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
<?php 

?>
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Playlist</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-md-3">
											 <strong class="card-title">Edit Playlist</strong>
										</div>
										<div class="col-md-9">
											 <a href="{{url('admin/live-streams')}}" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> LiveStream List</a>
										</div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>

					@if(session()->has('message'))
					<script type="text/javascript">
					jQuery(document).ready(function($){
					        $.notify({
					            icon: "nc-icon nc-app",
					            message: '{{ session()->get('message') }}',
					        }, {
					            type: 'success',
					            timer: 8000,
					        });
					 });
					 </script>
					@endif
					@if ($errors->any())
						  @foreach ($errors->all() as $error)
								<script type="text/javascript">
								jQuery(document).ready(function($){
								        $.notify({
								            icon: "nc-icon nc-app",
								            message: '{{ $error }}',
								        }, {
								            type: 'danger',
								            timer: 8000,
								        });												
								});
								 </script>
						  @endforeach
					@endif        

        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">
						<div class="card"> 
							<!-- <div class="card-header">
								<strong class="card-title">Edit Playlist</strong>
							</div> -->
							<div class="card-body row">
								<div class="col-md-8">
									<form method="POST" action="<?php echo URL::to('/admin/live-stream/update'); ?>" class="artist_edit_form"  enctype="multipart/form-data">
									 {{ csrf_field() }}
								<div class="form-group row"><label for="name" class="col-md-4 col-form-label">Title</label>
									<div class="col-md-6"><input id="title" name="title" value="{{$stream->title}}" required="required" placeholder="Title" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								@if ($errors->has('title'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif								
								<div class="form-group row"><label for="url" class="col-md-4 col-form-label">Live Url</label>
									<div class="col-md-6"><input id="url" name="url" value="{{$stream->url}}" placeholder="Live Url" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								@if ($errors->has('url'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                                <div class="form-group row"><label for="description" class="col-md-4 col-form-label">Description</label>
									<div class="col-md-6"><textarea name="description" class="form-control" placeholder="Description" style="height: auto !important">{{$stream->description}}</textarea></div>
								</div>
								<div class="form-group row"><label for="start_time" class="col-md-4 col-form-label">Start Time</label>	
									<div class="col-md-6">
										<input name="start_time" value="{{$stream->start_time}}" required="required" autofocus="autofocus" class="form-control datetimepicker" type="text" id="datetimepicker">
									</div>
								</div>
								<div class="form-group row"><label for="start_time" class="col-md-4 col-form-label">End Time</label>	
									<div class="col-md-6">
										<input name="end_time" value="{{$stream->end_time}}" required="required" autofocus="autofocus" class="form-control datetimepicker" type="text">
									</div>
								</div>
								<div class="form-group row"><label for="image" class="col-md-4 col-form-label">Image</label>
									<div class="col-md-6"><input id="image" name="image" value="" required="required" autofocus="autofocus" class="form-control " type="file"></div>
								</div>
								@if ($errors->has('image'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                                
								<div class="form-group row">
									<label for="artist_id" class="col-md-4 col-form-label">Artist</label>
									<div class="col-md-6">
										<select name="artist_id[]" value="" class="js-example-basic-multiple form-control" multiple="multiple">
											  @foreach($artists as $artist)
											  	<option value="{{ $artist->id }}" @if(in_array($artist->id,collect($stream->artists)->pluck('id')->all())) selected @endif>{{ $artist->name }}</option>
											  @endforeach
											</select>
									</div> 
								</div>									
										
										<div class="form-group row mb-0 user_form_submit">
											<div class="col-md-6 offset-md-4">
												<input type="hidden" name="livestream_id" value="{{$stream->id}}" /> 
												<a class="btn btn-primary update_artist_btn">Update Live Stream</a>
											</div>
										</div>
									</form>
								</div>
								<div class="col-md-4">
									<div class="card">
				                            <div class="card-image">
				                                <img src="{{$stream->image}}" class="artist_image">
				                            </div>
				                            <div class="card-body">
				                            		<h3 class="title">{{$stream->title}}</h3>
				                           	</div>
				                            <hr>
				                    </div>
								</div>
							</div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/plugins/select2/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/css/flatpickr.min.css') }}">
<!-- <script src="{{ asset('admin_assets/js/vendor/jquery-2.1.4.min.js') }}"></script> -->
<script src="{{ asset('admin_assets/plugins/select2/select2.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/flatpickr.js') }}"></script>

<script type="text/javascript">
	
    $(document).ready(function() {
    	$(".datetimepicker").flatpickr({
		    enableTime: true,
		    dateFormat: "Y-m-d H:i:s",
		});
	    $('.js-example-basic-multiple').select2();
	});
	$(window).load(function(){
		$('#start_time input').val('2018-10-11 16:46:51');
	})
</script>
	
@stop
