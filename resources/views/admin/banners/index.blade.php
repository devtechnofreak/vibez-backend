@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
		<!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Banner</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-4">
			                        		<strong class="card-title newslist-name">Banners List</strong>
			                        	</div>
			                            <div class="col-sm-8">	
			                            	<a class="btn btn-primary float-right ml-2" href="<?php echo URL::to('/admin/banners/add'); ?>"><strong class="card-title"><i class="fa fa-plus"></i> Add New</strong></a>
			                            	<a href="{{ url('admin/banner/trash')}}" class="btn btn-secondary float-right ml-2"><strong class="card-title"><i class="fa fa-trash"></i> View Trash ({{App\Banner::onlyTrashed()->count()}})</strong></a>
			                            </div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>



        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <strong class="card-title">Banners List</strong>
                            <div class="float-right">
                            	<a class="btn btn-primary float-right ml-2" href="<?php echo URL::to('/admin/banners/add'); ?>"><strong class="card-title">Add New</strong></a>
                            </div>
                        </div> -->
                        <div class="card-body">
							@if(session()->has('message'))
								<div class="alert alert-success">
									{{ session()->get('message') }}
								</div>
							@endif
							<div class="video_bulk_action">
	                        	<label>Bulk Action 
	                        		<select name="bulk_action" class="form-control form-control-sm">
	                        			<option value="">Select action</option>
	                        			<option value="trash">Move to trash</option>
	                        			<option value="permenentt">Permanently delete</option>
	                        		</select>
	                        	</label>
	                        	<a class="apply_action">Apply</a>
	                        </div>
							<table id="banner_list" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>
										<th><input type="checkbox" class="deleteall delete_row"></th>
										<th>Image</th>
										<th>Title</th>
										<th>Type</th>
										<th>Items</th>
										<th>Clicked</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th><input type="checkbox" class="deleteall delete_row"></th>
										<th>Image</th>
										<th>Title</th>
										<th>Type</th>
										<th>Items</th>
										<th>Clicked</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
                        </div>
                    </div>
                </div>
				<input type="hidden" class="csrf_token" value="{{ csrf_token() }}">
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
<script src="{{ asset('admin_assets/js/lib/data-table/datatables.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/jszip.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/vfs_fonts.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/datatables-init.js') }}"></script>

<script type="text/javascript">

	$(document).ready(function() {
		$('.deleteall').click(function(){
			$('#banner_list tbody .delete_row').each(function(){
				if($(this).is(':checked')){
					$(this).prop('checked',false);	
				}else{
					$(this).prop('checked',true);
				}
				
			})
		});
		$('.apply_action').click(function(){
			var action = "";
			if($('.video_bulk_action select').val() == "trash"){
				var r = confirm("Are you sure want to move to trash selected banners!");
			    if (r == true) {
			        action = "trash";
			    } else {
			     return false
			    }
			}
			if($('.video_bulk_action select').val() == "permenentt"){
				var r = confirm("Are you sure want to permenently delete selected banners!");
			    if (r == true) {
			        action = "delete";
			    } else {
			     return false
			    }
			}
			if(action != ""){
				var banners = [];
				$('#banner_list').find('.delete_bulk').each(function(){ 
					if($(this).is(':checked')){
						banners.push($(this).val());
					}
				});
				var csrf_token = jQuery(document).find('.csrf_token').val();
				$.ajax({
		       	    beforeSend: function(xhrObj){
			                xhrObj.setRequestHeader("X-CSRF-TOKEN",csrf_token); 
			        },
				   url: site_url+"/admin/banner/bulk/action",
				   data: {action	: action,banners : banners},
				   success: function(data) {
				   	window.location.reload();
				   },
				   type: 'POST'
				});
			}
		})
		var csrf_token = jQuery(document).find('.csrf_token').val();
		$('#banner_list').DataTable( {
			"processing": true,
			"serverSide": true,
			"bAutoWidth": false,
			"pageLength": 50,
			"ajax": {
				"url": "banners/get-banners",
				"type": "POST",
				"dataType": 'json',
                "error": function(data){
                        console.log(data);
                    },
				"headers" : {
						'X-CSRF-TOKEN': csrf_token
				}
			},
			"columns": [
				{ "data": "id", "orderable": false,    render: function ( data, type, row ) {
						return '<input type="checkbox" data-id="'+data+'" class="delete_bulk delete_row" value="'+data+'">';
					return data;
				} },
				{ "data": "image" ,     render: function ( data, type, row ) {
						return '<img src="'+data+'" class="img-thumbnail" style="" />';
					return data;
				},"width": "12%"},
				{ "data": "title" },
				{ "data": "type","width": "150px"  },
				{ "data": "item",     render: function ( data, type, row ) {
						// console.log(data.type);
						var edittype = data.type.toLowerCase();
						var bannerlink = "<?= URL::to('/admin/"+edittype+"/edit/"+data.bannerable_id+"'); ?>";
						return '<a class="text-warning ml-2" href="'+bannerlink+'">'+data.name+'</a>';
					
					return data.name;
				}},				
				{ "data": "cnt" },						
				{ "data": "id",     render: function ( data, type, row ) {
						// console.log(data);
						var editlink = "<?= URL::to('/admin/banners/edit/"+data+"'); ?>";
						var deletelink = "<?= URL::to('/admin/banners/delete/"+data+"'); ?>";
						return '<li class="libtnlink"><a class="text-warning ml-2" href="'+editlink+'"><i class="fa fa-pencil"></i></a></li><li class="libtnlink"><a href="#confirmationDelete" class="btn btn-danger delete-smt-btn delete_link text-danger ml-2" data-toggle="modal" data-id="'+data+'" data-url="'+deletelink+'" data-target="#confirmationDelete"><i class="fa fa-trash"></i></a></li>';
					
					return data;
				}}
			]
		} );
		
		
    } );
</script>	
<script type="text/javascript">
$(document).ready(function(){
	$('#banner_list').on('click', '.delete-smt-btn', function(e){
		$('#confirmationDelete').modal("show");
		var id = $(this).attr('data-id');
		var url = $(this).attr('data-url');
		$(".del-link").attr("href",url);
	});
});
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var csrf_token = jQuery(document).find('.csrf_token').val();
	$('tbody').sortable({
    	stop: function(event, ui) {
        	//alert("New position: " + ui.item.index());

        	var selectedData = new Array();
        	var selectedTable = $('#banner_list').DataTable();
			var selectedTableInfo = selectedTable.page.info();
        	

        	$('#banner_list .ui-sortable tr').each(function(index, item) {


        		var uniq_id = $(this).find('.delete_row').attr('data-id');
        		var row_id	= $(this).attr('id');
        		var position = selectedTableInfo.start + index;
        			selectedData.push({'position': position, 'uniq_id': uniq_id, 'row_id': row_id});

			});
			//console.log(selectedData);
			updateOrder(selectedData);
    	}
	});


	function updateOrder(data)  
	{
		var csrf_token = jQuery(document).find('.csrf_token').val();
		$.ajax({
		beforeSend: function(xhrObj){
	                xhrObj.setRequestHeader("X-CSRF-TOKEN",csrf_token); 
	        },
        url: site_url+"/admin/banner/updateOrder",
        data : { 'position' :data},
        type: 'POST',
        	success: function (data) {
            	
        	}
    	});
	}  
});  

</script>
<!-- Delete Model -->

<!-- <form action="" method="POST" class="remove-record-model"> -->
    <div id="confirmationDelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;top: 25% !important;">
        <div class="modal-dialog modal-md modal-notify modal-danger" style="width:55%;">
            <div class="modal-content">
                <div class="modal-header">
                	<h4 class="modal-title" id="custom-width-modalLabel">Are you sure you want to delete this record?</h4>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect remove-data-from-delete-form" data-dismiss="modal">Close</button>

                    <a class="btn btn-default text-danger del-link" href="<?php echo URL::to('/admin/album/delete/'); ?>">Delete</a>
                </div>
            </div>
        </div>

    </div>
<!-- </form> -->
@stop

