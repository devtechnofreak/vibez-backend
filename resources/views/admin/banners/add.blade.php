@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->

        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Banners</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="breadcrumbs navbar-top">
            <div class="card">
                <div class="card-header border-header">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-3">
                                <strong class="card-title">Add Banner</strong>
                            </div>
                            <div class="col-md-9">
                                <a href="{{ url('admin/banners') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Banners List</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
					@if(session()->has('message'))
					<script type="text/javascript">
					jQuery(document).ready(function($){
					        $.notify({
					            icon: "nc-icon nc-app",
					            message: '{{ session()->get('message') }}',
					        }, {
					            type: 'success',
					            timer: 8000,
					        });
					 });
					 </script>
					@endif
					@if ($errors->any())
						  @foreach ($errors->all() as $error)
								<script type="text/javascript">
								jQuery(document).ready(function($){
								        $.notify({
								            icon: "nc-icon nc-app",
								            message: '{{ $error }}',
								        }, {
								            type: 'danger',
								            timer: 8000,
								        });												
								});
								 </script>
						  @endforeach
					@endif
        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card"> 
                        <!-- <div class="card-header">
							<strong class="card-title">Add Banner</strong>
							<a href="{{ url('admin/banners') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Back</a>
						</div> -->
						<div class="card-body">
							<form method="POST" action="<?php echo URL::to('/admin/banners/create'); ?>" enctype="multipart/form-data" class="add_banner">
							 	{{ csrf_field() }}
								
								<div class="form-group row"><label for="name" class="col-md-4 col-form-label">Title</label>
									<div class="col-md-6"><input id="title" name="title" value="" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								@if ($errors->has('title'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
								<div class="form-group row"><label for="image" class="col-md-4 col-form-label">Image</label>
									<div class="col-md-6"><input id="image" name="image" value="" required="required" autofocus="autofocus" class="form-control" type="file"></div>
								</div>
								@if ($errors->has('image'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                                
                                <div class="form-group row">
                                	<label for="" class="col-md-4 col-form-label">Type</label>
									<div class="col-md-6 ">
										<select name="type" value="" class="col-md-6 form-control banner_type">
											<option name="type" value="">Select Type</option>
											<option name="type" value="Album">Album</option>
											<option name="type" value="Artist">Artist</option>
											<option name="type" value="Playlist">Playlist</option>
											<option name="type" value="Music">Music</option>
											<option name="type" value="News">News</option>
											<option name="type" value="Video">Video</option>
											<option name="type" value="LiveStream">Live</option>
										</select>
									</div> 
								</div> 
								<div class="form-group row"><label for="item" class="col-md-4 col-form-label">Items</label>
									<div class="col-md-6">
										<select name="item" value="" class="col-md-6 form-control banner_item">
											<option name="type" value="">Select Item</option>
										</select>
									</div>
								</div>
								
								<div class="form-group row mb-0">
									<div class="col-md-6 offset-md-4">
										<a class="btn btn-primary add_banner_btn">Add Banner</a>
									</div>
								</div>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
@stop

