@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
<?php 

?>
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Banner</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-4">
			                        		<strong class="card-title newslist-name">Edit Banner</strong>
			                        	</div>
			                            <div class="col-sm-8">	
			                            	<a href="{{ url('admin/banners') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Banners List</a>
			                            </div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>

        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif
						<div class="card"> 
							<!-- <div class="card-header">
								<strong class="card-title">Edit Banner</strong>
								<a href="{{ url('admin/banners') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Back</a>
							</div> -->
							<div class="card-body row">
								 @if ($errors->any())
									  <div class="alert alert-danger">
										  <ul>
											  @foreach ($errors->all() as $error)
												  <li>{{ $error }}</li>
											  @endforeach
										  </ul>
									  </div><br/>
								  @endif
								<div class="col-md-8">
									<form method="POST" action="<?php echo URL::to('/admin/banners/editdata'); ?>" class="banner_edit_form"  enctype="multipart/form-data">
									 {{ csrf_field() }}
									
									<div class="form-group row"><label for="name" class="col-md-4 col-form-label">Title</label>
										<div class="col-md-6"><input id="title" name="title" value="{{ $banner->title}}" required="required" autofocus="autofocus" class="form-control" type="text"></div>
									</div>
									@if ($errors->has('title'))
	                                    <span class="invalid-feedback">
	                                        <strong>{{ $errors->first('title') }}</strong>
	                                    </span>
	                                @endif
									<div class="form-group row"><label for="image" class="col-md-4 col-form-label">Image</label>
										<div class="col-md-6"> <input id="image" name="image" value="" required="required" autofocus="autofocus" class="form-control" type="file"></div>
									</div>
									@if ($errors->has('image'))
	                                    <span class="invalid-feedback">
	                                        <strong>{{ $errors->first('image') }}</strong>
	                                    </span>
	                                @endif
	                                
	                                <div class="form-group row"><label for="dob" class="col-md-4 col-form-label">Type</label>
										<div class="col-md-6 ">
											<select name="type" value="" class="col-md-6 form-control banner_type">
												<option name="type" value="">Select Type</option>
												<option name="type" value="Album" @if($banner->type == 'Album') selected @endif>Album</option>
												<option name="type" value="Artist" @if($banner->type == 'Artist') selected @endif>Artist</option>
												<option name="type" value="Playlist" @if($banner->type == 'Playlist') selected @endif>Playlist</option>
												<option name="type" value="Music" @if($banner->type == 'Music') selected @endif>Music</option>
												<option name="type" value="News" @if($banner->type == 'News') selected @endif>News</option>
												<option name="type" value="Video" @if($banner->type == 'Video') selected @endif>Video</option>
												<option name="type" value="Video" @if($banner->type == 'LiveStream') selected @endif>Live</option>
											</select>
										</div> 
									</div>
									<div class="form-group row"><label for="dob" class="col-md-4 col-form-label">Items</label>
										<div class="col-md-6">
											<select name="item" value="" class="col-md-6 form-control banner_item">
											@foreach($banner->bannerable->all() as $obj)
												<option name="item" value="{{$obj->id}}" @if($banner->bannerable->id == $obj->id) selected @endif>{{$obj->title?:$obj->name}}</option>
											@endforeach
											</select>
											<!-- <div class="d-inline-block bg-info pl-2 pr-2 mb-2 mt-2 text-light">{{$banner->bannerable->title?:$banner->bannerable->name}} 
											</div> -->
											
										</div>
									</div>
										<div class="form-group row mb-0 user_form_submit">
											<div class="col-md-6 offset-md-4">
												<input type="hidden" name="banner_id" value="{{$banner->id}}" /> 
												<a class="btn btn-primary update_banner_btn">Update Banner</a>
											</div>
										</div>
									</form>
								</div>
								<div class="col-md-4">
									<div class="card">
				                            <div class="card-image">
				                                <img src="{{$banner->image}}" class="artist_image">
				                            </div>
				                            <div class="card-body">
				                            		<h3 class="title">{{ $banner->title}}</h3>
			                                      
				                                	<div class="d-inline-block bg-info pl-2 pr-2 mb-2 text-light">{{$banner->bannerable->title?:$banner->bannerable->name}}</div>
				                                	
				                            </div>
				                            <hr>
				                    </div>
								</div>
							</div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
@stop

