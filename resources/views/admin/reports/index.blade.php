@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->

        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Staff Users</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Staff Users</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-12">
			                        		<strong class="card-title user-name">Reports</strong>
			                        	</div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>

        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <strong class="card-title">Staff Users List</strong>
                        </div> -->
                        <div class="card-body">
							<table id="users" class="table table-striped table-bordered usertb" style="width:100%">
								<thead>
									<tr>
										<th>From User</th>
										<th>Report User</th>
										<th>Description</th>
										<th>Feedback</th>
										<th>Created At</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>From User</th>
										<th>Report User</th>
										<th>Description</th>
										<th>Feedback</th>
										<th>Created At</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
                        </div>
                    </div>
                </div>
				<input type="hidden" class="csrf_token" value="{{ csrf_token() }}">
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	<script src="{{ asset('admin_assets/js/lib/data-table/datatables.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/jszip.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/vfs_fonts.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/datatables-init.js') }}"></script>


<script type="text/javascript">
	$(document).ready(function() {
		var csrf_token = jQuery(document).find('.csrf_token').val();
		$('#users').DataTable( {
			"processing": true,
			"serverSide": false,
			"bAutoWidth": false,
			"pageLength": 50,
			"ajax": {
				"url": "report/all",
				"type": "POST",
				"dataType": 'json',
                "error": function(data){
                        console.log(data);
                    },
				"headers" : {
						'X-CSRF-TOKEN': csrf_token
				}
			},
			"columns": [
				{ "data": "from_user"},
				{ "data": "to_user" },
				{ "data": "description","width": "300px"},
				{ "data": "feedback","width": "300px"},
				{ "data": "created_at" },
				{ "data": "id",     render: function ( data, type, row ) {
						// console.log(data);
						var editlink = "<?php echo URL::to('/admin/report/feedback/"+data+"'); ?>";
						return '<li class="libtnlink"><a class="edit_link" href="'+editlink+'" alt="Reply"><i class="fa fa-mail-reply-all"></i></a></li>';
					
					return data;
				}}
			]
		} );
		
		
    } );
</script>
@stop

