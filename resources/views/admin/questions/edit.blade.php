@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
<?php 
$address_line1 = array_search('address_line1', array_column($data[0]['user_meta'], 'meta_key'));
$address_line2 = array_search('address_line2', array_column($data[0]['user_meta'], 'meta_key'));
$pincode = array_search('pincode', array_column($data[0]['user_meta'], 'meta_key'));
$city = array_search('city', array_column($data[0]['user_meta'], 'meta_key'));
$state = array_search('state', array_column($data[0]['user_meta'], 'meta_key'));
$country = array_search('country', array_column($data[0]['user_meta'], 'meta_key'));


$address_line1 	= (is_numeric($address_line1)) ? $data[0]['user_meta'][$address_line1]['meta_value'] : '';
$address_line2 	= (is_numeric($address_line2)) ? $data[0]['user_meta'][$address_line2]['meta_value'] : '';
$pincode 		= (is_numeric($pincode)) ? $data[0]['user_meta'][$pincode]['meta_value'] : '';
$city 			= (is_numeric($city)) ? $data[0]['user_meta'][$city]['meta_value'] : '';
$state 			= (is_numeric($state)) ? $data[0]['user_meta'][$state]['meta_value'] : '';
$country 	= (is_numeric($country)) ? $data[0]['user_meta'][$country]['meta_value'] : '';


?>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li class="active">Edit User</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif
						<div class="card"> 
							<div class="card-header">
								<strong class="card-title">Edit User</strong>
							</div>
							<div class="card-body">
								<h5>User details : </h5>
								 @if ($errors->any())
									  <div class="alert alert-danger">
										  <ul>
											  @foreach ($errors->all() as $error)
												  <li>{{ $error }}</li>
											  @endforeach
										  </ul>
									  </div><br/>
								  @endif
								<form method="POST" action="<?php echo URL::to('/admin/user/editdata'); ?>">
								 {{ csrf_field() }}
									<div class="form-group row"><label for="name" class="col-md-2 col-form-label text-md-left">Name</label>
										<div class="col-md-6"><input id="name" name="name" required="required" autofocus="autofocus" class="form-control" value="<?php echo $data[0]['name']; ?>" type="text"></div>
									</div>
									@if ($errors->has('name'))
										<span class="invalid-feedback">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
									@endif
									<div class="form-group row"><label for="email" class="col-md-2 col-form-label text-md-left">E-Mail Address</label>
										<div class="col-md-6"><input id="email" name="email"  required="required" class="form-control" type="email" value="<?php echo $data[0]['email']; ?>" disabled></div>
									</div>
									@if ($errors->has('email'))
										<span class="invalid-feedback">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
									@endif
									<div class="form-group row"><label for="password" class="col-md-2 col-form-label text-md-left">Password</label>
										<div class="col-md-6"><input id="password" name="password" class="form-control" type="password"></div>
									</div>
									@if ($errors->has('password'))
										<span class="invalid-feedback">
											<strong>{{ $errors->first('password') }}</strong>
										</span>
									@endif
									<div class="form-group row"><label for="password-confirm" class="col-md-2 col-form-label text-md-left">Confirm Password</label>
										<div class="col-md-6"><input id="password-confirm" name="password_confirmation" class="form-control" type="password"></div>
									</div>
									<h5>User Additional details : </h5>
									<div class="form-group row"><label for="user-address1" class="col-md-2 col-form-label text-md-left">Address Line 1</label>
										<div class="col-md-6"><input id="user-address1" name="address_line1" class="form-control" type="text" value="<?php echo $address_line1; ?>"></div>
									</div>
									<div class="form-group row"><label for="user-address2" class="col-md-2 col-form-label text-md-left">Address Line 2</label>
										<div class="col-md-6"><input id="user-address2" name="address_line2" class="form-control" type="text" value="<?php echo $address_line2; ?>"></div>
									</div>
									<div class="form-group row"><label for="user-pincode" class="col-md-2 col-form-label text-md-left">Pincode</label>
										<div class="col-md-6"><input id="user-pincode" name="pincode" class="form-control" type="text" value="<?php echo $pincode; ?>"></div>
									</div>
									<div class="form-group row"><label for="user-city" class="col-md-2 col-form-label text-md-left">City</label>
										<div class="col-md-6"><input id="user-city" name="city" class="form-control" type="text" value="<?php echo $city; ?>"></div>
									</div>
									<div class="form-group row"><label for="user-state" class="col-md-2 col-form-label text-md-left">State</label>
										<div class="col-md-6"><input id="user-state" name="state" class="form-control" type="text" value="<?php echo $state; ?>"></div>
									</div>
									<div class="form-group row"><label for="user-country" class="col-md-2 col-form-label text-md-left">Country</label>
										<div class="col-md-6"><input id="user-country" name="country" class="form-control" type="text" value="<?php echo $country; ?>"></div>
									</div>
									<div class="form-group row mb-0 user_form_submit">
										<div class="col-md-6 offset-md-4">
											<input type="hidden" name="user_id" value="<?php echo $data[0]['id']; ?>" /> 
											<button type="submit" class="btn btn-primary">
												Update User
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
@stop

