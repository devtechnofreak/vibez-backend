@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li class="active">Question Categories</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Question Categories</strong>
                        </div>
                        <div class="card-body">
						
							<table id="categories" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>									
										<th>Category</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>Category</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
                        </div>
                    </div>
                </div>
				<input type="hidden" class="csrf_token" value="{{ csrf_token() }}">
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
		
		
	<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/css/lib/datatable/jquery.dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/css/lib/datatable/buttons.dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/css/lib/datatable/select.dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/css/lib/datatable/editor.dataTables.min.css') }}">

	
	<script src="{{ asset('admin_assets/js/lib/data-table/datatables.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.select.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.editor.min.js') }}"></script>

<script src="{{ asset('admin_assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/jszip.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/vfs_fonts.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/datatables-init.js') }}"></script>


<script type="text/javascript">

var editor;
var csrf_token = jQuery(document).find('.csrf_token').val();
	$(document).ready(function() {
		
		editor = new $.fn.dataTable.Editor( {
		ajax: {
				"url": "/admin/question/categories/updatedata",
				"type": "POST",
				"dataType": 'json',
                "error": function(data){
                        console.log(data);
                    },
				"headers" : {
						'X-CSRF-TOKEN': csrf_token
				}
			},
		
		table: "#categories",
		fields: [ {
				label: "Category:",
				name: "category"
			}
		]
	} );

	// Activate an inline edit on click of a table cell
	$('#categories').on( 'click', 'tbody td:not(:first-child)', function (e) {
		editor.inline( this, {
			buttons: { label: '&gt;', fn: function () { this.submit(); } }
		} );
	} );

	$('#categories').DataTable( {
		dom: "Bfrtip",
		ajax: {
				"url": "/admin/question/categories/getdata",
				"type": "POST",
				"dataType": 'json',
                "error": function(data){
                        console.log(data);
                    },
				"headers" : {
						'X-CSRF-TOKEN': csrf_token
				}
			},
		columns: [
				{ "data": "category" },
				{ "data": "id",     render: function ( data, type, row ) {
						console.log(data);
						var editlink = "<?php echo URL::to('/admin/user/edit/"+data+"'); ?>";
						var deletelink = "<?php echo URL::to('/admin/user/delete/"+data+"'); ?>";
						return '<a class="edit_link" href="'+editlink+'"><i class="fa fa-pencil"></i></a><a class="delete_link" href="'+deletelink+'"><i class="fa fa-trash"></i></a>';
					
					return data;
				} }
			],
		order: [ 1, 'asc' ],
		select: {
			style:    'os',
			selector: 'td:first-child'
		},
		buttons: [
			{ extend: "create", editor: editor },
			{ extend: "edit",   editor: editor },
			{ extend: "remove", editor: editor }
		]
	} );
		
    } );
</script>	
@stop

