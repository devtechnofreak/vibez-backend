@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li class="active">Data table</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Data Table</strong>
                        </div>
                        <div class="card-body">
							<table id="questions" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>Question</th>
										<th>Category</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>Question</th>
										<th>Category</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
                        </div>
                    </div>
                </div>
				<input type="hidden" class="csrf_token" value="{{ csrf_token() }}">
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	<script src="{{ asset('admin_assets/js/lib/data-table/datatables.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/jszip.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/vfs_fonts.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/datatables-init.js') }}"></script>


<script type="text/javascript">
	$(document).ready(function() {
		var csrf_token = jQuery(document).find('.csrf_token').val();
		$('#questions').DataTable( {
			"processing": true,
			"serverSide": true,
			"pageLength": 50,
			"bLengthChange": false,
			"ajax": {
				"url": "question/getdata",
				"type": "POST",
				"dataType": 'json',
                "error": function(data){
                        console.log(data);
                    },
				"headers" : {
						'X-CSRF-TOKEN': csrf_token
				}
			},
			"columns": [
				{ "data": "question" },
				{ "data": "category" },
				{ "data": "id",     render: function ( data, type, row ) {
						console.log(data);
						var editlink = "<?php echo URL::to('/admin/user/edit/"+data+"'); ?>";
						var deletelink = "<?php echo URL::to('/admin/user/delete/"+data+"'); ?>";
						return '<a class="edit_link" href="'+editlink+'"><i class="fa fa-pencil"></i></a><a class="delete_link" href="'+deletelink+'"><i class="fa fa-trash"></i></a>';
					
					return data;
				} }
			]
		} );
		
		
    } );
</script>	
@stop

