@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
<?php 

?>
		<div class="breadcrumbs navbar-top">
			<div class="card">
				<div class="card-header border-header">
				<div class="col-sm-12">
					<div class="row">
						<div class="col-md-3">
							<strong class="card-title">Edit User Help</strong>
						</div>
						<div class="col-md-9">
							<a href="{{ url('admin/help') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> User Request</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">
					@if(session()->has('message'))
					<script type="text/javascript">
					jQuery(document).ready(function($){
					        $.notify({
					            icon: "nc-icon nc-app",
					            message: '{{ session()->get('message') }}',
					        }, {
					            type: 'success',
					            timer: 8000,
					        });
					 });
					 </script>
					@endif
						<div class="card"> 
							<!-- <div class="card-header border-header">
								<strong class="card-title">Edit Artist</strong>
								<a href="{{ url('admin/artists') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Artist List</a>
							</div> -->
							<div class="card-body row">
								 @if ($errors->any())
									  @foreach ($errors->all() as $error)
											<script type="text/javascript">
											jQuery(document).ready(function($){
											        $.notify({
											            icon: "nc-icon nc-app",
											            message: '{{ $error }}',
											        }, {
											            type: 'danger',
											            timer: 8000,
											        });												
											});
											 </script>
									  @endforeach
								  @endif
								  <div class="col-md-7">
										<div class="help_form">
											<fieldset>
												<legend>User Info</legend>
												<form>
												  <div class="form-group row">
												    <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>
												    <div class="col-sm-10">
												      <input type="text" readonly class="form-control" id="inputEmail3" placeholder="Email" value="{{$userhelpdate[0]['name']}}">
												    </div>
												  </div>
												  <div class="form-group row">
												    <label for="inputPassword3" class="col-sm-2 col-form-label">Email</label>
												    <div class="col-sm-10">
												      <input type="text" readonly class="form-control" id="inputPassword3" placeholder="Password" value="{{$userhelpdate[0]['email']}}">
												    </div>
												  </div>
												  <div class="form-group row">
												    <label for="inputPassword3" class="col-sm-2 col-form-label">Vibez ID</label>
												    <div class="col-sm-10">
												      <input type="text" readonly class="form-control" id="inputPassword3" placeholder="Password" value="{{$userhelpdate[0]['vibez_id']}}">
												    </div>
												  </div>
												  <div class="form-group row">
												    <label for="inputPassword3" class="col-sm-2 col-form-label">User Image</label>
												    <div class="col-sm-10">
												    	@php
												    		$img = "/".$userhelpdate[0]['image'];
												    		if ($userhelpdate[0]['image'] == '') {
												    			$img = "/images/user_placeholder.png";
												    		}
												    	@endphp
												      <img src="{{$img}}" class="img-thumbnail" alt="Cinque Terre" style="height:150px;">
												    </div>
												  </div>											  												  
												</form>
											</fieldset>
											<hr>
											<fieldset>
												<legend>User Request</legend>
												<div class="form-group row">
												    <label for="inputEmail3" class="col-sm-2 col-form-label">Request</label>
												    <div class="col-sm-10">												      
												      <textarea class="form-control" rows="5" readonly>{{$userhelpdate[0]['help']}}</textarea>
												    </div>
												</div>
											</fieldset>
											<hr>
											<fieldset>
												<legend>Reply</legend>
												<form class="edit_user_subscription" method="post" action="<?php echo URL::to('/admin/help/reply'); ?>" enctype="multipart/form-data">
													{{ csrf_field() }} 
													<div class="form-group row">
														<label for="description" class="col-sm-2 col-form-label">Feedback</label>
														<div class="col-sm-10">
															<textarea id="description" name="feedback" value="" required="required" autofocus="autofocus" class="form-control"></textarea>
														</div>
													</div>
													<div class="form-group row">
														<label for="gender" class="col-sm-2 col-form-label text-md-left">Status</label>
														<div class="col-sm-10">
															<select class="form-control" name="status" required>
																<option value="">Please select Status</option>
																<option value="0">Pending</option>
																<option value="1">Resolve</option>
																<option value="2">Close</option>
															</select>
														</div>
													</div>
													<div class="form-group row">
														<div class="col-sm-2"></div>												      
													    <div class="col-sm-10">
													    	<input type="hidden" name="help_id" value="<?php echo $userhelpdate[0]['id']; ?>">
													    	<input type="hidden" name="user_id" value="<?php echo $userhelpdate[0]['user_id']; ?>">
													    	<input type="hidden" name="name"  value="{{$userhelpdate[0]['name']}}">
													    	<input type="hidden" name="email" value="{{$userhelpdate[0]['email']}}">
													      	<input class="btn btn-success" type="submit" name="submit" value="Reply">
													    </div>
													</div>
												</form>	
											</fieldset>
										</div>
										
									</div>
									<div class="col-md-5">
										<div class="user_help_history">
											<h4>Help Overview</h4>
											<div class="help_section">
												<p class="user"><?php echo $userhelpdate[0]['help'] ?></p>
											</div>
											@php
												$status = ['pending', 'Solved', 'Closed'];
											@endphp	
											<table class="user_subscription_history" border="1">
												<thead>
													<tr>
														<th>Reply</th>
														<th>Status</th>
														<th>Date</th>
													</tr>
												</thead>
												<tbody>
													@foreach ($user_help_feedback as $single_feedback)
														<tr>
															<td>{!! $single_feedback['feedback'] !!}</td>
															<td>{{ $status[$single_feedback['status']] }}</td>
															<td>{{ $single_feedback['created_at'] }}</td>
														</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
							</div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
		<script src="{{ asset('admin_assets/plugins/ckeditor/ckeditor.js') }}"></script>

		<script>
			$(document).ready(function(){

				 setTimeout( function () {
				 	$('[name="status"]').val("{{ $userhelpdate[0]['status'] }}");
				 }, 2000);

				CKEDITOR.config.allowedContent = true;
			    CKEDITOR.config.extraAllowedContent = '*(*);*{*}';
			    CKEDITOR.replace('description', {
				    toolbar: [
				        // ["Source"],
				        ["Bold","Italic","Underline"],
				        ["Link","Unlink","Anchor"],
				        ["Styles","Format","Font","FontSize"],
				        ["TextColor","BGColor"],
				        ["UIColor","Maximize","ShowBlocks"],
				        // ['Image'],
				        "/",
				        ["NumberedList","BulletedList","Outdent","Indent","Blockquote","CreateDiv","JustifyLeft","JustifyCenter","JustifyRight","JustifyBlock"],
				        
				    ],
				    // filebrowserImageUploadUrl: 'https://www.thevibez.net/admin/news/attachImage?type=Images&_token=' + $('meta[name=csrf-token]').attr("content"),
			    });
			})
		</script>
@stop

