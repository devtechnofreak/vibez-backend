@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
		
        <div class="breadcrumbs navbar-top">
                <!-- <div class="float-left"> -->
                    <!-- <div class="page-title"> -->
                        <!-- <h1>Artists</h1> -->
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-4">
			                        		<strong class="card-title artist-name">User's Help List</strong>
			                        	</div>
			                        </div>
	                        	</div>
	                    	</div>
	                    <!-- </div> -->
	                <!-- </div> -->
            			</div>
        </div>

        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                       <!--  <div class="card-header border-header">
                            <strong class="card-title artist-name">Artists List</strong>
                            	<a href="{{ url('admin/artist/add')}}" class="btn btn-primary float-right ml-2"><strong class="card-title"><i class="fa fa-plus"></i> Add New</strong></a>
                            	<a href="{{ url('admin/artist/import/bulk') }}" class="btn btn-success float-right ml-2"><strong class="card-title"><i class="fa fa-plus"></i> Bulk Import Artists</strong></a>
                            	<a href="{{ url('admin/artists/trash')}}" class="btn btn-secondary float-right ml-2"><strong class="card-title"><i class="fa fa-trash"></i> View Trash ({{App\Artist::onlyTrashed()->count()}})</strong></a>
                        </div> -->
                        <div class="card-body">
							@if(session()->has('message'))
								<div class="alert alert-success">
									{{ session()->get('message') }}
								</div>
							@endif
							<table id="manual_payment_list" class="table table-striped table-bordered artisttb" style="width:100%">
								<thead>
									<tr>
										<th>Name</th>
										<th>Email</th>
										<th>Image</th>
										<th>Request</th>
										<th>Date</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>Name</th>
										<th>Email</th>
										<th>Image</th>
										<th>Request</th>
										<th>Date</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
                        </div>
                    </div>
                </div>
				<input type="hidden" class="csrf_token" value="{{ csrf_token() }}">
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

<script src="{{ asset('admin_assets/js/lib/data-table/datatables.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/jszip.min.js') }}"></script>
<!-- <script src="{{ asset('admin_assets/js/lib/data-table/pdfmake.min.js') }}"></script> -->
<script src="{{ asset('admin_assets/js/lib/data-table/vfs_fonts.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
<!-- <script src="{{ asset('admin_assets/js/lib/data-table/buttons.print.min.js') }}"></script> -->
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/datatables-init.js') }}"></script>


<script type="text/javascript">
	$(document).ready(function() {
		var csrf_token = jQuery(document).find('.csrf_token').val();
		$('#manual_payment_list').DataTable( {
			"processing": true,
			"serverSide": true,
			"bAutoWidth": false,
			"ajax": {
				"url": "help/getlist",
				"type": "POST",
				"dataType": 'json',
                "error": function(data){
                        console.log(data);
                    },
				"headers" : {
						'X-CSRF-TOKEN': csrf_token
				}
			},
			"columns": [
				{ "data": "name" },
				{ "data": "email" },
				{ "data": "image" ,     render: function ( data, type, row ) {
						var image_url = data;
						return '<img src="/'+image_url+'" class="img-thumbnail" />';
					return data;
				},"width": "8%" },
				{ "data": "help" },
				{ "data": "created_at" },
				{ "data": "status" },
				{ "data": "id",     render: function ( data, type, row ) {
						var editlink = "<?php echo URL::to('/admin/help/edit/"+data+"'); ?>";						
						return '<li class="libtnlink"><a class="edit_link text-warning" href="'+editlink+'"><i class="fa fa-pencil"></i></a></li>';
					return data;
				},"border": "none" }
			]
		} );
    } );
</script>
<script type="text/javascript">
$(document).ready(function(){
	
});
</script>
<!-- Delete Model -->

@endSection
<!-- <i class="fa fa-eye"></i><i class="fa fa-pencil"></i><i class="fa fa-trash"></i> -->
<!-- <img src="{{asset('assets/icons/view.png')}}" width="25px"/> -->
<!-- <button type="button" data-toggle="modal" data-target="#confirmationDelete" class="delete_link text-danger" data-href="'+deletelink+'"><i class="fa fa-trash"></i></button> -->


