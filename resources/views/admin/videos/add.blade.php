@extends('layouts.admin.layout')
@section('main_content')

<!-- Header-->

        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Video</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
                        <div class="card">
                            <div class="card-header border-header">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <strong class="card-title videolist-name">Add Video</strong>
                                        </div>
                                        <div class="col-md-9">
                                            <a href="{{ url('admin/videos') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Video List</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>
					@if(session()->has('message'))
					<script type="text/javascript">
					jQuery(document).ready(function($){
					        $.notify({
					            icon: "nc-icon nc-app",
					            message: '{{ session()->get('message') }}',
					        }, {
					            type: 'success',
					            timer: 8000,
					        });
					 });
					 </script>
					@endif
					@if ($errors->any())
						  @foreach ($errors->all() as $error)
								<script type="text/javascript">
								jQuery(document).ready(function($){
								        $.notify({
								            icon: "nc-icon nc-app",
								            message: '{{ $error }}',
								        }, {
								            type: 'danger',
								            timer: 8000,
								        });												
								});
								 </script>
						  @endforeach
					@endif
        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">

                    <div class="card"> 
                       <!--  <div class="card-header">
                            <strong class="card-title">Add Video</strong>
                        </div> -->
						<div class="card-body">

							<form method="POST" action="<?php echo URL::to('/admin/videos/create'); ?>" enctype="multipart/form-data" class="add_video">
							 	{{ csrf_field() }}
								
								<div class="form-group row">
									<label for="name" class="col-md-2 col-form-label">Title</label>
									<div class="col-md-6"><input id="title" name="title" value="" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								@if ($errors->has('title'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
								<div class="form-group row"><label for="description" class="col-md-2 col-form-label">Description</label>
									<div class="col-md-6"><textarea name="description" class="form-control txtbio" placeholder="Description"></textarea></div>
								</div>
                                
                                <div class="form-group row">
                                	<label for="audio" class="col-md-2 col-form-label">Video</label>
									<div class="col-md-2">
										<input id="video" name="video" value="" required="required" autofocus="autofocus" class="form-control" type="file">					
									</div>
									
								</div>
								@if ($errors->has('video'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('video') }}</strong>
                                    </span>
                                @endif

                                <div class="form-group row">
                                	<label for="image" class="col-md-2 col-form-label">Image</label>
									<div class="col-md-6"><input id="image" name="image" value="" required="required" autofocus="autofocus" class="form-control" type="file" style="width: 50%;float: left;">
										<input class="form-check-input d-none" type="checkbox" name="select_generated" value="" id="select_thumbnail">
										  <label class="form-check-label" for="select_generated">
										    Select generated Image
										  </label>
									</div>
									<div class="col-md-2"><a class="btn btn-primary generate_thumbnail d-none" href="#">Generate Thumbnail</a></div>
								</div>
								@if ($errors->has('image'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                                
                                <div class="form-group row">
                                	<label for="date_released" class="col-md-2 col-form-label">Release Date</label>
									<div class="col-md-6"><input type="text" class="form-control release_date" name="date_released" required></div>
								</div>
								@if ($errors->has('date_released'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('date_released') }}</strong>
                                    </span>
                                @endif
                                
                                <div class="form-group row">
                                	<label for="artist" class="col-md-2 col-form-label">Artist</label>
									<div class="col-md-6">
										<select class="js-example-basic-multiple form-control" name="artist_id[]" value="" multiple="multiple">
											@foreach($artists as $key => $artist)
											  <option name="artist_id" value="{{ $artist->id }}">{{ $artist->name }}</option>
											@endforeach
										</select>
									</div> 
								</div>
								<div class="form-group row">
                                	<label for="album_tags" class="col-md-2 col-form-label">Tags</label>
										<div class="col-md-6">
											<input id="album_tags" name="tag_id" value="" placeholder="Enter comma , separated Tags" autofocus="autofocus" class="form-control mb-2" type="text">
										</div>
								</div>
								
								<div class="form-group row">
									<label for="cateories" class="col-md-2 col-form-label">Category</label>
									<div class="col-md-6">
										<select name="category_id" value="" class="form-control">
												@foreach($categories as $key => $category)
													  <option value="{{ $category->id }}">{{ $category->name }}</option>
												@endforeach
											</select>
									</div> 
								</div>
								<div class="form-group row"><label for="is_featured" class="col-md-2 col-form-label">Is Featured</label>
									<div class="col-md-6"><input value="1" type="checkbox" id="isFeatured" name="is_featured"></div> 
								</div>
								<div class="form-group row mb-0">
									<div class="col-md-6 offset-md-4">
										<a class="btn btn-primary add_video_btn" href="#">Add Video</a>
									</div>
								</div>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/plugins/select2/select2.min.css') }}">
<!-- <script src="{{ asset('admin_assets/js/vendor/jquery-2.1.4.min.js') }}"></script> -->
<script src="{{ asset('admin_assets/plugins/select2/select2.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
	    $('.js-example-basic-multiple').select2({
	    	placeholder: "Select a item",
    		allowClear: true
	    });
	});
</script>

@endSection
