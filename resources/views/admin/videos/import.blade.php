@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
		<style type="text/css">
			.mt-5
			{
				margin-top: 5rem !important;
			}
		</style>
        <div class="breadcrumbs navbar-top">
            <div class="card">
                <div class="card-header border-header">
                	<div class="col-sm-12">
                		<div class="row">
                        	<div class="col-sm-4">
                        		<strong class="card-title videolist-name">Bulk Import Videos</strong>
                        	</div> 
                            <div class="col-sm-8">	
                            	<a class="btn btn-primary float-right ml-2" href="<?php echo URL::to('/admin/videos'); ?>"><strong class="card-title"><i class="fa fa-angle-left"></i> Back To Videos</strong></a>
                            </div>
                        </div>
                	</div>
            	</div>
			</div>
        </div>

        <div class="content mt-5 bulk_video_import">
            <div class="animated fadeIn">
                <div class="row">
				
                <div class="col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif

                    <div class="card"> 
                        <div class="card-header">
                            <strong class="card-title">Import Video</strong>
                        </div>
						<div class="card-body" id="drop-zone">
							 @if ($errors->any())
								  <div class="alert alert-danger">
									  <ul>
										  @foreach ($errors->all() as $error)
											  <li>{{ $error }}</li>
										  @endforeach
									  </ul>
								  </div><br/>
							  @endif
							<videos-dropzone></videos-dropzone>
						</div>
                    </div>
                    <div class="col-md-12 common_fields">
						<p>Note : If you set values to below fields then it will be apply for all video upload for this session, however you can change the values for particular video by manually editing.</p>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group row"> 
									<label for="name" class="col-md-2 col-form-label">Artist</label>
									<div class="col-md-10">
										<select class="js-example-basic-multiple form-control common_artist_id"  value="" multiple="multiple">
											@foreach($artists as $key => $artist)
											  <option name="artist_id" value="{{ $artist->id }}">{{ $artist->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group row">
									<label for="name" class="col-md-3 col-form-label">Category</label>
									<div class="col-md-9">
										<select name="category_id" value="" class="form-control common_category_id">
											@foreach($categories as $key => $category)
												  <option value="{{ $category->id }}">{{ $category->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group row">
									<label for="name" class="col-md-2 col-form-label">Tag</label>
									<div class="col-md-10">
										<input value="" class="form-control common_tag_id" type="text">
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group row">
				                	<label for="date_released" class="col-md-4 col-form-label">Release Date</label>
									<div class="col-md-8"><input type="text" class="form-control  common_release_date"></div>
								</div>
							</div>
						</div>
					</div>
                    <div class="music_data">
                    	<form method="POST" action="<?php echo URL::to('admin/videos/import'); ?>" enctype="multipart/form-data" class="add_music_bulk">
                    		{!! csrf_field() !!}
                    		<table class="table table-striped">
                    			<thead>
                    			<tr>
                    				<th scope="col" style="width:13%">Title</th>
                    				<th scope="col" style="width:10%">Image</th>
                    				<th scope="col" style="width:10%">Description</th>
                    				<th scope="col" style="width:10%">Artist</th>
                    				<th scope="col" style="width:10%">Category</th>
                    				<th scope="col" style="width:7%">Release Date</th>
                    				<th scope="col" style="width:10%">Featured artist</th>
                    				<?php /* <th scope="col" style="width:10%">Tags</th> */ ?>
                    				<th scope="col" style="width:10%">Is Featured</th> 
                    				<th scope="col" style="width:10%">Generate thumbnail</th>
                    			</tr>
                    			</thead>
  								<tbody class="WEDO">
  								</tbody>
                    		</table>
                    		<div class="formsubmit">
                    			<input type="submit" class="btn btn-primary add_artist_btn" value="Import Videos">
                    		</div>
                    	</form>
                    </div>
                    <div class="hidden_divs" style="display: none;">
                    	<div class="artist_data">
                    		<select class="js-example-basic-multiple form-control artist_id"  value="" multiple="multiple">
								@foreach($artists as $key => $artist)
								  <option name="artist_id" value="{{ $artist->id }}">{{ $artist->name }}</option>
								@endforeach
							</select>
                    	</div>
                    	<div class="category_data">
                    			<select name="category_id" value="" class="form-control category_id">
									@foreach($categories as $key => $category)
										  <option value="{{ $category->id }}">{{ $category->name }}</option>
									@endforeach
								</select>
                    	</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
@endsection
@section('headscripts')
<script src="{{ asset('js/app.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/plugins/select2/select2.min.css') }}">
<!-- <script src="{{ asset('admin_assets/js/vendor/jquery-2.1.4.min.js') }}"></script> --> 
<script src="{{ asset('admin_assets/plugins/select2/select2.min.js') }}"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
		jQuery(document).ready(function(){
			jQuery('.menu-item-has-children.dropdown .dropdown-toggle').click(function(){
				jQuery(this).parent().toggleClass('show');
				jQuery(this).next().toggleClass('show');
			});
			var onchangeFunction = function(){
				var current_artist_id = jQuery('.common_artist_id').val();
				var common_category_id = jQuery('.common_category_id').val();
				var common_tag_id = jQuery('.common_tag_id').val();
				var common_release_date = jQuery('.common_release_date').val(); 
				jQuery('.music_data .add_music_bulk table tbody').find('tr').each(function(){
					console.log(current_artist_id);
					jQuery(this).find('td:eq(3)').find('select').val(current_artist_id).select2();
					jQuery(this).find('td:eq(4)').find('select').val(common_category_id);
					jQuery(this).find('td:eq(5)').find('input').val(common_release_date);
					jQuery(this).find('td:eq(6)').find('input').val(common_tag_id);
				})
			}
			jQuery('.common_release_date').datepicker({
							dateFormat: "dd-mm-yy",
							changeYear: true,
							changeMonth: true,
							yearRange: "1970:2050",
						});
			jQuery('.common_artist_id').select2().on('change', function () {
				console.log(1001001);
				onchangeFunction();
			});
			jQuery('.common_release_date').change(function(){
				onchangeFunction();
			});
			jQuery('.common_tag_id').change(function(){
				onchangeFunction();
			});
			jQuery('.common_category_id').change(function(){
				onchangeFunction();
			});
			
			
			
			
		}); 
		var createSelect2 = function(){
			setTimeout(function(){
							var current_artist_id = jQuery('.common_artist_id').val();
							var common_category_id = jQuery('.common_category_id').val();
							var common_tag_id = jQuery('.common_tag_id').val();
							var common_release_date = jQuery('.common_release_date').val(); 
							
							
							var target = jQuery('.music_data .add_music_bulk table tbody').find('tr:last-child').find('.multiplegenerate');
							jQuery('.music_data .add_music_bulk table tbody').find('tr:last-child').find('.js-example-basic-multiple').val(current_artist_id).select2();
							var videoname = jQuery(target).closest('tr').find('td:eq(0)').find('input[type="hidden"]').val();
							var attrname = jQuery(target).closest('tr').find('td:eq(0)').find('input[type="hidden"]').attr('name');
							var attrname = attrname.replace("filename", "generated_thumbnail");
							target.append('<img id="ajax_loader" src="'+site_url+'/images/ajax-loader.gif" style="width:20px;"/>');
							$.ajax({
						       	    beforeSend: function(xhrObj){
							                xhrObj.setRequestHeader("X-CSRF-TOKEN",jQuery(document).find('input[name="_token"]').val()); 
							        },
								   url: site_url+"/admin/video/thumb/multiplegenerate",
								   data: {filename	: videoname},
								   success: function(data) {
								   	console.log(data); 
								   	$(document).find('#ajax_loader').hide();
								   	var htmlcontent = '';
								   	htmlcontent+='<img src="'+site_url+'/'+data.image+'" class="artist_image img-thumbnail" /><input type="hidden" name="'+attrname+'" value="'+data.filename+'"/>';
								   	target.parent().append(htmlcontent);
								   	//target.closest('tr').find('td:eq(0)').find('input[type="hidden"]').val(data.new_video); 
								   	target.remove();
								   	jQuery('#select_thumbnail').prop('checked', true);
								   },
								   type: 'POST'
								});
							console.log(jQuery('.music_data .add_music_bulk table tbody').find('tr:last-child').find('.release_date'));  
							jQuery('.music_data .add_music_bulk table tbody').find('tr:last-child').find('.release_date').datepicker({
								dateFormat: "dd-mm-yy",
								changeYear: true,
								changeMonth: true,
								yearRange: "1970:2050",
							});
							
							jQuery(target).closest('tr').find('td:eq(4)').find('select').val(common_category_id);
							jQuery(target).closest('tr').find('td:eq(5)').find('input').val(common_release_date);
							jQuery(target).closest('tr').find('td:eq(6)').find('input').val(common_tag_id);
						
						
						
			}, 100);

			
		}
	</script>
@endsection
