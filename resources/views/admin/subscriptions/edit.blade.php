@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
<?php 

?>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Subscription Plans</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif
						<div class="card"> 
							<div class="card-header">
								<strong class="card-title">Edit Subscription Plan</strong>
							</div>
							<div class="card-body">
								 @if ($errors->any())
									  <div class="alert alert-danger">
										  <ul>
											  @foreach ($errors->all() as $error)
												  <li>{{ $error }}</li>
											  @endforeach
										  </ul>
									  </div><br/>
								  @endif
								<form method="POST" action="<?php echo URL::to('/admin/subscription/editdata'); ?>">
								 {{ csrf_field() }}
									<div class="form-group row"><label for="name" class="col-md-2 col-form-label text-md-left">Name</label>
										<div class="col-md-6"><input id="name" name="name" required="required" autofocus="autofocus" class="form-control" value="<?php echo $data[0]['name']; ?>" type="text"></div>
									</div>
									@if ($errors->has('name'))
										<span class="invalid-feedback">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
									@endif
									<div class="form-group row"><label for="price" class="col-md-2 col-form-label text-md-left">Price</label>
										<div class="col-md-6"><input id="price" name="price" required="required" class="form-control" value="<?php echo $data[0]['price']; ?>" type="text"></div>
									</div>
									@if ($errors->has('name'))
										<span class="invalid-feedback">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
									@endif
									<div class="form-group row"><label for="duration" class="col-md-2 col-form-label text-md-left">Duration (in Days)</label>
										<div class="col-md-6"><input id="duration" name="duration" required="required" class="form-control" value="<?php echo $data[0]['duration']; ?>" type="text"></div>
									</div>
									@if ($errors->has('name'))
										<span class="invalid-feedback">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
									@endif
									
									
									
									<div class="form-group row mb-0 user_form_submit">
										<div class="col-md-6 offset-md-4">
											<input type="hidden" name="id" value="<?php echo $data[0]['plan_id']; ?>" /> 
											<button type="submit" class="btn btn-primary">
												Update Subscription Plan
											</button> 
 										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
@stop

