@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->

        <!-- <div class="breadcrumbs">
            <div class="col-sm-12">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Albums</h1>
                    </div>
                </div>
            </div>
        </div> -->
        <style type="text/css">
        	.emoji-wysiwyg-editor
        	{
        		max-height :100px !important;
        	}
        </style>
         <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-md-3">
											 <strong class="card-title"><i class="fa fa-pencil"></i> Edit Album</strong>
										</div>
										<div class="col-md-9">
											 <a href="{{url('admin/albums')}}" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Album List</a>
											 <?php /* <form method="POST" action="<?php echo URL::to('/admin/album/send_pushnotification'); ?>" class="push_notification_form">
											 	{{ csrf_field() }}
												<div class="col">
													<input type="hidden" name="album_id" value="{{$album->id}}">
													<button type="button" class="btn btn-success float-right send_push_notification_btn"><i class="fa fa-save"></i>Send Push Notification</button>
												</div>
											</form> */ ?>
											<?php /* <form method="POST" action="<?php echo URL::to('/admin/news/sendPushNotification_Album'); ?>" class="push_notification_form">
											 	{{ csrf_field() }}
												<div class="col">
													<input type="hidden" name="album_id" value="{{$album->id}}">
													<button type="button" class="btn btn-success float-right send_push_notification_btn"><i class="fa fa-save"></i>FOR TEST</button>
												</div>
											</form> */ ?>
											<div class="col">
												<button type="button" class="btn btn-success float-right send_push_notification_btn" data-toggle="modal" data-target="#AlbumNotificationModal"><i class="fa fa-save"></i>Send Push Notification</button>
											</div>
											<?php /* <div class="col">
												<button type="button" class="btn btn-success float-right send_push_notification_btn" data-toggle="modal" data-target="#AlbumNotificationModalTest"><i class="fa fa-save"></i>FOR TEST</button>
											</div> */ ?>
										</div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>


        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

					@if(session()->has('message'))
					<script type="text/javascript">
					jQuery(document).ready(function($){
					        $.notify({
					            icon: "nc-icon nc-app",
					            message: '{{ session()->get('message') }}',
					        }, {
					            type: 'success',
					            timer: 10000,
					        });
					 });
					 </script>
					@endif
					@if ($errors->any())
						  @foreach ($errors->all() as $error)
								<script type="text/javascript">
								jQuery(document).ready(function($){
								        $.notify({
								            icon: "nc-icon nc-app",
								            message: '{{ $error }}',
								        }, {
								            type: 'danger',
								            timer: 10000,
								        });												
								});
								 </script>
						  @endforeach
					  @endif
				<div class="col-md-12">
                    <div class="card"> 
                      <!--   <div class="card-header border-header">
                            <strong class="card-title"><i class="fa fa-pencil"></i> Edit Album</strong>
                            <a href="{{ url('admin/artist/view/'.$album->artists[0]->id) }}" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Album List</a>
                            <a href="{{url('admin/albums')}}" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Album List</a>
                        </div> -->
						<div class="card-body row">
						<div class="col-md-8">	  
							<form method="POST" action="<?php echo URL::to('/admin/album/update'); ?>" enctype="multipart/form-data" class="update_album">
							 	{{ csrf_field() }}
								<input type="hidden" name="album_id" value="{{$album->id}}"/>
								<div class="form-group row"><label for="name" class="col-md-2 col-form-label">Title</label>
									<div class="col-md-10"><input id="title" name="title" value="{{$album->title}}" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								@if ($errors->has('title'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                                <div class="form-group row"><label for="description" class="col-md-2 col-form-label">Description</label>
									<div class="col-md-10"><textarea name="description" class="form-control txtbio" placeholder="Description" rows="5">{{$album->description}}</textarea></div>
								</div>
								<div class="form-group row">
									<label for="image" class="col-md-2 col-form-label">Image:</label>
									<div class="col-md-10"><input id="image" name="image" value="" autofocus="autofocus" class="form-control" type="file"></div>
									
								</div>
								@if ($errors->has('image'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                                <div class="form-group row"><label for="album_tags" class="col-md-2 col-form-label">Tags</label>
                                		<?php 
                                			$tags = array();
                                			foreach ($album->tags as $tag) {
												$tags[] = $tag->name;
											}
                                		?>
										<div class="col-md-10"><input id="album_tags" name="album_tags" value="<?php echo implode(',', $tags); ?>" placeholder="Enter comma , separated Tags" autofocus="autofocus" class="form-control mb-2" type="text">
										</div> 
								</div>


								<div class="form-group row"><label for="album_tags" class="col-md-2 col-form-label">Artist</label>
                                		<div class="col-md-10">
                                			{{$album->artists[0]->name}}
										</div> 
								</div>


								<div class="form-group row"><label for="album_tags" class="col-md-2 col-form-label">ARTISTS FEATURED</label>
                                		<div class="col-md-10">
                                			<?php 
													$related_artist = $album->related_artist;
													$related_artist_ids = array();
													if($related_artist != ""){
														$related_artist_ids = explode(',', $related_artist);
													}
												?>
                                			<select name="featured_artist_id[]" value="" class="js-example-basic-multiple form-control" multiple="multiple">
												<option value="">Select Artist</option>
												@foreach($artists as $key => $artist)
													<option name="artist_id" value="{{ $artist->id }}" @if(in_array($artist->id,$related_artist_ids)) selected @endif>{{ $artist->name }}</option>
												@endforeach
											</select>
										</div> 
								</div>

								<div class="form-group row"><label for="album_tags" class="col-md-2 col-form-label">Release Year</label>
                            		<div class="col-md-10">
                            			<select name="release_year" class="form-control mb-2">
                            				<option value="">-Please select-</option>
                            				<?php 
                            				$firstYear = (int)date('Y') - 60;
											$lastYear = (int)date('Y');
											for($i=$firstYear;$i<=$lastYear;$i++)
											{ ?>

											    <option value="<?php echo $i; ?>" <?php  if(isset($album->release_year)) { if($i==$album->release_year)  {echo "selected"; } }?>><?php echo $i; ?></option>
											<?php }
                            				?>

                            			</select>	
									</div>
								</div>
								<div class="form-group row mb-2">
									<div class="col">
										<button type="submit" class="btn btn-success float-right"><i class="fa fa-save"></i> Update</button>
									</div>
									<div class="col">
										<a class="btn btn-success add_tracks"><i class="fa fa-plus"></i> Add Tracks</a>
									</div>
									
								</div>
							</form>
							</div>
			                <div class="col-md-4">
	                        <div class="card">
	                            <div class="card-image">
	                                <img src="{{$album->image}}" alt="...">
	                            </div>
	                            <div class="card-body">
	                            		<h3 class="title">{{$album->title}}</h3>
                                        <h5 class="title">{{$album->artists[0]->name}}</h5>
                                        @foreach($album->tags as $tag)
	                                	<div class="d-inline-block bg-info pl-2 pr-2 mb-2 text-light">{{$tag->name}}</div>
	                                	@endforeach
	                            </div>
	                            <hr>
	                        </div>
	                		</div>
						</div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card"> 
                        <div class="card-header">
                            <strong class="card-title">Music List</strong>
                            
                            
                        </div>
                        <div class="card-body">
                        	<form method="POST" action="<?php echo URL::to('/admin/musics_bulk/update'); ?>" class="update_musics_bulk">
                        		<input type="hidden" class="album_id" value="{{$album->id}}">
                        		{{ csrf_field() }}
                        		<div class="music_update_section">
                        			<button type="submit" class="btn btn-success float-right update_bulk_music"><i class="fa fa-save"></i> Update</button>
                        			<button type="button" class="btn btn-success float-right edit_bulk_music"><i class="fa fa-pencil"></i>Edit</button>
                        		</div>
                        		<table class="update_musics_bulk_table" border="1">
	                        		<thead>
	                        			<tr>
	                        				<th>Image</th>
	                        				<th>Title</th>
	                        				<th>Is New</th>
	                        				<th>Is Trending</th>
	                        			</tr>
	                        		</thead>
	                        		<tbody>
	                        			<?php
	                        			$id=0; 
	                        			foreach ($album->musics as $music) { 
	                        			
	                        			?>
											<tr>
												<td>
													<img src="{{$music->image}}" alt="{{$music->title}}" class="rounded img-fluid music_img"/>
												</td>
												<td>
													<div class="edit">
														<input type="text" class="form-control" name="music[<?php echo $id; ?>][title]" value="<?php echo $music->title; ?>">
														<input type="hidden" class="music_id" name="music[<?php echo $id; ?>][id]" value="<?php echo $music->id; ?>">
													</div>
													<div class="view">
														{{$music->title}}
													</div>
												</td>
												<td>
													<div class="edit">
														<input value="1" type="checkbox" name="music[<?php echo $id; ?>][is_new]" {{$music->is_new ?'checked' : ''}}>
													</div>
													<div class="view">
														{{$music->is_new ?'Yes' : 'No'}}
													</div>
												</td>
												<td>
													<div class="edit">
														<input value="1" type="checkbox" name="music[<?php echo $id; ?>][is_trending]" {{$music->is_trending ?'checked' : ''}}>
													</div>
													<div class="view">
														{{$music->is_trending ?'Yes' : 'No'}}
													</div>
												</td>
											</tr>	
										<?php 
										$id++;
										} ?>
	                        		</tbody>
	                        	</table>
                        	</form>
                        </div>
                    </div>
                   </div>

                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

<!-- Modal -->
<div class="modal fade" id="AlbumNotificationModal" role="dialog">
    <div class="modal-dialog">
    	<form method="POST" action="<?php echo URL::to('/admin/album/send_pushnotification'); ?>" class="push_notification_form">
    		{{ csrf_field() }}
	        <div class="modal-content">
	            <!-- Modal Header -->
	            <!-- Modal Body -->
	            <div class="modal-body">
                    <div class="form-group">
                        <label for="inputName">Type</label><br/>
                        <label class="radio-inline"><input type="radio" name="notificationtype" value="default" checked>Default</label>
						<label class="radio-inline"><input type="radio" name="notificationtype" value="custom">Custom</label>
						
                    </div>

					<input type="hidden" name="album_id" value="{{$album->id}}">


                    <div class="for_custom" style="display: none;">
                    	
						<div class="form-group">
	                        <label for="inputEmail">Title</label>
	                        <p class="lead emoji-picker-container">
				              <textarea class="form-control" rows="10" data-emojiable="true" data-emoji-input="unicode" name="album_title">{{$album->title}}</textarea>
				            </p>
	                    </div>
	                    <div class="form-group">
	                        <label for="inputMessage">Description</label>
	                        <p class="lead emoji-picker-container">
				              <textarea class="form-control" rows="10" data-emojiable="true" data-emoji-input="unicode" name="album_description">{{$album->description}}</textarea>
				            </p>
	                    </div>
	                </div>
	            </div>
	            
	            <!-- Modal Footer -->
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                <input type="submit" name="submit" class="btn btn-primary" value="SUBMIT">
	            </div>
	        </div>
	    </form>    
    </div>
</div>        


         
		
@endsection
@section('headscripts')
<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/plugins/select2/select2.min.css') }}">
<script src="{{ asset('admin_assets/js/vendor/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('admin_assets/plugins/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<script src="{{ asset('admin_assets/js/jquery.ui.touch-punch.min.js') }}"></script>


<script type="text/javascript">
    $(document).ready(function() {
    	$('.edit_bulk_music').click(function(){
    		$('.update_musics_bulk_table').find('.view').toggle();
    		$('.update_musics_bulk_table').find('.edit').toggle();
    		$('.update_bulk_music').toggle();
    	})
    	/*$('.send_push_notification_btn').click(function(){
			var r = confirm("Are you sure want to send push notification to users for this album?");
			if (r == true) {
				$('.push_notification_form').submit();
			}
		});*/
    	// dragula([document.getElementById(container)]);
    	
		$(".update_musics_bulk_table tbody").sortable({
                start: function (event, ui) {
                        ui.item.toggleClass("highlight");
                        console.log(000000);
                },
                stop: function (event, ui) {
                        ui.item.toggleClass("highlight");
                        console.log(111111);
                        var music_order = [];
                        var albumid = jQuery('.album_id').val();
                        jQuery('.update_musics_bulk_table tr').each(function(){
                        	var music_id = jQuery(this).find('.music_id').val();
                        	music_order.push(music_id);
                        });
                        console.log(music_order);
                        $.ajax({
				       	    beforeSend: function(xhrObj){
					               xhrObj.setRequestHeader("X-CSRF-TOKEN",jQuery(document).find('input[name="_token"]').val()); 
					        },
						   url: site_url+"/admin/album/musics/reorder",
						   data: {album	: albumid,music : music_order},
						   success: function(data) {
						   		console.log(data);
						   },
						   type: 'POST'
						});
                        
                }
        });
        $("#items").disableSelection();
	    $('.js-example-basic-multiple').select2({
	    	placeholder: "Select an Artist",
    		allowClear: true
	    });

	    $('input[name=notificationtype]').change(function(){
		var value = $( 'input[name=notificationtype]:checked' ).val();
		if(value == 'custom'){
			$('.for_custom').css("display","block");
		}
		else if(value == 'deafult'){
			$('.for_custom').css("display","none");
		}
		else{
			$('.for_custom').css("display","none");	
		}
		});

		$(function() {
	        // Initializes and creates emoji set from sprite sheet
	        window.emojiPicker = new EmojiPicker({
	          emojiable_selector: '[data-emojiable=true]',
	          assetsPath: '{{asset('admin_assets/plugins/emoji')}}' +'/lib/img/',
	          popupButtonClasses: 'fa fa-smile-o'
	        });
	        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
	        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
	        // It can be called as many times as necessary; previously converted input fields will not be converted again
	        window.emojiPicker.discover();
	      });
	});
</script>
