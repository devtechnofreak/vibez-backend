@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Albums</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif
                    <div class="card"> 
                        <div class="card-header">
                            <strong class="card-title">Import Albums</strong>
                            <strong class="text-success ml-4">
                            	
                            @if(isset($artist_id))
                            {{'Artist : '.App\Artist::find($artist_id)->name}}
                            @endif	
                            </strong>
                            <a href="{{ url('admin/albums') }}" class="btn btn-primary float-right  ml-2"><i class="fa fa-arrow-left"></i> Back</a>
                            
                            <a href="{{ url('sample/import_albums.csv') }}" class="btn btn-success  float-right ml-2"><i class="fa fa-download"></i> Download Sample Csv</a>
                            @if(isset($csv))
                            @if(isset($artist_id))
                            <a href="{{ url('admin/album/import/bulk/'.$artist_id) }}" class="btn btn-success  float-right ml-2"><i class="fa fa-refresh"></i></a>
                            @else
                            <a href="{{ url('admin/album/import/bulk') }}" class="btn btn-success  float-right ml-2"><i class="fa fa-refresh"></i></a>
                            @endif
                            @endif
                            <a class="album_import_cron_manual btn btn-success  float-right" style="color: #87CB16;"><i class="fa fa-refresh"></i> Album Import Cron</a>
                        </div>
						<div class="card-body">
							 @if ($errors->any())
								  <div class="alert alert-danger">
									  <ul>
										  @foreach ($errors->all() as $error)
											  <li>{{ $error }}</li>
										  @endforeach
									  </ul>
								  </div><br/>
							  @endif
							<form method="POST" action="{{ url('admin/album/import/bulk')}}" enctype="multipart/form-data" class="form-inline">
							 {{ csrf_field() }}
							@if(isset($csv))
							<table class="table album_bulk_import">
							  <thead>
							    <tr>
							      <th scope="col">#</th>
							      <th scope="col">Title</th>
							      <th scope="col">Description</th>
							      <th scope="col">Artist</th>
							      <th scope="col">Tags</th>
							      <th scope="col" style="display: table-cell !important;">Folder</th>
							    </tr>
							  </thead>
							  <tbody>
							@foreach($csv as $key => $data)
							    <tr>
							      <th scope="row">{{$key+1}}</th>
							      <td><input name="album[{{$key}}][title]" required="required" autofocus="autofocus" class="form-control" value="{{$data['title']}}" type="text"></td>
							      <td><textarea rows="4" cols="50" name="album[{{$key}}][description]" class="form-control">{{$data['description']}}</textarea></td>
							      <td>
							      	<select name="album[{{$key}}][artist]" value="" class="col-md-6 form-control banner_type">
							      		<option value="">Select Artist</option>
										@foreach($artists as $artist)
										@if(isset($artist_id))
											<option value="{{$artist->id}}" {{$artist->id==$artist_id?'selected':''}}>{{$artist->name}}</option>
										@else
											<option value="{{$artist->id}}">{{$artist->name}}</option>	
										@endif
										@endforeach
									</select>
							      </td>
							      <td><input name="album[{{$key}}][tags]" value="{{$data['tags']}}" placeholder="Enter comma(,) separated Tags" autofocus="autofocus" class="form-control mb-2" type="text"></td>
							      <td>
							      	<select class="js-example-basic-multiple form-control" name="album[{{$key}}][folder]">
							      		<?php 
							      			$folders = glob("/home/admin/web/thevibez.net/public_html/public/import/music/*", GLOB_ONLYDIR );
							      			foreach ($folders as $folder) {
												$foldername = substr($folder, strrpos($folder, '/') + 1);	
												echo '<option value="'.$foldername.'">'.$foldername.'</option>';  
											}
							      		?>
							      	</select>
							      </td>
							    </tr>
							@endforeach
							  </tbody>
							</table>
							@else
							  <div class="form-group mx-sm-3 mb-2">
							  	@if(isset($artist_id))
									<input type="hidden" value="{{$artist_id}}" name="artist_id" />
								@endif
							    <label for="inputfile" class="sr-only">Password</label>
							    <input type="file" class="form-control-file" id="inputfile" name="csv_import" placeholder="Csv File" required="reequired">
							  </div>
							  @if ($errors->has('name'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('csv_import') }}</strong>
								</span>
							  @endif
							@endif
							  <button type="submit" class="btn btn-primary mb-2">Import</button>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
@stop
<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/plugins/select2/select2.min.css') }}">
<script src="{{ asset('admin_assets/js/vendor/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('admin_assets/plugins/select2/select2.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
	    $('.js-example-basic-multiple').select2();
	    $('.album_import_cron_manual').click(function(){
	    	var r = confirm("Are you sure want to run album import cron manually ?");
			if (r == true) {
			    $.ajax({
	               type:'GET',
	               url:'/run_album_cron_manually',
	               data:'_token = <?php echo csrf_token() ?>',
	               success:function(data){
	                  alert("Cronjob is running in background, please wait for few minutes!")
	               }
	            });
			} else {
			    alert(20);
			}
	    });
	});
</script>