@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
        <div class="breadcrumbs">
            <div class="col-sm-12">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Albums</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif

                    <div class="card"> 
                        <div class="card-header">
                            <strong class="card-title">Add Musics</strong>
                            @if(isset($album_id))
                            <a href="{{ url('admin/album/edit/'.$album_id) }}" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Back</a>
                            @else
                            <a href="{{ URL::previous() }}" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Back</a>
                            @endif
                        </div>
						<div class="card-body" id="drop-zone">
							 @if ($errors->any())
								  <div class="alert alert-danger">
									  <ul>
										  @foreach ($errors->all() as $error)
											  <li>{{ $error }}</li>
										  @endforeach
									  </ul>
								  </div><br/>
							  @endif
							<dropzone-component></dropzone-component>
						</div>
                    </div>
                    
                    <div class="music_data">
                    	<form method="POST" action="<?php echo URL::to('admin/album/import/musics'); ?>" enctype="multipart/form-data" class="add_music_bulk">
                    		{!! csrf_field() !!}
                    		@if(isset($album_id))
			                <input type="hidden" value="{{$album_id}}" name="album_id"/>
			                @else
			                <div class="form-group row"><label for="artist" class="col-md-4 col-form-label">Artist</label>
								<div class="col-md-6">
									<select name="album_id" value="" class="col-md-6 form-control banner_type">
										@foreach($albums as $key => $album)
										<option value="{{$album->id}}">{{$album->title}}</option>
										@endforeach
									</select>
								</div> 
							</div>
			                @endif
                    		<table class="table table-striped">
                    			<thead>
                    			<tr>
                    				<th scope="col">Title</th>
                    				<th scope="col">Meta Tags</th>
                    				<th scope="col" style="width: 10%;">Is Trending</th>
                    				<th scope="col">Tags</th>
                    			</tr>
                    			</thead>
  								<tbody>
  								</tbody>
                    		</table>
                    		<div class="formsubmit">
                    			<?php 
                    				$url = $_SERVER['REQUEST_URI'];
                    				$template = substr($url, strrpos($url, '/') + 1);
                    			?>
                    			<input type="hidden" name="template" value="<?php echo $template; ?>">
                    			<input type="submit" class="btn btn-primary add_artist_btn" value="Add Songs To Album">
                    		</div>
                    	</form>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
@endsection
@section('headscripts')
	<script src="{{ asset('js/app.js')}}"></script>
	<script>
		jQuery(document).ready(function(){
			jQuery('.menu-item-has-children.dropdown .dropdown-toggle').click(function(){
				jQuery(this).parent().toggleClass('show');
				jQuery(this).next().toggleClass('show');
			});
		});
	</script>
@endsection
