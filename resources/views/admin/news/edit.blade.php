@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
<?php 

?>
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>News</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->

        <style type="text/css">
        	.table-wrapper {
				width: 700px;
				background: #fff;
		        padding: 20px;	
		        box-shadow: 0 1px 1px rgba(0,0,0,.05);
		    }
		    .table-title {
		        padding-bottom: 10px;
		        margin: 0 0 10px;
		    }
		    .table-title h2 {
		        margin: 6px 0 0;
		        font-size: 22px;
		    }
		    .table-title .add-new {
		        float: right;
				height: 30px;
				font-weight: bold;
				font-size: 12px;
				text-shadow: none;
				min-width: 100px;
				border-radius: 50px;
				line-height: 13px;
		    }
			.table-title .add-new i {
				margin-right: 4px;
			}
		    table.table {
		        table-layout: fixed;
		    }
		    table.table tr th, table.table tr td {
		        border-color: #e9e9e9;
		    }
		    table.table th i {
		        font-size: 13px;
		        margin: 0 5px;
		        cursor: pointer;
		    }
		    table.table th:last-child {
		        width: 100px;
		    }
		    table.table td a {
				cursor: pointer;
		        display: inline-block;
		        margin: 0 5px;
				min-width: 24px;
		    }    
			table.table td a.add {
		        color: #27C46B;
		    }
		    table.table td a.edit {
		        color: #FFC107;
		    }
		    table.table td a.delete {
		        color: #E34724;
		    }
		    table.table td i {
		        font-size: 19px;
		    }
			table.table td a.add i {
		        font-size: 24px;
		    	margin-right: -1px;
		        position: relative;
		        top: 3px;
		    }    
		    table.table .form-control {
		        height: 32px;
		        line-height: 32px;
		        box-shadow: none;
		        border-radius: 2px;
		    }
			table.table .form-control.error {
				border-color: #f50000;
			}
			table.table td .add {
				display: block !important;
			}

			.twitter-tweet
			{
				position: static;
    			visibility: visible;
			    display: block;
			    transform: rotate(0deg);
			    max-width: 217px;
			    width: 100% !important;
			    min-width: 100% !important;
			    margin-top: 10px;
			    margin-bottom: 10px;
			}
			.instagram-media
			{
				position: static;
    			visibility: visible;
			    display: block;
			    transform: rotate(0deg);
			    max-width: 217px;
			    width: 100% !important;
			    min-width: 100% !important;
			    margin-top: 10px;
			    margin-bottom: 10px;
			}

        </style>

        <?php 
        /*echo "<pre>";
        print_r($news->short_codes);
        echo "</pre>";
        echo "<pre>";
        print_r($short_codes);
        echo "</pre>";
        exit;*/
        ?>


        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-md-3">
											 <strong class="card-title newslist-name">Edit News</strong>
										</div>
										<div class="col-md-9">
											 <a href="{{ url('admin/news') }}" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> News List</a>
											 <?php /* <form method="POST" action="<?php echo URL::to('/admin/news/send_pushnotification'); ?>" class="push_notification_form">
											 	{{ csrf_field() }}
												<div class="col">
													<input type="hidden" name="news_id" value="{{$news->id}}">
													<button type="button" class="btn btn-success float-right send_push_notification_btn"><i class="fa fa-save"></i>Send Push Notification</button>
												</div>
											</form>*/ ?>
											<div class="col">
												<input type="hidden" name="news_id" value="{{$news->id}}">
												<button type="button" class="btn btn-success float-right send_push_notification_btn" data-toggle="modal" data-target="#NewsNotificationModal"><i class="fa fa-save"></i>Send Push Notification</button>
											</div>
											<?php /* <div class="col">
												<input type="hidden" name="news_id" value="{{$news->id}}">
												<button type="button" class="btn btn-success float-right send_push_notification_btn" data-toggle="modal" data-target="#NewsNotificationModalTest"><i class="fa fa-save"></i>For Test</button>
											</div> */ ?>
											<?php /* <form method="POST" action="<?php echo URL::to('/admin/news/sendPushNotification_News'); ?>" class="push_notification_form">
											 	{{ csrf_field() }}
												<div class="col">
													<input type="hidden" name="news_id" value="{{$news->id}}">
													<button type="button" class="btn btn-success float-right send_push_notification_btn"><i class="fa fa-save"></i>FOR TEST </button>
												</div>
											</form> */ ?>
										</div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>


        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif
					@if ($errors->any())
					  	<div class="alert alert-danger">
						  	<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
							  	@endforeach
						  	</ul>
					  </div>
					  <br/>
				  	@endif

				  		<div class="card"> 
							<!-- <div class="card-header border-header">
								<strong class="card-title newslist-name">Edit News</strong>
								<a href="{{ url('admin/news') }}" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> News List</a>
							</div> -->
							<div class="card-body row">
								<div class="col-md-8">
									<form method="POST" action="<?php echo URL::to('/admin/news/editdata'); ?>" class="artist_edit_form"  enctype="multipart/form-data">
									 {{ csrf_field() }}
									 	<input type="hidden" name="news_id" value="{{$news->id}}" /> 
										<div class="form-group row"><label for="name" class="col-md-2 col-form-label">Title</label>
											<div class="col-md-6"><input id="title" name="title" required="required" autofocus="autofocus" class="form-control" value="{{$news->title}}" type="text"></div>
										</div>
										@if ($errors->has('title'))
											<span class="invalid-feedback">
												<strong>{{ $errors->first('title') }}</strong>
											</span>
										@endif
										<div class="form-group row"><label for="image" class="col-md-2 col-form-label">Image</label>
											<div class="col-md-6"><input id="image" name="image" class="form-control" value="" type="file"></div>
										</div>
										@if ($errors->has('image'))
											<span class="invalid-feedback">
												<strong>{{ $errors->first('image') }}</strong>
											</span>
										@endif
										
										<div class="form-group row"><label for="description" class="col-md-2 col-form-label">Description</label>
										<div class="col-md-6"><textarea id="description" name="description" value="" required="required" autofocus="autofocus" class="form-control">{{$news->description}}</textarea></div>
									</div>
									<div class="form-group row"><label for="artist" class="col-md-2 col-form-label">Artist</label>
										<div class="col-md-6">
											<select class="js-example-basic-multiple form-control" name="artist_id[]" value="" multiple="multiple">
												@foreach($artists as $key => $artist)
														<option name="artist_id" value="{{ $artist->id }}" @if(in_array($artist->id,collect($news->artists)->pluck('id')->all())) selected @endif>{{ $artist->name }}</option>
												@endforeach
											</select>
										</div> 
									</div>
									<div class="form-group row"><label for="artist" class="col-md-2 col-form-label">Artists Featured</label>
										<div class="col-md-6">
											<?php 
												$related_artist = $news->related_artist;
												$related_artist_ids = array();
												if($related_artist != ""){
													$related_artist_ids = explode(',', $related_artist);
												}
											?>
											<select class="js-example-basic-multiple form-control" name="featured_artist_id[]" value="" multiple="multiple">
												@foreach($artists as $key => $artist)
														<option name="artist_id" value="{{ $artist->id }}" @if(in_array($artist->id,$related_artist_ids)) selected @endif>{{ $artist->name }}</option>
												@endforeach
											</select>
										</div> 
									</div>
	                                <div class="form-group row">
	                                	<label for="date_released" class="col-md-2 col-form-label">Date</label>
										<div class="col-md-6"><input type="text" class="form-control release_date" name="created_at" value="{{substr($news->created_at, 0, strrpos($news->created_at, ' '))}}" required></div>
									</div>
	                                 <div class="form-group row"><label for="dob" class="col-md-2 col-form-label">Is Featured</label>
										<div class="col-md-6"><input value="1" @if($news->is_featured == 1) checked="checked" @endif type="checkbox" id="isFeatured" name="is_featured"></div> 
									</div>


									<div class="form-group row">
										<label for="is_featured" class="col-md-2 col-form-label"><h5>Shortcode <b>Details</b></h5></label>
										<div class="table-wrapper col-md-10">
								            <div class="table-title">
								                <div class="row">
								                    <div class="col-sm-12">
								                        <button type="button" class="btn btn-info add-new"><i class="fa fa-plus"></i> Add New</button>
								                    </div>
								                </div>
								            </div>
								            <table class="table table-bordered">
								                <thead>
								                    <tr>
								                        <th>Type</th>
								                        <th>Text</th>
								                        <th>Shortcode</th>
								                        <th>Action</th>
								                    </tr>
								                </thead>
								                <tbody>
								                    <?php
								                   if(is_null($news->short_codes) || ($news->short_codes == '')) { ?>
								                    	 <tr>
									                        <td><select class="form-control shcode_type" name="shcode_type"><option value="twitter">twitter</option><option value="instagram">instagram</option></select></td>
									                        <td><input type="text" class="form-control" name="shcode_text" id="shcode_text"></td>
									                        <td class="gc"><button type="button" class="btn btn-info add">Generate</button></td>
									                        <td><a class="delete" title="Delete" data-toggle="tooltip"><i class="fa fa-trash"></i></a></td>
									                    </tr>
								                    <?php }
								                    else
								                    {
								                    	foreach ($short_codes as $key => $value) { 
								                    	?>
								                    	<tr>
									                        <td><?php echo $value['type']; ?></td>
									                        <td><?php echo substr(htmlspecialchars(base64_decode($value['text'])), 0, 50); ?></td>
									                        <td><?php echo $value['code']; ?></td>
									                        <td><a class="delete" title="Delete" data-toggle="tooltip" data-id="<?php echo $value['id']; ?>"><i class="fa fa-trash"></i></a></td>
								                    	</tr>	

								                    <?php } } ?>
												</tbody>
								                <input type="hidden" class="csrf_token" value="{{ csrf_token() }}">
								                <input type="hidden" class="short_code_id" name="short_code_id[]" value="<?php echo $news->short_codes.','; ?>">
								            </table>
								        </div>
									</div>
										
									<div class="form-group row mb-0 user_form_submit">
										<div class="col-md-6 offset-md-4">
											
											<a class="btn btn-primary update_artist_btn">Update News</a>
										</div>
									</div>
									</form>
								</div>
								<div class="col-md-4">
									<div class="card">
				                            <div class="card-image">
				                                <img src="{{$news->image}}" class="artist_image">
				                            </div>
				                            <div class="card-body">
				                            		<h3 class="title">{{$news->title}}</h3>
			                                        @foreach($news->artists as $key => $artist)
				                                	<div class="d-inline-block bg-info pl-2 pr-2 mb-2 text-light">{{$artist->name}}</div>
				                                	@endforeach
				                            </div>
				                            <hr>
				                    </div>
								</div>
							</div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

<!-- Modal -->
<div class="modal fade" id="NewsNotificationModal" role="dialog">
    <div class="modal-dialog">
    	<form method="POST" action="<?php echo URL::to('/admin/news/send_pushnotification'); ?>" class="push_notification_form">
    		{{ csrf_field() }}
	        <div class="modal-content">
	            <!-- Modal Header -->
	            <!-- Modal Body -->
	            <div class="modal-body">
                    <div class="form-group">
                        <label for="inputName">Type</label><br/>
                        <label class="radio-inline"><input type="radio" name="notificationtype" value="default" checked>Default</label>
						<label class="radio-inline"><input type="radio" name="notificationtype" value="custom">Custom</label>
						
                    </div>

					<input type="hidden" name="news_id" value="{{$news->id}}" /> 


                    <div class="for_custom" style="display: none;">
                    	<?php
                    	$unicodeChar = '\uD83D\uDC4B';
						$new_uni_code = json_decode('"'.$unicodeChar.'"');
                    	?>
						<div class="form-group">
	                        <label for="inputEmail">Title</label>
	                        <p class="lead emoji-picker-container">
				              <textarea class="form-control" rows="10" cols="50" data-emojiable="true" data-emoji-input="unicode" name="news_title">{{ $new_uni_code." Vibez Update: '".$news->title}}</textarea>
				            </p>
	                    </div>
	                    <div class="form-group">
	                        <label for="inputMessage">Description</label>
	                        <p class="lead emoji-picker-container">
				              <textarea class="form-control" rows="10" cols="50" data-emojiable="true" data-emoji-input="unicode" name="news_description"></textarea>
				            </p>
	                    </div>
	                </div>
	            </div>
	            
	            <!-- Modal Footer -->
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                <input type="submit" name="submit" class="btn btn-primary" value="SUBMIT">
	            </div>
	        </div>
	    </form>    
    </div>
</div>        


<div class="modal fade" id="NewsNotificationModalTest" role="dialog">
    <div class="modal-dialog">
    	<form method="POST" action="<?php echo URL::to('/admin/news/sendPushNotification_News'); ?>" class="push_notification_form">
    		{{ csrf_field() }}
	        <div class="modal-content">
	            <!-- Modal Header -->
	            <!-- Modal Body -->
	            <div class="modal-body">
                    <div class="form-group">
                        <label for="inputName">Type</label><br/>
                        <label class="radio-inline"><input type="radio" name="notificationtypetest" value="default" checked>Default</label>
						<label class="radio-inline"><input type="radio" name="notificationtypetest" value="custom">Custom</label>
						
                    </div>

					<input type="hidden" name="news_id_test" value="{{$news->id}}" /> 


                    <div class="for_custom_test" style="display: none;">
                    	<?php
                    	$unicodeChar = '\uD83D\uDC4B';
						$new_uni_code = json_decode('"'.$unicodeChar.'"');
                    	?>
						<div class="form-group">
	                        <label for="inputEmail">Title</label>
	                        <p class="lead emoji-picker-container">
				              <textarea class="form-control" rows="10" cols="50" data-emojiable="true" data-emoji-input="unicode" name="news_title_test">{{ $new_uni_code." Vibez Update: '".$news->title}}</textarea>
				            </p>
	                    </div>
	                    <div class="form-group">
	                        <label for="inputMessage">Description</label>
	                        <p class="lead emoji-picker-container">
				              <textarea class="form-control" rows="10" cols="50" data-emojiable="true" data-emoji-input="unicode" name="news_description_test"></textarea>
				            </p>
	                    </div>
	                </div>
	            </div>
	            
	            <!-- Modal Footer -->
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                <input type="submit" name="submit" class="btn btn-primary" value="SUBMIT">
	            </div>
	        </div>
	    </form>    
    </div>
</div> 
		
	
<script src="{{ asset('admin_assets/plugins/ckeditor/ckeditor.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/plugins/select2/select2.min.css') }}">
<!-- <script src="{{ asset('admin_assets/js/vendor/jquery-2.1.4.min.js') }}"></script> -->
<script src="{{ asset('admin_assets/plugins/select2/select2.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
	    $('.js-example-basic-multiple').select2({
	    	placeholder: "Select a item",
    		allowClear: true
	    });
	     /*$('.send_push_notification_btn').click(function(){ 
			var r = confirm("Are you sure want to send push notification to users for this news?");
			if (r == true) {
				$('.push_notification_form').submit();
			}
		});*/
	});
</script>

<script type="text/javascript">
$(document).ready(function(){

	$('input[name=notificationtype]').change(function(){
	var value = $( 'input[name=notificationtype]:checked' ).val();
	if(value == 'custom'){
		$('.for_custom').css("display","block");
	}
	else if(value == 'deafult'){
		$('.for_custom').css("display","none");
	}
	else{
		$('.for_custom').css("display","none");	
	}
	});

	$('input[name=notificationtypetest]').change(function(){
	var value = $( 'input[name=notificationtypetest]:checked' ).val();
	if(value == 'custom'){
		$('.for_custom_test').css("display","block");
	}
	else if(value == 'deafult'){
		$('.for_custom_test').css("display","none");
	}
	else{
		$('.for_custom_test').css("display","none");	
	}
	});

	$('[data-toggle="tooltip"]').tooltip();
	var actions = $("table td:last-child").html();
	// Append table with add row form on add new button click
    $(".add-new").click(function(){
		//$(this).attr("disabled", "disabled");
		var index = $("table tbody tr:last-child").index();
		var row = '<tr>' +
            '<td><select class="form-control shcode_type" name="shcode_type"><option value="twitter">twitter</option><option value="instagram">instagram</option></select></td>' +
            '<td><input type="text" class="form-control" name="shcode_text" id="shcode_text"></td>' +
            '<td class="gc"><button type="button" class="btn btn-info add">Generate</button></td>' +
			'<td>' + actions + '</td>' +
        '</tr>';
    	$("table").append(row);		
		$("table tbody tr").eq(index + 1).find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();

        


    });
	// Add row on add button click
	$(document).on("click", ".add", function(){

		var btn_add = $(this);
		var csrf_token = jQuery(document).find('.csrf_token').val();
		var sh_type = $(this).parent().parent().find('.shcode_type :selected').text();
		var sh_text = $(this).parent().parent().find('#shcode_text').val();
		$.ajax({
		       	    beforeSend: function(xhrObj){
			                xhrObj.setRequestHeader("X-CSRF-TOKEN",csrf_token); 
			        },
				   url: site_url+"/admin/shortcode_create",
				   type: 'POST',
				   data: {'sh_type' : sh_type, 'sh_text' : sh_text},
				   success: function(data) 
				   {
				   		var obj = jQuery.parseJSON(data);
				   		btn_add.replaceWith(obj.code);
				   		$(".short_code_id").val(function() {
                           return this.value + obj.id +',';
                        });

				   }
				});
			

    });
	
	// Delete row on delete button click
	$(document).on("click", ".delete", function(){
        

		//$(".add-new").removeAttr("disabled");
		var csrf_token = jQuery(document).find('.csrf_token').val();
		var delete_id = $(this).attr('data-id');
		var shortcode_val = $('.short_code_id').val();
      	var shortcode_val  = shortcode_val.replace(delete_id+',','');
      	$('.short_code_id').val(shortcode_val);
      	
		$.ajax({
		       	    beforeSend: function(xhrObj){
			                xhrObj.setRequestHeader("X-CSRF-TOKEN",csrf_token); 
			        },
				   url: site_url+"/admin/shortcode_delete",
				   type: 'POST',
				   data: {'delete_id' : delete_id},
				   success: function(data) 
				   {

				  }
				});
		$(this).parents("tr").remove();

			
    });
});
</script>

<script type="text/javascript">
$(document).ready(function(){

    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.extraAllowedContent = '*(*);*{*}';
    CKEDITOR.replace('description', {
    toolbar: [
    	["Source"],
        ["Bold","Italic","Underline"],
        // ["Link","Unlink","Anchor"],
        // ["Styles","Format","Font","FontSize"],
        // ["TextColor","BGColor"],
        ["UIColor","Maximize","ShowBlocks"],
        ['oembed'],
        ['Image'],
        "/",
        // ["NumberedList","BulletedList","Outdent","Indent","Blockquote","CreateDiv","JustifyLeft","JustifyCenter","JustifyRight","JustifyBlock"],
        ["JustifyLeft"], 
    ],
    filebrowserImageUploadUrl: 'https://www.thevibez.net/admin/news/attachImage?type=Images&_token=' + $('meta[name=csrf-token]').attr("content"),
    });


    $(function() {
        // Initializes and creates emoji set from sprite sheet
        window.emojiPicker = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          assetsPath: '{{asset('admin_assets/plugins/emoji')}}' +'/lib/img/',
          popupButtonClasses: 'fa fa-smile-o'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        window.emojiPicker.discover();
      });
});

</script>
@endSection

