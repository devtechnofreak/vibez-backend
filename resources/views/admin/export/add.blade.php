@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
        
        <style type="text/css">
            .ds-none {
                display: none;
            }
        </style>

        <div class="breadcrumbs navbar-top">
            <div class="card">
                <div class="card-header border-header">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-3">
                                <strong class="card-title user-name">Export</strong>
                            </div>                                        
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="card"> 
                        <div class="card-header">
                            <!-- <strong class="card-title">Add User</strong> -->
                        </div>
                        <div class="card-body">
                             @if ($errors->any())
                                  <div class="alert alert-danger">
                                      <ul>
                                          @foreach ($errors->all() as $error)
                                              <li>{{ $error }}</li>
                                          @endforeach
                                      </ul>
                                  </div><br/>
                              @endif
                            <form method="POST" action="<?php echo URL::to('/admin/export/create'); ?>" class="add_artist">
                             
                                {{ csrf_field() }}

                                <div class="form-group row">
                                    <label for="dob" class="col-md-2 col-form-label">Artists</label>
                                    <div class="col-md-4">
                                        <select class="form-control myArtistCls" name="artist_id[]" multiple="multiple">
                                            @if( $artists->count() > 0 )
                                                @foreach($artists as $key => $value)
                                                    <option value="{{ $value->id }}" > {{ $value->name }} </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                {{-- <div class="form-group row">
                                    <label for="dob" class="col-md-2 col-form-label">Select Content</label>
                                    <div class="col-md-6">
                                        <select class="form-control" name="contentType">
                                            <option value="">Both</option>
                                            <option value="music">Music</option>
                                            <option value="video">Video</option>
                                        </select>
                                    </div>
                                </div> --}}

                                <div class="form-group row">
                                    <label for="dob" class="col-md-2 col-form-label">Select Content</label>
                                    <div class="col-md-4">
                                        <label class="checkbox-inline">
                                            <input type="radio" name="contentRadio" id="bothCheck" value="both"> &nbsp; Both
                                        </label>
                                        &nbsp;&nbsp;
                                        <label class="checkbox-inline">
                                            <input type="radio" name="contentRadio" id="musicCheck" value="music"> &nbsp; Music
                                        </label>
                                        &nbsp;&nbsp;
                                        <label class="checkbox-inline">
                                            <input type="radio" name="contentRadio" id="videoCheck" value="video"> &nbsp; Video
                                        </label>
                                        &nbsp;&nbsp;
                                        <label class="checkbox-inline">
                                            <i id="loader-1" class="fa fa-spin fa-spinner ds-none" aria-hidden="true" style="font-size:20px;"></i>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="dob" class="col-md-2 col-form-label">Select Music/Video</label>
                                    <div class="col-md-4">
                                        <select class="form-control mySelectedData" name="artistContentData[]" multiple="multiple">
                                            @if( $video->count() > 0 )
                                                @foreach($video as $key => $value)
                                                    <option value="{{ $value->newId }}-video" > {{ $value->title." - video" }} </option>
                                                @endforeach
                                            @endif
                                            @if( $music->count() > 0 )
                                                @foreach($music as $key => $value)
                                                    <option value="{{ $value->newId }}-music" > {{ $value->title." - music" }} </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="dob" class="col-md-2 col-form-label">Date Filter</label>
                                    <div class="col-md-2">
                                        <select class="form-control" name="dateFilter">
                                            <option value="all" selected>All Time</option>
                                            <option value="this_year">This Year - {{ date('Y') }}</option>
                                            <option value="last_year">Last Year - {{ ((int)date('Y')) - 1 }} </option>
                                            <option value="last_30">Last 30 Days</option>
                                            <option value="last_60">Last 60 Days</option>
                                            <option value="last_90">Last 90 Days</option>
                                            <option value="custom">Custom</option>
                                        </select>
                                    </div>
                                    <div class="col-md-1">
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <i id="loader-2" class="fa fa-spin fa-spinner ds-none" aria-hidden="true" style="font-size:20px;margin-top:10px;"></i>
                                    </div>
                                </div>

                                <div class="form-group row ds-none hasCustomSelected">
                                    <label for="dob" class="col-md-2 col-form-label">Date From</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control datepick" name="date_from" autocomplete="off">
                                    </div>
                                </div>

                                <div class="form-group row ds-none hasCustomSelected">
                                    <label for="dob" class="col-md-2 col-form-label">Date To</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control datepick" name="date_to" autocomplete="off">
                                    </div>                                    
                                </div>

                                <div class="form-group row mb-0">
                                    <label for="dob" class="col-md-2 col-form-label"></label>
                                    <div class="col-md-4">
                                        <input type="submit" name="Generate" class="btn btn-primary" value="Generate">                                        
                                    </div>
                                </div>  
                            </form>
                        </div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/plugins/select2/select2.min.css') }}">
<!-- <script src="{{ asset('admin_assets/js/vendor/jquery-2.1.4.min.js') }}"></script> -->
<script src="{{ asset('admin_assets/plugins/select2/select2.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('.alert').delay(3500).fadeOut('slow');

        // $('.js-example-basic-multiple').select2({});
        $('.myArtistCls').select2({
            placeholder: "Select an artist, leave it blank if you want the all artist",
            allowClear: true,
            search : true
        });

        $('.mySelectedData').select2({
            placeholder: "Select musics or videos",
            allowClear: true,
            search : true
        });
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
        }); 

        $('.add_artist').submit(function(event) {
            // event.preventDefault();

            // if ( $('[name="dateFilter"]').val() == '' || $('[name="dateFilter"]').val() != 'all' ) {
            if ( $('[name="dateFilter"]').val() === 'custom') {
                
                var date_from = $('[name="date_from"]').val();
                var date_to   = $('[name="date_to"]').val();

                if (date_from == '' || date_to == '') {
                    alert('Please select the dates');
                    return false;
                }

                date_from = new Date(date_from);
                date_to   = new Date(date_to);

                if ( date_from >= date_to ) { 
                    alert('DATE TO should be greater than DATE FROM');
                    return false;
                }
            }                
        });
        
        $(document).on('change', '[name="dateFilter"]', function(event) {
            
            if ( $(this).val() === 'custom') {

                $('.hasCustomSelected').removeClass('ds-none');

                // $.ajax({
                //     url: "{{ url('admin/export/getDateByFilter') }}",
                //     type: 'POST',
                //     dataType: 'json',
                //     data: { filter_str : $.trim($(this).val()) },
                //     success : function(dataRes) {
                //         $('#loader-2').addClass('ds-none');
                //         if (dataRes.success == 1) {
                //             $('[name="date_from"]').val(dataRes.newdate.date_from);
                //             $('[name="date_to"]').val(dataRes.newdate.date_to);
                //         } else {
                //             $('[name="date_from"]').val('');
                //             $('[name="date_to"]').val('');            
                //         }
                //     },
                //     error : function(error) {
                //         alert('Oops something went wrong...');
                //     }
                // });
            } else {
                $('[name="date_from"]').val('');
                $('[name="date_to"]').val('');
                $('.hasCustomSelected').addClass('ds-none');
            }
        });


        $(document).on('change', 'input[type="radio"]', function(event) {
            
            var contentType = $.trim($(this).val());
            var artist_ids  = $('.myArtistCls').val();            

            if( artist_ids != '') {

                if ($('.mySelectedData').val())
                    $('.mySelectedData').select2('val', null);
                
                $('#loader-1').removeClass('ds-none');

                $.ajax({
                    url: "{{ url('admin/export/getDataByArtist') }}",
                    type: 'POST',
                    dataType: 'json',
                    data: { artist_ids : artist_ids, contentType : contentType },
                    success : function(dataRes) {
                        $('#loader-1').addClass('ds-none');
                        if (dataRes.success == 1) {
                            $('.mySelectedData').html($.trim(dataRes.dataHtml));
                        } else {
                            $('.mySelectedData').html('');                            
                        }
                    },
                    error : function(error) {
                        alert('Oops something went wrong...');
                    }
                });                
            }
            // else {
            //     $('.mySelectedData').html('');   
            // }
        });

        $(document).on('change', '.myArtistCls', function(event) {
            // event.preventDefault();
            if($(this).val() != '') {

                if ($('.mySelectedData').val())
                    $('.mySelectedData').select2('val', null);
                
                $('#loader-1').removeClass('ds-none');

                $.ajax({
                    url: "{{ url('admin/export/getDataByArtist') }}",
                    type: 'POST',
                    dataType: 'json',
                    data: { artist_ids : $(this).val(), contentType : $.trim($('input[type="radio"]:checked').val()) },
                    success : function(dataRes) {
                        $('#loader-1').addClass('ds-none');
                        if (dataRes.success == 1) {
                            $('.mySelectedData').html($.trim(dataRes.dataHtml));
                        }else {
                            $('.mySelectedData').html('');                            
                        }
                    },
                    error : function(error) {
                        alert('Oops something went wrong...');
                    }
                });                
            }
            // else {
            //     $('.mySelectedData').html('');   
            // }
        });

        $('.datepick').each(function(){
            $(this).datepicker();
        });

    });
</script>       
    
@stop

