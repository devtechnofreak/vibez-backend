@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
		
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Tags</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-4">
			                        		<strong class="card-title taglist-name">Tags List</strong>
			                        	</div>
			                            <div class="col-sm-8">	
			                            	<a class="btn btn-primary float-right ml-2" href="<?php echo URL::to('/admin/tag/add'); ?>"><strong class="card-title"><i class="fa fa-plus"></i> Add New</strong></a>
			                            </div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>

        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                       <!--  <div class="card-header">
                            <strong class="card-title">Tags List</strong>
                            <div class="float-right">
                            	<a class="btn btn-primary float-right ml-2" href="<?php echo URL::to('/admin/tag/add'); ?>"><strong class="card-title">Add New</strong></a>
                            </div>
                        </div> -->
                        <div class="card-body">
							@if(session()->has('message'))
								<div class="alert alert-success">
									{{ session()->get('message') }}
								</div>
							@endif
							<table id="users" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>Name</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>Name</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
                        </div>
                    </div>
                </div>
				<input type="hidden" class="csrf_token" value="{{ csrf_token() }}">
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	<script src="{{ asset('admin_assets/js/lib/data-table/datatables.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/jszip.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/vfs_fonts.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/datatables-init.js') }}"></script>


<script type="text/javascript">
	$(document).ready(function() {
		var csrf_token = jQuery(document).find('.csrf_token').val();
		$('#users').DataTable( {
			"processing": true,
			"serverSide": false,
			"pageLength": 50,
			"ajax": {
				"url": "tag/gettags",
				"type": "POST",
				"dataType": 'json',
                "error": function(data){
                        console.log(data);
                    },
				"headers" : {
						'X-CSRF-TOKEN': csrf_token
				}
			},
			"columns": [
				{ "data": "name" },
				{ "data": "id",     render: function ( data, type, row ) {
						// console.log(data);
						var editlink = "<?php echo URL::to('/admin/tag/edit/"+data+"'); ?>";
						var deletelink = "<?php echo URL::to('/admin/tag/delete/"+data+"'); ?>";
						return '<li class="libtnlink"><a class="edit_link" href="'+editlink+'"><i class="fa fa-pencil"></i></a></li><li class="libtnlink"><a class="delete_link" href="'+deletelink+'"><i class="fa fa-trash"></i></a></li>';
					return data; 
				} }
			]
		} );
		
		
    } );
</script>	
@stop

