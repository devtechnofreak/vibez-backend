@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
<?php 

?>
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Tag</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-4">
			                        		<strong class="card-title taglist-name">Edit Tag</strong>
			                        	</div>
			                            <div class="col-sm-8">	
			                            	<a href="{{ url('admin/tags') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Tag List</a>
			                            </div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>

        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif
						<div class="card"> 
							<!-- <div class="card-header">
								<strong class="card-title">Edit Tag</strong>
							</div> -->
							<div class="card-body">
								 @if ($errors->any())
									  <div class="alert alert-danger">
										  <ul>
											  @foreach ($errors->all() as $error)
												  <li>{{ $error }}</li>
											  @endforeach
										  </ul>
									  </div><br/>
								  @endif
								<form method="POST" action="<?php echo URL::to('/admin/tag/editdata'); ?>">
								 {{ csrf_field() }}
									<div class="form-group row"><label for="name" class="col-md-2 col-form-label text-md-left">Name</label>
										<div class="col-md-6"><input id="name" name="name" required="required" autofocus="autofocus" class="form-control" value="{{$tag->name}}" type="text"></div>
									</div>
									@if ($errors->has('name'))
										<span class="invalid-feedback">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
									@endif
									
									
									<div class="form-group row mb-0 user_form_submit">
										<div class="col-md-6 offset-md-4">
											<input type="hidden" name="tag_id" value="{{$tag->tag_id}}" /> 
											<button type="submit" class="btn btn-primary">
												Update Tag
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
@stop

