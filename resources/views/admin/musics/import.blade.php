@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Musics</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3 music_import">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif
                    <div class="card"> 
                        <div class="card-header">
                            <strong class="card-title">Add Music</strong>
                        </div>
						<div class="card-body">
							 @if ($errors->any())
								  <div class="alert alert-danger">
									  <ul>
										  @foreach ($errors->all() as $error)
											  <li>{{ $error }}</li>
										  @endforeach
									  </ul>
								  </div><br/>
							  @endif
							<form method="POST" action="<?php echo URL::to('/admin/musics/import'); ?>" enctype="multipart/form-data" class="add_artist">
							 	{{ csrf_field() }}
								<div class="row">
									<div class="col-md-2">Image</div>
									<div class="col-md-2">Title</div>
									<div class="col-md-2">Release Date</div>
									<div class="col-md-3">Add Artist</div>
									<div class="col-md-3">Add Tag</div>
								</div>
								
								@foreach($images as $key => $image)
								<div class="row" style="padding-bottom:100px">
									<div class="col-md-2"><img src="{{ asset('import/music/images').'/'. $image }}" class="import_image" style="width:50px;height:50px"><input type="hidden" name="{{ $key.'_image' }}" value="{{$image}}"></div>
									<div class="col-md-2"><input type="text" class="form-control" name="{{ $key.'_title' }}" placeholder="Enter Title" id="title" value=""></div>
									<div class="col-md-2"><input type="text" class="form-control release_date" name="{{ $key.'_date' }}" placeholder="DD-MM-YYYY"></div>
									<div class="col-md-3">
										<select class="js-example-basic-multiple form-control artists" name="{{ $key.'_artists_id[]' }}" value="" multiple="multiple">
											@foreach($artists as $artist)
											  <option name="artist_id" value="{{ $artist->id }}">{{ $artist->name }}</option>
											@endforeach
										</select>
									</div>
									<div class="col-md-3">
										<select class="js-example-basic-multiple form-control tags" name="{{ $key.'_tags_id[]' }}" value="" multiple="multiple">
											@foreach($tags as $tag)
											  <option name="tag_id" value="{{ $tag->tag_id }}">{{ $tag->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								@endforeach
								
								<div class="form-group row mb-0">
									<div class="col-md-6 offset-md-4">
										<button class="btn btn-primary" type="submit">Import</button>
									</div>
								</div>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
@stop
<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/plugins/select2/select2.min.css') }}">
<script src="{{ asset('admin_assets/js/vendor/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('admin_assets/plugins/select2/select2.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		jQuery('.menu-item-has-children.dropdown .dropdown-toggle').click(function(){
				jQuery(this).parent().toggleClass('show');
				jQuery(this).next().toggleClass('show');
			})
	    $('.artists').select2({
	    	placeholder: "Select Artist"
	    });
	    $('.tags').select2({
	    	placeholder: "Select Tag"
	    });
	});
</script>
<style text="text/css">
	/* ------Atrist add style for music---- */
	
.music_import .col-md-1, .music_import .col-md-2, .music_import .col-md-3, .music_import .col-md-4, .music_import .col-md-5{
	
	position: relative;
	overflow: hidden;
	
}
.music_import #artists {
	width: 75%;
}

.music_import #tags {
	width: 75%;
}

.music_import .add_artist_ajax{
	position: absolute;
    right: 0;
    top: 0px;
    border: 1px solid #ced4da;
    padding: 5px;
    transition: width 0.5s;
    display: none; 
    cursor: pointer;
}


/*----------------------------*/

.music_import .add_tag_ajax{
	position: absolute;
    right: 0;
    top: 0px;
    border: 1px solid #ced4da;
    padding: 5px;
    transition: width 0.5s;
    display: none; 
    cursor: pointer;
}

</style>