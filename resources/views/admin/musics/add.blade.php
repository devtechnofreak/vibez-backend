@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Musics</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif
                    <div class="card"> 
                        <div class="card-header">
                            <strong class="card-title">Add Music</strong>
                        </div>
						<div class="card-body">
							 @if ($errors->any())
								  <div class="alert alert-danger">
									  <ul>
										  @foreach ($errors->all() as $error)
											  <li>{{ $error }}</li>
										  @endforeach
									  </ul>
								  </div><br/>
							  @endif
							<form method="POST" action="<?php echo URL::to('/admin/musics/create'); ?>" enctype="multipart/form-data" class="add_artist">
							 	{{ csrf_field() }}
								<!-- <div class="form-group row"><label for="dob" class="col-md-4 col-form-label">Artist</label>
									<div class="col-md-6">
										<select class="js-example-basic-multiple form-control" name="artist_id[]" value="" multiple="multiple">
											@foreach($artists as $key => $artist)
											  <option name="artist_id" value="{{ $artist->id }}">{{ $artist->name }}</option>
											@endforeach
										</select>
									</div> 
								</div> -->
								
								<div class="form-group row"><label for="dob" class="col-md-4 col-form-label">Artist</label>
									<div class="col-md-6">
										<input type="text" id="artists" name="artists" class="form-control" placeholder="Artists" autocomplete="off">
										<input type="hidden" id="artistsId" name="artistsId" class="form-control" placeholder="Artists">
										<a class="add_artist_ajax">Add Artist</a>
										<div class="artists_result">
											<ul>
												
											</ul>
										</div>
										<div class="display_artists">
											<ul>
												
											</ul>
										</div>
										
									</div>
								</div>
								
								<div class="form-group row"><label for="name" class="col-md-4 col-form-label">Title</label>
									<div class="col-md-6"><input id="title" name="title" value="" placeholder="Enter Title" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								@if ($errors->has('title'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
								<div class="form-group row"><label for="image" class="col-md-4 col-form-label">Image</label>
									<div class="col-md-6"><input id="image" name="image" value="" required="required" autofocus="autofocus" class="form-control" type="file"></div>
								</div>
								@if ($errors->has('image'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                                <div class="form-group row"><label for="date" class="col-md-4 col-form-label">Release Date</label>
									<div class="col-md-6"><input type="text" class="form-control release_date" name="date"></div>
								</div>
								@if ($errors->has('date'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif
                                
                                <div class="form-group row"><label for="audio" class="col-md-4 col-form-label">Audio</label>
									<div class="col-md-6"><input id="audio" name="audio" value="" required="required" autofocus="autofocus" class="form-control" type="file"></div>
								</div>
								@if ($errors->has('audio'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('audio') }}</strong>
                                    </span>
                                @endif
                                
                                <div class="form-group row"><label for="dob" class="col-md-4 col-form-label">Tags</label>
									<div class="col-md-6">
										<input type="text" id="tags" name="tags" class="form-control" placeholder="Tags" autocomplete="off">
										<input type="hidden" id="tagsId" name="tagsId" class="form-control" placeholder="Tags">
										<a class="add_tag_ajax">Add Tags</a>
										<div class="tags_result">
											<ul>
												
											</ul>
										</div>
										<div class="display_tags">
											<ul>
												
											</ul>
										</div>
										
									</div>
								</div>
								
								<div class="form-group row mb-0">
									<div class="col-md-6 offset-md-4">
										<a class="btn btn-primary add_music_btn">Add Music</a>
									</div>
								</div>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
@stop
<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/plugins/select2/select2.min.css') }}">
<script src="{{ asset('admin_assets/js/vendor/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('admin_assets/plugins/select2/select2.min.js') }}"></script>




