@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
		
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Musics</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Musics List</strong>
                            <div class="float-right">
                            	<a class="btn btn-success float-right ml-2" href="{{url('admin/album/add')}}"><strong class="card-title"><i class="fa fa-plus"></i> Bulk Import Musics</strong></a>
                            </div>
                        </div>
                        <div class="card-body">
							@if(session()->has('message'))
								<div class="alert alert-success">
									{{ session()->get('message') }}
								</div>
							@endif
							<table id="music_list" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>Title</th>
										<th>Uploaded At</th>
										<th>Image</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>Title</th>
										<th>Uploaded At</th>
										<th>Image</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
                        </div>
                    </div>
                </div>
				<input type="hidden" class="csrf_token" value="{{ csrf_token() }}">
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	<script src="{{ asset('admin_assets/js/lib/data-table/datatables.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/jszip.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/vfs_fonts.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/datatables-init.js') }}"></script>


<script type="text/javascript">

	$(document).ready(function() {
		var csrf_token = jQuery(document).find('.csrf_token').val();
		$('#music_list').DataTable( {
			"processing": true,
			"serverSide": false,
			"bAutoWidth": false,
			"pageLength": 50,
			"ajax": {
				"url": "musics/getmusics",
				"type": "POST",
				"dataType": 'json',
                "error": function(data){
                        console.log(data);
                    },
				"headers" : {
						'X-CSRF-TOKEN': csrf_token
				}
			},
			"columns": [
				{ "data": "title" },
				{ "data": "uploaded","width": "150px" },
				{ "data": "image" ,     render: function ( data, type, row ) {
					var aws_url = "{{ env('AWS_URL') }}";
						
						var image_url = data;
						
						return '<img src="'+image_url+'" class="img-thumbnail"/>';
					return data;
				},"width": "200px" },
				{ "data": "id",     render: function ( data, type, row ) {
						console.log(data);
						var editlink = "<?php echo URL::to('/admin/musics/edit/"+data+"'); ?>";
						var deletelink = "<?php echo URL::to('/admin/musics/delete/"+data+"'); ?>";
						return '<a class="edit_link pl-2 text-warning" href="'+editlink+'"><i class="fa fa-pencil"></i></a><a class="delete_link pl-2 text-danger" href="'+deletelink+'"><i class="fa fa-trash"></i></a>';
					
					return data;
				},"width": "100px" }
			]
		} );
		
		
    } );
</script>	
@stop

