@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
<?php 

?>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Music</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif
					 @if ($errors->any())
						  <div class="alert alert-danger">
							  <ul>
								  @foreach ($errors->all() as $error)
									  <li>{{ $error }}</li>
								  @endforeach
							  </ul>
						  </div><br/>
					  @endif
						<div class="card"> 
							<div class="card-header border-header">
								<strong class="card-title"><i class="fa fa-pencil"></i> Edit Music</strong>
								<a href="{{ url('admin/album/edit/'.$music->album[0]->id) }}" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Back</a>
							</div>
							<div class="card-body">

								<form method="POST" action="<?php echo URL::to('/admin/musics/update'); ?>" class="music_edit_form"  enctype="multipart/form-data">
								 {{ csrf_field() }}
									<div class="form-group row"><label for="name" class="col-md-2 col-form-label">Title</label>
										<div class="col-md-6"><input id="title" name="title" required="required" autofocus="autofocus" class="form-control" value="{{$music->title}}" type="text"></div>
									</div>
									@if ($errors->has('title'))
										<span class="invalid-feedback">
											<strong>{{ $errors->first('title') }}</strong>
										</span>
									@endif
									<div class="form-group row"><label for="image" class="col-md-2 col-form-label">Image</label>
										<div class="col-md-6"><div class="col-md-2"><img src="{{$music->image}}" class="artist_image img-fluid img-thumbnail"></div>
										<div class="col-md-10">
											<input id="image" name="image" class="form-control" value="" type="file">
										</div>
										</div>
									</div>
									@if ($errors->has('image'))
										<span class="invalid-feedback">
											<strong>{{ $errors->first('image') }}</strong>
										</span>
									@endif
									<div class="form-group row"><label for="dob" class="col-md-2 col-form-label">Is Trending</label>
										<div class="col-md-6"><input value="1" type="checkbox" name="isTrending" {{$music->is_trending ?'checked' : ''}}></div> 
									</div>
									<div class="form-group row"><label for="dob" class="col-md-2 col-form-label">Is New</label>
										<div class="col-md-6"><input value="1" type="checkbox" name="isNew" {{$music->is_new ?'checked' : ''}}></div> 
									</div>
									<div class="form-group row"><label for="date" class="col-md-2 col-form-label">Release Date</label>
										<div class="col-md-6"><input type="text" class="hasDatepicker" id="dob" name="date" value="{{$music->date}}" required></div>
									</div>
									@if ($errors->has('date'))
	                                    <span class="invalid-feedback">
	                                        <strong>{{ $errors->first('date') }}</strong>
	                                    </span>
	                                @endif
	                                <div class="form-group row">
	                                	<label for="artist" class="col-md-2 col-form-label">Artist</label>
										<div class="col-md-6">
											<select name="artist" value="" class="col-md-6 form-control">
												@foreach($artists as $artist)
												<option value="{{$artist->id}}" {{$artist->id==$music->artists[0]->id?'selected':''}}>{{$artist->name}}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group row"><label for="music_tags" class="col-md-2 col-form-label">Tags</label>										
										<div class="col-md-6"><input id="music_tags" name="music_tags" value="" required="required" placeholder="Enter comma , separated Tags" autofocus="autofocus" class="form-control mb-2" type="text">
										</div>
									</div>									
									<div class="form-group row mb-0 user_form_submit">
										<div class="col-md-6 offset-md-4">
											<input type="hidden" name="music_id" value="{{$music->id}}" /> 
											<a class="btn btn-primary update_music_btn">Update Music</a>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
@stop

