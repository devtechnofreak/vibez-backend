@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->

        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>News</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="breadcrumbs navbar-top">
                        <div class="card">
                            <div class="card-header border-header">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <strong class="card-title newslist-name"><i class="fa fa-plus"></i> Add Page</strong>
                                        </div>
                                        <div class="col-md-9">
                                            <a href="{{ url('admin/news') }}" class="btn btn-primary float-right backbtn"><i class="fa fa-arrow-left"></i> News List</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>

		@if(session()->has('message'))
		<script type="text/javascript">
		jQuery(document).ready(function($){
		        $.notify({
		            icon: "nc-icon nc-app",
		            message: '{{ session()->get('message') }}',
		        }, {
		            type: 'success',
		            timer: 8000,
		        });
		 });
		 </script>
		@endif
		@if ($errors->any())
			  @foreach ($errors->all() as $error)
					<script type="text/javascript">
					jQuery(document).ready(function($){
					        $.notify({
					            icon: "nc-icon nc-app",
					            message: '{{ $error }}',
					        }, {
					            type: 'danger',
					            timer: 8000,
					        });												
					});
					 </script>
			  @endforeach
		@endif
        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card"> 
                        <!-- <div class="card-header border-header">
                            <strong class="card-title newslist-name">Add News</strong>
                            <a href="{{ url('admin/news') }}" class="btn btn-primary float-right backbtn"><i class="fa fa-arrow-left"></i> News List</a>
                        </div> -->
						<div class="card-body">
							<form method="POST" action="<?php echo URL::to('/admin/page/create'); ?>" enctype="multipart/form-data" class="add_news">
							 	{{ csrf_field() }}
								<div class="form-group row"><label for="title" class="col-md-4 col-form-label">Title</label>
									<div class="col-md-6"><input id="title" name="title" value="" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								@if ($errors->has('title'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
								<div class="form-group row"><label for="slug" class="col-md-4 col-form-label">Slug</label>
									<div class="col-md-6"><input id="slug" name="slug" value="" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								@if ($errors->has('slug'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                @endif                                
                                <div class="form-group row"><label for="description" class="col-md-4 col-form-label">Content</label>
									<div class="col-md-6"><textarea id="description" name="content" value="" required="required" autofocus="autofocus" class="form-control"></textarea></div>
								</div>
								
								<div class="form-group row mb-0">
									<div class="col-md-6 offset-md-4">
										<a class="btn btn-primary add_news_btn">Add Page</a>
									</div>
								</div>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
<script src="{{ asset('admin_assets/plugins/ckeditor/ckeditor.js') }}"></script>

<script type="text/javascript">

$(document).ready(function(){

    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.extraAllowedContent = '*(*);*{*}';
    CKEDITOR.replace('description', {
    toolbar: [
        ["Source"],
        ["Bold","Italic","Underline"],
        ["Link","Unlink","Anchor"],
        ["Styles","Format","Font","FontSize"],
        ["TextColor","BGColor"],
        ["UIColor","Maximize","ShowBlocks"],
        "/",
        ["NumberedList","BulletedList","Outdent","Indent","Blockquote","CreateDiv","JustifyLeft","JustifyCenter","JustifyRight","JustifyBlock"],
    ]
    });
});

</script>
@endSection
