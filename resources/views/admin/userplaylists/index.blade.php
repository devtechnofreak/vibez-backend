@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
		
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Playlists</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-4">
			                        		<strong class="card-title playlist-name">User playlists</strong>
			                        	</div>
			                            <div class="col-sm-8">	
			                            	
			                            </div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>


        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                       <!--  <div class="card-header border-header">
                            <strong class="card-title playlist-name">PlayLists</strong>
                            <div class="float-right">
                            	<a class="btn btn-primary float-right ml-2" href="<?php echo URL::to('/admin/play-lists/add'); ?>"><strong class="card-title"><i class="fa fa-plus"></i> Add New</strong></a>
                            </div>
                        </div> -->
                        <div class="card-body">
							@if(session()->has('message'))
								<div class="alert alert-success">
									{{ session()->get('message') }}
								</div>
							@endif
							<div class="video_bulk_action">
	                        	<label>Playlists</label>
	                        </div>
							<table id="play_lists" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>
										
										<th>Image</th>
										<th>User</th>
										<th>Title</th>
										<th>Description</th>
										<th>Musics</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										
										<th>Image</th>
										<th>User</th>
										<th>Title</th>
										<th>Description</th>
										<th>Musics</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
                        </div>
                    </div>
                </div>
				<input type="hidden" class="csrf_token" value="{{ csrf_token() }}">
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	<script src="{{ asset('admin_assets/js/lib/data-table/datatables.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/jszip.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/vfs_fonts.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/datatables-init.js') }}"></script>


<script type="text/javascript">

	$(document).ready(function() {
		var csrf_token = jQuery(document).find('.csrf_token').val();
		$('#play_lists').DataTable( {
			"processing": true,
			"serverSide": true,
			"bAutoWidth": false,
			"pageLength": 50,
			"lengthMenu": [50, 75, 100 ],
			"ajax": {
				"url": "user-play-lists/get-play-lists",
				"data"	: {"user_id" : 1},
				"type": "POST",
				"dataType": 'json',
                "error": function(data){
                        console.log(data);
                    },
				"headers" : {
						'X-CSRF-TOKEN': csrf_token
				}
			},
			"columns": [
				{ "data": "image" ,     render: function ( data, type, row ) {
						
						var aws_url = "{{ env('AWS_URL') }}";
						
						var image_url = data;
						
						return '<img src="'+image_url+'" style="height: 50; width:50" class="img-thumbnail" />';
					return data;
				},"width": "7%" },
				{ "data": "user_name","width": "10%" },
				{ "data": "title","width": "12%" },
				{ "data": "description","width": "15%" },
				{ "data": "musics" ,     render: function ( data, type, row ) {
					var str ='';
					jQuery.each( data, function( key, value ) {
					  str += '<div class="d-inline-block bg-info pl-2 pr-2 mb-2 mr-2 text-light">'+value+'</div>';
					});
						return str;
					return data;
				}},
				{ "data": "id",     render: function ( data, type, row ) {
						var editlink = "<?php echo URL::to('/admin/user-playlist/edit/"+data+"'); ?>";
						var addtomainlink = "<?php echo URL::to('/admin/user-playlist/addtomain/"+data+"'); ?>";
						return '<li class="libtnlink"><a class="text-warning" href="'+editlink+'"><i class="fa fa-eye"></i></a></li><li class="libtnlink"><a title="Add to main playlist" class="text-warning" href="'+addtomainlink+'"><i class="fa fa-plus"></i></a></li>';
					
					return data;
				}}
			]
		} );
		
		
    } );
</script>	
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
@stop