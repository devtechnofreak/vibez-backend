@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
<?php 

?>
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Playlist</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <style type="text/css">
        	.emoji-wysiwyg-editor
        	{
        		max-height :100px !important;
        	}
        </style>
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-md-3">
											 <strong class="card-title">Edit Playlist</strong>
										</div>
										<?php if($playlist->is_admin == '0') { ?>
										<div class="col-md-9">
											 <a href="{{url('admin/user-play-lists')}}" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i>User playlist</a>
										</div>
										<?php } else { ?>
										<div class="col-md-9">
											<a href="{{url('admin/play-lists')}}" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i>Back to main playlist</a>
										</div>	
										<?php } ?>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>

        

        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">
					@if(session()->has('message'))
					<script type="text/javascript">
					jQuery(document).ready(function($){
					        $.notify({
					            icon: "nc-icon nc-app",
					            message: '{{ session()->get('message') }}',
					        }, {
					            type: 'success',
					            timer: 8000,
					        });
					 });
					 </script>
					@endif
					@if ($errors->any())
						  @foreach ($errors->all() as $error)
								<script type="text/javascript">
								jQuery(document).ready(function($){
								        $.notify({
								            icon: "nc-icon nc-app",
								            message: '{{ $error }}',
								        }, {
								            type: 'danger',
								            timer: 8000,
								        });												
								});
								 </script>
						  @endforeach
					  @endif
						<div class="card"> 
							<div class="card-body row">
								 @if ($errors->any())
									  <div class="alert alert-danger">
										  <ul>
											  @foreach ($errors->all() as $error)
												  <li>{{ $error }}</li>
											  @endforeach
										  </ul>
									  </div><br/>
								  @endif
								<div class="col-md-8">
									<?php $id = $playlist->id; ?>
									<form method="get" action="<?php echo URL::to("/admin/user-playlist/addtomain/$id"); ?>" class="artist_edit_form">
									<div class="form-group row"><label for="name" class="col-md-4 col-form-label">Title</label>
										<div class="col-md-6">{{$playlist->title}}</div>
									</div>
									@if ($errors->has('title'))
	                                    <span class="invalid-feedback">
	                                        <strong>{{ $errors->first('title') }}</strong>
	                                    </span>
	                                @endif
	                                 <div class="form-group row"><label for="description" class="col-md-4 col-form-label">Description</label>
										<div class="col-md-6">{{$playlist->description}}</div>
									</div>
									
									<div class="form-group row"><label for="musics" class="col-md-4 col-form-label">Artist Featured</label>
										<div class="col-md-6">
											
											  @foreach($featured_artist as $artist)
											  	<span class="badge badge-pill badge-info" style="margin-bottom: 5px;font-size : 1rem;">{{ $artist->artist_name }}</span>
											  @endforeach
											 
											
										</div>	
									</div>	
					                        
										<?php if($playlist->is_admin == '0') { ?>
										<div class="form-group row mb-0 user_form_submit">
											<div class="col-md-6 offset-md-4">
												<input type="hidden" name="playlist_id" value="{{$playlist->id}}" /> 
												<a class="btn btn-primary update_artist_btn">Add to main playlist</a>
											</div>
										</div>
										<?php } ?>
										
										
									
									</form>
									
									
								</div>
								<div class="col-md-4">
									<div class="card">
				                            <div class="card-image">
				                                <img src="{{$playlist->image}}" class="artist_image">
				                            </div>
				                            <div class="card-body">
				                            		<h3 class="title">{{$playlist->title}}</h3>
				                           	</div>
				                            <hr>
				                    </div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header">
								<strong class="card-title">Playlist Musics</strong>
							</div>
							 <div class="card-body row">
							 	<?php 
							 		$playlist_music = $playlist->musics->pluck('id','title')->toArray();
							 	?>
							 	<div class="row playlist_music_drag" id="sortable">
	                        		<ul id="items" data-album-id="{{$playlist->id}}"> 
	                        		@foreach($playlist_music as $title => $id)
                        				<li class="list" data-music-id="{{$id}}">
			                        		<?php echo $title ?>
			                        		<i class="fa fa-arrows" aria-hidden="true"></i>
			                        	</li>
	                        		@endforeach
	                        		</ul>
	                        	</div>
							 </div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

@stop
<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/plugins/select2/select2.min.css') }}">
<script src="{{ asset('admin_assets/js/vendor/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('admin_assets/plugins/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>