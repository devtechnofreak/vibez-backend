@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
<?php 

?>
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Category</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-4">
			                        		<strong class="card-title categorylist-name">Edit Category</strong>
			                        	</div>
			                            <div class="col-sm-8">	
			                            	<a href="{{ url('admin/categories') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Category List</a>
			                            </div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>

        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif
						<div class="card"> 
							<div class="card-header">
								<strong class="card-title">Edit Category</strong>
							</div>
							<div class="card-body row">
								 @if ($errors->any())
									  <div class="alert alert-danger">
										  <ul>
											  @foreach ($errors->all() as $error)
												  <li>{{ $error }}</li>
											  @endforeach
										  </ul>
									  </div><br/>
								  @endif
								<div class="col-md-8">  
									<form method="POST" action="<?php echo URL::to('/admin/category/editdata'); ?>" enctype="multipart/form-data">
									 {{ csrf_field() }}
										<div class="form-group row"><label for="name" class="col-md-2 col-form-label text-md-left">Name</label>
											<div class="col-md-6"><input id="name" name="name" required="required" autofocus="autofocus" class="form-control" value="{{$category->name}}" type="text"></div>
										</div>
										@if ($errors->has('name'))
											<span class="invalid-feedback">
												<strong>{{ $errors->first('name') }}</strong>
											</span>
										@endif
										<div class="form-group row"><label for="slug" class="col-md-2 col-form-label text-md-left">Slug</label>
											<div class="col-md-6"><input id="slug" name="slug" required="required" class="form-control" value="{{$category->slug}}" type="text"></div>
										</div>
										@if ($errors->has('name'))
											<span class="invalid-feedback">
												<strong>{{ $errors->first('name') }}</strong>
											</span>
										@endif

										<div class="form-group row"><label for="image" class="col-md-2 col-form-label text-md-left">Image</label>
												<div class="col-md-6"><input id="image" name="image" class="form-control" value="" type="file"></div>
											</div>
											@if ($errors->has('image'))
												<span class="invalid-feedback">
													<strong>{{ $errors->first('image') }}</strong>
												</span>
											@endif
											
										<div class="form-group row"><label for="dob" class="col-md-2 col-form-label">Is Featured</label>
											<div class="col-md-6"><input value="1" @if($category->is_featured == 1) checked="checked" @endif type="checkbox" id="isFeatured" name="is_featured"></div> 
										</div>	
										
										<div class="form-group row mb-0 user_form_submit">
											<div class="col-md-6 offset-md-4">
												<input type="hidden" name="category_id" value="{{$category->id}}" /> 
												<button type="submit" class="btn btn-primary">
													Update Category
												</button>
											</div>
										</div>
									</form>
								</div>
								<div class="col-md-4">
									<div class="card">
			                            <div class="card-image">
			                                <img src="{{$category->image}}" class="artist_image">
			                            </div>
			                        </div>
			                    </div>	
							</div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
@stop

