@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->

        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Categories</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
                        <div class="card">
                            <div class="card-header border-header">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <strong class="card-title categorylist-name">Add Category</strong>
                                        </div>
                                        <div class="col-md-9">
                                            <a href="{{ url('admin/categories') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Category List</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>

        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif
                    <div class="card"> 
                        <div class="card-header">
                            <strong class="card-title">Add Category</strong>
                        </div>
						<div class="card-body">
							 @if ($errors->any())
								  <div class="alert alert-danger">
									  <ul>
										  @foreach ($errors->all() as $error)
											  <li>{{ $error }}</li>
										  @endforeach
									  </ul>
								  </div><br/>
							  @endif
							<form method="POST" action="<?php echo URL::to('/admin/category/create'); ?>" enctype="multipart/form-data">
							 {{ csrf_field() }}
								<div class="form-group row"><label for="name" class="col-md-4 col-form-label text-md-right">Name</label>
									<div class="col-md-6"><input id="name" name="name" value="" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								@if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
								<div class="form-group row"><label for="slug" class="col-md-4 col-form-label text-md-right">Slug</label>
									<div class="col-md-6"><input id="slug" name="slug" value="" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								@if ($errors->has('slug'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                @endif
                                <div class="form-group row"><label for="image" class="col-md-4 col-form-label text-md-right">Image</label>
                                    <div class="col-md-6"><input id="image" name="image" value="" required="required" autofocus="autofocus" class="form-control" type="file"></div>
                                </div>
                                @if ($errors->has('image'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif

                                 <div class="form-group row"><label for="is_featured" class="col-md-4 col-form-label text-md-right">Is Featured</label>
                                    <div class="col-md-6"><input value="1" type="checkbox" id="isFeatured" name="is_featured"></div> 
                                </div>
								
								<div class="form-group row mb-0">
									<div class="col-md-6 offset-md-4">
										<button type="submit" class="btn btn-primary">
											Add Category
										</button>
									</div>
								</div>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
@stop

