@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
<?php 

?>
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Artist</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-md-3">
											<strong class="card-title">Edit Artist</strong>
										</div>
										<div class="col-md-9">
											<a href="{{ url('admin/artists') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Artist List</a>
										</div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>


        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">
					@if(session()->has('message'))
					<script type="text/javascript">
					jQuery(document).ready(function($){
					        $.notify({
					            icon: "nc-icon nc-app",
					            message: '{{ session()->get('message') }}',
					        }, {
					            type: 'success',
					            timer: 8000,
					        });
					 });
					 </script>
					@endif
						<div class="card"> 
							<!-- <div class="card-header border-header">
								<strong class="card-title">Edit Artist</strong>
								<a href="{{ url('admin/artists') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Artist List</a>
							</div> -->
							<div class="card-body row">
								 @if ($errors->any())
									  @foreach ($errors->all() as $error)
											<script type="text/javascript">
											jQuery(document).ready(function($){
											        $.notify({
											            icon: "nc-icon nc-app",
											            message: '{{ $error }}',
											        }, {
											            type: 'danger',
											            timer: 8000,
											        });												
											});
											 </script>
									  @endforeach
								  @endif
								  <div class="col-md-8">
										<form method="POST" action="<?php echo URL::to('/admin/artist/editdata'); ?>" class="artist_edit_form"  enctype="multipart/form-data">
										 {{ csrf_field() }}
											<div class="form-group row"><label for="name" class="col-md-2 col-form-label">Name</label>
												<div class="col-md-10"><input id="name" name="name" required="required" autofocus="autofocus" class="form-control" value="{{$artist->name}}" type="text"></div>
											</div>
											@if ($errors->has('name'))
												<span class="invalid-feedback">
													<strong>{{ $errors->first('name') }}</strong>
												</span>
											@endif
											<div class="form-group row"><label for="bio" class="col-md-2 col-form-label">Bio</label>
												<div class="col-md-10"><textarea rows="4" cols="50" id="bio" name="bio" class="form-control txtbio">{{$artist->bio}}</textarea></div> 
											</div>
											<div class="form-group row"><label for="image" class="col-md-2 col-form-label">Image</label>
												<div class="col-md-10"><input id="image" name="image" class="form-control" value="" type="file"></div>
											</div>
											@if ($errors->has('image'))
												<span class="invalid-feedback">
													<strong>{{ $errors->first('image') }}</strong>
												</span>
											@endif
											
											<div class="form-group row"><label for="dob" class="col-md-2 col-form-label">Date of Birth</label>
												<div class="col-md-10"><input type="text" id="dob" name="dob" value="{{$artist->dob}}" autocomplete="off" required></div>
											</div>
											@if ($errors->has('dob'))
			                                    <span class="invalid-feedback">
			                                        <strong>{{ $errors->first('dob') }}</strong>
			                                    </span>
			                                @endif
			                                <div class="form-group row"><label for="dob" class="col-md-2 col-form-label">Is Featured</label>
												<div class="col-md-10"><input value="1" type="checkbox" id="isFeatured" name="isFeatured" {{$artist->is_featured ?'checked' : ''}}></div> 
											</div>
			                                <div class="form-group row"><label for="name" class="col-md-2 col-form-label">Tags</label>
			                                	<?php 
		                                			$tags = array();
		                                			foreach ($artist->tags as $tag) {
														$tags[] = $tag->name;
													}
		                                		?>
												<div class="col-md-10"><input id="artist_tags" name="artist_tags" value="<?php echo implode(',', $tags); ?>" required="required" placeholder="Enter comma(,) separated Tags" autofocus="autofocus" class="form-control mb-2" type="text">
												<!-- @foreach($artist->tags as $tag) -->
			                                	<!-- <div class="d-inline-block bg-info pl-2 pr-2 mb-2 text-light">{{$tag->name}}</div> -->
			                                	<!-- @endforeach -->
												</div>
											</div>
											<div class="form-group row"><label for="name" class="col-md-2 col-form-label">Country</label>
												<div class="col-md-10">
													<select class="form-control" name="country" value="">
														<option value="">Please select Country</option>
														<?php
															foreach ($countries as $single) { ?>
																<option value="<?php echo $single->name; ?>" <?php echo ($artist->country == $single->name) ? 'selected' : ''; ?>><?php echo $single->name; ?></option>
														<?php 	}
														?>
													</select>
												</div>
											</div>
											<div class="form-group row mb-0 user_form_submit">
												<div class="col-md-6 offset-md-4">
													<input type="hidden" name="id" value="{{$artist->id}}" />
													<a class="btn btn-success update_artist_btn text-white">Update</a>
												</div>
											</div>
										</form>
									</div>
									<div class="col-md-4">
										<div class="card">
				                            <div class="card-image">
				                                <img src="{{$artist->image}}" alt="..." class="artist_image">
				                            </div>
				                            <div class="card-body">
				                            		<h3 class="title">{{$artist->name}}</h3>
			                                        @foreach($artist->tags as $tag)
				                                	<div class="d-inline-block bg-info pl-2 pr-2 mb-2 text-light">{{$tag->name}}</div>
				                                	@endforeach
				                            </div>
				                            <hr>
				                        </div>
									</div>
							</div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
@stop

