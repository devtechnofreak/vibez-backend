@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
        <!-- <div class="breadcrumbs">
            <div class="col-sm-12">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Artist</h1>
                    </div>
                    
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <style type="text/css">
        	.edbtn
        	{
        		    border-color: #FFF;
    				color: #FFF;
        	}
        </style>
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-md-3">
										<strong class="card-title artist-name">{{ucfirst($artist->name)}}</strong>
									</div>
									<div class="col-md-9">
										<a href="{{ url('admin/artists') }}" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Artist List</a>
										<a href="{{url('admin/album/add/'.$artist->id)}}" class="btn btn-primary float-right ml-2"><i class="fa fa-plus"></i> Add Album</a>
		              					<a href="{{url('admin/artist/edit/'.$artist->id)}}" class="btn btn-primary float-right ml-2"><i class="fa fa-edit"></i> Edit Artist</a>
		              					<a href="{{ url('admin/album/import/bulk/'.$artist->id) }}" class="btn btn-success float-right ml-2"><strong class="card-title"><i class="fa fa-plus"></i> Bulk Import Albums</strong></a>
									</div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>

        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-12 col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif
					 @if ($errors->any())
						  <div class="alert alert-danger">
							  <ul>
								  @foreach ($errors->all() as $error)
									  <li>{{ $error }}</li>
								  @endforeach
							  </ul>
						  </div><br/>
					  @endif
						<div class="card"> 
							<!-- <div class="card-header border-header">
								<div class="row">
									<div class="col-md-3">
										<strong class="card-title artist-name">Artist Name : {{ucfirst($artist->name)}}</strong>
									</div>
									<div class="col-md-9">
										<a href="{{ url('admin/artists') }}" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Artist List</a>
										<a href="{{url('admin/album/add/'.$artist->id)}}" class="btn btn-primary float-right ml-2"><i class="fa fa-plus"></i> Add Album</a>
		              					<a href="{{url('admin/artist/edit/'.$artist->id)}}" class="btn btn-primary float-right ml-2"><i class="fa fa-edit"></i> Edit Artist</a>
		              					<a href="{{ url('admin/album/import/bulk/'.$artist->id) }}" class="btn btn-success float-right ml-2"><strong class="card-title"><i class="fa fa-plus"></i> Bulk Import Albums</strong></a>
									</div>
								</div>
								
								
							</div> -->
							<div class="card-body bg-light tbalbum">
								<div class="row">
								<div class="col-md-2">
									<img src="{{$artist->image}}" alt="{{$artist->name}}" class="img-thumbnail img-fluid img-height"/>
								</div>
								<div class="col-md-10">
					                <table class="table table-user-information table-striped">
					                    <tbody>
					                      <!-- <tr>
					                        <td class="col-1">Name:</td>
					                        <td>{{$artist->name}}</td>
					                      </tr> -->
					                      <tr>
					                        <td class="col-1">Bio:</td>
					                        <td>{{$artist->bio}}</td>
					                      </tr>
					                      <tr>
					                        <td class="col-1">Date of Birth:</td>
					                        <td>{{date('F j, Y',strtotime($artist->dob))}}</td>
					                      </tr>
			                              <tr class="col-1">
					                        <td>Tags:</td>
					                        
					                        <td>
					                        	@foreach($artist->tags as $tag)
					                        	<div class="d-inline-block bg-dark pl-2 pr-2 mb-2 text-light">{{$tag->name}}</div>
					                        	@endforeach
			                                	
					                       </td>
					                      </tr>
					                    </tbody>
					                  </table>
								</div>
								</div>
							</div>
						</div>
						<div class="card"> 
							<div class="card-header border-header album-listback">
								<strong class="card-title">{{'Album List'}}</strong>
							</div>
							<div class="container-fluid card-body">
								@foreach($albums as $album)
								<div class="card album-list"> 
										<div class="card-header text-dark album-listborder">
											<span class="album-listheader"><strong class="card-title">{{ucfirst($album->title)}}</strong></span>
											<a href="{{url('admin/album/edit/'.$album->id)}}" class="btn btn-secondary float-right edbtn"><i class="fa fa-edit"></i> Edit</a>
										</div>
										<div class="container-fluid cardbody-padding">
											<div class="row">
												<div class="col-md-3 img-padding">
													<img src="{{$album->image}}" alt="{{$album->title}}" class="img-thumbnail rounded img-fluid"/>
												</div>
												<div class="col-md-9">
									                <table class="table table-user-information table-condensed">
									                    <tbody>
									                      <!-- <tr>
									                        <td class="col-1">Title:</td>
									                        <td>{{$album->title}}</td>
									                      </tr> -->
									                      <tr>
									                      	<td class="col-12 vertical-td"><span class=""><b>Description : </b><span>{{str_limit(strip_tags($album->description), $limit = 160, $end = '... ')}}<br>
									                      		<?php
									                      		$string = strip_tags($album->description);

																if (strlen($string) > 160) {
																	echo '<a href="#myModal" class="readmore" data-toggle="modal" data-id="'. $album->id .'" data-target="#myModal'. $album->id .'"> Read More  →</a>';
																}
																?>
									                      		

									                      	</span></span></td>	
									                      	
									                        
									                        <!-- <td>{{$album->description}}</td> -->
									                        <div class="modal myModal" id="myModal{{ $album->id }}">
															    <div class="modal-dialog">
															      	<div class="modal-content">
															      		
																        <div class="modal-header">
																          <h4 class="modal-title">Artist Name : {{ucfirst($album->title)}}</h4>
																          <button type="button" class="close" data-dismiss="modal">&times;</button>
																        </div>
															        
																      
																        <div class="modal-body edit-content">
																          <b>	Description : </b>{{ $album->description }}
																        </div>
																      
																        <div class="modal-footer">
																          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
																        </div>
																      
															      	</div>
															    </div>
															</div>

									                      </tr>
							                              <!-- <tr>
									                        <td class="col-1">Tracks:</td>
									                        <td>
									                      	@foreach($album->musics as $music)
									                        	<div class="d-inline-block bg-info pl-2 pr-2 mb-2 text-light">{{$music->title}}</div>
								                        	@endforeach
									                       </td>
									                      </tr> -->
									                    </tbody>
									                  </table>
												</div>
											</div>
										</div>
										<div class="container-fluid card-body">
											<div class="row">
												<div class="col-md-12 col-padding">Tracks : @foreach($album->musics as $music)
										                        	<span class="bg-info pl-2 pr-2 mb-2 text-light">{{$music->title}}</span>
									                    @endforeach</div>
												<!-- <div class="col-md-10">
														
												</div> -->
											</div>
										</div>
								</div>
								@endforeach
								<div class="text-center col-12 col-md-12">
								{{ $albums->links() }}
								</div>
							</div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
@stop

