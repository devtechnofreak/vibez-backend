@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Artists</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif
                    <div class="card"> 
                        <div class="card-header">
                            <strong class="card-title">Import Artists</strong>
                            <a href="{{ url('admin/artists') }}" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Back</a>
                            <a href="{{ url('sample/import_artists.csv') }}" class="btn btn-success float-right ml-2"><i class="fa fa-download"></i> Download Sample Csv</a>
                            @if(isset($csv))
                            <a href="{{ url('admin/artist/import/bulk') }}" class="btn btn-success float-right ml-2"><i class="fa fa-refresh"></i></a>
                            @endif
                        </div>
						<div class="card-body">
							 @if ($errors->any())
								  <div class="alert alert-danger">
									  <ul>
										  @foreach ($errors->all() as $error)
											  <li>{{ $error }}</li>
										  @endforeach
									  </ul>
								  </div><br/>
							  @endif
							<form method="POST" action="{{ url('admin/artist/import/bulk')}}" enctype="multipart/form-data" class="form-inline">
							 {{ csrf_field() }}
							@if(isset($csv))
							<table class="table">
							  <thead>
							    <tr>
							      <th scope="col">#</th>
							      <th scope="col">Name</th>
							      <th scope="col">Bio</th>
							      <th scope="col">Is Featured</th>
							      <th scope="col">Date of Birth</th>
							      <th scope="col">Tags</th>
							      <th scope="col">Image</th>
							      <th scope="col">Country</th>
							    </tr>
							  </thead>
							  <tbody>
							@foreach($csv as $key => $data)
							    <tr>
							      <th scope="row">{{$key+1}}</th>
							      <td><input name="artist[{{$key}}][name]" required="required" autofocus="autofocus" class="form-control" value="{{$data['name']}}" type="text"></td>
							      <td><textarea rows="8" cols="20" name="artist[{{$key}}][bio]" class="form-control">{{$data['bio']}}</textarea></td>
							      <td><input value="1" type="checkbox" name="artist[{{$key}}][isFeatured]" {{$data['is_featured'] ?'checked' : ''}}></td>
							      <td><input class="release_date" type="text" name="artist[{{$key}}][dob]" value="{{$data['dob']}}" autocomplete="off"></td>
							      <td><input name="artist[{{$key}}][artist_tags]" value="{{$data['tags']}}" placeholder="Enter comma(,) separated Tags" autofocus="autofocus" class="form-control mb-2" type="text"></td>
							      <td><input name="artist[{{$key}}][image]" class="form-control" value="" type="file"/></td>
							      <td>
							      	<select class="form-control" style="width: 200px" name="artist[{{$key}}][country]" value="">
										<option value="">Please select Country</option>
										<?php
											foreach ($countries as $single) { ?>
												<option value="<?php echo $single->name; ?>" <?php echo ($data['country'] == $single->name) ? 'selected' : ''; ?>><?php echo $single->name; ?></option>
										<?php 	}
										?>
									</select>
							      	</td>
							    </tr>
							@endforeach
							  </tbody>
							</table>
							@else
							  <div class="form-group mx-sm-3 mb-2">
							    <label for="inputfile" class="sr-only">Password</label>
							    <input type="file" class="form-control-file" id="inputfile" name="csv_import" placeholder="Csv File" required>
							  </div>
							  @if ($errors->has('name'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('csv_import') }}</strong>
								</span>
							  @endif
							@endif
							  <button type="submit" class="btn btn-primary mb-2">Import</button>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
@stop

