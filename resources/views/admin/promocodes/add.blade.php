@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->

        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>News</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="breadcrumbs navbar-top">
                        <div class="card">
                            <div class="card-header border-header">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <strong class="card-title newslist-name"><i class="fa fa-plus"></i> Add Promocode</strong>
                                        </div>
                                        <div class="col-md-9">
                                            <a href="{{ url('admin/promocode') }}" class="btn btn-primary float-right backbtn"><i class="fa fa-arrow-left"></i> Promocodes List</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>


        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif
                    <div class="card"> 
                        <!-- <div class="card-header border-header">
                            <strong class="card-title newslist-name">Add News</strong>
                            <a href="{{ url('admin/news') }}" class="btn btn-primary float-right backbtn"><i class="fa fa-arrow-left"></i> News List</a>
                        </div> -->
						<div class="card-body">
							 @if ($errors->any())
								  <div class="alert alert-danger">
									  <ul>
										  @foreach ($errors->all() as $error)
											  <li>{{ $error }}</li>
										  @endforeach
									  </ul>
								  </div><br/>
							  @endif
							<form method="POST" action="<?php echo URL::to('/admin/promocode/create'); ?>" enctype="multipart/form-data" class="add_news">
							 	{{ csrf_field() }}
							 	<div class="form-group row"><label for="artist" class="col-md-4 col-form-label">Artist</label>
									<div class="col-md-6">
										<select class="js-example-basic-multiple form-control" name="artist_id" value="" required>
											<option value="">Select Artist</option>
											@foreach($artists as $key => $artist)
											  <option name="artist_id" value="{{ $artist->id }}">{{ $artist->name }}</option>
											@endforeach
										</select>
									</div> 
								</div>
								<div class="form-group row"><label for="title" class="col-md-4 col-form-label">Code</label>
									<div class="col-md-6"><input id="code" name="code" value="" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								<div class="form-group row"><label for="start_time" class="col-md-4 col-form-label">Start Date</label>
									<div class="col-md-6"><input type="text" class="form-control datepicker" name="start_date" autocomplete="off" required></div>
								</div>
								<div class="form-group row"><label for="start_time" class="col-md-4 col-form-label">End Date</label>
									<div class="col-md-6"><input type="text" class="form-control datepicker" name="end_date" autocomplete="off"></div>
								</div>
								<div class="form-group row mb-0">
									<div class="col-md-6 offset-md-4">
										<a class="btn btn-primary add_news_btn">Add Promocode</a>
									</div>
								</div>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
		<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/plugins/select2/select2.min.css') }}">
		<!-- <script src="{{ asset('admin_assets/js/vendor/jquery-2.1.4.min.js') }}"></script> -->
		<script src="{{ asset('admin_assets/plugins/select2/select2.min.js') }}"></script>
		<script type="text/javascript">
		    $(document).ready(function() {
			    $('.js-example-basic-multiple').select2({
			    	placeholder: "Select a item",
		    		allowClear: true
			    });
			    $('.datepicker').datepicker({
					dateFormat: "dd-mm-yy",
					changeYear: true,
					changeMonth: true
				});
			}); 
		</script>
@endSection