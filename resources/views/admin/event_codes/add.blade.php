@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Event Codes</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif
                    <div class="card"> 
                        <div class="card-header">
                            <strong class="card-title">Import Event Codes</strong>
                            <strong class="text-success ml-4">
                            	
                            @if(isset($artist_id))
                            {{'Artist : '.App\Artist::find($artist_id)->name}}
                            @endif	
                            </strong>
                            <a href="{{ url('admin/eventcodes') }}" class="btn btn-primary float-right  ml-2"><i class="fa fa-arrow-left"></i> Back</a>
                            <a href="{{ url('sample/import_eventcodes.csv') }}" class="btn btn-success  float-right ml-2"><i class="fa fa-download"></i> Download Sample Csv</a>
                            @if(isset($csv))
                            @if(isset($artist_id))
                            <a href="{{ url('admin/eventcodes/add'.$artist_id) }}" class="btn btn-success  float-right ml-2"><i class="fa fa-refresh"></i></a>
                            @else
                            <a href="{{ url('admin/eventcodes/add') }}" class="btn btn-success  float-right ml-2"><i class="fa fa-refresh"></i></a>
                            @endif
                            @endif
                        </div>
						<div class="card-body">
							 @if ($errors->any())
								  <div class="alert alert-danger">
									  <ul>
										  @foreach ($errors->all() as $error)
											  <li>{{ $error }}</li>
										  @endforeach
									  </ul>
								  </div><br/>
							  @endif
							<form method="POST" action="{{ url('admin/eventcodes/add')}}" enctype="multipart/form-data" class="form-inline">
							 {{ csrf_field() }}
							@if(isset($csv))
							<table class="eventcodes_csv" style="width : 100%;">
							  <thead>
							    <tr>
							      <th style="width : 30%;">#</th>
							      <th style="width : 70%;">Codes</th>
							    </tr>
							  </thead>
							  <tbody>
							@foreach($csv as $key => $data)
							    <tr>
							      <td>{{$key+1}}</td>
							      <td><input name="eventcodes[{{$key}}][codes]" required="required" autofocus="autofocus" class="form-control" value="{{$data['codes']}}" type="text"></td>
							    </tr>
							@endforeach
							  </tbody>
							</table>
							@else
							  <div class="form-group mx-sm-3 mb-2">
							    <input type="file" class="form-control-file" id="inputfile" name="csv_import" placeholder="Csv File">
							  </div>
							  @if ($errors->has('name'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('csv_import') }}</strong>
								</span>
							  @endif
							@endif
							  <button type="submit" class="btn btn-primary mb-2">Import</button>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
@stop
<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/plugins/select2/select2.min.css') }}">
<script src="{{ asset('admin_assets/js/vendor/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('admin_assets/plugins/select2/select2.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
	    $('.js-example-basic-multiple').select2();
	});
</script>