@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
		
        <!-- <div class="breadcrumbs">
            <div class="col-sm-12">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Albums</h1>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-4">
			                        		<strong class="card-title artist-name">Announcement List</strong>
			                        	</div>
			                            <div class="col-sm-8">	
			                            	<a class="btn btn-primary float-right ml-2" href="{{url('admin/announcement/add')}}"><strong class="card-title"><i class="fa fa-plus"></i> Add New</strong></a>
			                            </div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>
        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                       <!--  <div class="card-header border-header">
                            <strong class="card-title artist-name">Album List</strong>
                            <div class="float-right">
                            	<a class="btn btn-primary float-right ml-2" href="{{url('admin/album/add')}}"><strong class="card-title"><i class="fa fa-plus"></i> Add New</strong></a>
                            	<a href="{{ url('admin/album/import/bulk') }}" class="btn btn-success float-right ml-2"><strong class="card-title"><i class="fa fa-plus"></i> Bulk Import Albums</strong></a>
                            	<a href="{{ url('admin/album/trash')}}" class="btn btn-secondary float-right ml-2"><strong class="card-title"><i class="fa fa-trash"></i> View Trash ({{App\Album::onlyTrashed()->count()}})</strong></a>
                            </div>
                        </div> -->
                        <div class="card-body">
							@if(session()->has('message'))
								<div class="alert alert-success">
									{{ session()->get('message') }}
								</div>
							@endif
							 <div class="video_bulk_action">
	                        	<label>Bulk Action 
	                        		<select name="bulk_action" class="form-control form-control-sm">
	                        			<option value="">Select action</option>
	                        			<option value="permenentt">Permanently delete</option>
	                        		</select>
	                        	</label>
	                        	<a class="apply_action">Apply</a>
	                        </div>
							<table id="announcement_list" class="table table-striped table-bordered albumtb" style="width:100%">
								<thead>
									<tr>
										<th><input type="checkbox" class="deleteall delete_row"></th>
										<th>Title</th>
										<th>Text</th>
										<th>Image</th>
										<th>Date</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th><input type="checkbox" class="deleteall delete_row"></th>
										<th>Title</th>
										<th>Text</th>
										<th>Image</th>
										<th>Date</th>
									</tr>
								</tfoot>
							</table>
                        </div>
                    </div>
                </div>
				<input type="hidden" class="csrf_token" value="{{ csrf_token() }}">
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
<script src="{{ asset('admin_assets/js/lib/data-table/datatables.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/jszip.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/vfs_fonts.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/lib/data-table/datatables-init.js') }}"></script>


<script type="text/javascript">
	$(document).ready(function() {
		$('.deleteall').click(function(){
			$('#announcement_list tbody .delete_row').each(function(){
				if($(this).is(':checked')){
					$(this).prop('checked',false);	
				}else{
					$(this).prop('checked',true);
				}
				
			})
		});
		$('.apply_action').click(function(){
			var action = "";
			
			if($('.video_bulk_action select').val() == "permenentt"){
				var r = confirm("Are you sure want to permenently delete selected announcement!");
			    if (r == true) {
			        action = "delete";
			    } else {
			     return false
			    }
			}
			if(action != ""){
				var announcement = [];
				$('#announcement_list').find('.delete_bulk').each(function(){ 
					if($(this).is(':checked')){
						announcement.push($(this).val());
					}
				});
				var csrf_token = jQuery(document).find('.csrf_token').val();
				$.ajax({
		       	    beforeSend: function(xhrObj){
			                xhrObj.setRequestHeader("X-CSRF-TOKEN",csrf_token); 
			        },
				   url: site_url+"/admin/announcement/bulk/action",
				   data: {action	: action,announcement : announcement},
				   success: function(data) {
				   	window.location.reload();
				   },
				   type: 'POST'
				});
			}
		})
		var csrf_token = jQuery(document).find('.csrf_token').val();
		$('#announcement_list').DataTable( {
			"processing": true,
			"serverSide": true,
			"bAutoWidth": false,
			"pageLength": 50,
			"ajax": {
				"url": "announcement/getaannouncement",
				"type": "POST",
				"dataType": 'json',
                "error": function(data){
                        console.log(data);
                    },
				"headers" : {
						'X-CSRF-TOKEN': csrf_token
				}
			},
			"columns": [
				{ "data": "id", "orderable": false,    render: function ( data, type, row ) {
						return '<input type="checkbox" class="delete_bulk delete_row" value="'+data+'">';
					return data;
				} },
				{ "data": "title" ,"width" : "16%"},
				{ "data": "content" ,"width" : "50%"},	
				{ "data": "image" ,     render: function ( data, type, row ) {
						var image_url = data;
						return '<img src="'+image_url+'" class="img-thumbnail" />';
					return data;
				},"width": "8%" },
				{ "data": "created_at"}
			]
		} );
		
		
    } );
</script>	
@stop

