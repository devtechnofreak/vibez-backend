@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->

        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>News</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="breadcrumbs navbar-top">
                        <div class="card">
                            <div class="card-header border-header">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <strong class="card-title newslist-name"><i class="fa fa-plus"></i> Add Announcement</strong>
                                        </div>
                                        <div class="col-md-9">
                                            <a href="{{ url('admin/announcement') }}" class="btn btn-primary float-right backbtn"><i class="fa fa-arrow-left"></i> Announcement List</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>


        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif
                    <div class="card"> 
                        <!-- <div class="card-header border-header">
                            <strong class="card-title newslist-name">Add News</strong>
                            <a href="{{ url('admin/news') }}" class="btn btn-primary float-right backbtn"><i class="fa fa-arrow-left"></i> News List</a>
                        </div> -->
						<div class="card-body">
							 @if ($errors->any())
								  <div class="alert alert-danger">
									  <ul>
										  @foreach ($errors->all() as $error)
											  <li>{{ $error }}</li>
										  @endforeach
									  </ul>
								  </div><br/>
							  @endif
							<form method="POST" action="<?php echo URL::to('/admin/announcement/create'); ?>" enctype="multipart/form-data" class="add_news">
							 	{{ csrf_field() }}
								<div class="form-group row"><label for="title" class="col-md-4 col-form-label">Title</label>
									<div class="col-md-6"><input id="title" name="title" value="" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								@if ($errors->has('title'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
								<div class="form-group row"><label for="image" class="col-md-4 col-form-label">Image</label>
									<div class="col-md-6"><input id="image" name="image" value="" required="required" autofocus="autofocus" class="form-control" type="file"></div>
								</div>
								@if ($errors->has('image'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                                
                                <div class="form-group row"><label for="content" class="col-md-4 col-form-label">Content</label>
									<div class="col-md-6"><textarea id="content" name="content" value="" required="required" autofocus="autofocus" class="form-control" maxlength="250" style="height:120px !important;"></textarea></div>
								</div>
								
								<div class="form-group row mb-0">
									<div class="col-md-6 offset-md-4">
										<a class="btn btn-primary add_news_btn">Add Announcement</a>
									</div>
								</div>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
@endSection