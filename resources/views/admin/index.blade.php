@extends('layouts.admin.layout')
@section('main_content')
<!-- Header-->
<?php $current_user_id = Auth::user()->id; ?>
<div class="content mt-3 dashboard_admin">
<div class="container-fluid">
    <?php if($current_user_id == '1902')
    {  }else { ?>
    <div class="row">
        <div class="col-md-2">
        	<div class="small-box bg-info">
              <div class="inner">
                <h3><?php echo $data['total_album'] ?></h3>

                <p>Total Albums</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-albums"></i>
              </div>

              <a href="{{url('admin/albums')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-md-2">
        	<div class="small-box bg-success">
              <div class="inner">
                <h3><?php echo $data['total_artist'] ?></h3>

                <p>Total Artists</p>
              </div>
              <div class="icon">
                <i class="ion ion-person"></i>
              </div>
              <a href="{{url('admin/artists')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-md-2">
        	<div class="small-box bg-warning">
              <div class="inner">
                <h3><?php echo $data['total_playlist'] ?></h3>

                <p>Total Playlists</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-albums"></i>
              </div>
              <a href="{{url('admin/play-lists')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-md-2">
        	<div class="small-box bg-danger">
              <div class="inner">
                <h3><?php echo $data['total_video'] ?></h3>

                <p>Total Videos</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-videocam"></i>
              </div>
              <a href="{{url('admin/videos')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="small-box bg-danger">
              <div class="inner">
                <h3><?php echo $data['total_music'] ?></h3>

                <p>Total Musics</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-musical-notes"></i>
              </div>
              <a href="{{url('admin/musics')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="small-box bg-warning">
              <div class="inner">
                <h3><?php echo $data['total_news'] ?></h3>

                <p>Total News</p>
              </div>
              <div class="icon">
                <i class="ion ion-laptop"></i>
              </div>
              <a href="{{url('admin/news')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>

    <?php } 
    if($current_user_id == '1902')
    {  } else { ?>
    <div class="row">
        <div class="col-md-3">
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?php echo $data['total_user'] ?></h3>
                <p><b>Total User</b></p>
                <p><?php echo $data['total_active_user'] ?>&nbsp;Active</p>
                <p><?php echo $data['total_expired_user'] ?>&nbsp;Expired</p>
              </div>
              <div class="icon">
                <i class="ion ion-person"></i>
              </div>
              <a  class="small-box-footer"> &nbsp;</a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="small-box bg-success">
              <div class="inner">
                <h3><?php echo $data['trialPlanAllUser'] ?></h3>
                <p><b>Trial subscriber</b></p>
                <p><?php echo $data['trialPlanActiveUser'] ?>&nbsp;Active</p>
                <p><?php echo $data['trialPlanExipredUser'] ?>&nbsp;Expired</p>

              </div>
              <div class="icon">
                <i class="ion ion-person"></i>
              </div>
              <a  class="small-box-footer"> &nbsp;</a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="small-box bg-warning">
              <div class="inner">
                <h3><?php echo $data['monthPlanAllUser'] ?></h3>
                <p><b>Monthly subscriber</b></p>
                <p><?php echo $data['monthPlanActiveUser'] ?>&nbsp;Active</p>
                <p><?php echo $data['monthPlanExpiredUser'] ?>&nbsp;Expired</p>
              </div>
              <div class="icon">
                <i class="ion ion-person"></i>
              </div>
              <a  class="small-box-footer"> &nbsp;</a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?php echo $data['yearPlanAllUser'] ?></h3>
                <p><b>Yearly subscriber</b></p>
                <p><?php echo $data['yearPlanActiveUser'] ?>&nbsp;Active</p>
                <p><?php echo $data['yearPlanExpiredUser'] ?>&nbsp;Expired</p>
              </div>
              <div class="icon">
                <i class="ion ion-person"></i>
              </div>
              <a  class="small-box-footer"> &nbsp;</a>
            </div>
        </div>
        
    </div>
    <?php } ?>
    <div class="row">
        <div class="col-md-6 dashboard_customer_request">
        	<div class="card">
        		<div class="card-header">
        			<h4 class="card-title">Customer Request</h4>
        		</div>
        		<div class="card-body">
        			<div class="table-full-width">
        				<table class="table">
        					<thead>
        						<tr>
        							<th>Image</th>
        							<th>Name</th>
        							<th>Request</th>
        							<th>Date</th>
        						</tr>
        					</thead>
    						<tbody>
    							<?php 
    								foreach ($data['user_help'] as $single_help) { ?>
    									<tr>
    										<td><img src="<?php echo $single_help->image; ?>"></td>
    										<td><?php echo $single_help->name; ?></td>
    										<td><?php echo strlen($single_help->help) > 40 ? substr($single_help->help,0,40)."..." : $single_help->help; ?></td>
    										<td><?php echo date('d / m / Y',strtotime($single_help->created_at)); ?></td>
    									</tr>
								<?php } ?> 
    						</tbody>
        				</table>
        				<p class="pull-right">
        					<button data-url="" class="btn btn-success btn-sm ad-click-event more_click">More <i class="fa fa-arrow-circle-right"></i></button>
        				</p>
        			</div>
        		</div>
        		
        	</div>
        </div>
        <div class="col-md-6 dashboard_albums">
        	<div class="card">
        		<div class="card-header">
        			<h4 class="card-title">Latest Albums</h4>
        		</div>
        		<div class="card-body">
        			<div class="table-full-width">
        				<table class="table">
        					<thead>
        						<tr>
        							<th>Image</th>
        							<th>Title</th>
        							<th>Total Songs</th>
        							<th>Date</th>
        						</tr>
        					</thead>
    						<tbody>
    							<?php 
    								foreach ($data['latest_album'] as $single_album) { ?>
    									<tr>
    										<td><img src="<?php echo $single_album->thumb; ?>"></td>
    										<td><?php echo $single_album->title; ?></td>
    										<td><?php echo $single_album->musics->count(); ?></td>
    										<td><?php echo date('d / m / Y',strtotime($single_album->created_at)); ?></td>
    									</tr>
								<?php } ?>
    						</tbody>
        				</table>
        				<p class="pull-right">
        					<button data-url="" class="btn btn-success btn-sm ad-click-event more_click">More <i class="fa fa-arrow-circle-right"></i></button>
        				</p>
        			</div>
        		</div>
        		
        	</div>
        </div>
        <div class="col-md-6 dashboard_videos">
    		<div class="card">
        		<div class="card-header">
        			<h4 class="card-title">Latest Vidoes</h4>
        		</div>
        		<div class="card-body">
        			<div class="table-full-width">
        				<table class="table">
        					<thead>
        						<tr>
        							<th>Image</th>
        							<th>Title</th>
        							<th>Category</th>
        							<th>Date</th>
        						</tr>
        					</thead>
    						<tbody>
    							<?php 
    								foreach ($data['latest_videos'] as $single_video) { ?>
    									<tr>
    										<td><img src="<?php echo $single_video->thumb; ?>"></td>
    										<td><?php echo $single_video->title; ?></td>
    										<td><?php echo $single_video->category->name; ?></td>
    										<td><?php echo date('d / m / Y',strtotime($single_video->created_at)); ?></td>
    									</tr>
								<?php } ?>
    						</tbody>
        				</table>
        				<p class="pull-right">
        					<button data-url="" class="btn btn-success btn-sm ad-click-event more_click">More <i class="fa fa-arrow-circle-right"></i></button>
        				</p>
        			</div>
        		</div>
        		
        	</div>    	
        </div>
        <div class="col-md-6 dashboard_musics">
        	<div class="card">
        		<div class="card-header">
        			<h4 class="card-title">Recently created Musics</h4>
        		</div>
        		<div class="card-body">
        			<div class="table-full-width">
        				<table class="table">
        					<thead>
        						<tr>
        							<th>Image</th>
        							<th>Title</th>
        							<th>Date</th>
        						</tr>
        					</thead>
    						<tbody>
    							<?php 
    								foreach ($data['latest_musics'] as $single_music) { ?>
    									<tr>
    										<td><img src="<?php echo $single_music->thumb; ?>"></td>
    										<td><?php echo $single_music->title; ?></td>
    										<td><?php echo date('d / m / Y',strtotime($single_music->created_at)); ?></td>
    									</tr>
								<?php } ?>
    						</tbody>
        				</table>
        				<p class="pull-right">
        					<button data-url="" class="btn btn-success btn-sm ad-click-event more_click">More <i class="fa fa-arrow-circle-right"></i></button>
        				</p>
        			</div>
        		</div>
        		
        	</div>
        </div>
        <div class="col-md-6 dashboard_musics">
        	<div class="card">
        		<div class="card-header">
        			<h4 class="card-title">Top Artists</h4>
        		</div>
        		<div class="card-body">
        			<div class="table-full-width">
        				<table class="table">
        					<thead>
        						<tr>
        							<th>Image</th>
        							<th>Name</th>
        							<th>Follow Count</th>
        						</tr>
        					</thead>
    						<tbody>
    							<?php 
                                    $aws_url = env('AWS_URL');
    								foreach ($data['latest_artists'] as $single_artist) { ?>
    									<tr>
    										<td><img src="<?php echo $aws_url.'Artists/'.$single_artist->image; ?>"></td>
    										<td><?php echo $single_artist->name; ?></td>
    										<td><?php echo $single_artist->total_count ?></td>
    									</tr>
								<?php } ?>
    						</tbody>
        				</table>
        			</div>
        		</div>
        		
        	</div>
        </div>
        <div class="col-md-6 dashboard_musics">
        	<div class="card">
        		<div class="card-header">
        			<h4 class="card-title">Promocode Usage</h4> 
        		</div>
        		<div class="card-body">
        			
        		</div>
        		
        	</div>
        </div>
    </div>
</div>
</div>
@stop