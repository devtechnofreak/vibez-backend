<html>
	<head>
		<title>Manual Payment Confirmation</title>
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<style>
			body{
				font-family : sans-serif;
				padding: 0px;
				margin: 0px;
				background-color: #eee;
			}
			.form-control .value input[type="text"]{
			    border: 1px solid #ddd;
			    padding: 10px;
			    margin-top: 5px;
			    margin-bottom: 20px;
			    width: 100%;
			    font-size: 17px;
			}
			p{
				text-align: center;
				font-size: 16px;
			}
			form{
				width: 80%;
				margin: 0 auto;
				margin-top: 40px; 
				margin-bottom: 30px;
			}
			.lable{
				font-size: 17px;
			}
			.form-control .value input[type="email"]{
				border: 1px solid #ddd;
			    padding: 10px;
			    margin-top: 5px;
			    margin-bottom: 20px;
			    width: 100%;
			    font-size: 17px;
			}
			.form_submit input[type="submit"]{
				border: none;
			    background: #ea6071;
			    font-size: 16px;
			    padding: 10px 20px;
			    color: #fff;
			    width: 100%;
			    cursor: pointer;
			    border-radius: 5px;
			}
			.error_msg{
				color: red;
				margin-top: 10px;
				margin-bottom: 20px;
			}
			.success_msg{
				color: green;
				margin-top: 10px;
				margin-bottom: 20px;
			}
			h3{
				text-align: center;
			    padding-top: 10px;
			    margin: 0;
			    padding-bottom: 10px;
			    background-color: #ea6071;
			    color: #fff;
			}
		</style>
		<script>
			$(document).ready(function(){
				 
			})
		</script>
	</head>	
	<body>
		
		<div class="manual_payment_confirmation">
			<h3>Welcome <?php echo $user[0]['name']; ?></h3> 
			<p>Please choose payment options!</p>
			
			<
			
		</div>
	</body>
</html>