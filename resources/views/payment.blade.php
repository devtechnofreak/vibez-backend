<html>
	<head>
		<title>Manual Payment Confirmation</title>
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<style>
			body{
				font-family : sans-serif;
				padding: 0px;
				margin: 0px;
				background-color: #eee;
			}
			.form-control .value input[type="text"]{
			    border: 1px solid #ddd;
			    padding: 10px;
			    margin-top: 5px;
			    margin-bottom: 20px;
			    width: 100%;
			    font-size: 17px;
			}
			p{
				text-align: center;
				font-size: 16px;
			}
			form{
				width: 80%;
				margin: 0 auto;
				margin-top: 40px; 
				margin-bottom: 30px;
			}
			.lable{
				font-size: 17px;
			}
			.form-control .value input[type="email"]{
				border: 1px solid #ddd;
			    padding: 10px;
			    margin-top: 5px;
			    margin-bottom: 20px;
			    width: 100%;
			    font-size: 17px;
			}
			.form_submit input[type="submit"]{
				border: none;
			    background: #ea6071;
			    font-size: 16px;
			    padding: 10px 20px;
			    color: #fff;
			    width: 100%;
			    cursor: pointer;
			    border-radius: 5px;
			}
			.error_msg{
				color: red;
				margin-top: 10px;
				margin-bottom: 20px;
			}
			.success_msg{
				color: green;
				margin-top: 10px;
				margin-bottom: 20px;
			}
			h3{
				text-align: center;
			    padding-top: 10px;
			    margin: 0;
			    padding-bottom: 10px;
			    background-color: #ea6071;
			    color: #fff;
			}
			.backbutton{
				position: absolute;
			    left: 10px;
			    color: #fff;
			    text-decoration: none;
			    font-weight: normal;
			}
			.backbutton span{
				margin-right: 5px;
			}
		</style>
		<script>
			$(document).ready(function(){
				 $(".bank_account_number").keydown(function (e) {
			        // Allow: backspace, delete, tab, escape, enter and .
			        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
			             // Allow: Ctrl+A, Command+A
			            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
			             // Allow: home, end, left, right, down, up
			            (e.keyCode >= 35 && e.keyCode <= 40)) {
			                 // let it happen, don't do anything
			                 return;
			        }
			        // Ensure that it is a number and stop the keypress
			        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			            e.preventDefault();
			        }
			    });
			    $( ".bank_name" ).keypress(function(e) {
                    var key = e.keyCode;
                    if ((key >= 48 && key <= 57) || key == 8) {
                        e.preventDefault();
                    }
                });
			})
		  $( function() {
		    $( "#datepicker" ).datepicker({dateFormat: "yy-mm-dd"});
		  } );
		</script>
	</head>	
	<body>
		
		<div class="manual_payment_confirmation">
			<h3><a href="<?php echo $site_url; ?>/api/manual_payment/info/<?php echo $user[0]['email']; ?>" class="backbutton"><span><</span>Back</a>Welcome <?php echo $user[0]['name']; ?></h3> 
			<p>Please provide your confirmation with below information, so that we can verify the information and will update your membership after that!</p>
			<form method="post" action="{{url('api/manual_payment/submit')}}">
				<?php 
					session_start();
					if(isset($_SESSION['errors'])){
						$class="error_msg";
						if($_SESSION['status'] == 1){
							$class="success_msg";
						}
						echo '<p class="'.$class.'">'.$_SESSION['errors'].'</p>';
						unset($_SESSION['errors']);
						unset($_SESSION['status']);
					}
				?>
				<div class="form-control">
					<div class="label">Vibez Id : </div>
					<div class="value"><input type="text" name="user_name" value="<?php echo $user[0]['vibez_id']; ?>" autocomplete="off" required readonly></div>
				</div> 
				<div class="form-control">
					<div class="label">Email : </div>
					<div class="value"><input type="email" name="user_email" value="<?php echo $user[0]['email']; ?>" autocomplete="off" required readonly></div>
				</div> 
				<div class="form-control">
					<div class="label">Date Paid : </div>
					<div class="value"><input type="text" name="date_paid" id="datepicker" autocomplete="off" required readonly></div>
				</div>
				<div class="form-control">
					<div class="label">Bank Name : </div>
					<div class="value"><input type="text" name="bank_name" class="bank_name" value="" autocomplete="off" required></div>
				</div>
				<div class="form-control">
					<div class="label">Bank Account Number : </div>
					<div class="value"><input type="text" name="bank_account_number" class="bank_account_number" value="" autocomplete="off" required></div>
				</div>
				<div class="form-control">
					<div class="label">Unique Transaction Id : </div>
					<div class="value"><input type="text" name="transaction_id" class="transaction_id" value="" autocomplete="off" required></div>
				</div>
				<div class="form_submit">
					<input type="hidden" name="user_id" value="<?php echo $user[0]['id']; ?>">
					<input type="submit" name="submit" value="Submit">
				</div>
			</form> 
		</div>
	</body>
</html>