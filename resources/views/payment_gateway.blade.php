<html>
	<head>
		<title>Manual Payment Confirmation</title>
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<style>
			body{
				font-family : sans-serif;
				padding: 0px;
				margin: 0px;
			}
			.form-control .value input[type="text"]{
			    border: 1px solid #ddd;
			    padding: 10px;
			    margin-top: 5px;
			    margin-bottom: 20px;
			    width: 100%;
			    font-size: 17px;
			}
			p{
				text-align: center;
				font-size: 16px;
			}
			form{
				width: 90%;
				margin: 0 auto;
				margin-top: 40px; 
				margin-bottom: 30px;
			}
			.lable{
				font-size: 17px;
			}
			.form-control .value input[type="email"]{
				border: 1px solid #ddd;
			    padding: 10px;
			    margin-top: 5px;
			    margin-bottom: 20px;
			    width: 100%;
			    font-size: 17px;
			}
			.form_submit input[type="submit"]{
				border: none;
			    background: #ea6071;
			    font-size: 16px;
			    padding: 10px 20px;
			    color: #fff;
			    width: 100%;
			    cursor: pointer;
			    border-radius: 5px;
			}
			.error_msg{
				color: red;
				margin-top: 10px;
				margin-bottom: 20px;
			}
			.success_msg{
				color: green;
				margin-top: 10px;
				margin-bottom: 20px;
			}
			h3{
				text-align: center;
			    padding-top: 10px;
			    margin: 0;
			    padding-bottom: 10px;
			    background-color: #ea6071;
			    color: #fff;
			}
			.container {
			    display: inline-block;
			    position: relative;
			    padding-left: 35px;
			    margin-bottom: 12px;
			    cursor: pointer;
			    font-size: 17px;
			    -webkit-user-select: none;
			    -moz-user-select: none;
			    -ms-user-select: none;
			    user-select: none;
			    width: 45%;
			}
			
			/* Hide the browser's default radio button */
			.container input {
			    position: absolute;
			    opacity: 0;
			    cursor: pointer;
			}
			
			/* Create a custom radio button */
			.checkmark {
			    position: absolute;
			    top: 0;
			    left: 0;
			    height: 25px;
			    width: 25px;
			    background-color: #eee;
			    border-radius: 50%;
			}
			
			/* On mouse-over, add a grey background color */
			.container:hover input ~ .checkmark {
			    background-color: #ccc;
			}
			
			/* When the radio button is checked, add a blue background */
			.container input:checked ~ .checkmark {
			    background-color: #ea6071;
			}
			
			/* Create the indicator (the dot/circle - hidden when not checked) */
			.checkmark:after {
			    content: "";
			    position: absolute;
			    display: none;
			}
			
			/* Show the indicator (dot/circle) when checked */
			.container input:checked ~ .checkmark:after {
			    display: block;
			}
			
			/* Style the indicator (dot/circle) */
			.container .checkmark:after {
			 	top: 9px;
				left: 9px;
				width: 8px;
				height: 8px;
				border-radius: 50%;
				background: white;
			}
			.payment_options{
				width: 100%;
				margin: 0 auto;
				margin-top: 30px;
			}
			.next_button{
				width: 120px;
			    display: block;
			    text-align: center;
			    font-size: 20px;
			    height: 35px;
			    line-height: 35px;
			    margin: 0 auto;
			    background: #ea6071;
			    color: #fff;
			    border-radius: 5px;
			    margin-top: 37px;
			    box-shadow: 0 0 4px rgba(0,0,0,0.3);
			}
			.backbutton{
				position: absolute;
			    left: 10px;
			    color: #fff;
			    text-decoration: none;
			    font-weight: normal;
			}
			.backbutton span{
				margin-right: 5px;
			}
			.plan_box{
				margin-top: 20px;
				margin-bottom: 25px;
			}
			.plan_box .single_plan{
				width: 45%;
    			border: 1px solid #ddd;
    			margin-right:10px; 
    			vertical-align: top;
    			display: inline-block;
    			-webkit-transition-duration: 0.5s;
			  -moz-transition-duration: 0.5s;
			  -o-transition-duration: 0.5s;
			  transition-duration: 0.5s;
			  border-radius: 4px;
			}
			.plan_box .single_plan.active{
				 background-color: #ea6071;
			    color: #fff; 
			}
			.plan_box .single_plan p.label {
			    margin-top: 10;
			    margin-bottom: 0;
			}
			.plan_box .single_plan p.value {
			   margin-top: 5px;
    		   margin-bottom: 10px;
			}
		</style>
		<script>
			$(document).ready(function(){
				 $('.next_button').click(function(){
				 	 var radioValue = $("input[name='radio']:checked").val();
		            if(radioValue){
		                if(radioValue == "manually"){
		                	window.location.href = "<?php echo $site_url; ?>/api/manual_payment/insert/<?php echo $user[0]['email']; ?>";
		                }
		                if(radioValue == "here"){
		                	window.location.href = "<?php echo $site_url; ?>";
		                }
		            }else{
		            	alert("Please select any option");
		            }
				 });
				 $('.single_plan').click(function(){
				 	$('.single_plan').removeClass('active');
				 	$(this).addClass('active');
				 });
			})
		</script>
	</head>	
	<body>
		
		<div class="manual_payment_confirmation">
			<h3><a href="<?php echo $site_url; ?>/api/manual_payment/info/<?php echo $user[0]['email']; ?>" class="backbutton"><span><</span>Back</a> Welcome <?php echo $user[0]['name']; ?></h3> 
			<p>Please choose payment options!</p>
			
			<form method="post" action="{{url('api/manual_payment/submit')}}">
				<?php 
					session_start();
					if(isset($_SESSION['errors'])){
						$class="error_msg";
						if($_SESSION['status'] == 1){
							$class="success_msg";
						}
						echo '<p class="'.$class.'">'.$_SESSION['errors'].'</p>';
						unset($_SESSION['errors']);
						unset($_SESSION['status']);
					}
				?>
				<div class="form-control">
					<div class="label">Vibez Id : </div>
					<div class="value"><input type="text" name="user_name" value="<?php echo $user[0]['vibez_id']; ?>" autocomplete="off" required readonly></div>
				</div> 
				<div class="form-control">
					<div class="label">Email : </div>
					<div class="value"><input type="email" name="user_email" value="<?php echo $user[0]['email']; ?>" autocomplete="off" required readonly></div>
				</div> 
				<div class="payment_options">
					<div class="label">Select Plan : </div>
					<div class="plan_box">
						<div class="single_plan" data-price="10">
							<p class="label">Monthly Plan</p>
							<p class="value">$10</p>
						</div>
						<div class="single_plan" data-price="100">
							<p class="label">Yearly Plan</p>
							<p class="value">$100</p>
						</div>
					</div>
				</div>
				<div class="form_submit">
					<input type="hidden" name="user_id" value="<?php echo $user[0]['id']; ?>">
					<input type="submit" name="submit" value="Pay Now">
				</div>
			</form> 
		</div>
	</body>
</html>