<!--   Core JS Files   -->
<script src="{{asset('assets/js/core/popper.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/js/moment.js')}}"></script>

<script src="{{asset('assets/js/core/bootstrap.min.js')}}" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<!-- <script src="{{asset('assets/js/plugins/confirmation.js')}}"></script>
<script src="{{asset('assets/js/plugins/bootstrap-switch.js')}}"></script> -->

@if(Request::is('admin'))
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--  Chartist Plugin  -->
<script src="{{asset('assets/js/plugins/chartist.min.js')}}"></script>
<script src="{{asset('assets/js/demo.js')}}"></script>

@endif
<!--  Notifications Plugin    -->
<script src="{{asset('assets/js/plugins/bootstrap-notify.js')}}"></script>
<!-- Control Center for Light Bootstrap Dashboard: scripts for the example pages etc -->
<script src="{{asset('assets/js/light-bootstrap-dashboard.js?v=2.0.1')}}" type="text/javascript"></script>
<script src="{{ asset('admin_assets/js/select2.js') }}"></script>
<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
<script src="{{ asset('admin_assets/js/main.js') }}"></script>
<!-- <script src="{{ asset('admin_assets/js/plugins.js') }}"></script> -->

<script type="text/javascript" src="{{asset('assets/js/bootstrap-datetimepicker.js')}}"></script>
@if(Request::is('admin'))
<script type="text/javascript">
    $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

        demo.showNotification();

    });
</script>
@endif

<!-- Begin emoji-picker JavaScript -->
<script src="{{asset('admin_assets/plugins/emoji/lib/js/config.js')}}"></script>
<script src="{{asset('admin_assets/plugins/emoji/lib/js/util.js')}}"></script>
<script src="{{asset('admin_assets/plugins/emoji/lib/js/jquery.emojiarea.js')}}"></script>
<script src="{{asset('admin_assets/plugins/emoji/lib/js/emoji-picker.js')}}"></script>
<!-- End emoji-picker JavaScript -->


