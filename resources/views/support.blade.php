<!DOCTYPE html>
<html lang="en">
<head>
  <title>Vibez</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
    .header 
{
  background-color: #ed213a;
  color: #ffffff;
  padding: 15px;
  text-align: center;
}
.img-responsive
{
  width: 230px;
}
  </style>
</head>
<body>
<div class="header">
    <img src="/assets/img/vibezlogo.png" class="img-responsive">
  </div>
<div class="container">

  

  <h2>Support</h2>

@if(session()->has('message'))
  <div class="alert alert-success">
    {{ session()->get('message') }}
  </div>
@endif
  
  <form class="form-horizontal" role="form" action="{{ route('support_mail') }}" method="post">
    {{ csrf_field() }}
    <div class="form-group">
      <label class="control-label col-sm-2" for="name">Name:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="name" placeholder="Enter your name" name="name" required="required">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="subject">Subject:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="subject" placeholder="Enter subject" name="subject" required="required">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="subject">Email:</label>
      <div class="col-sm-10">          
        <input type="email" class="form-control" id="email" placeholder="Enter your email address" name="email" required="required">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="message">Message:</label>
      <div class="col-sm-10">          
        <textarea class="form-control" name="message" required="required" placeholder="Enter your message"></textarea>
      </div>
    </div>
    
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>
  </form>
</div>

</body>
</html>