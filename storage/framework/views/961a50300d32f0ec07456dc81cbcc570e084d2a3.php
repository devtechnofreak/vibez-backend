
<?php $__env->startSection('main_content'); ?>
<!-- Header-->

        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>News</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="breadcrumbs navbar-top">
                        <div class="card">
                            <div class="card-header border-header">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <strong class="card-title newslist-name"><i class="fa fa-plus"></i> Add Announcement</strong>
                                        </div>
                                        <div class="col-md-9">
                                            <a href="<?php echo e(url('admin/announcement')); ?>" class="btn btn-primary float-right backbtn"><i class="fa fa-arrow-left"></i> Announcement List</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>


        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
					<?php if(session()->has('message')): ?>
						<div class="alert alert-success">
							<?php echo e(session()->get('message')); ?>

						</div>
					<?php endif; ?>
                    <div class="card"> 
                        <!-- <div class="card-header border-header">
                            <strong class="card-title newslist-name">Add News</strong>
                            <a href="<?php echo e(url('admin/news')); ?>" class="btn btn-primary float-right backbtn"><i class="fa fa-arrow-left"></i> News List</a>
                        </div> -->
						<div class="card-body">
							 <?php if($errors->any()): ?>
								  <div class="alert alert-danger">
									  <ul>
										  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											  <li><?php echo e($error); ?></li>
										  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									  </ul>
								  </div><br/>
							  <?php endif; ?>
							<form method="POST" action="<?php echo URL::to('/admin/announcement/create'); ?>" enctype="multipart/form-data" class="add_news">
							 	<?php echo e(csrf_field()); ?>

								<div class="form-group row"><label for="title" class="col-md-4 col-form-label">Title</label>
									<div class="col-md-6"><input id="title" name="title" value="" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								<?php if($errors->has('title')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('title')); ?></strong>
                                    </span>
                                <?php endif; ?>
								<div class="form-group row"><label for="image" class="col-md-4 col-form-label">Image</label>
									<div class="col-md-6"><input id="image" name="image" value="" required="required" autofocus="autofocus" class="form-control" type="file"></div>
								</div>
								<?php if($errors->has('image')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('image')); ?></strong>
                                    </span>
                                <?php endif; ?>
                                
                                <div class="form-group row"><label for="content" class="col-md-4 col-form-label">Content</label>
									<div class="col-md-6"><textarea id="content" name="content" value="" required="required" autofocus="autofocus" class="form-control" maxlength="250" style="height:120px !important;"></textarea></div>
								</div>
								
								<div class="form-group row mb-0">
									<div class="col-md-6 offset-md-4">
										<a class="btn btn-primary add_news_btn">Add Announcement</a>
									</div>
								</div>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>