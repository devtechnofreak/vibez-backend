<?php $__env->startSection('main_content'); ?>
<!-- Header-->

        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li class="active">Add User</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
                        <div class="card">
                            <div class="card-header border-header">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <strong class="card-title user-name">Add User</strong>
                                        </div>
                                        <div class="col-md-9">
                                            <a href="<?php echo e(url('admin/users')); ?>" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> User List</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>

        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
					<?php if(session()->has('message')): ?>
						<div class="alert alert-success">
							<?php echo e(session()->get('message')); ?>

						</div>
					<?php endif; ?>
                    <div class="card"> 
                        <div class="card-header">
                            <strong class="card-title">Add User</strong>
                        </div>
						<div class="card-body">
							 <?php if($errors->any()): ?>
								  <div class="alert alert-danger">
									  <ul>
										  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											  <li><?php echo e($error); ?></li>
										  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									  </ul>
								  </div><br/>
							  <?php endif; ?>
							<form method="POST" action="<?php echo URL::to('/admin/user/create'); ?>">
							 <?php echo e(csrf_field()); ?>

								<div class="form-group row"><label for="name" class="col-md-4 col-form-label text-md-right">Name</label>
									<div class="col-md-6"><input id="name" name="name" value="" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								<?php if($errors->has('name')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                <?php endif; ?>
								<div class="form-group row"><label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
									<div class="col-md-6"><input id="email" name="email" value="" required="required" class="form-control" type="email"></div>
								</div>
								<?php if($errors->has('email')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                                <div class="form-group row"><label for="vibez_id" class="col-md-4 col-form-label text-md-right">Vibez Id</label>
									<div class="col-md-6"><input id="vibez_id" name="vibez_id" value="" required="required" class="form-control" type="text"></div>
								</div>
								<?php if($errors->has('email')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
								<div class="form-group row"><label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
									<div class="col-md-6"><input id="password" name="password" required="required" class="form-control" type="password"></div>
								</div>
								<?php if($errors->has('password')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
								<div class="form-group row"><label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirm Password</label>
									<div class="col-md-6"><input id="password-confirm" name="password_confirmation" required="required" class="form-control" type="password"></div>
								</div>
								<div class="form-group row mb-0">
									<div class="col-md-6 offset-md-4">
										<button type="submit" class="btn btn-primary">
											Add User
										</button>
									</div>
								</div>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>