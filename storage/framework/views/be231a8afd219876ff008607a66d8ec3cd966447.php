<div class="header" style="padding: 25px 0;background-color: #ed213a;text-align: center;">
    <img style="width:130px;" src="https://www.thevibez.net/assets/img/vibezlogo.png"> 
</div>
<table class="m_839837753987678212m_5786463994150025933inner-body" align="center" width="570" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;background-color:#ffffff;margin:0 auto;padding:0;width:570px">
    <tbody>
        <tr>
            <td class="m_839837753987678212m_5786463994150025933content-cell" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;padding:35px">
                    <h1 style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#2f3133;font-size:19px;font-weight:bold;margin-top:0;text-align:left">Hey Admin, </h1>
                    <p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left"> <?php echo e($user['content']); ?> </p>                    
            </td>
        </tr>
    </tbody>
</table>
<div class="35" style="padding: 35px 0;background-color: #ed213a;text-align: center;color : #fff;">
    © <?php echo date('Y'); ?> Vibez. All rights reserved. 
</div>