<?php $__env->startSection('main_content'); ?>
<!-- Header-->
        <!-- <div class="breadcrumbs">
            <div class="col-sm-12">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Artist</h1>
                    </div>
                    
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <style type="text/css">
        	.edbtn
        	{
        		    border-color: #FFF;
    				color: #FFF;
        	}
        </style>
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-md-3">
										<strong class="card-title artist-name"><?php echo e(ucfirst($artist->name)); ?></strong>
									</div>
									<div class="col-md-9">
										<a href="<?php echo e(url('admin/artists')); ?>" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Artist List</a>
										<a href="<?php echo e(url('admin/album/add/'.$artist->id)); ?>" class="btn btn-primary float-right ml-2"><i class="fa fa-plus"></i> Add Album</a>
		              					<a href="<?php echo e(url('admin/artist/edit/'.$artist->id)); ?>" class="btn btn-primary float-right ml-2"><i class="fa fa-edit"></i> Edit Artist</a>
		              					<a href="<?php echo e(url('admin/album/import/bulk/'.$artist->id)); ?>" class="btn btn-success float-right ml-2"><strong class="card-title"><i class="fa fa-plus"></i> Bulk Import Albums</strong></a>
									</div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>

        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-12 col-md-12">
					<?php if(session()->has('message')): ?>
						<div class="alert alert-success">
							<?php echo e(session()->get('message')); ?>

						</div>
					<?php endif; ?>
					 <?php if($errors->any()): ?>
						  <div class="alert alert-danger">
							  <ul>
								  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									  <li><?php echo e($error); ?></li>
								  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							  </ul>
						  </div><br/>
					  <?php endif; ?>
						<div class="card"> 
							<!-- <div class="card-header border-header">
								<div class="row">
									<div class="col-md-3">
										<strong class="card-title artist-name">Artist Name : <?php echo e(ucfirst($artist->name)); ?></strong>
									</div>
									<div class="col-md-9">
										<a href="<?php echo e(url('admin/artists')); ?>" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Artist List</a>
										<a href="<?php echo e(url('admin/album/add/'.$artist->id)); ?>" class="btn btn-primary float-right ml-2"><i class="fa fa-plus"></i> Add Album</a>
		              					<a href="<?php echo e(url('admin/artist/edit/'.$artist->id)); ?>" class="btn btn-primary float-right ml-2"><i class="fa fa-edit"></i> Edit Artist</a>
		              					<a href="<?php echo e(url('admin/album/import/bulk/'.$artist->id)); ?>" class="btn btn-success float-right ml-2"><strong class="card-title"><i class="fa fa-plus"></i> Bulk Import Albums</strong></a>
									</div>
								</div>
								
								
							</div> -->
							<div class="card-body bg-light tbalbum">
								<div class="row">
								<div class="col-md-2">
									<img src="<?php echo e($artist->image); ?>" alt="<?php echo e($artist->name); ?>" class="img-thumbnail img-fluid img-height"/>
								</div>
								<div class="col-md-10">
					                <table class="table table-user-information table-striped">
					                    <tbody>
					                      <!-- <tr>
					                        <td class="col-1">Name:</td>
					                        <td><?php echo e($artist->name); ?></td>
					                      </tr> -->
					                      <tr>
					                        <td class="col-1">Bio:</td>
					                        <td><?php echo e($artist->bio); ?></td>
					                      </tr>
					                      <tr>
					                        <td class="col-1">Date of Birth:</td>
					                        <td><?php echo e(date('F j, Y',strtotime($artist->dob))); ?></td>
					                      </tr>
			                              <tr class="col-1">
					                        <td>Tags:</td>
					                        
					                        <td>
					                        	<?php $__currentLoopData = $artist->tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					                        	<div class="d-inline-block bg-dark pl-2 pr-2 mb-2 text-light"><?php echo e($tag->name); ?></div>
					                        	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			                                	
					                       </td>
					                      </tr>
					                    </tbody>
					                  </table>
								</div>
								</div>
							</div>
						</div>
						<div class="card"> 
							<div class="card-header border-header album-listback">
								<strong class="card-title"><?php echo e('Album List'); ?></strong>
							</div>
							<div class="container-fluid card-body">
								<?php $__currentLoopData = $albums; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $album): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div class="card album-list"> 
										<div class="card-header text-dark album-listborder">
											<span class="album-listheader"><strong class="card-title"><?php echo e(ucfirst($album->title)); ?></strong></span>
											<a href="<?php echo e(url('admin/album/edit/'.$album->id)); ?>" class="btn btn-secondary float-right edbtn"><i class="fa fa-edit"></i> Edit</a>
										</div>
										<div class="container-fluid cardbody-padding">
											<div class="row">
												<div class="col-md-3 img-padding">
													<img src="<?php echo e($album->image); ?>" alt="<?php echo e($album->title); ?>" class="img-thumbnail rounded img-fluid"/>
												</div>
												<div class="col-md-9">
									                <table class="table table-user-information table-condensed">
									                    <tbody>
									                      <!-- <tr>
									                        <td class="col-1">Title:</td>
									                        <td><?php echo e($album->title); ?></td>
									                      </tr> -->
									                      <tr>
									                      	<td class="col-12 vertical-td"><span class=""><b>Description : </b><span><?php echo e(str_limit(strip_tags($album->description), $limit = 160, $end = '... ')); ?><br>
									                      		<?php
									                      		$string = strip_tags($album->description);

																if (strlen($string) > 160) {
																	echo '<a href="#myModal" class="readmore" data-toggle="modal" data-id="'. $album->id .'" data-target="#myModal'. $album->id .'"> Read More  →</a>';
																}
																?>
									                      		

									                      	</span></span></td>	
									                      	
									                        
									                        <!-- <td><?php echo e($album->description); ?></td> -->
									                        <div class="modal myModal" id="myModal<?php echo e($album->id); ?>">
															    <div class="modal-dialog">
															      	<div class="modal-content">
															      		
																        <div class="modal-header">
																          <h4 class="modal-title">Artist Name : <?php echo e(ucfirst($album->title)); ?></h4>
																          <button type="button" class="close" data-dismiss="modal">&times;</button>
																        </div>
															        
																      
																        <div class="modal-body edit-content">
																          <b>	Description : </b><?php echo e($album->description); ?>

																        </div>
																      
																        <div class="modal-footer">
																          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
																        </div>
																      
															      	</div>
															    </div>
															</div>

									                      </tr>
							                              <!-- <tr>
									                        <td class="col-1">Tracks:</td>
									                        <td>
									                      	<?php $__currentLoopData = $album->musics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $music): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									                        	<div class="d-inline-block bg-info pl-2 pr-2 mb-2 text-light"><?php echo e($music->title); ?></div>
								                        	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									                       </td>
									                      </tr> -->
									                    </tbody>
									                  </table>
												</div>
											</div>
										</div>
										<div class="container-fluid card-body">
											<div class="row">
												<div class="col-md-12 col-padding">Tracks : <?php $__currentLoopData = $album->musics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $music): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										                        	<span class="bg-info pl-2 pr-2 mb-2 text-light"><?php echo e($music->title); ?></span>
									                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></div>
												<!-- <div class="col-md-10">
														
												</div> -->
											</div>
										</div>
								</div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<div class="text-center col-12 col-md-12">
								<?php echo e($albums->links()); ?>

								</div>
							</div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>