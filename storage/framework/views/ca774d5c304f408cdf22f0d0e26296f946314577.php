<?php $__env->startSection('main_content'); ?>
<!-- Header-->
        
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">

        <div class="breadcrumbs navbar-top">
            <div class="card">
                <div class="card-header border-header">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-3">
                                <strong class="card-title user-name">Export Report</strong>
                            </div>
                            <div class="col-md-9">
                                <a href="<?php echo e(url('admin/export/all')); ?>" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Return</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card"> 
                            <div class="card-header">
                                <!-- <strong class="card-title">Add User</strong> -->
                            </div>
                            <div class="card-body">
                               <div class="row clearfix">
                                        <div class="col-md-12 column">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">               
                                                            <!-- <button type="submit" id="export" onclick="fnExcelReport22();">Export</button> -->
                                                            
                                                        </div>
                                                    </h3>
                                                </div>
                                 
                                                <div class="panel-body" id="dvData">
                                                     <table id="headerTable" class="table table-striped table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>Artist Name</th>
                                                                    <th>Content Type</th>
                                                                    <th>Content Title</th>
                                                                    <th>User Viewed</th>
                                                                    <th>Total Views</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <tr>
                                                                        <td> <?php echo e($row->name); ?> </td>
                                                                        <td> <?php echo e($row->viewable_type); ?> </td>
                                                                        <td> <?php echo e($row->title); ?> </td>
                                                                        <td> <?php echo e($row->count_click); ?> </td>
                                                                        <td> <?php echo e($row->sum); ?> </td>
                                                                    </tr>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->



<script src="<?php echo e(asset('admin_assets/js/lib/data-table/datatables.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/dataTables.bootstrap.min.js')); ?>"></script>

<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

<script type='text/javascript'>
    $(document).ready(function () {
        $('#headerTable').DataTable({
            "paging": false,
            "searching": false,
            dom: 'Bfrtip',
            buttons: [
                // 'copy', 'excel', 'print', {
                //     extend: 'csv',
                //     text: 'Export'
                // }
                {
                    extend: 'csv',
                    text: 'Export'
                }
            ]
        });
    });
</script>
    
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>