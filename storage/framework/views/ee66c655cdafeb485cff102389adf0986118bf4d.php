
<?php $__env->startSection('main_content'); ?>
<!-- Header-->
		
        <!-- <div class="breadcrumbs">
            <div class="col-sm-12">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Albums</h1>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-4">
			                        		<strong class="card-title artist-name">Event Codes List</strong>
			                        	</div>
			                            <div class="col-sm-8">	
			                            	<a class="btn btn-primary float-right ml-2" href="<?php echo e(url('admin/eventcodes/add')); ?>"><strong class="card-title"><i class="fa fa-plus"></i> Add New</strong></a>
			                            </div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>
        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                       <!--  <div class="card-header border-header">
                            <strong class="card-title artist-name">Album List</strong>
                            <div class="float-right">
                            	<a class="btn btn-primary float-right ml-2" href="<?php echo e(url('admin/album/add')); ?>"><strong class="card-title"><i class="fa fa-plus"></i> Add New</strong></a>
                            	<a href="<?php echo e(url('admin/album/import/bulk')); ?>" class="btn btn-success float-right ml-2"><strong class="card-title"><i class="fa fa-plus"></i> Bulk Import Albums</strong></a>
                            	<a href="<?php echo e(url('admin/album/trash')); ?>" class="btn btn-secondary float-right ml-2"><strong class="card-title"><i class="fa fa-trash"></i> View Trash (<?php echo e(App\Album::onlyTrashed()->count()); ?>)</strong></a>
                            </div>
                        </div> -->
                        <div class="card-body">
							<?php if(session()->has('message')): ?>
								<div class="alert alert-success">
									<?php echo e(session()->get('message')); ?>

								</div>
							<?php endif; ?>
							 <div class="video_bulk_action">
	                        	<label>Bulk Action 
	                        		<select name="bulk_action" class="form-control form-control-sm">
	                        			<option value="">Select action</option>
	                        			<option value="permenentt">Permanently delete</option>
	                        		</select>
	                        	</label>
	                        	<a class="apply_action">Apply</a>
	                        </div>
							<table id="artist_list" class="table table-striped table-bordered albumtb " style="width:100%">
								<thead>
									<tr>
										<th><input type="checkbox" class="deleteall delete_row"></th>
										<th>Code</th>
										<th>Name</th>
										<th>Date</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th><input type="checkbox" class="deleteall delete_row"></th>
										<th>Code</th>
										<th>Name</th>
										<th>Date</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
                        </div>
                    </div>
                </div>
				<input type="hidden" class="csrf_token" value="<?php echo e(csrf_token()); ?>"> 
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/datatables.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/dataTables.buttons.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/buttons.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/jszip.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/pdfmake.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/vfs_fonts.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/buttons.html5.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/buttons.print.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/buttons.colVis.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/datatables-init.js')); ?>"></script>


<script type="text/javascript">
	$(document).ready(function() {
		$('.deleteall').click(function(){
			$('#artist_list tbody .delete_row').each(function(){
				if($(this).is(':checked')){
					$(this).prop('checked',false);	
				}else{
					$(this).prop('checked',true);
				}
				
			})
		});
		$('.apply_action').click(function(){
			var action = "";
			
			if($('.video_bulk_action select').val() == "permenentt"){
				var r = confirm("Are you sure want to permenently delete selected eventcodes!");
			    if (r == true) {
			        action = "delete";
			    } else {
			     return false
			    }
			}
			if(action != ""){
				var eventcodes = [];
				$('#artist_list').find('.delete_bulk').each(function(){ 
					if($(this).is(':checked')){
						eventcodes.push($(this).val());
					}
				});
				var csrf_token = jQuery(document).find('.csrf_token').val();
				$.ajax({
		       	    beforeSend: function(xhrObj){
			                xhrObj.setRequestHeader("X-CSRF-TOKEN",csrf_token); 
			        },
				   url: site_url+"/admin/eventcodes/bulk/action",
				   data: {action	: action,eventcodes : eventcodes},
				   success: function(data) {
				   	window.location.reload();
				   },
				   type: 'POST'
				});
			}
		})
		$('#artist_list').on('click', '.delete-smt-btn', function(e){
			$('#confirmationDelete').modal("show");
			var id = $(this).attr('data-id');
			var url = $(this).attr('data-url');
			// $(".remove-record-model").attr("action",url);
			$(".del-link").attr("href",url);
		});
		var csrf_token = jQuery(document).find('.csrf_token').val();
		$('#artist_list').DataTable( {
			"processing": true,
			"serverSide": true,
			"bAutoWidth": false,
			"pageLength": 50,
			"ajax": {
				"url": "geteventcodes",
				"type": "POST",
				"dataType": 'json',
                "error": function(data){
                        console.log(data);
                    },
				"headers" : {
						'X-CSRF-TOKEN': csrf_token
				}
			},
			"columns": [
				{ "data": "id", "orderable": false,    render: function ( data, type, row ) {
						return '<input type="checkbox" class="delete_bulk delete_row" value="'+data+'">';
					return data;
				} },
				{ "data": "code" ,"width" : "16%",className: "text-center" },
				{ "data": "name" ,"width" : "20%",className: "text-center"},
				{ "data": "date" ,"width" : "20%"},
				{ "data": "id",     render: function ( data, type, row ) {
						var deletelink = "<?php echo URL::to('/admin/eventcodes/delete/"+data+"'); ?>";
						return '<li class="libtnlink"><a href="#confirmationDelete" class="btn btn-danger delete-smt-btn delete_link text-danger" data-toggle="modal" data-id="'+data+'" data-url="'+deletelink+'" data-target="#confirmationDelete"><i class="fa fa-trash"></i></a></li>';
					return data;
				},"border": "none" }
			]
		} );
		
		
    } );
</script>
 <div id="confirmationDelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;top: 25% !important;">
        <div class="modal-dialog modal-md modal-notify modal-danger" style="width:55%;">
            <div class="modal-content">
                <div class="modal-header">
                	<h4 class="modal-title" id="custom-width-modalLabel">Are you sure you want to delete this record?</h4>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect remove-data-from-delete-form" data-dismiss="modal">Close</button>

                    <a class="btn btn-default text-danger del-link" href="<?php echo URL::to('/admin/promocode/delete/'); ?>">Delete</a>
                </div>
            </div>
        </div>
    </div>	
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>