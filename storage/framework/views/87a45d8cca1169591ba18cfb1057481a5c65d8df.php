<table class="action" align="center" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <a href="<?php echo e($url); ?>" class="button button-<?php echo e($color ?? 'red'); ?>" target="_blank" style="background-color: #ED213A;border-top: 10px solid #ED213A;border-right: 18px solid #ED213A;border-bottom: 10px solid #ED213A;border-left: 18px solid #ED213A"><?php echo e($slot); ?></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
