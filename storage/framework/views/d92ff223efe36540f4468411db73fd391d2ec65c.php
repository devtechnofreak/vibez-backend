<?php $__env->startSection('main_content'); ?>

<!-- Header-->

        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Video</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
                        <div class="card">
                            <div class="card-header border-header">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <strong class="card-title videolist-name">Add Video</strong>
                                        </div>
                                        <div class="col-md-9">
                                            <a href="<?php echo e(url('admin/videos')); ?>" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Video List</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>
					<?php if(session()->has('message')): ?>
					<script type="text/javascript">
					jQuery(document).ready(function($){
					        $.notify({
					            icon: "nc-icon nc-app",
					            message: '<?php echo e(session()->get('message')); ?>',
					        }, {
					            type: 'success',
					            timer: 8000,
					        });
					 });
					 </script>
					<?php endif; ?>
					<?php if($errors->any()): ?>
						  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<script type="text/javascript">
								jQuery(document).ready(function($){
								        $.notify({
								            icon: "nc-icon nc-app",
								            message: '<?php echo e($error); ?>',
								        }, {
								            type: 'danger',
								            timer: 8000,
								        });												
								});
								 </script>
						  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php endif; ?>
        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">

                    <div class="card"> 
                       <!--  <div class="card-header">
                            <strong class="card-title">Add Video</strong>
                        </div> -->
						<div class="card-body">

							<form method="POST" action="<?php echo URL::to('/admin/videos/create'); ?>" enctype="multipart/form-data" class="add_video">
							 	<?php echo e(csrf_field()); ?>

								
								<div class="form-group row">
									<label for="name" class="col-md-2 col-form-label">Title</label>
									<div class="col-md-6"><input id="title" name="title" value="" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								<?php if($errors->has('title')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('title')); ?></strong>
                                    </span>
                                <?php endif; ?>
								<div class="form-group row"><label for="description" class="col-md-2 col-form-label">Description</label>
									<div class="col-md-6"><textarea name="description" class="form-control txtbio" placeholder="Description"></textarea></div>
								</div>
                                
                                <div class="form-group row">
                                	<label for="audio" class="col-md-2 col-form-label">Video</label>
									<div class="col-md-2">
										<input id="video" name="video" value="" required="required" autofocus="autofocus" class="form-control" type="file">					
									</div>
									
								</div>
								<?php if($errors->has('video')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('video')); ?></strong>
                                    </span>
                                <?php endif; ?>

                                <div class="form-group row">
                                	<label for="image" class="col-md-2 col-form-label">Image</label>
									<div class="col-md-6"><input id="image" name="image" value="" required="required" autofocus="autofocus" class="form-control" type="file" style="width: 50%;float: left;">
										<input class="form-check-input d-none" type="checkbox" name="select_generated" value="" id="select_thumbnail">
										  <label class="form-check-label" for="select_generated">
										    Select generated Image
										  </label>
									</div>
									<div class="col-md-2"><a class="btn btn-primary generate_thumbnail d-none" href="#">Generate Thumbnail</a></div>
								</div>
								<?php if($errors->has('image')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('image')); ?></strong>
                                    </span>
                                <?php endif; ?>
                                
                                <div class="form-group row">
                                	<label for="date_released" class="col-md-2 col-form-label">Release Date</label>
									<div class="col-md-6"><input type="text" class="form-control release_date" name="date_released" required></div>
								</div>
								<?php if($errors->has('date_released')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('date_released')); ?></strong>
                                    </span>
                                <?php endif; ?>
                                
                                <div class="form-group row">
                                	<label for="artist" class="col-md-2 col-form-label">Artist</label>
									<div class="col-md-6">
										<select class="js-example-basic-multiple form-control" name="artist_id[]" value="" multiple="multiple">
											<?php $__currentLoopData = $artists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $artist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											  <option name="artist_id" value="<?php echo e($artist->id); ?>"><?php echo e($artist->name); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
									</div> 
								</div>
								<div class="form-group row">
                                	<label for="album_tags" class="col-md-2 col-form-label">Tags</label>
										<div class="col-md-6">
											<input id="album_tags" name="tag_id" value="" placeholder="Enter comma , separated Tags" autofocus="autofocus" class="form-control mb-2" type="text">
										</div>
								</div>
								
								<div class="form-group row">
									<label for="cateories" class="col-md-2 col-form-label">Category</label>
									<div class="col-md-6">
										<select name="category_id" value="" class="form-control">
												<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													  <option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</select>
									</div> 
								</div>
								<div class="form-group row"><label for="is_featured" class="col-md-2 col-form-label">Is Featured</label>
									<div class="col-md-6"><input value="1" type="checkbox" id="isFeatured" name="is_featured"></div> 
								</div>
								<div class="form-group row mb-0">
									<div class="col-md-6 offset-md-4">
										<a class="btn btn-primary add_video_btn" href="#">Add Video</a>
									</div>
								</div>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('admin_assets/plugins/select2/select2.min.css')); ?>">
<!-- <script src="<?php echo e(asset('admin_assets/js/vendor/jquery-2.1.4.min.js')); ?>"></script> -->
<script src="<?php echo e(asset('admin_assets/plugins/select2/select2.min.js')); ?>"></script>

<script type="text/javascript">
    $(document).ready(function() {
	    $('.js-example-basic-multiple').select2({
	    	placeholder: "Select a item",
    		allowClear: true
	    });
	});
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>