
<?php $__env->startSection('main_content'); ?>
<!-- Header-->
<?php 

?>
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Artist</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-md-3">
											<strong class="card-title">Edit Manual Payment</strong>
										</div>
										<div class="col-md-9">
											<a href="<?php echo e(url('admin/manual-payment')); ?>" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Manual Payment</a>
										</div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>
        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">
					<?php if(session()->has('message')): ?>
					<script type="text/javascript">
					jQuery(document).ready(function($){
					        $.notify({
					            icon: "nc-icon nc-app",
					            message: '<?php echo e(session()->get('message')); ?>',
					        }, {
					            type: 'success',
					            timer: 8000,
					        });
					 });
					 </script>
					<?php endif; ?>
						<div class="card"> 
							<!-- <div class="card-header border-header">
								<strong class="card-title">Edit Artist</strong>
								<a href="<?php echo e(url('admin/artists')); ?>" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Artist List</a>
							</div> -->
							<div class="card-body row">
								 <?php if($errors->any()): ?>
									  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<script type="text/javascript">
											jQuery(document).ready(function($){
											        $.notify({
											            icon: "nc-icon nc-app",
											            message: '<?php echo e($error); ?>',
											        }, {
											            type: 'danger',
											            timer: 8000,
											        });												
											});
											 </script>
									  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								  <?php endif; ?>
								  <div class="col-md-8">
										<div class="form-group row"><label for="gender" class="col-md-5 col-form-label text-md-left">Name</label>
											<div class="col-md-6"><label><?php echo e($userdata[0]['name']); ?></label></div>
										</div>
										<div class="form-group row"><label for="gender" class="col-md-5 col-form-label text-md-left">Email</label>
											<div class="col-md-6"><label><?php echo e($userdata[0]['email']); ?></label></div>
										</div>
										<div class="form-group row"><label for="gender" class="col-md-5 col-form-label text-md-left">Date Paid</label>
											<div class="col-md-6"><label><?php echo e($userdata[0]['date_paid']); ?></label></div>
										</div>
										<div class="form-group row"><label for="gender" class="col-md-5 col-form-label text-md-left">Bank Name</label>
											<div class="col-md-6"><label><?php echo e($userdata[0]['bank_name']); ?></label></div>
										</div>
										<div class="form-group row"><label for="gender" class="col-md-5 col-form-label text-md-left">Bank Account Number</label>
											<div class="col-md-6"><label><?php echo e($userdata[0]['bank_account_number']); ?></label></div>
										</div>
										
										<div class="Subscription_section">
											<h4>Edit Subscription</h4>
											<form class="edit_user_subscription" method="post" action="<?php echo URL::to('/admin/user/editusersubscription'); ?>" enctype="multipart/form-data">
												<div class="form-group row"><label for="gender" class="col-md-4 col-form-label text-md-left">Status</label>
													<div class="col-md-6">
														<select class="form-control" name="status">
															<option value="">Please select Status</option>
															<option value="0">Pending</option>
															<option value="1">Approve</option>
															<option value="2">Disapprove</option>
														</select>
													</div>
												</div>
												<div class="form-group row"><label for="gender" class="col-md-4 col-form-label text-md-left">Subscription Plan</label>
													<div class="col-md-6">
														<select class="form-control" name="status">
															<option value="">Please select Plan</option>
															<option value="0">Monthly Plan( $10 )</option>
															<option value="1">Yearly Plan ( $100 )</option>
														</select>
													</div>
												</div>
												<div class="form-group row"><label for="gender" class="col-md-4 col-form-label text-md-left">Subscription Start Date</label>
													<div class="col-md-6">
														<input type="text" id="start_date" class="form-control" name="start_date" value="" autocomplete="off" required>
													</div>
												</div>
												<div class="form-group row"><label for="gender" class="col-md-4 col-form-label text-md-left">Subscription End Date</label>
													<div class="col-md-6">
														<input type="text" id="end_date" class="form-control" name="end_date" value="" autocomplete="off" required>
													</div>
												</div>
												<div class="form-group row mb-0 user_form_submit">
													<input type="hidden" name="id" value="<?php echo $userdata[0]['id']; ?>">
													<a class="btn btn-success update_artist_btn text-white">Update Subscription</a>
												</div>
											</form>
										</div>
										<div class="user_subscription_history_section">
											<h4>Subscription History</h4>
											<?php 
												if(sizeof($user_subscription_history) > 0){ ?>
													<table class="user_subscription_history" border="1">
														<thead>
															<tr>
																<th>Date</th>
																<th>Plan</th>
																<th>Platform</th>
																<th>Amount</th>
																<th>transaction_id</th>
															</tr>
														</thead>
														<tbody>
															<?php
																foreach ($user_subscription_history as $single) {?>
																	<tr>
																		<td><?php echo $single['date']; ?></td>
																		<td><?php echo $single['plan']; ?></td>
																		<td><?php echo $single['subscription_platform']; ?></td>
																		<td><?php echo $single['subscription_amount']; ?></td>
																		<td><?php echo $single['transaction_id']; ?></td>
																	</tr>
															<?php }
															?>
														</tbody>
													</table>	
												<?php }else{ ?>
													<h5>There is no subscription history for this user!</h5>
											<?php 	}
											?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="card">
				                            <div class="card-image">
				                                <img src="/<?php echo e($userdata[0]['image']); ?>" alt="..." class="artist_image">
				                            </div>
				                            <div class="card-body">
				                                <div class="author">
				                                    <h5 class="title"><?php echo e($userdata[0]['name']); ?></h5>
				                                </div>
				                            </div>
				                            <hr>
				                        </div>
									</div>
							</div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
		<script>
			$(document).ready(function(){
				 $( "#start_date" ).datepicker();
				 $( "#end_date" ).datepicker();
			})
		</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>