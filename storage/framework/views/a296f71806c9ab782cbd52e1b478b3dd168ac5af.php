<!--- Sufee Admin --->	
	<!--
	<link rel="stylesheet" href="<?php echo e(asset('admin_assets/css/normalize.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('admin_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('admin_assets/css/font-awesome.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('admin_assets/css/themify-icons.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('admin_assets/css/flag-icon.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('admin_assets/css/cs-skin-elastic.css')); ?>">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo e(asset('admin_assets/scss/select2.css')); ?>">
    
    <link rel="stylesheet" href="<?php echo e(asset('admin_assets/scss/style.css')); ?>">
    <link href="<?php echo e(asset('admin_assets/css/lib/vector-map/jqvmap.min.css')); ?>" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo e(asset('admin_assets/css/custom.css')); ?>">
	-->
	<!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
<!--- CREATIVE TIM Admin --->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="<?php echo e(asset('assets/css/bootstrap.min.css')); ?>" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/bootstrap-datetimepicker.min.css')); ?>">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="<?php echo e(asset('assets/css/light-bootstrap-dashboard.css?v=2.0.1')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('admin_assets/css/lib/vector-map/jqvmap.min.css')); ?>" rel="stylesheet">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="<?php echo e(asset('assets/css/demo.css')); ?>" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('admin_assets/css/custom.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('admin_assets/plugins/datepicker/datepicker.css')); ?>">

    <!-- Begin emoji-picker Stylesheets -->
    <link href="<?php echo e(asset('admin_assets/plugins/emoji/lib/css/emoji.css')); ?>" rel="stylesheet">
    <!-- End emoji-picker Stylesheets -->
    
    <script src="<?php echo e(asset('assets/js/core/jquery.3.2.1.min.js')); ?>" type="text/javascript"></script>
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- <script src="<?php echo e(asset('admin_assets/js/vendor/jquery-2.1.4.min.js')); ?>"></script> -->
	<script>
		var site_url = "<?php echo URL::to('/'); ?>";
	</script> 
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>