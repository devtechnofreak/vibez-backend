<?php $__env->startSection('main_content'); ?>
<!-- Header-->
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li class="active">Edit User</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <style type="text/css">
        	.user_admin_content
        	{
        		margin-top: 6rem!important;
        	}
        	.playlist-content
        	{
        		margin-bottom: 3rem!important;
        	}
        	.main-panel>.content {
    			padding: 0px 15px !important;
    			min-height: auto !important;
    		}	
    		.card .table tbody td:last-child, .card .table thead th:last-child
    		{
    			padding-right: 15px;
    			display: table-cell;

    		}	
    	</style>
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-4">
			                        		<strong class="card-title user-name">Edit User</strong>
			                        	</div>
			                            <div class="col-sm-8">
			                            	<a href="<?php echo e(url('admin/users')); ?>" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> User List</a>
			                            	<form class="suspend_user_form" method="POST" action="<?php echo URL::to('/admin/user/suspenduser'); ?>">
			                            		<?php echo e(csrf_field()); ?>

			                            		<input type="hidden" name="user_id"	value=<?php echo e($data['data']->id); ?>>
			                            		<button type="button" class="btn btn-danger float-right suspend_user_btn">Suspend this user</button>
			                            	</form>
			                            </div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>
					<?php if(session()->has('message')): ?>
					<script type="text/javascript">
					jQuery(document).ready(function($){
					        $.notify({
					            icon: "nc-icon nc-app",
					            message: '<?php echo e(session()->get('message')); ?>',
					        }, {
					            type: 'success',
					            timer: 8000,
					        });
					 });
					 </script>
					<?php endif; ?>
					<?php if($errors->any()): ?>
						  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<script type="text/javascript">
								jQuery(document).ready(function($){
								        $.notify({
								            icon: "nc-icon nc-app",
								            message: '<?php echo e($error); ?>',
								        }, {
								            type: 'danger',
								            timer: 8000,
								        });												
								});
								 </script>
						  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					  <?php endif; ?>
        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-8">
						<div class="card"> 
							<div class="card-header">
								<strong class="card-title">Edit User</strong>
							</div>
							<div class="card-body">
								<form method="POST" action="<?php echo URL::to('/admin/user/editdata'); ?>" enctype="multipart/form-data">
								 <?php echo e(csrf_field()); ?>

									<div class="form-group row"><label for="name" class="col-md-2 col-form-label text-md-left">Name</label>
										<div class="col-md-6"><input id="name" name="name" required="required" autofocus="autofocus" class="form-control" value="<?php echo e($data['data']->name); ?>" type="text"></div>
									</div>
									<?php if($errors->has('name')): ?>
										<span class="invalid-feedback">
											<strong><?php echo e($errors->first('name')); ?></strong>
										</span>
									<?php endif; ?>
									<!-- <div class="form-group row"><label for="email" class="col-md-2 col-form-label text-md-left">E-Mail Address</label>
										<div class="col-md-6"><input id="email" name="email"  required="required" class="form-control" type="email" value="<?php echo e($data['data']->email); ?>" disabled></div>
									</div> -->
									<?php if($errors->has('email')): ?>
										<span class="invalid-feedback">
											<strong><?php echo e($errors->first('email')); ?></strong>
										</span>
									<?php endif; ?>
									<div class="form-group row"><label for="vibez_id" class="col-md-2 col-form-label text-md-left">Vibez ID</label>
										<div class="col-md-6"><input id="vibez_id" name="vibez_id"  required="required" class="form-control" type="text" value="<?php echo e($data['data']->vibez_id); ?>" disabled></div>
									</div>
									<?php if($errors->has('vibez_id')): ?>
										<span class="invalid-feedback">
											<strong><?php echo e($errors->first('vibez_id')); ?></strong>
										</span>
									<?php endif; ?>
									<div class="form-group row"><label for="gender" class="col-md-2 col-form-label text-md-left">Gender</label>
										<div class="col-md-6"><input id="gender" name="gender"  required="required" class="form-control" type="text" value="<?php echo e($data['data']->gender); ?>"></div>
									</div>
									<?php if($errors->has('gender')): ?>
										<span class="invalid-feedback">
											<strong><?php echo e($errors->first('gender')); ?></strong>
										</span>
									<?php endif; ?>
									<div class="form-group row"><label for="image" class="col-md-2 col-form-label">Profile</label>
										<div class="col-md-6"><input id="image" name="image" class="form-control" value="" type="file"/></div>
									</div>
									<?php if($errors->has('image')): ?>
										<span class="invalid-feedback">
											<strong><?php echo e($errors->first('image')); ?></strong>
										</span>
									<?php endif; ?>
									<div class="form-group row"><label for="dob" class="col-md-2 col-form-label">Date of Birth</label>
										<div class="col-md-6"><input type="text" id="dob" name="dob" value="<?php echo e($data['data']->dob); ?>" autocomplete="off" required></div>
									</div>
									<?php if($errors->has('dob')): ?>
	                                    <span class="invalid-feedback">
	                                        <strong><?php echo e($errors->first('dob')); ?></strong>
	                                    </span>
	                                <?php endif; ?>
									<div class="form-group row"><label for="phone" class="col-md-2 col-form-label text-md-left">Phone</label>
										<div class="col-md-6"><input id="phone" name="phone" class="form-control" type="text" value="<?php echo e($data['data']->phone); ?>"></div>
									</div>
									<?php if($errors->has('phone')): ?>
										<span class="invalid-feedback">
											<strong><?php echo e($errors->first('phone')); ?></strong>
										</span>
									<?php endif; ?>
									<div class="form-group row"><label for="password" class="col-md-2 col-form-label text-md-left">Password</label>
										<div class="col-md-6"><input id="password" name="password" class="form-control" type="password"></div>
									</div>
									<?php if($errors->has('password')): ?>
										<span class="invalid-feedback">
											<strong><?php echo e($errors->first('password')); ?></strong>
										</span>
									<?php endif; ?>
									<div class="form-group row"><label for="password-confirm" class="col-md-2 col-form-label text-md-left">Confirm Password</label>
										<div class="col-md-6"><input id="password-confirm" name="password_confirmation" class="form-control" type="password"></div>
									</div>
									<div class="form-group row mb-0 user_form_submit">
										<div class="col-md-6 offset-md-4">
											<input type="hidden" name="user_id" value="<?php echo e($data['data']->id); ?>" /> 
											<button type="submit" class="btn btn-primary">
												Update User
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
                    <div class="col-md-4">
                        <div class="card card-user">
                            <div class="card-image">
                                <img src="/public/assets/img/vibez-logo-eduser.jpg" alt="...">
                            </div>
                            <div class="card-body">
                                <div class="author">
                                    <a href="#">
                                        <img class="avatar border-gray" src="<?php echo e($data['data']->image); ?>" alt="...">
                                        <h5 class="title"><?php echo e($data['data']->name); ?></h5>
                                        <h5 class="title"><?php echo e($data['data']->email); ?></h5>
                                    </a>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
        
        <div class="content mt-5">
        	<h4>User Subscription </h4>
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                       <!--  <div class="card-header border-header">
                            <strong class="card-title playlist-name">PlayLists</strong>
                            <div class="float-right">
                            	<a class="btn btn-primary float-right ml-2" href="<?php echo URL::to('/admin/play-lists/add'); ?>"><strong class="card-title"><i class="fa fa-plus"></i> Add New</strong></a>
                            </div>
                        </div> -->
                        <div class="card-body">
                        	<div class="subscription_info">
                        		<h4>Current Subscription</h4> 
                        		<?php
									if(sizeof($data['subscription_info']) > 0){ ?>
										<table class="user_subscription_history" border="1">
											<thead>
												<tr>
													<th>Start Date</th>
													<th>End Date</th>
													<th>Plan</th>
													<th>Platform</th>
													<th>Amount</th>
													<th>transaction_id</th>
												</tr>
											</thead>
											<tbody>
												<?php
													foreach ($data['subscription_info'] as $single) {?>
														<tr>
															<td><?php echo $single['start_date']; ?></td>
															<td><?php echo $single['end_date']; ?></td>
															<td><?php echo $single['plan']; ?></td>
															<td><?php echo $single['subscription_platform']; ?></td>
															<td><?php echo $single['subscription_amount']; ?></td>
															<td><?php echo $single['transaction_id']; ?></td>
														</tr>
												<?php }
												?>
											</tbody>
										</table>	
									<?php }else{ ?>
										<h5>There is no subscription for this user!</h5>
									<?php } ?>
                        	</div>
							<div class="user_subscription_history_section">
								<h4>Subscription History</h4>
								<?php 
									if(sizeof($data['user_subscription_history']) > 0){ ?>
										<table class="user_subscription_history" border="1">
											<thead>
												<tr>
													<th>Date</th>
													<th>Plan</th>
													<th>Platform</th>
													<th>Amount</th>
													<th>transaction_id</th>
												</tr>
											</thead>
											<tbody>
												<?php
													foreach ($data['user_subscription_history'] as $single) {?>
														<tr>
															<td><?php echo $single['date']; ?></td>
															<td><?php echo $single['plan']; ?></td>
															<td><?php echo $single['subscription_platform']; ?></td>
															<td><?php echo $single['subscription_amount']; ?></td>
															<td><?php echo $single['transaction_id']; ?></td>
														</tr>
												<?php }
												?>
											</tbody>
										</table>	
									<?php }else{ ?>
										<h5>There is no subscription history for this user!</h5>
								<?php 	}
								?>
							</div>
                        </div>
                    </div>
                </div>
				<input type="hidden" class="csrf_token" value="<?php echo e(csrf_token()); ?>">
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
        
        <div class="content mt-5 playlist-content">
        	<h4>User Playlists</h4>
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                       <!--  <div class="card-header border-header">
                            <strong class="card-title playlist-name">PlayLists</strong>
                            <div class="float-right">
                            	<a class="btn btn-primary float-right ml-2" href="<?php echo URL::to('/admin/play-lists/add'); ?>"><strong class="card-title"><i class="fa fa-plus"></i> Add New</strong></a>
                            </div>
                        </div> -->
                        <div class="card-body">
							<?php if(session()->has('message')): ?>
								<div class="alert alert-success">
									<?php echo e(session()->get('message')); ?>

								</div>
							<?php endif; ?>
							<table id="user_play_lists" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>Image</th>
										<th class="hidden">User</th>
										<th>Title</th>
										<th>Description</th>
										<th>Musics</th>
										<th style="display: none !important;">Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>Image</th>
										<th class="hidden">User</th>
										<th>Title</th>
										<th>Description</th>
										<th>Musics</th>
										<th style="display: none !important;">Action</th>
									</tr>
								</tfoot>
							</table>
                        </div>
                    </div>
                </div>
				<input type="hidden" class="csrf_token" value="<?php echo e(csrf_token()); ?>">
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
		<script src="<?php echo e(asset('admin_assets/js/lib/data-table/datatables.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/dataTables.buttons.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/datatables-init.js')); ?>"></script>
<script type="text/javascript">

	$(document).ready(function() {
		$('.suspend_user_btn').click(function(){
			
			var r = confirm("Are you sure want to suspend this user?");
			if (r == true) {
				$('.suspend_user_form').submit();
			}
			
		});
		var csrf_token = jQuery(document).find('.csrf_token').val();
		$('#user_play_lists').DataTable( {
			"processing": true,
			"serverSide": true,
			"bAutoWidth": false,
			"pageLength": 50,
			"lengthMenu": [50, 75, 100 ],
			"ajax": {
				"url": "/admin/play-lists/get-play-lists",
				"data"	: {"user_id" : <?php echo $data['data']->id; ?>},
				"type": "POST",
				"dataType": 'json',
                "error": function(data){
                        console.log(data);
                    },
				"headers" : {
						'X-CSRF-TOKEN': csrf_token
				}
			},
			"columns": [
				{ "data": "image" ,     render: function ( data, type, row ) {
						
						var aws_url = "<?php echo e(env('AWS_URL')); ?>";
						
						var image_url = data;
						
						return '<img src="'+image_url+'" style="height: 50; width:50" class="img-thumbnail" />';
					return data;
				},"width": "7%" },
				{ "data": "user_name","width": "10%","className": "hidden" },
				{ "data": "title","width": "20%" },
				{ "data": "description","width": "20%" },
				{ "data": "musics" ,     render: function ( data, type, row ) {
					var str ='';
					jQuery.each( data, function( key, value ) {
					  str += '<div class="d-inline-block bg-info pl-2 pr-2 mb-2 mr-2 text-light">'+value+'</div>';
					});
						return str;
					return data;
				}},
				{ "data": "id","className": "hidden",     render: function ( data, type, row ) {
						var editlink = "<?php echo URL::to('/admin/playlist/edit/"+data+"'); ?>";
						var deletelink = "<?php echo URL::to('/admin/playlist/delete/"+data+"'); ?>";
						return '<li class="libtnlink"><a class="text-warning" href="'+editlink+'"><i class="fa fa-pencil"></i></a></li><li class="libtnlink"><a href="#confirmationDelete" class="btn btn-danger delete-smt-btn delete_link text-danger" data-toggle="modal" data-id="'+data+'" data-url="'+deletelink+'" data-target="#confirmationDelete"><i class="fa fa-trash"></i></a></li>';
					
					return data;
				}}
			]
		} );
		
		
    } );
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>