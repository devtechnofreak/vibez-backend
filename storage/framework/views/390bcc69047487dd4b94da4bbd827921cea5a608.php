<?php $__env->startSection('main_content'); ?>
<!-- Header-->
		
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Categories</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-4">
			                        		<strong class="card-title categorylist-name">Categories List</strong>
			                        	</div>
			                            <div class="col-sm-8">	
			                            	<a class="btn btn-primary float-right ml-2" href="<?php echo URL::to('/admin/category/add'); ?>"><strong class="card-title"><i class="fa fa-plus"></i> Add New</strong></a>
			                            </div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>

        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                       <!--  <div class="card-header">
                            <strong class="card-title">Categories List</strong>
                            <div class="float-right">
                            	<a href="<?php echo URL::to('/admin/category/add'); ?>"><strong class="card-title">Add New</strong></a>
                            </div>
                        </div> -->
                        <div class="card-body">
							<?php if(session()->has('message')): ?>
								<div class="alert alert-success">
									<?php echo e(session()->get('message')); ?>

								</div>
							<?php endif; ?>
							<table id="users" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>Name</th>
										<th>Slug</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>Name</th>
										<th>Slug</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
                        </div>
                    </div>
                </div>
				<input type="hidden" class="csrf_token" value="<?php echo e(csrf_token()); ?>">
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	<script src="<?php echo e(asset('admin_assets/js/lib/data-table/datatables.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/dataTables.buttons.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/buttons.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/jszip.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/pdfmake.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/vfs_fonts.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/buttons.html5.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/buttons.print.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/buttons.colVis.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/datatables-init.js')); ?>"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/rowreorder/1.2.5/js/dataTables.rowReorder.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		var csrf_token = jQuery(document).find('.csrf_token').val();
		select_table = $('#users').DataTable( {
			"processing": true,
			"serverSide": true,
			"bLengthChange": false,
			"pageLength": 50,
			"ajax": {
				"url": "category/getcategories",
				"type": "POST",
				"dataType": 'json',
                "error": function(data){
                        console.log(data);
                    },
				"headers" : {
						'X-CSRF-TOKEN': csrf_token
				}
			},
			
			"columns": [
				{ "data": "name","className": "reorder" },
				{ "data": "slug" },
				{ "data": "id",     render: function ( data, type, row ) {
						console.log(data);
						var editlink = "<?php echo URL::to('/admin/category/edit/"+data+"'); ?>";
						var deletelink = "<?php echo URL::to('/admin/category/delete/"+data+"'); ?>";
						return '<li class="libtnlink"><a class="edit_link" href="'+editlink+'"><i class="fa fa-pencil"></i></a></li><li class="libtnlink"><a class="delete_link" href="'+deletelink+'"><i class="fa fa-trash"></i></a><input type="hidden" class="currentCat" value="'+data+'"></li>';
					
					return data;
				} }
			],
			"columnDefs": [
	            { "orderable": false, "targets": [ 1,2 ] }
	        ],
			"rowReorder": {
	            "dataSrc": "name"
	        },
		} );
		select_table.on( 'row-reorder', function ( e, details, edit ) {
			var catArray = [];
	   		$('#users tbody tr').each(function(){
	   			var currentcat = $(this).find('.currentCat').val();
	   			catArray.push(currentcat);
	   			
	   		});
	   		$.ajax({
				       	    beforeSend: function(xhrObj){
					               xhrObj.setRequestHeader("X-CSRF-TOKEN",$('meta[name="csrf-token"]').attr('content')); 
					        },
						   url: site_url+"/admin/category/reorder",
						   data: {catArray	: catArray},
						   success: function(data) {
						   		console.log(data);
						   },
						   type: 'POST'
						});
    	});
    } );
</script>	
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>