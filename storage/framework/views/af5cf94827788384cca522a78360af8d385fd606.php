<?php $__env->startSection('main_content'); ?>
<!-- Header-->
<?php 

?>
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Playlist</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <style type="text/css">
        	.emoji-wysiwyg-editor
        	{
        		max-height :100px !important;
        	}
        </style>
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-md-3">
											 <strong class="card-title">Edit Playlist</strong>
										</div>
										<div class="col-md-9">
											 <a href="<?php echo e(url('admin/play-lists')); ?>" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Playlist</a>
											 <?php /* <form method="POST" action="<?php echo URL::to('/admin/playlist/noti'); ?>" class="push_notification_form">
											 	{{ csrf_field() }}
												<div class="col">
													<input type="hidden" name="playlist_id" value="{{$playlist->id}}">
													<button type="submit" class="btn btn-success float-right send_push_notification_btn"><i class="fa fa-save"></i>Send Push Notification</button>
												</div>
											</form> */ ?>
											<?php /* <form method="POST" action="<?php echo URL::to('/admin/news/sendPushNotification_Playlist'); ?>" class="push_notification_form">
											 	{{ csrf_field() }}
												<div class="col">
													<input type="hidden" name="playlist_id" value="{{$playlist->id}}">
													<button type="submit" class="btn btn-success float-right send_push_notification_btn"><i class="fa fa-save"></i>FOR TEST</button>
												</div>
											</form> */ ?>
											<div class="col">
												<button type="button" class="btn btn-success float-right send_push_notification_btn" data-toggle="modal" data-target="#PlaylistNotificationModal"><i class="fa fa-save"></i>Send Push Notification</button>
											</div>
											<?php /* <div class="col">
												<button type="button" class="btn btn-success float-right send_push_notification_btn" data-toggle="modal" data-target="#NPlaylistNotificationModalTest"><i class="fa fa-save"></i>FOR TEST</button>
											</div> */ ?>
										</div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>

        

        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">
					<?php if(session()->has('message')): ?>
					<script type="text/javascript">
					jQuery(document).ready(function($){
					        $.notify({
					            icon: "nc-icon nc-app",
					            message: '<?php echo e(session()->get('message')); ?>',
					        }, {
					            type: 'success',
					            timer: 8000,
					        });
					 });
					 </script>
					<?php endif; ?>
					<?php if($errors->any()): ?>
						  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<script type="text/javascript">
								jQuery(document).ready(function($){
								        $.notify({
								            icon: "nc-icon nc-app",
								            message: '<?php echo e($error); ?>',
								        }, {
								            type: 'danger',
								            timer: 8000,
								        });												
								});
								 </script>
						  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					  <?php endif; ?>
						<div class="card"> 
							<!-- <div class="card-header">
								<strong class="card-title">Edit Playlist</strong>
							</div> -->
							<div class="card-body row">
								 <?php if($errors->any()): ?>
									  <div class="alert alert-danger">
										  <ul>
											  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												  <li><?php echo e($error); ?></li>
											  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										  </ul>
									  </div><br/>
								  <?php endif; ?>
								<div class="col-md-8">
									<form method="POST" action="<?php echo URL::to('/admin/play-lists/editdata'); ?>" class="artist_edit_form"  enctype="multipart/form-data">
									 <?php echo e(csrf_field()); ?>

									
									<div class="form-group row"><label for="name" class="col-md-4 col-form-label">Title</label>
										<div class="col-md-6"><input id="title" name="title" value="<?php echo e($playlist->title); ?>" required="required" autofocus="autofocus" class="form-control" type="text"></div>
									</div>
									<?php if($errors->has('title')): ?>
	                                    <span class="invalid-feedback">
	                                        <strong><?php echo e($errors->first('title')); ?></strong>
	                                    </span>
	                                <?php endif; ?>
	                                 <div class="form-group row"><label for="description" class="col-md-4 col-form-label">Description</label>
										<div class="col-md-6"><textarea name="description" class="form-control" placeholder="Description"><?php echo e($playlist->description); ?></textarea></div>
									</div>
									<div class="form-group row"><label for="image" class="col-md-4 col-form-label">Image</label>
										<div class="col-md-6"> <input id="image" name="image" value="" required="required" autofocus="autofocus" class="form-control" type="file"></div>
									</div>
									<?php if($errors->has('image')): ?>
	                                    <span class="invalid-feedback">
	                                        <strong><?php echo e($errors->first('image')); ?></strong>
	                                    </span>
	                                <?php endif; ?>
	                                <div class="form-group row"><label for="musics" class="col-md-4 col-form-label">Musics</label>
		                                <div class="col-md-6">
			                                <select class="js-example-basic-multiple form-control" name="music_id[]" value="" multiple="multiple">
											  <?php $__currentLoopData = $musics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $music): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											  	<option value="<?php echo e($music->id); ?>" <?php if(in_array($music->id,collect($playlist->musics)->pluck('id')->all())): ?> selected <?php endif; ?>><?php echo e($music->title); ?>-<?php echo e($music->artist); ?></option>
											  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											 
											</select>
										</div>
									</div>


									<div class="form-group row"><label for="musics" class="col-md-4 col-form-label">Artist Featured</label>
										<div class="col-md-6">
											
											  <?php $__currentLoopData = $featured_artist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $artist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											  	<span class="badge badge-pill badge-info" style="margin-bottom: 5px;font-size : 1rem;"><?php echo e($artist->artist_name); ?></span>
											  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											 
											
										</div>	
									</div>	
					                        
										
										<div class="form-group row mb-0 user_form_submit">
											<div class="col-md-6 offset-md-4">
												<input type="hidden" name="playlist_id" value="<?php echo e($playlist->id); ?>" /> 
												<a class="btn btn-primary update_artist_btn">Update Playlist</a>
												<input value="1" <?php if($playlist->is_pushed == 1): ?> checked="checked" <?php endif; ?> type="checkbox" id="isPushed" name="is_pushed"><label for="dob" class="col-form-label">Do not send notification</label>
											</div>
										</div>
									</form>
									
									
								</div>
								<div class="col-md-4">
									<div class="card">
				                            <div class="card-image">
				                                <img src="<?php echo e($playlist->image); ?>" class="artist_image">
				                            </div>
				                            <div class="card-body">
				                            		<h3 class="title"><?php echo e($playlist->title); ?></h3>
				                           	</div>
				                            <hr>
				                    </div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header">
								<strong class="card-title">Reorder Playlist Musics</strong>
							</div>
							 <div class="card-body row">
							 	<?php 
							 		$playlist_music = $playlist->musics->pluck('id','title')->toArray();
							 	?>
							 	<div class="row playlist_music_drag" id="sortable">
	                        		<ul id="items" data-album-id="<?php echo e($playlist->id); ?>"> 
	                        		<?php $__currentLoopData = $playlist_music; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $title => $id): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        				<li class="list" data-music-id="<?php echo e($id); ?>">
			                        		<?php echo $title ?>
			                        		<i class="fa fa-arrows" aria-hidden="true"></i>
			                        	</li>
	                        		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	                        		</ul>
	                        	</div>
							 </div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

<!-- Modal -->
<div class="modal fade" id="PlaylistNotificationModal" role="dialog">
    <div class="modal-dialog">
    	<form method="POST" action="<?php echo URL::to('/admin/playlist/noti'); ?>" class="push_notification_form">
    		<?php echo e(csrf_field()); ?>

	        <div class="modal-content">
	            <!-- Modal Header -->
	            <!-- Modal Body -->
	            <div class="modal-body">
                    <div class="form-group">
                        <label for="inputName">Type</label><br/>
                        <label class="radio-inline"><input type="radio" name="notificationtype" value="default" checked>Default</label>
						<label class="radio-inline"><input type="radio" name="notificationtype" value="custom">Custom</label>
						
                    </div>

                    

					<input type="hidden" name="playlist_id" value="<?php echo e($playlist->id); ?>" /> 


                    <div class="for_custom" style="display: none;">
                    	
						<div class="form-group">
	                        <label for="inputEmail">Title</label>
	                        <p class="lead emoji-picker-container">
				              <textarea class="form-control" rows="10" data-emojiable="true" data-emoji-input="unicode" name="playlist_title"><?php echo e($playlist->title); ?></textarea>
				            </p>
	                    </div>
	                    <div class="form-group">
	                        <label for="inputMessage">Description</label>
	                        <p class="lead emoji-picker-container">
				              <textarea class="form-control" rows="10" data-emojiable="true" data-emoji-input="unicode" name="playlist_description"><?php echo e($playlist->description); ?></textarea>
				            </p>
	                    </div>
	                </div>
	            </div>
	            
	            <!-- Modal Footer -->
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                <input type="submit" name="submit" class="btn btn-primary" value="SUBMIT">
	            </div>
	        </div>
	    </form>    
    </div>
</div>        

<?php $__env->stopSection(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('admin_assets/plugins/select2/select2.min.css')); ?>">
<script src="<?php echo e(asset('admin_assets/js/vendor/jquery-2.1.4.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/plugins/select2/select2.min.js')); ?>"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>

<script type="text/javascript">
	
    $(document).ready(function() {
	    $('.js-example-basic-multiple').select2(); 

	    $('input[name=notificationtype]').change(function(){
		var value = $( 'input[name=notificationtype]:checked' ).val();
		if(value == 'custom'){
			$('.for_custom').css("display","block");
		}
		else if(value == 'deafult'){
			$('.for_custom').css("display","none");
		}
		else{
			$('.for_custom').css("display","none");	
		}
		});

		$('input[name=notificationtypetest]').change(function(){
		var value = $( 'input[name=notificationtypetest]:checked' ).val();
		if(value == 'custom'){
			$('.for_custom_test').css("display","block");
		}
		else if(value == 'deafult'){
			$('.for_custom_test').css("display","none");
		}
		else{
			$('.for_custom_test').css("display","none");	
		}
		});
	    
	    $("#items").sortable({
                start: function (event, ui) {
                        ui.item.toggleClass("highlight");
                        console.log(000000);
                },
                stop: function (event, ui) {
                        ui.item.toggleClass("highlight");
                        console.log(111111);
                        var music_order = [];
                        var albumid = jQuery('#sortable ul#items').attr('data-album-id');
                        jQuery('#sortable ul#items .list').each(function(){
                        	music_order.push(jQuery(this).attr('data-music-id'));
                        });
                        console.log(music_order);
                        $.ajax({
				       	    beforeSend: function(xhrObj){
					               xhrObj.setRequestHeader("X-CSRF-TOKEN",jQuery(document).find('input[name="_token"]').val()); 
					        },
						   url: site_url+"/admin/playlist/musics/reorder",
						   data: {playlist	: albumid,music : music_order},
						   success: function(data) {
						   		console.log(data);
						   },
						   type: 'POST'
						});
                        
                }
        });
        $("#items").disableSelection();
	    $('.js-example-basic-multiple').select2({
	    	placeholder: "Select an Artist",
    		allowClear: true
	    });

	    $(function() {
	        // Initializes and creates emoji set from sprite sheet
	        window.emojiPicker = new EmojiPicker({
	          emojiable_selector: '[data-emojiable=true]',
	          assetsPath: '<?php echo e(asset('admin_assets/plugins/emoji')); ?>' +'/lib/img/',
	          popupButtonClasses: 'fa fa-smile-o'
	        });
	        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
	        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
	        // It can be called as many times as necessary; previously converted input fields will not be converted again
	        window.emojiPicker.discover();
	      });
	});
</script>

<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>