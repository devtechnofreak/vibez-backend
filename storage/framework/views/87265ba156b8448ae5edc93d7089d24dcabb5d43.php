<!-- Left Panel -->
<?php /**
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="<?php echo URL::to('/admin'); ?>">Vibez Music Admin</a>
                <a class="navbar-brand hidden" href="<?php echo URL::to('/admin'); ?>"><img src="{{ asset('admin_assets/images/logo2.png') }}" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="<?php echo URL::to('/admin'); ?>"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="<?php echo URL::to('/admin/artists'); ?>" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Artists</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/artists'); ?>">All Artists</a></li>
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/artist/add'); ?>">Add New</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="<?php echo URL::to('/admin/albums'); ?>" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Albums</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/albums'); ?>">All Albums</a></li>
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/album/add'); ?>">Add New</a></li>
                        </ul>
                    </li>
					<li class="menu-item-has-children dropdown">
                        <a href="<?php echo URL::to('/admin/musics'); ?>" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Musics</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/musics'); ?>">All Music</a></li>
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/musics/add'); ?>">Add New</a></li>
                        </ul>
                    </li>
                    
                    <li class="menu-item-has-children dropdown">
                        <a href="<?php echo URL::to('/admin/play-lists'); ?>" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Play Lists</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/play-lists'); ?>">All Play Lists</a></li>
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/play-lists/add'); ?>">Add Play List</a></li>
                        </ul>
                    </li>
                    
                    <li class="menu-item-has-children dropdown">
                        <a href="<?php echo URL::to('/admin/news'); ?>" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>News</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/news'); ?>">All News</a></li>
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/news/add'); ?>">Add New</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="<?php echo URL::to('/admin/banners'); ?>" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Banners</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/banners'); ?>">All Banners</a></li>
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/banners/add'); ?>">Add Banner</a></li>
                        </ul>
                    </li>
                    
                    <li class="menu-item-has-children dropdown">
                        <a href="<?php echo URL::to('/admin/videos'); ?>" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Videos</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/videos'); ?>">All Videos</a></li>
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/videos/add'); ?>">Add Video</a></li>
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/categories'); ?>">All Categories</a></li>
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/category/add'); ?>">Add New</a></li>
                        </ul>
                    </li>
					<li class="menu-item-has-children dropdown">
                        <a href="<?php echo URL::to('/admin/tags'); ?>" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Tags</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/tags'); ?>">All Tags</a></li>
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/tag/add'); ?>">Add New</a></li>
                        </ul>
                    </li>
					<li class="menu-item-has-children dropdown">
                        <a href="<?php echo URL::to('/admin/subscriptions'); ?>" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Subscription Plans</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/subscriptions'); ?>">All Plans</a></li>
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/subscription/add'); ?>">Add New</a></li>
                        </ul>
                    </li>
                    
                    <li class="menu-item-has-children dropdown">
                        <a href="<?php echo URL::to('/admin/users'); ?>" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Staff Users</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/users'); ?>">All Staff Users</a></li>
                            <li><i class="fa fa-user"></i><a href="<?php echo URL::to('/admin/user/add'); ?>">Add New</a></li>
                        </ul>
                    </li>
                    
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->
 */?>
<div class="sidebar-wrapper">
    <div class="logo">
        <div class="row">
            <div class="col-md-9">
                <a href="<?php echo e(url('admin')); ?>" class="simple-text">
                    <img src="<?php echo e(asset('assets/icons/vibez-logo.png')); ?>" alt="<?php echo e('Vibez App'); ?>"/>
                </a>
            </div>
            <div class="col-md-3">
                <a class="logouticon" href="<?php echo e(route('admin.logout')); ?>">
                    <img src="<?php echo e(asset('assets/icons/logout.png')); ?>" alt="<?php echo e('Logout'); ?>" title="Logout" width="25" />
                </a>
            </div>
        </div>
        
    </div>
    <nav class="navbar navbar-expand-sm navbar-default">
    <div id="main-menu" class="main-menu">
    <ul class="nav navbar-nav">
        <li class="active">
            <a class="nav-link" href="<?php echo e(url('admin')); ?>">
                <i class="nc-icon nc-chart-pie-35"></i>
                <p>Dashboard</p>
            </a>
        </li>
        <?php 
        	$current_user_id = Auth::user()->id;
            if($current_user_id == '1902')
            { ?>
            <li class="menu-item-has-children dropdown">
                <a href="<?php echo e(url('admin/artists')); ?>" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img class="icons" src="<?php echo e(asset('assets/icons/Artists.png')); ?>" alt="artists" /><p>Artist</p></a>
                 <ul class="sub-menu children dropdown-menu">
                    <li><a class="nav-link" href="<?php echo e(url('admin/artists')); ?>"><i class="nc-icon nc-notes"></i><p>Artist List</p></a></li>
                    <li><a class="nav-link" href="<?php echo e(url('admin/artist/add')); ?>"><i class="nc-icon nc-simple-add"></i><p>Add New</p></a></li>
                    <li><a class="nav-link" href="<?php echo e(url('admin/artist/import/bulk')); ?>"><i class="fa fa-plus"></i> Bulk Import Artists</a></li>
                </ul>
            </li> 
            
            <li class="menu-item-has-children dropdown">
                <a href="<?php echo e(url('admin/news')); ?>" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img class="icons" src="<?php echo e(asset('assets/icons/News.png')); ?>" alt="news" /><p>News</p></a>
                 <ul class="sub-menu children dropdown-menu">
                    <li><a class="nav-link" href="<?php echo e(url('admin/news')); ?>"><i class="nc-icon nc-notes"></i><p>News List</p></a></li>
                    <li><a class="nav-link" href="<?php echo e(url('admin/news/add')); ?>"><i class="nc-icon nc-simple-add"></i><p>Add New</p></a></li>
                </ul>
            </li>   

            <?php } else {
        ?>
        <li class="menu-item-has-children dropdown">
        	<a href="<?php echo e(url('admin/artists')); ?>" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img class="icons" src="<?php echo e(asset('assets/icons/Artists.png')); ?>" alt="artists" /><p>Artist</p></a>
             <ul class="sub-menu children dropdown-menu">
             	<li><a class="nav-link" href="<?php echo e(url('admin/artists')); ?>"><i class="nc-icon nc-notes"></i><p>Artist List</p></a></li>
                <li><a class="nav-link" href="<?php echo e(url('admin/artist/add')); ?>"><i class="nc-icon nc-simple-add"></i><p>Add New</p></a></li>
                <li><a class="nav-link" href="<?php echo e(url('admin/artist/import/bulk')); ?>"><i class="fa fa-plus"></i> Bulk Import Artists</a></li>
            </ul>
        </li>
        <li class="menu-item-has-children dropdown">
        	<a href="<?php echo e(url('admin/albums')); ?>" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img class="icons" src="<?php echo e(asset('assets/icons/Albums.png')); ?>" alt="albums" /><p>Album</p></a>
             <ul class="sub-menu children dropdown-menu">
             	<li><a class="nav-link" href="<?php echo e(url('admin/albums')); ?>"><i class="nc-icon nc-notes"></i><p>Album List</p></a></li>
                <li><a class="nav-link" href="<?php echo e(url('admin/album/add')); ?>"><i class="nc-icon nc-simple-add"></i><p>Add New</p></a></li>
                <li><a href="<?php echo e(url('admin/album/import/bulk')); ?>" class="nav-link"><i class="fa fa-plus"></i> Bulk Import Albums</a></li>
            </ul>
        </li>
        
         <li class="menu-item-has-children dropdown">
        	<a href="<?php echo e(url('admin/announcement')); ?>" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img class="icons" src="<?php echo e(asset('assets/icons/Staff-Users.png')); ?>" alt="users" /><p>Announcement</p></a>
             <ul class="sub-menu children dropdown-menu">
             	<li><a class="nav-link" href="<?php echo e(url('admin/announcement')); ?>"><i class="nc-icon nc-notes"></i><p>Announcement List</p></a></li>
                <li><a class="nav-link" href="<?php echo e(url('admin/announcement/add')); ?>"><i class="nc-icon nc-simple-add"></i><p>Add New</p></a></li>
            </ul>
        </li> 
        
       <!--  <li class="menu-item-has-children dropdown">
        	<a href="<?php echo e(url('admin/musics')); ?>" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img class="icons" src="<?php echo e(asset('assets/icons/Musics.png')); ?>" alt="albums" /><p>Music</p></a>
             <ul class="sub-menu children dropdown-menu">
             	<li><a class="nav-link" href="<?php echo e(url('admin/musics')); ?>"><i class="nc-icon nc-notes"></i><p>Music List</p></a></li>
                <li><a class="nav-link" href="<?php echo e(url('admin/music/add')); ?>"><i class="nc-icon nc-simple-add"></i><p>Add New</p></a></li>
            </ul>
        </li> -->
        
        <li class="menu-item-has-children dropdown">
        	<a href="<?php echo e(url('admin/banners')); ?>" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img class="icons" src="<?php echo e(asset('assets/icons/Banners.png')); ?>" alt="banners" /><p>Banner</p></a>
             <ul class="sub-menu children dropdown-menu">
             	<li><a class="nav-link" href="<?php echo e(url('admin/banners')); ?>"><i class="nc-icon nc-notes"></i><p>Banner List</p></a></li>
                <li><a class="nav-link" href="<?php echo e(url('admin/banners/add')); ?>"><i class="nc-icon nc-simple-add"></i><p>Add New</p></a></li>
            </ul>
        </li>
		<li class="menu-item-has-children dropdown">
        	<a href="<?php echo e(url('admin/live-streams')); ?>" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img class="icons" src="<?php echo e(asset('assets/icons/Musics.png')); ?>" alt="live-stream" /><p>LiveStream</p></a>
             <ul class="sub-menu children dropdown-menu">
             	<li><a class="nav-link" href="<?php echo e(url('admin/live-streams')); ?>"><i class="nc-icon nc-notes"></i><p>LiveStream List</p></a></li>
                <li><a class="nav-link" href="<?php echo e(url('admin/live-stream/add')); ?>"><i class="nc-icon nc-simple-add"></i><p>Add New</p></a></li>
            </ul>
        </li>
        <li class="menu-item-has-children dropdown">
        	<a href="<?php echo e(url('admin/manual-payment')); ?>" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img class="icons" src="<?php echo e(asset('assets/icons/Staff-Users.png')); ?>" alt="live-stream" /><p>Manual Payment</p></a>
             <ul class="sub-menu children dropdown-menu">
             	<li><a class="nav-link" href="<?php echo e(url('admin/manual-payment')); ?>"><i class="nc-icon nc-notes"></i><p>Manual Payments</p></a></li>
            </ul>
        </li>
        
        <li class="menu-item-has-children dropdown">
        	<a href="<?php echo e(url('admin/news')); ?>" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img class="icons" src="<?php echo e(asset('assets/icons/News.png')); ?>" alt="news" /><p>News</p></a>
             <ul class="sub-menu children dropdown-menu">
             	<li><a class="nav-link" href="<?php echo e(url('admin/news')); ?>"><i class="nc-icon nc-notes"></i><p>News List</p></a></li>
                <li><a class="nav-link" href="<?php echo e(url('admin/news/add')); ?>"><i class="nc-icon nc-simple-add"></i><p>Add New</p></a></li>
            </ul>
        </li>
        
        <li class="menu-item-has-children dropdown">
        	<a href="<?php echo e(url('admin/pages')); ?>" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img class="icons" src="<?php echo e(asset('assets/icons/Tags.png')); ?>" alt="Pages" /><p>Page</p></a>
             <ul class="sub-menu children dropdown-menu">
             	<li><a class="nav-link" href="<?php echo e(url('admin/pages')); ?>"><i class="nc-icon nc-notes"></i><p>Page List</p></a></li>
                <li><a class="nav-link" href="<?php echo e(url('admin/page/add')); ?>"><i class="nc-icon nc-simple-add"></i><p>Add Page</p></a></li>
            </ul>
        </li>
        
         <li class="menu-item-has-children dropdown">
        	<a href="<?php echo e(url('admin/play-lists')); ?>" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img class="icons" src="<?php echo e(asset('assets/icons/Play-Lists.png')); ?>" alt="playlists" /><p>Playlist</p></a>
             <ul class="sub-menu children dropdown-menu">
             	<li><a class="nav-link" href="<?php echo e(url('admin/play-lists')); ?>"><i class="nc-icon nc-notes"></i><p>Playlists</p></a></li>
                <li><a class="nav-link" href="<?php echo e(url('admin/play-lists/add')); ?>"><i class="nc-icon nc-simple-add"></i><p>Add New</p></a></li>
            </ul>
        </li>
        <li class="menu-item-has-children dropdown">
        	<a href="<?php echo e(url('admin/promocode')); ?>" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img style="width : 24px;" class="icons" src="<?php echo e(asset('assets/icons/coupon.png')); ?>" alt="users" /><p>Promocode</p></a>
             <ul class="sub-menu children dropdown-menu">
             	<li><a class="nav-link" href="<?php echo e(url('admin/promocode')); ?>"><i class="nc-icon nc-notes"></i><p>Promocode List</p></a></li>
                <li><a class="nav-link" href="<?php echo e(url('admin/promocode/add')); ?>"><i class="nc-icon nc-simple-add"></i><p>Add New</p></a></li>
            </ul>
        </li> 
         <li class="menu-item-has-children dropdown">
        	<a href="<?php echo e(url('admin/reports')); ?>" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img class="icons" src="<?php echo e(asset('assets/icons/News.png')); ?>" alt="reports" /><p>Report</p></a>
             <ul class="sub-menu children dropdown-menu">
             	<li><a class="nav-link" href="<?php echo e(url('admin/reports')); ?>"><i class="nc-icon nc-notes"></i><p>Reports</p></a></li>
            </ul>
        </li>
        <li class="menu-item-has-children dropdown">
        	<a href="<?php echo e(url('admin/users')); ?>" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img class="icons" src="<?php echo e(asset('assets/icons/Staff-Users.png')); ?>" alt="users" /><p>User</p></a>
             <ul class="sub-menu children dropdown-menu">
             	<li><a class="nav-link" href="<?php echo e(url('admin/users')); ?>"><i class="nc-icon nc-notes"></i><p>User List</p></a></li>
             	<li><a class="nav-link" href="<?php echo e(url('admin/help')); ?>"><i class="nc-icon nc-notes"></i><p>User Help</p></a></li>
                <li><a class="nav-link" href="<?php echo e(url('admin/user/add')); ?>"><i class="nc-icon nc-simple-add"></i><p>Add New</p></a></li>
            </ul>
        </li>
        <li class="menu-item-has-children dropdown">
        	<a href="<?php echo e(url('admin/eventcodes')); ?>" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img style="width : 24px;" class="icons" src="<?php echo e(asset('assets/icons/coupon.png')); ?>" alt="users" /><p>Event Codes</p></a>
             <ul class="sub-menu children dropdown-menu">
             	<li><a class="nav-link" href="<?php echo e(url('admin/eventcodes')); ?>"><i class="nc-icon nc-notes"></i><p>Event Codes List</p></a></li>
                <li><a class="nav-link" href="<?php echo e(url('admin/eventcodes/add')); ?>"><i class="nc-icon nc-simple-add"></i><p>Add New</p></a></li>
            </ul>
        </li> 
        
        <li class="menu-item-has-children dropdown">
        	<a href="<?php echo e(url('admin/videos')); ?>" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img class="icons" src="<?php echo e(asset('assets/icons/Videos.png')); ?>" alt="videos" /><p>Video</p></a>
             <ul class="sub-menu children dropdown-menu">
             	<li><a class="nav-link" href="<?php echo e(url('admin/videos')); ?>"><i class="nc-icon nc-notes"></i><p>Video List</p></a></li>
                <li><a class="nav-link" href="<?php echo e(url('admin/videos/add')); ?>"><i class="nc-icon nc-simple-add"></i><p>Add New</p></a></li>
                <li><a class="nav-link" href="<?php echo e(url('admin/categories')); ?>"><i class="nc-icon nc-notes"></i><p>Category</p></a></li>
                <li><a class="nav-link" href="<?php echo e(url('admin/category/add')); ?>"><i class="nc-icon nc-simple-add"></i><p>Add Category</p></a></li>
                <li><a href="<?php echo e(url('admin/video/import/bulk')); ?>" class="nav-link"><i class="fa fa-plus"></i> Bulk Import Videos</a></li>
            </ul>
        </li>


         <li>
            <a class="nav-link" href="<?php echo e(url('admin/export/all')); ?>">
                <i class="nc-icon nc-chart-pie-35"></i>
                <p>Export</p>
            </a>
        </li>
       <?php } ?>
       
       <!--  <li class="nav-item active active-pro">
            <a class="nav-link active" href="upgrade.html">
                <i class="nc-icon nc-alien-33"></i>
                <p>Upgrade to PRO</p>
            </a>
        </li> -->
    </ul>
    </div>
    </nav>
</div>
