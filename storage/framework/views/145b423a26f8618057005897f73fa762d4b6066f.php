<div class="header" style="padding: 25px 0;background-color: #ed213a;text-align: center;">
	<img style="width:130px;" src="https://www.thevibez.net/assets/img/vibezlogo.png"> 
</div>
<?php 
$today=date('Y-m-d');
$event=date('2018-12-16');

if($event > $today) { ?>
<table style="width:700px;margin: 0 auto;padding: 50px 0;">
	<tr>
		<td>
			<h1>Dear <?php echo $userData['user']->name; ?>,</h1>
			<p style="font-size:15px;">Congratulations, You've successfully subscribed with Vibez</p>
			<p style="font-size:15px;">Your subscription also grants you a ticket to enjoy the Born in Africa Festival on the 16th December at Eko Atlantic Boulevard.</p>
			<p style="font-size:15px;">Please use the code <strong>[<?php echo $userData['event_code']->code; ?>]</strong> at the venue to redeem your ticket.</p>
		</td>		
	</tr>
	<tr>
		<td>
			<table style="width:100%;margin-top:20px;margin-bottom: 30px;border: 1px solid #CCC;" border="1">
				<thead style="border: 1px solid #CCC;">
					<tr style="border: 1px solid #CCC;">
						<th style="padding-top:7px;padding-bottom: 7px;border: 1px solid #CCC;">Name</th>
						<?php /* <th style="padding-top:7px;padding-bottom: 7px;border: 1px solid #CCC;">Event Code</th> */ ?>
						<th style="padding-top:7px;padding-bottom: 7px;border: 1px solid #CCC;">Subscription Date</th>
						<th style="padding-top:7px;padding-bottom: 7px;border: 1px solid #CCC;">Expiry Date</th>
					</tr>
				</thead>
				<tbody style="border: 1px solid #CCC;">
					<tr style="border: 1px solid #CCC;">
						<th style="padding-top:7px;padding-bottom: 7px;border: 1px solid #CCC;"><?php echo $userData['data']['plan']; ?></th>
						<?php /* <th style="padding-top:7px;padding-bottom: 7px;border: 1px solid #CCC;"><?php echo $userData['event_code']->code; ?></th> */ ?>
						<th style="padding-top:7px;padding-bottom: 7px;border: 1px solid #CCC;"><?php echo date('Y-m-d'); ?></th>
						<th style="padding-top:7px;padding-bottom: 7px;border: 1px solid #CCC;"><?php echo $userData['data']['end_date']; ?></th>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-size:15px;">Cheers!<br><img src="https://www.thevibez.net/assets/img/vibez_name.png" style="width:65px"></p>
		</td>
	</tr>
</table>	
<?php 
}else{ ?>
	<table style="width:700px;margin: 0 auto;padding: 50px 0;">
	<tr>
		<td>
			<h1>Dear <?php echo $userData['user']->name; ?>,</h1>
			<p style="font-size:15px;">Congratulations, You've successfully subscribed with Vibez</p>
			
		</td>		
	</tr>
	<tr>
		<td>
			<table style="width:100%;margin-top:20px;margin-bottom: 30px" border="1">
				<thead>
					<tr>
						<th style="padding-top:7px;padding-bottom: 7px;">Name</th>
						<?php /* <th style="padding-top:7px;padding-bottom: 7px;">Event Code</th> */ ?>
						<th style="padding-top:7px;padding-bottom: 7px;">Subscription Date</th>
						<th style="padding-top:7px;padding-bottom: 7px;">Expiry Date</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th style="padding-top:7px;padding-bottom: 7px;"><?php echo $userData['data']['plan']; ?></th>
						<?php /* <th style="padding-top:7px;padding-bottom: 7px;"><?php echo $userData['event_code']->code; ?></th> */ ?>
						<th style="padding-top:7px;padding-bottom: 7px;"><?php echo date('Y-m-d'); ?></th>
						<th style="padding-top:7px;padding-bottom: 7px;"><?php echo $userData['data']['end_date']; ?></th>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-size:15px;">Enjoy the Vibez with your favourite African artists and discover new talent as we scour the continent to bring you the best Africa has to offer.</p>	
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-size:15px;">Cheers!<br><img src="https://www.thevibez.net/assets/img/vibez_name.png" style="width:65px"></p>
		</td>
	</tr>
</table>
<?php }
?>

<div class="35" style="padding: 35px 0;background-color: #ed213a;text-align: center;color : #fff;">
	© <?php echo date('Y'); ?> Vibez. All rights reserved. 
</div>