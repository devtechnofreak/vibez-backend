<?php $__env->startSection('main_content'); ?>
<!-- Header-->
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li class="active">Edit User</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-4">
			                        		<strong class="card-title user-name">Feedback</strong>
			                        	</div>
			                            <div class="col-sm-8">	
			                            	<a href="<?php echo e(url('admin/reports')); ?>" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Reports</a>
			                            </div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>

        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">
					<?php if(session()->has('message')): ?>
					<script type="text/javascript">
					jQuery(document).ready(function($){
					        $.notify({
					            icon: "nc-icon nc-app",
					            message: '<?php echo e(session()->get('message')); ?>',
					        }, {
					            type: 'success',
					            timer: 8000,
					        });
					 });
					 </script>
					<?php endif; ?>
					<?php if($errors->any()): ?>
						  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<script type="text/javascript">
								jQuery(document).ready(function($){
								        $.notify({
								            icon: "nc-icon nc-app",
								            message: '<?php echo e($error); ?>',
								        }, {
								            type: 'danger',
								            timer: 8000,
								        });												
								});
								 </script>
						  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					  <?php endif; ?>
						<div class="card"> 
							<div class="card-body">
								
								<form method="POST" action="<?php echo URL::to('/admin/report/update'); ?>" enctype="multipart/form-data">
								 <?php echo e(csrf_field()); ?>

									<div class="form-group row"><label for="description" class="col-md-2 col-form-label text-md-left">Description</label>
										<div class="col-md-6"><textarea id="description" name="description" required="required" autofocus="autofocus" class="form-control" rows="5" style="height: auto !important"></textarea></div>
									</div>
									<?php if($errors->has('description')): ?>
										<span class="invalid-feedback">
											<strong><?php echo e($errors->first('description')); ?></strong>
										</span>
									<?php endif; ?>
									<div class="form-group row mb-0 user_form_submit">
										<div class="col-md-6 offset-md-4">
											<input type="hidden" name="report_id" value="<?php echo e($id); ?>" /> 
											<button type="submit" class="btn btn-primary">
												Submit
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>