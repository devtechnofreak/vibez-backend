
<?php $__env->startSection('main_content'); ?>
<!-- Header-->

        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>News</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="breadcrumbs navbar-top">
                        <div class="card">
                            <div class="card-header border-header">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <strong class="card-title newslist-name"><i class="fa fa-plus"></i> Add Promocode</strong>
                                        </div>
                                        <div class="col-md-9">
                                            <a href="<?php echo e(url('admin/promocode')); ?>" class="btn btn-primary float-right backbtn"><i class="fa fa-arrow-left"></i> Promocodes List</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>


        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
					<?php if(session()->has('message')): ?>
						<div class="alert alert-success">
							<?php echo e(session()->get('message')); ?>

						</div>
					<?php endif; ?>
                    <div class="card"> 
                        <!-- <div class="card-header border-header">
                            <strong class="card-title newslist-name">Add News</strong>
                            <a href="<?php echo e(url('admin/news')); ?>" class="btn btn-primary float-right backbtn"><i class="fa fa-arrow-left"></i> News List</a>
                        </div> -->
						<div class="card-body">
							 <?php if($errors->any()): ?>
								  <div class="alert alert-danger">
									  <ul>
										  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											  <li><?php echo e($error); ?></li>
										  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									  </ul>
								  </div><br/>
							  <?php endif; ?>
							<form method="POST" action="<?php echo URL::to('/admin/promocode/create'); ?>" enctype="multipart/form-data" class="add_news">
							 	<?php echo e(csrf_field()); ?>

							 	<div class="form-group row"><label for="artist" class="col-md-4 col-form-label">Artist</label>
									<div class="col-md-6">
										<select class="js-example-basic-multiple form-control" name="artist_id" value="" required>
											<option value="">Select Artist</option>
											<?php $__currentLoopData = $artists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $artist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											  <option name="artist_id" value="<?php echo e($artist->id); ?>"><?php echo e($artist->name); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
									</div> 
								</div>
								<div class="form-group row"><label for="title" class="col-md-4 col-form-label">Code</label>
									<div class="col-md-6"><input id="code" name="code" value="" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								<div class="form-group row"><label for="start_time" class="col-md-4 col-form-label">Start Date</label>
									<div class="col-md-6"><input type="text" class="form-control datepicker" name="start_date" autocomplete="off" required></div>
								</div>
								<div class="form-group row"><label for="start_time" class="col-md-4 col-form-label">End Date</label>
									<div class="col-md-6"><input type="text" class="form-control datepicker" name="end_date" autocomplete="off"></div>
								</div>
								<div class="form-group row mb-0">
									<div class="col-md-6 offset-md-4">
										<a class="btn btn-primary add_news_btn">Add Promocode</a>
									</div>
								</div>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('admin_assets/plugins/select2/select2.min.css')); ?>">
		<!-- <script src="<?php echo e(asset('admin_assets/js/vendor/jquery-2.1.4.min.js')); ?>"></script> -->
		<script src="<?php echo e(asset('admin_assets/plugins/select2/select2.min.js')); ?>"></script>
		<script type="text/javascript">
		    $(document).ready(function() {
			    $('.js-example-basic-multiple').select2({
			    	placeholder: "Select a item",
		    		allowClear: true
			    });
			    $('.datepicker').datepicker({
					dateFormat: "dd-mm-yy",
					changeYear: true,
					changeMonth: true
				});
			}); 
		</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>