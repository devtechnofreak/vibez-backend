<?php $__env->startSection('main_content'); ?>
<!-- Header-->
		
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>News</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-4">
			                        		<strong class="card-title newslist-name">Page List</strong>
			                        	</div>
			                            <div class="col-sm-8">	
			                            	<a class="btn btn-primary float-right ml-2" href="<?php echo URL::to('/admin/page/add'); ?>"><strong class="card-title"><i class="fa fa-plus"></i> Add New</strong></a>
			                            </div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>



        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <!-- <div class="card-header border-header">
                            <strong class="card-title newslist-name">News List</strong>
                             <div class="float-right">
                            	<a class="btn btn-primary float-right ml-2" href="<?php echo URL::to('/admin/news/add'); ?>"><strong class="card-title"><i class="fa fa-plus"></i> Add New</strong></a>
                            </div>
                        </div> -->
                        <div class="card-body">
							<?php if(session()->has('message')): ?>
								<div class="alert alert-success">
									<?php echo e(session()->get('message')); ?>

								</div>
							<?php endif; ?>
							<table id="news_list" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>Title</th>
										<th>Slug</th>
										<th>Posted</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>Title</th>
										<th>Slug</th>
										<th>Posted</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
                        </div>
                    </div>
                </div>
				<input type="hidden" class="csrf_token" value="<?php echo e(csrf_token()); ?>">
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	<script src="<?php echo e(asset('admin_assets/js/lib/data-table/datatables.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/dataTables.buttons.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/buttons.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/jszip.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/pdfmake.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/vfs_fonts.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/buttons.html5.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/buttons.print.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/buttons.colVis.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/datatables-init.js')); ?>"></script>


<script type="text/javascript">
	$(document).ready(function() {
		var csrf_token = jQuery(document).find('.csrf_token').val();
		$('#news_list').DataTable( {
			"processing": true,
			"serverSide": false,
			"bAutoWidth": false,
			"pageLength": 50,
			"ajax": {
				"url": "page/getpages",
				"type": "POST",
				"dataType": 'json',
                "error": function(data){
                        console.log(data);
                    },
				"headers" : {
						'X-CSRF-TOKEN': csrf_token
				}
			},
			"columns": [
				{ "data": "title" },
				{ "data": "slug" },
				{ "data": "created_at" },
				{ "data": "id",     render: function ( data, type, row ) {
						var editlink = "<?php echo URL::to('/admin/page/edit/"+data+"'); ?>";
						var deletelink = "<?php echo URL::to('/admin/page/delete/"+data+"'); ?>";
						return '<li class="libtnlink"><a class="text-warning ml-2" href="'+editlink+'"><i class="fa fa-pencil"></i></a></li><li class="libtnlink"><a href="#confirmationDelete" class="btn btn-danger delete-smt-btn delete_link text-danger ml-2" data-toggle="modal" data-id="'+data+'" data-url="'+deletelink+'" data-target="#confirmationDelete"><i class="fa fa-trash"></i></a></li>';
					
					return data;
				}}
			]
		} );
		
		
    } );
</script>	
<script type="text/javascript">
$(document).ready(function(){
	$('#news_list').on('click', '.delete-smt-btn', function(e){
		$('#confirmationDelete').modal("show");
		var id = $(this).attr('data-id');
		var url = $(this).attr('data-url');
		$(".del-link").attr("href",url);
	});
});
</script>
<!-- Delete Model -->

<!-- <form action="" method="POST" class="remove-record-model"> -->
    <div id="confirmationDelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;top: 25% !important;">
        <div class="modal-dialog modal-md modal-notify modal-danger" style="width:55%;">
            <div class="modal-content">
                <div class="modal-header">
                	<h4 class="modal-title" id="custom-width-modalLabel">Are you sure you want to delete this record?</h4>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect remove-data-from-delete-form" data-dismiss="modal">Close</button>

                    <a class="btn btn-default text-danger del-link" href="<?php echo URL::to('/admin/news/delete/'); ?>">Delete</a>
                </div>
            </div>
        </div>
    </div>
<!-- </form> -->
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>