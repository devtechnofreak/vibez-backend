<?php $__env->startSection('main_content'); ?>
<!-- Header-->
<?php 

?>
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Category</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-4">
			                        		<strong class="card-title categorylist-name">Edit Category</strong>
			                        	</div>
			                            <div class="col-sm-8">	
			                            	<a href="<?php echo e(url('admin/categories')); ?>" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Category List</a>
			                            </div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>

        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">
					<?php if(session()->has('message')): ?>
						<div class="alert alert-success">
							<?php echo e(session()->get('message')); ?>

						</div>
					<?php endif; ?>
						<div class="card"> 
							<div class="card-header">
								<strong class="card-title">Edit Category</strong>
							</div>
							<div class="card-body">
								 <?php if($errors->any()): ?>
									  <div class="alert alert-danger">
										  <ul>
											  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												  <li><?php echo e($error); ?></li>
											  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										  </ul>
									  </div><br/>
								  <?php endif; ?>
								<form method="POST" action="<?php echo URL::to('/admin/category/editdata'); ?>">
								 <?php echo e(csrf_field()); ?>

									<div class="form-group row"><label for="name" class="col-md-2 col-form-label text-md-left">Name</label>
										<div class="col-md-6"><input id="name" name="name" required="required" autofocus="autofocus" class="form-control" value="<?php echo e($category->name); ?>" type="text"></div>
									</div>
									<?php if($errors->has('name')): ?>
										<span class="invalid-feedback">
											<strong><?php echo e($errors->first('name')); ?></strong>
										</span>
									<?php endif; ?>
									<div class="form-group row"><label for="slug" class="col-md-2 col-form-label text-md-left">Slug</label>
										<div class="col-md-6"><input id="slug" name="slug" required="required" class="form-control" value="<?php echo e($category->slug); ?>" type="text"></div>
									</div>
									<?php if($errors->has('name')): ?>
										<span class="invalid-feedback">
											<strong><?php echo e($errors->first('name')); ?></strong>
										</span>
									<?php endif; ?>
									
									<div class="form-group row mb-0 user_form_submit">
										<div class="col-md-6 offset-md-4">
											<input type="hidden" name="category_id" value="<?php echo e($category->id); ?>" /> 
											<button type="submit" class="btn btn-primary">
												Update Category
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>