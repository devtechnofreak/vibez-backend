<?php $__env->startSection('main_content'); ?>
<!-- Header-->

        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>News</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->

         <style type="text/css">
            .table-wrapper {
                width: 700px;
                background: #fff;
                padding: 20px;  
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .table-title {
                padding-bottom: 10px;
                margin: 0 0 10px;
            }
            .table-title h2 {
                margin: 6px 0 0;
                font-size: 22px;
            }
            .table-title .add-new {
                float: right;
                height: 30px;
                font-weight: bold;
                font-size: 12px;
                text-shadow: none;
                min-width: 100px;
                border-radius: 50px;
                line-height: 13px;
            }
            .table-title .add-new i {
                margin-right: 4px;
            }
            table.table {
                table-layout: fixed;
            }
            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table th:last-child {
                width: 100px;
            }
            table.table td a {
                cursor: pointer;
                display: inline-block;
                margin: 0 5px;
                min-width: 24px;
            }    
            table.table td a.add {
                color: #27C46B;
            }
            table.table td a.edit {
                color: #FFC107;
            }
            table.table td a.delete {
                color: #E34724;
            }
            table.table td i {
                font-size: 19px;
            }
            table.table td a.add i {
                font-size: 24px;
                margin-right: -1px;
                position: relative;
                top: 3px;
            }    
            table.table .form-control {
                height: 32px;
                line-height: 32px;
                box-shadow: none;
                border-radius: 2px;
            }
            table.table .form-control.error {
                border-color: #f50000;
            }
            table.table td .add {
                display: block !important;
            }
        </style>

        <div class="breadcrumbs navbar-top">
                        <div class="card">
                            <div class="card-header border-header">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <strong class="card-title newslist-name"><i class="fa fa-plus"></i> Add News</strong>
                                        </div>
                                        <div class="col-md-9">
                                            <a href="<?php echo e(url('admin/news')); ?>" class="btn btn-primary float-right backbtn"><i class="fa fa-arrow-left"></i> News List</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>


        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
					<?php if(session()->has('message')): ?>
						<div class="alert alert-success">
							<?php echo e(session()->get('message')); ?>

						</div>
					<?php endif; ?>
                    <div class="card"> 
                        <!-- <div class="card-header border-header">
                            <strong class="card-title newslist-name">Add News</strong>
                            <a href="<?php echo e(url('admin/news')); ?>" class="btn btn-primary float-right backbtn"><i class="fa fa-arrow-left"></i> News List</a>
                        </div> -->
						<div class="card-body">
							 <?php if($errors->any()): ?>
								  <div class="alert alert-danger">
									  <ul>
										  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											  <li><?php echo e($error); ?></li>
										  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									  </ul>
								  </div><br/>
							  <?php endif; ?>
							<form method="POST" action="<?php echo URL::to('/admin/news/create'); ?>" enctype="multipart/form-data" class="add_news">
							 	<?php echo e(csrf_field()); ?>

								<div class="form-group row"><label for="name" class="col-md-4 col-form-label">Title</label>
									<div class="col-md-6"><input id="title" name="title" value="" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								<?php if($errors->has('title')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('title')); ?></strong>
                                    </span>
                                <?php endif; ?>
								<div class="form-group row"><label for="image" class="col-md-4 col-form-label">Image</label>
									<div class="col-md-6"><input id="image" name="image" value="" required="required" autofocus="autofocus" class="form-control" type="file"></div>
								</div>
								<?php if($errors->has('image')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('image')); ?></strong>
                                    </span>
                                <?php endif; ?>
                                
                                <div class="form-group row"><label for="description" class="col-md-4 col-form-label">Description</label>
									<div class="col-md-6"><textarea id="description" name="description" value="" required="required" autofocus="autofocus" class="form-control"></textarea></div>
								</div>
								<div class="form-group row"><label for="artist" class="col-md-4 col-form-label">Artist</label>
									<div class="col-md-6">
										<select class="js-example-basic-multiple form-control" name="artist_id[]" value="" multiple="multiple">
											<?php $__currentLoopData = $artists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $artist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											  <option name="artist_id" value="<?php echo e($artist->id); ?>"><?php echo e($artist->name); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
									</div> 
								</div>
                                <div class="form-group row">
                                	<label for="date_released" class="col-md-4 col-form-label">Date</label>
									<div class="col-md-6"><input type="text" class="form-control release_date" name="created_at" required></div>
								</div>
                                 <div class="form-group row"><label for="is_featured" class="col-md-4 col-form-label">Is Featured</label>
									<div class="col-md-6"><input value="1" type="checkbox" id="isFeatured" name="is_featured"></div> 
								</div>

                                <div class="form-group row">
                                    <label for="is_featured" class="col-md-4 col-form-label"><h5>Shortcode <b>Details</b></h5></label>
                                        <div class="table-wrapper col-md-6">
                                            <div class="table-title">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <button type="button" class="btn btn-info add-new"><i class="fa fa-plus"></i> Add New</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Type</th>
                                                        <th>Text</th>
                                                        <th>Shortcode</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><select class="form-control shcode_type" name="shcode_type"><option value="twitter">twitter</option><option value="instagram">instagram</option></select></td>
                                                        <td><input type="text" class="form-control" name="shcode_text" id="shcode_text"></td>
                                                        <td class="gc"><button type="button" class="btn btn-info add">Generate</button></td>
                                                        <td><a class="delete" title="Delete" data-toggle="tooltip"><i class="fa fa-trash"></i></a></td>
                                                    </tr>
                                                         
                                                </tbody>
                                                <input type="hidden" class="csrf_token" value="<?php echo e(csrf_token()); ?>">
                                                <input type="hidden" class="short_code_id" name="short_code_id[]">
                                            </table>
                                        </div>
                                    </div>
								
								<div class="form-group row mb-0">
									<div class="col-md-6 offset-md-4">
										<a class="btn btn-primary add_news_btn">Add News</a>
                                        <input value="1" type="checkbox" id="isPushed" name="is_pushed"><label for="dob" class="col-form-label">Do not send notification</label>
									</div>
								</div>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
<script src="<?php echo e(asset('admin_assets/plugins/ckeditor/ckeditor.js')); ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('admin_assets/plugins/select2/select2.min.css')); ?>">
<!-- <script src="<?php echo e(asset('admin_assets/js/vendor/jquery-2.1.4.min.js')); ?>"></script> -->
<script src="<?php echo e(asset('admin_assets/plugins/select2/select2.min.js')); ?>"></script>

<script type="text/javascript">
    $(document).ready(function() {
	    $('.js-example-basic-multiple').select2({
	    	placeholder: "Select a item",
    		allowClear: true
	    });

        $(".short_code_type").on('change', function() {
          var val_tt = this.value;
          if(val_tt == "IMG")
          {
            $("#shortcode_desc").html('<input id="short_code_image" name="short_code_desc" class="form-control" type="file">')
          }
          else
          {
            $("#shortcode_desc").html('<textarea class="form-control" name="short_code_desc"></textarea>')
          }
        });
	});
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    var actions = $("table td:last-child").html();
    // Append table with add row form on add new button click
    $(".add-new").click(function(){
        //$(this).attr("disabled", "disabled");
        var index = $("table tbody tr:last-child").index();
        var row = '<tr>' +
            '<td><select class="form-control shcode_type" name="shcode_type"><option value="twitter">twitter</option><option value="instagram">instagram</option></select></td>' +
            '<td><input type="text" class="form-control" name="shcode_text" id="shcode_text"></td>' +
            '<td class="gc"><button type="button" class="btn btn-info add">Generate</button></td>' +
            '<td>' + actions + '</td>' +
        '</tr>';
        $("table").append(row);     
        $("table tbody tr").eq(index + 1).find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();

        


    });
    // Add row on add button click
    $(document).on("click", ".add", function(){

        var btn_add = $(this);
        var csrf_token = jQuery(document).find('.csrf_token').val();
        var sh_type = $(this).parent().parent().find('.shcode_type :selected').text();
        var sh_text = $(this).parent().parent().find('#shcode_text').val();
        $.ajax({
                    beforeSend: function(xhrObj){
                            xhrObj.setRequestHeader("X-CSRF-TOKEN",csrf_token); 
                    },
                   url: site_url+"/admin/shortcode_create",
                   type: 'POST',
                   data: {'sh_type' : sh_type, 'sh_text' : sh_text},
                   success: function(data) 
                   {
                        var obj = jQuery.parseJSON(data);
                        btn_add.replaceWith(obj.code);
                        $(".short_code_id").val(function() {
                           return this.value + obj.id +',';
                        });


                   }
                });
            

    });
    
    // Delete row on delete button click
    $(document).on("click", ".delete", function(){
        //$(this).parents("tr").remove();
        var csrf_token = jQuery(document).find('.csrf_token').val();
        var delete_id = $(this).attr('data-id');
        var shortcode_val = $('.short_code_id').val();
        var shortcode_val  = shortcode_val.replace(delete_id+',','');
        $('.short_code_id').val(shortcode_val);
        $.ajax({
                    beforeSend: function(xhrObj){
                            xhrObj.setRequestHeader("X-CSRF-TOKEN",csrf_token); 
                    },
                   url: site_url+"/admin/shortcode_delete",
                   type: 'POST',
                   data: {'delete_id' : delete_id},
                   success: function(data) 
                   {

                  }
                });
        $(this).parents("tr").remove();

            
    });
});
</script>
<script type="text/javascript">

$(document).ready(function(){

    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.extraAllowedContent = '*(*);*{*}';
    CKEDITOR.replace('description', {
    toolbar: [
        ["Source"],
        ["Bold","Italic","Underline"],
        // ["Link","Unlink","Anchor"],
        // ["Styles","Format","Font","FontSize"],
        // ["TextColor","BGColor"],
        ["UIColor","Maximize","ShowBlocks"],
        ['Image'],
        "/",
        // ["NumberedList","BulletedList","Outdent","Indent","Blockquote","CreateDiv","JustifyLeft","JustifyCenter","JustifyRight","JustifyBlock"],
        ["JustifyLeft"],        
    ],
    filebrowserImageUploadUrl: 'https://www.thevibez.net/admin/news/attachImage?type=Images&_token=' + $('meta[name=csrf-token]').attr("content"),
    });
    
});

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>