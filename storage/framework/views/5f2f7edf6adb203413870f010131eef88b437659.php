<footer class="footer">
    <div class="container">
        <!-- <nav> -->
            <!-- <ul class="footer-menu">
                <li>
                    <a href="#">
                        Home
                    </a>
                </li>
                <li>
                    <a href="#">
                        Company
                    </a>
                </li>
                <li>
                    <a href="#">
                        Portfolio
                    </a>
                </li>
                <li>
                    <a href="#">
                        Blog
                    </a>
                </li>
            </ul> -->
           
            <p class="copyright text-center">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>
                <?php /* <a href="http://www.wedowebapps.com">WeDoWebApps</a>, made with love for a better web */ ?>
                OS Digital Technologies LTD
            </p>
        <!-- </nav> -->
    </div>
</footer>

<!--- Sufee Admin --->	
<!--
    <script src="<?php echo e(asset('admin_assets/js/vendor/jquery-2.1.4.min.js')); ?>"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="<?php echo e(asset('admin_assets/js/plugins.js')); ?>"></script>
    <script src="<?php echo e(asset('admin_assets/js/main.js')); ?>"></script>
    <script src="<?php echo e(asset('admin_assets/js/custom.js')); ?>"></script>
    <script src="<?php echo e(asset('admin_assets/js/select2.js')); ?>"></script>


    <script src="<?php echo e(asset('admin_assets/js/lib/chart-js/Chart.bundle.js')); ?>"></script>
    <script src="<?php echo e(asset('admin_assets/js/dashboard.js')); ?>"></script>
    <script src="<?php echo e(asset('admin_assets/js/widgets.js')); ?>"></script>
    <script src="<?php echo e(asset('admin_assets/js/lib/vector-map/jquery.vmap.js')); ?>"></script>
    <script src="<?php echo e(asset('admin_assets/js/lib/vector-map/jquery.vmap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('admin_assets/js/lib/vector-map/jquery.vmap.sampledata.js')); ?>"></script>
    <script src="<?php echo e(asset('admin_assets/js/lib/vector-map/country/jquery.vmap.world.js')); ?>"></script>
    <script>
        ( function ( $ ) {
            "use strict";

            jQuery( '#vmap' ).vectorMap( {
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: [ '#1de9b6', '#03a9f5' ],
                normalizeFunction: 'polynomial'
            } );
        } )( jQuery );

    </script>
-->