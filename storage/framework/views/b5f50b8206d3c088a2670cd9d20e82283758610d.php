<?php $__env->startSection('main_content'); ?>
<!-- Header-->

        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Playlist</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
            <div class="card">
                <div class="card-header border-header">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-3">
                                <strong class="card-title playlist-name">Add Playlist</strong>
                            </div>
                            <div class="col-md-9">
                                <a href="<?php echo e(url('admin/play-lists')); ?>" class="btn btn-primary float-right backbtn"><i class="fa fa-arrow-left"></i> PlayLists</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
					<?php if(session()->has('message')): ?>
					<script type="text/javascript">
					jQuery(document).ready(function($){
					        $.notify({
					            icon: "nc-icon nc-app",
					            message: '<?php echo e(session()->get('message')); ?>',
					        }, {
					            type: 'success',
					            timer: 8000,
					        });
					 });
					 </script>
					<?php endif; ?>
					<?php if($errors->any()): ?>
						  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<script type="text/javascript">
								jQuery(document).ready(function($){
								        $.notify({
								            icon: "nc-icon nc-app",
								            message: '<?php echo e($error); ?>',
								        }, {
								            type: 'danger',
								            timer: 8000,
								        });												
								});
								 </script>
						  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php endif; ?>


        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card"> 
                       <!--  <div class="card-header border-header">
                            <strong class="card-title playlist-name">Add Playlist</strong>
                            <a href="<?php echo e(url('admin/play-lists')); ?>" class="btn btn-primary float-right backbtn"><i class="fa fa-arrow-left"></i> PlayLists</a>
                        </div> -->
						<div class="card-body">
							<form method="POST" action="<?php echo URL::to('/admin/play-lists/create'); ?>" enctype="multipart/form-data" class="add_playlist">
							 	<?php echo e(csrf_field()); ?>

								
								<div class="form-group row"><label for="name" class="col-md-4 col-form-label">Title</label>
									<div class="col-md-6"><input id="title" name="title" value="" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								<?php if($errors->has('title')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('title')); ?></strong>
                                    </span>
                                <?php endif; ?>
                                <div class="form-group row"><label for="description" class="col-md-4 col-form-label">Description</label>
									<div class="col-md-6"><textarea name="description" class="form-control" placeholder="Description"></textarea></div>
								</div>
								<div class="form-group row"><label for="image" class="col-md-4 col-form-label">Image</label>
									<div class="col-md-6"><input id="image" name="image" value="" required="required" autofocus="autofocus" class="form-control" type="file"></div>
								</div>
								<?php if($errors->has('image')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('image')); ?></strong>
                                    </span>
                                <?php endif; ?>
                                
                                <div class="form-group row"><label for="musics" class="col-md-4 col-form-label">Musics</label>
	                                <div class="col-md-6">
		                                <select class="js-example-basic-multiple form-control" name="music_id[]" multiple="multiple">
										  <?php $__currentLoopData = $musics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $music): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										  	<option value="<?php echo e($music->id); ?>"><?php echo e($music->title); ?> - <?php echo e($music->artist); ?></option>
										  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
									</div>
								</div>
				                 
								<div class="form-group row mb-0">
									<div class="col-md-6 offset-md-4">
										<a class="btn btn-primary add_playlist_btn">Add Playlist</a>
									</div>
								</div>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
<?php $__env->stopSection(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('admin_assets/plugins/select2/select2.min.css')); ?>">
<script src="<?php echo e(asset('admin_assets/js/vendor/jquery-2.1.4.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/plugins/select2/select2.min.js')); ?>"></script>

<script type="text/javascript">
    $(document).ready(function() {
	    $('.js-example-basic-multiple').select2();
	});
</script>


<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>