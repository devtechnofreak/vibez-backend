
<?php $__env->startSection('main_content'); ?>
<!-- Header-->
        <div class="breadcrumbs">
            <div class="col-sm-12">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Albums</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
					<?php if(session()->has('message')): ?>
						<div class="alert alert-success">
							<?php echo e(session()->get('message')); ?>

						</div>
					<?php endif; ?>

                    <div class="card"> 
                        <div class="card-header">
                            <strong class="card-title">Add Musics</strong>
                            <?php if(isset($album_id)): ?>
                            <a href="<?php echo e(url('admin/album/edit/'.$album_id)); ?>" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Back</a>
                            <?php else: ?>
                            <a href="<?php echo e(URL::previous()); ?>" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Back</a>
                            <?php endif; ?>
                        </div>
						<div class="card-body" id="drop-zone">
							 <?php if($errors->any()): ?>
								  <div class="alert alert-danger">
									  <ul>
										  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											  <li><?php echo e($error); ?></li>
										  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									  </ul>
								  </div><br/>
							  <?php endif; ?>
							<dropzone-component></dropzone-component>
						</div>
                    </div>
                    
                    <div class="music_data">
                    	<form method="POST" action="<?php echo URL::to('admin/album/import/musics'); ?>" enctype="multipart/form-data" class="add_music_bulk">
                    		<?php echo csrf_field(); ?>

                    		<?php if(isset($album_id)): ?>
			                <input type="hidden" value="<?php echo e($album_id); ?>" name="album_id"/>
			                <?php else: ?>
			                <div class="form-group row"><label for="artist" class="col-md-4 col-form-label">Artist</label>
								<div class="col-md-6">
									<select name="album_id" value="" class="col-md-6 form-control banner_type">
										<?php $__currentLoopData = $albums; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $album): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<option value="<?php echo e($album->id); ?>"><?php echo e($album->title); ?></option>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</select>
								</div> 
							</div>
			                <?php endif; ?>
                    		<table class="table table-striped">
                    			<thead>
                    			<tr>
                    				<th scope="col">Title</th>
                    				<th scope="col">Meta Tags</th>
                    				<th scope="col" style="width: 10%;">Is Trending</th>
                    				<th scope="col">Tags</th>
                    			</tr>
                    			</thead>
  								<tbody>
  								</tbody>
                    		</table>
                    		<div class="formsubmit">
                    			<?php 
                    				$url = $_SERVER['REQUEST_URI'];
                    				$template = substr($url, strrpos($url, '/') + 1);
                    			?>
                    			<input type="hidden" name="template" value="<?php echo $template; ?>">
                    			<input type="submit" class="btn btn-primary add_artist_btn" value="Add Songs To Album">
                    		</div>
                    	</form>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
<?php $__env->stopSection(); ?>
<?php $__env->startSection('headscripts'); ?>
	<script src="<?php echo e(asset('js/app.js')); ?>"></script>
	<script>
		jQuery(document).ready(function(){
			jQuery('.menu-item-has-children.dropdown .dropdown-toggle').click(function(){
				jQuery(this).parent().toggleClass('show');
				jQuery(this).next().toggleClass('show');
			});
		});
	</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>