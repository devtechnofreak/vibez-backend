<?php $__env->startSection('main_content'); ?>
<!-- Header-->
<?php 

?>
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>News</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-md-3">
											 <strong class="card-title newslist-name">Edit Page</strong>
										</div>
										<div class="col-md-9">
											 <a href="<?php echo e(url('admin/pages')); ?>" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Page List</a>
										</div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>
					<?php if(session()->has('message')): ?>
					<script type="text/javascript">
					jQuery(document).ready(function($){
					        $.notify({
					            icon: "nc-icon nc-app",
					            message: '<?php echo e(session()->get('message')); ?>',
					        }, {
					            type: 'success',
					            timer: 8000,
					        });
					 });
					 </script>
					<?php endif; ?>
					<?php if($errors->any()): ?>
						  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<script type="text/javascript">
								jQuery(document).ready(function($){
								        $.notify({
								            icon: "nc-icon nc-app",
								            message: '<?php echo e($error); ?>',
								        }, {
								            type: 'danger',
								            timer: 8000,
								        });												
								});
								 </script>
						  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php endif; ?>

        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">

						<div class="card"> 
							<!-- <div class="card-header border-header">
								<strong class="card-title newslist-name">Edit News</strong>
								<a href="<?php echo e(url('admin/news')); ?>" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> News List</a>
							</div> -->
							<div class="card-body row">

								<div class="col-md-12">
									<form method="POST" action="<?php echo URL::to('/admin/page/update'); ?>" class="artist_edit_form"  enctype="multipart/form-data">
									 <?php echo e(csrf_field()); ?>

									 	<input type="hidden" name="page_id" value="<?php echo e($page->id); ?>" /> 
										<div class="form-group row"><label for="name" class="col-md-2 col-form-label">Title</label>
											<div class="col-md-8"><input id="title" name="title" required="required" autofocus="autofocus" class="form-control" value="<?php echo e($page->title); ?>" type="text"></div>
										</div>
										<?php if($errors->has('title')): ?>
											<span class="invalid-feedback">
												<strong><?php echo e($errors->first('title')); ?></strong>
											</span>
										<?php endif; ?>
								<div class="form-group row"><label for="slug" class="col-md-2 col-form-label">Slug</label>
									<div class="col-md-8"><input id="slug" name="slug" value="<?php echo e($page->slug); ?>" required="required" autofocus="autofocus" class="form-control" type="text" disabled></div>
								</div>
                                <div class="form-group row"><label for="description" class="col-md-2 col-form-label">Content</label>
									<div class="col-md-8"><textarea id="description" name="content" value="" required="required" autofocus="autofocus" class="form-control"><?php echo e($page->content); ?></textarea></div>
								</div>										
									<div class="form-group row mb-0 user_form_submit">
										<div class="col-md-6 offset-md-4">
											
											<a class="btn btn-primary update_artist_btn">Update Page</a>
										</div>
									</div>
									</form>
								</div>

							</div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
<script src="<?php echo e(asset('admin_assets/plugins/ckeditor/ckeditor.js')); ?>"></script>

<script type="text/javascript">

$(document).ready(function(){

    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.extraAllowedContent = '*(*);*{*}';
    CKEDITOR.replace('description', {
    toolbar: [
        ["Source"],
        ["Bold","Italic","Underline"],
        ["Link","Unlink","Anchor"],
        ["Styles","Format","Font","FontSize"],
        ["TextColor","BGColor"],
        ["UIColor","Maximize","ShowBlocks"],
        "/",
        ["NumberedList","BulletedList","Outdent","Indent","Blockquote","CreateDiv","JustifyLeft","JustifyCenter","JustifyRight","JustifyBlock"],
    ]
    });
});

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>