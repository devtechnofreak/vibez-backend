<?php $__env->startSection('main_content'); ?>
<!-- Header-->
<?php 

?>
        <!-- <div class="breadcrumbs">
            <div class="col-sm-12">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Video</h1>
                    </div>
                </div>
            </div>
        </div> -->
         <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-4">
			                        		<strong class="card-title videolist-name">Edit Video</strong>
			                        	</div>
			                            <div class="col-sm-8">	
			                            	<a href="<?php echo e(url('admin/videos')); ?>" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Video List</a>
			                            	<?php /* <form method="POST" action="<?php echo URL::to('/admin/videos/send_pushnotification'); ?>" class="push_notification_form">
											 	{{ csrf_field() }}
												<div class="col">
													<input type="hidden" name="video_id" value="{{$video->id}}">
													<button type="button" class="btn btn-success float-right send_push_notification_btn"><i class="fa fa-save"></i>Send Push Notification</button>
												</div>
											</form>

											<form method="POST" action="<?php echo URL::to('/admin/news/sendPushNotification_Video'); ?>" class="push_notification_form">
											 	{{ csrf_field() }}
												<div class="col">
													<input type="hidden" name="video_id" value="{{$video->id}}">
													<button type="button" class="btn btn-success float-right send_push_notification_btn"><i class="fa fa-save"></i>FOR TEST</button>
												</div>
											</form>*/ ?>
											<div class="col">
												<input type="hidden" name="video_id" value="<?php echo e($video->id); ?>">
												<button type="button" class="btn btn-success float-right send_push_notification_btn" data-toggle="modal" data-target="#VideoNotificationModal"><i class="fa fa-save"></i>Send Push Notification</button>
											</div>
											<?php /* <div class="col">
												<input type="hidden" name="video_id" value="{{$video->id}}">
												<button type="button" class="btn btn-success float-right send_push_notification_btn" data-toggle="modal" data-target="#VideoNotificationModalTest"><i class="fa fa-save"></i>For Test</button>
											</div> */ ?>


										</div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>
					<?php if(session()->has('message')): ?>
					<script type="text/javascript">
					jQuery(document).ready(function($){
					        $.notify({
					            icon: "nc-icon nc-app",
					            message: '<?php echo e(session()->get('message')); ?>',
					        }, {
					            type: 'success',
					            timer: 8000,
					        });
					 });
					 </script>
					<?php endif; ?>
					<?php if($errors->any()): ?>
						  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<script type="text/javascript">
								jQuery(document).ready(function($){
								        $.notify({
								            icon: "nc-icon nc-app",
								            message: '<?php echo e($error); ?>',
								        }, {
								            type: 'danger',
								            timer: 8000,
								        });												
								});
								 </script>
						  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					  <?php endif; ?>
        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">
						<div class="card"> 
							<!-- <div class="card-header">
								<strong class="card-title">Edit Video</strong>
							</div> -->
							<div class="card-body row">
								<div class="col-md-8">
									<form method="POST" action="<?php echo URL::to('/admin/videos/editdata'); ?>" class="artist_edit_form"  enctype="multipart/form-data">
									 <?php echo e(csrf_field()); ?>

										<div class="form-group row"><label for="name" class="col-md-2 col-form-label">Title</label>
											<div class="col-md-6"><input id="title" name="title" required="required" autofocus="autofocus" class="form-control" value="<?php echo e($video->title); ?>" type="text"></div>
										</div>
										<?php if($errors->has('title')): ?>
											<span class="invalid-feedback">
												<strong><?php echo e($errors->first('title')); ?></strong>
											</span>
										<?php endif; ?>
										<div class="form-group row"><label for="description" class="col-md-2 col-form-label">Description</label>
											<div class="col-md-6"><textarea name="description" class="form-control txtbio" placeholder="Description" rows="5"><?php echo e($video->description); ?></textarea></div>
										</div>
										<div class="form-group row"><label for="image" class="col-md-2 col-form-label">Image</label>
											<div class="col-md-6"><input id="image" name="image" class="form-control" value="" type="file"><img src="<?php echo e($video->thumb); ?>" class="artist_image"> </div>
										</div>
										<?php if($errors->has('image')): ?>
											<span class="invalid-feedback">
												<strong><?php echo e($errors->first('image')); ?></strong>
											</span>
										<?php endif; ?>
										
										<!-- <div class="form-group row"><label class="col-md-2 col-form-label">Video</label>
											<div class="col-md-6">
												<video width="320" height="240" controls>
												  <source src="<?php echo e($video->url); ?>" type="video/mp4">
												</video>
											</div>
										</div> -->
										<?php if($errors->has('image')): ?>
											<span class="invalid-feedback">
												<strong><?php echo e($errors->first('image')); ?></strong>
											</span>
										<?php endif; ?>
										
		                                <div class="form-group row"><label for="date_released" class="col-md-2 col-form-label">Release Date</label>
											<div class="col-md-6"><input type="text" class="form-control release_date" value="<?php echo e(date('d-m-Y',strtotime($video->date_released))); ?>" name="date_released" required></div>
										</div>
										<?php if($errors->has('date_released')): ?>
		                                    <span class="invalid-feedback">
		                                        <strong><?php echo e($errors->first('date_released')); ?></strong>
		                                    </span>
		                                <?php endif; ?>
		                                
		                                <div class="form-group row"><label for="artist" class="col-md-2 col-form-label">Artist</label>
											<div class="col-md-6">
												<select class="js-example-basic-multiple form-control" name="artist_id[]" value="" multiple="multiple">
													<?php $__currentLoopData = $artists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $artist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<option name="artist_id" value="<?php echo e($artist->id); ?>" <?php if(in_array($artist->id,collect($video->artists)->pluck('id')->all())): ?> selected <?php endif; ?>><?php echo e($artist->name); ?></option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</select>
											</div> 
										</div>
										<div class="form-group row"><label for="artist" class="col-md-2 col-form-label">Artists Featured</label>
											<div class="col-md-6">
												<?php 
													$related_artist = $video->related_artist;
													$related_artist_ids = array();
													if($related_artist != ""){
														$related_artist_ids = explode(',', $related_artist);
													}
												?>
												<select class="js-example-basic-multiple form-control" name="featured_artist_id[]" value="" multiple="multiple">
													<?php $__currentLoopData = $artists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $artist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<option name="artist_id" value="<?php echo e($artist->id); ?>" <?php if(in_array($artist->id,$related_artist_ids)): ?> selected <?php endif; ?>><?php echo e($artist->name); ?></option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</select>
											</div> 
										</div>
		                                <div class="form-group row">
		                                	<label for="album_tags" class="col-md-2 col-form-label">Tags</label>
												<div class="col-md-6">
													<?php 
														$video_tags = array();
														foreach ($video->tags as $single) {
															$video_tags[] = $single->name;
														}
													?>
													<input id="album_tags" name="tag_id" value="<?php echo implode(',',$video_tags); ?>" placeholder="Enter comma , separated Tags" autofocus="autofocus" class="form-control mb-2" type="text">
												</div>
										</div>
										
										<div class="form-group row"><label for="cateories" class="col-md-2 col-form-label">Category</label>
											<div class="col-md-6">
												<select name="category_id" value="" class="form-control">
													<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
														  <option value="<?php echo e($category->id); ?>" <?php if($category->id==$video->category_id): ?> selected <?php endif; ?>><?php echo e($category->name); ?></option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</select>
											</div> 
										</div>
										 <div class="form-group row"><label for="dob" class="col-md-2 col-form-label">Is Featured</label>
											<div class="col-md-6"><input value="1" <?php if($video->is_featured == 1): ?> checked="checked" <?php endif; ?> type="checkbox" id="isFeatured" name="is_featured"></div> 
										</div>
										
										<div class="form-group row mb-0 user_form_submit">
											<div class="col-md-6 offset-md-4">
												<input type="hidden" name="video_id" value="<?php echo e($video->id); ?>" /> 
												<a class="btn btn-primary update_artist_btn">Update Video</a>
											</div>
										</div>
									</form>
								</div>
								<div class="col-md-4">
									<div class="card">
				                            <div class="card-image">
				                                <video width="100%" height="300" controls>
				                                	
												  <source src="<?php echo e($video->url); ?>" type="video/mp4">
												</video>
				                            </div>
				                            <div class="card-body">
				                            		<h3 class="title"><?php echo e($video->title); ?></h3>
			                                      	<?php $__currentLoopData = $tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			                                      		<?php if(in_array($tag->tag_id,collect($video->tags)->pluck('tag_id')->all())): ?> 
			                                      		<div class="d-inline-block bg-info pl-2 pr-2 mb-2 text-light"><?php echo e($tag->name); ?></div>
			                                      		<?php endif; ?>
				                                		
				                                	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				                                	<div class="col-md-12 text-center pt-5"><a class="btn btn-success" href="<?php echo e(url('admin/video/thumb/set/'.$video->id)); ?>">Set Thumbnail From Source</a></div>
				                            </div>
				                            <hr>
				                    </div>
								</div>
							</div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->


<!-- Modal -->
<div class="modal fade" id="VideoNotificationModal" role="dialog">
    <div class="modal-dialog">
    	<form method="POST" action="<?php echo URL::to('/admin/videos/send_pushnotification'); ?>" class="push_notification_form">
    		<?php echo e(csrf_field()); ?>

	        <div class="modal-content">
	            <!-- Modal Header -->
	            <!-- Modal Body -->
	            <div class="modal-body">
                    <div class="form-group">
                        <label for="inputName">Type</label><br/>
                        <label class="radio-inline"><input type="radio" name="notificationtype" value="default" checked>Default</label>
						<label class="radio-inline"><input type="radio" name="notificationtype" value="custom">Custom</label>
						
                    </div>

					<input type="hidden" name="video_id" value="<?php echo e($video->id); ?>" /> 


                    <div class="for_custom" style="display: none;">
                    	<div class="form-group">
	                        <label for="inputEmail">Title</label>
	                        <p class="lead emoji-picker-container">
				              <textarea class="form-control" rows="10" cols="50" data-emojiable="true" data-emoji-input="unicode" name="video_title"><?php echo e($video->title); ?></textarea>
				            </p>
	                    </div>
	                    
	                </div>
	            </div>
	            
	            <!-- Modal Footer -->
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                <input type="submit" name="submit" class="btn btn-primary" value="SUBMIT">
	            </div>
	        </div>
	    </form>    
    </div>
</div>        


<div class="modal fade" id="VideoNotificationModalTest" role="dialog">
    <div class="modal-dialog">
    	<form method="POST" action="<?php echo URL::to('/admin/videos/sendPushNotification_Video'); ?>" class="push_notification_form">
    		<?php echo e(csrf_field()); ?>

	        <div class="modal-content">
	            <!-- Modal Header -->
	            <!-- Modal Body -->
	            <div class="modal-body">
                    <div class="form-group">
                        <label for="inputName">Type</label><br/>
                        <label class="radio-inline"><input type="radio" name="notificationtypetest" value="default" checked>Default</label>
						<label class="radio-inline"><input type="radio" name="notificationtypetest" value="custom">Custom</label>
						
                    </div>

					<input type="hidden" name="video_id_test" value="<?php echo e($video->id); ?>" /> 


                    <div class="for_custom_test" style="display: none;">
                    	<div class="form-group">
	                        <label for="inputEmail">Title</label>
	                        <p class="lead emoji-picker-container">
				              <textarea class="form-control" rows="10" cols="50" data-emojiable="true" data-emoji-input="unicode" name="video_title_test"><?php echo e($video->title); ?></textarea>
				            </p>
	                    </div>
	                </div>
	            </div>
	            
	            <!-- Modal Footer -->
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                <input type="submit" name="submit" class="btn btn-primary" value="SUBMIT">
	            </div>
	        </div>
	    </form>    
    </div>
</div>         
		
	
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('admin_assets/plugins/select2/select2.min.css')); ?>">
<!-- <script src="<?php echo e(asset('admin_assets/js/vendor/jquery-2.1.4.min.js')); ?>"></script> -->
<script src="<?php echo e(asset('admin_assets/plugins/select2/select2.min.js')); ?>"></script>

<script type="text/javascript">
    $(document).ready(function() {
	    $('.js-example-basic-multiple').select2({
	    	placeholder: "Select a item",
    		allowClear: true
	    });
	    /*$('.send_push_notification_btn').click(function(){ 
			var r = confirm("Are you sure want to send push notification to users for this video?");
			if (r == true) {
				$('.push_notification_form').submit();
			}
		});*/
		$('input[name=notificationtype]').change(function(){
		var value = $( 'input[name=notificationtype]:checked' ).val();
		if(value == 'custom'){
			$('.for_custom').css("display","block");
		}
		else if(value == 'deafult'){
			$('.for_custom').css("display","none");
		}
		else{
			$('.for_custom').css("display","none");	
		}
		});

		$('input[name=notificationtypetest]').change(function(){
		var value = $( 'input[name=notificationtypetest]:checked' ).val();
		if(value == 'custom'){
			$('.for_custom_test').css("display","block");
		}
		else if(value == 'deafult'){
			$('.for_custom_test').css("display","none");
		}
		else{
			$('.for_custom_test').css("display","none");	
		}
		});

		$(function() {
	        // Initializes and creates emoji set from sprite sheet
	        window.emojiPicker = new EmojiPicker({
	          emojiable_selector: '[data-emojiable=true]',
	          assetsPath: '<?php echo e(asset('admin_assets/plugins/emoji')); ?>' +'/lib/img/',
	          popupButtonClasses: 'fa fa-smile-o'
	        });
	        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
	        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
	        // It can be called as many times as necessary; previously converted input fields will not be converted again
	        window.emojiPicker.discover();
	      });
	});
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>