<div class="header" style="padding: 25px 0;background-color: #ed213a;text-align: center;">
    <img style="width:130px;" src="https://www.thevibez.net/assets/img/vibezlogo.png"> 
</div>
<table class="m_839837753987678212m_5786463994150025933inner-body" align="center" width="570" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;background-color:#ffffff;margin:0 auto;padding:0;width:570px">
    <tbody>
        <tr>
            <td class="m_839837753987678212m_5786463994150025933content-cell" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;padding:35px">
                    <h1 style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#2f3133;font-size:19px;font-weight:bold;margin-top:0;text-align:left">User asking for some support</h1>
                    <p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left"> Name :- <?php echo e($user['name']); ?> </p>
                    <p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left"> Subject :- <?php echo e($user['subject']); ?> </p>
                    <p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left"> Email :- <?php echo e($user['email']); ?> </p>
                    <p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left"> Message :- <?php echo e($user['message']); ?> </p>
                    <p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">Regards,<br><img src="https://ci5.googleusercontent.com/proxy/FbRdnWjBHhoYbLzxmvzPL3Obo-aYZJJr2MI9gzSkAx3TMUPtFMp4XAsxgB84yJQ39j-b9nLDUxVaGQjiSrx9XRFYJsKt=s0-d-e1-ft#https://www.thevibez.net/assets/img/vibez_name.png" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;max-width:100%;width:65px" class="CToWUd"></p>
            </td>
        </tr>
    </tbody>
</table>
<div class="35" style="padding: 35px 0;background-color: #ed213a;text-align: center;color : #fff;">
    © <?php echo date('Y'); ?> Vibez. All rights reserved. 
</div>