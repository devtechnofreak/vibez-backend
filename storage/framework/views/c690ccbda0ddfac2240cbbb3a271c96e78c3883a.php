
<?php $__env->startSection('main_content'); ?>
<!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Albums</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
					<?php if(session()->has('message')): ?>
						<div class="alert alert-success">
							<?php echo e(session()->get('message')); ?>

						</div>
					<?php endif; ?>
                    <div class="card"> 
                        <div class="card-header">
                            <strong class="card-title">Import Albums</strong>
                            <strong class="text-success ml-4">
                            	
                            <?php if(isset($artist_id)): ?>
                            <?php echo e('Artist : '.App\Artist::find($artist_id)->name); ?>

                            <?php endif; ?>	
                            </strong>
                            <a href="<?php echo e(url('admin/albums')); ?>" class="btn btn-primary float-right  ml-2"><i class="fa fa-arrow-left"></i> Back</a>
                            
                            <a href="<?php echo e(url('sample/import_albums.csv')); ?>" class="btn btn-success  float-right ml-2"><i class="fa fa-download"></i> Download Sample Csv</a>
                            <?php if(isset($csv)): ?>
                            <?php if(isset($artist_id)): ?>
                            <a href="<?php echo e(url('admin/album/import/bulk/'.$artist_id)); ?>" class="btn btn-success  float-right ml-2"><i class="fa fa-refresh"></i></a>
                            <?php else: ?>
                            <a href="<?php echo e(url('admin/album/import/bulk')); ?>" class="btn btn-success  float-right ml-2"><i class="fa fa-refresh"></i></a>
                            <?php endif; ?>
                            <?php endif; ?>
                            <a class="album_import_cron_manual btn btn-success  float-right" style="color: #87CB16;"><i class="fa fa-refresh"></i> Album Import Cron</a>
                        </div>
						<div class="card-body">
							 <?php if($errors->any()): ?>
								  <div class="alert alert-danger">
									  <ul>
										  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											  <li><?php echo e($error); ?></li>
										  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									  </ul>
								  </div><br/>
							  <?php endif; ?>
							<form method="POST" action="<?php echo e(url('admin/album/import/bulk')); ?>" enctype="multipart/form-data" class="form-inline">
							 <?php echo e(csrf_field()); ?>

							<?php if(isset($csv)): ?>
							<table class="table album_bulk_import">
							  <thead>
							    <tr>
							      <th scope="col">#</th>
							      <th scope="col">Title</th>
							      <th scope="col">Description</th>
							      <th scope="col">Artist</th>
							      <th scope="col">Tags</th>
							      <th scope="col" style="display: table-cell !important;">Folder</th>
							    </tr>
							  </thead>
							  <tbody>
							<?php $__currentLoopData = $csv; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							    <tr>
							      <th scope="row"><?php echo e($key+1); ?></th>
							      <td><input name="album[<?php echo e($key); ?>][title]" required="required" autofocus="autofocus" class="form-control" value="<?php echo e($data['title']); ?>" type="text"></td>
							      <td><textarea rows="4" cols="50" name="album[<?php echo e($key); ?>][description]" class="form-control"><?php echo e($data['description']); ?></textarea></td>
							      <td>
							      	<select name="album[<?php echo e($key); ?>][artist]" value="" class="col-md-6 form-control banner_type">
							      		<option value="">Select Artist</option>
										<?php $__currentLoopData = $artists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $artist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php if(isset($artist_id)): ?>
											<option value="<?php echo e($artist->id); ?>" <?php echo e($artist->id==$artist_id?'selected':''); ?>><?php echo e($artist->name); ?></option>
										<?php else: ?>
											<option value="<?php echo e($artist->id); ?>"><?php echo e($artist->name); ?></option>	
										<?php endif; ?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</select>
							      </td>
							      <td><input name="album[<?php echo e($key); ?>][tags]" value="<?php echo e($data['tags']); ?>" placeholder="Enter comma(,) separated Tags" autofocus="autofocus" class="form-control mb-2" type="text"></td>
							      <td>
							      	<select class="js-example-basic-multiple form-control" name="album[<?php echo e($key); ?>][folder]">
							      		<?php 
							      			$folders = glob("/home/admin/web/thevibez.net/public_html/public/import/music/*", GLOB_ONLYDIR );
							      			foreach ($folders as $folder) {
												$foldername = substr($folder, strrpos($folder, '/') + 1);	
												echo '<option value="'.$foldername.'">'.$foldername.'</option>';  
											}
							      		?>
							      	</select>
							      </td>
							    </tr>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							  </tbody>
							</table>
							<?php else: ?>
							  <div class="form-group mx-sm-3 mb-2">
							  	<?php if(isset($artist_id)): ?>
									<input type="hidden" value="<?php echo e($artist_id); ?>" name="artist_id" />
								<?php endif; ?>
							    <label for="inputfile" class="sr-only">Password</label>
							    <input type="file" class="form-control-file" id="inputfile" name="csv_import" placeholder="Csv File" required="reequired">
							  </div>
							  <?php if($errors->has('name')): ?>
								<span class="invalid-feedback">
									<strong><?php echo e($errors->first('csv_import')); ?></strong>
								</span>
							  <?php endif; ?>
							<?php endif; ?>
							  <button type="submit" class="btn btn-primary mb-2">Import</button>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
<?php $__env->stopSection(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('admin_assets/plugins/select2/select2.min.css')); ?>">
<script src="<?php echo e(asset('admin_assets/js/vendor/jquery-2.1.4.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/plugins/select2/select2.min.js')); ?>"></script>

<script type="text/javascript">
    $(document).ready(function() {
	    $('.js-example-basic-multiple').select2();
	    $('.album_import_cron_manual').click(function(){
	    	var r = confirm("Are you sure want to run album import cron manually ?");
			if (r == true) {
			    $.ajax({
	               type:'GET',
	               url:'/run_album_cron_manually',
	               data:'_token = <?php echo csrf_token() ?>',
	               success:function(data){
	                  alert("Cronjob is running in background, please wait for few minutes!")
	               }
	            });
			} else {
			    alert(20);
			}
	    });
	});
</script>
<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>