<div class="header" style="padding: 25px 0;background-color: #ed213a;text-align: center;">
	<img style="width:130px;" src="https://www.thevibez.net/assets/img/vibezlogo.png"> 
</div>
<table style="width:700px;margin: 0 auto;padding: 50px 0;">
	<tr>
		<td>
			<h1>Hello!</h1>
			<p style="font-size:15px;">Cronjob for album import has been executed successfully at <?php echo date('d-m-Y H:i:s'); ?></p>
			<p style="font-size:15px;">Attachment contains the list of folders which are not successfully imported due to not satisfying the following reason</p>
		</td>
		<tr>
			<td>
				<ol>
					<li style="font-size:15px;">.jpg, .mp3, .csv must included in the folder</li>
					<li style="font-size:15px;">csv contains proper data (must contain "title","description","tags","artist" column)</li>
					<li style="font-size:15px;">artist with exact name must exist in the backend</li>
					<li style="font-size:15px;">album name must not exist in backend</li>
				</ol>
			</td>
		</tr>
	</tr>
</table>
<div class="35" style="padding: 35px 0;background-color: #ed213a;text-align: center;color : #fff;">
	© <?php echo date('Y'); ?> Vibez. All rights reserved. 
</div>