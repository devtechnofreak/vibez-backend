<?php $__env->startSection('main_content'); ?>
<!-- Header-->
<?php 

?>
        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Banner</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-4">
			                        		<strong class="card-title newslist-name">Edit Banner</strong>
			                        	</div>
			                            <div class="col-sm-8">	
			                            	<a href="<?php echo e(url('admin/banners')); ?>" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Banners List</a>
			                            </div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>

        <div class="content mt-5 user_admin_content">
            <div class="animated fadeIn">
                <div class="row">
					<div class="col-md-12">
					<?php if(session()->has('message')): ?>
						<div class="alert alert-success">
							<?php echo e(session()->get('message')); ?>

						</div>
					<?php endif; ?>
						<div class="card"> 
							<!-- <div class="card-header">
								<strong class="card-title">Edit Banner</strong>
								<a href="<?php echo e(url('admin/banners')); ?>" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Back</a>
							</div> -->
							<div class="card-body row">
								 <?php if($errors->any()): ?>
									  <div class="alert alert-danger">
										  <ul>
											  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												  <li><?php echo e($error); ?></li>
											  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										  </ul>
									  </div><br/>
								  <?php endif; ?>
								<div class="col-md-8">
									<form method="POST" action="<?php echo URL::to('/admin/banners/editdata'); ?>" class="banner_edit_form"  enctype="multipart/form-data">
									 <?php echo e(csrf_field()); ?>

									
									<div class="form-group row"><label for="name" class="col-md-4 col-form-label">Title</label>
										<div class="col-md-6"><input id="title" name="title" value="<?php echo e($banner->title); ?>" required="required" autofocus="autofocus" class="form-control" type="text"></div>
									</div>
									<?php if($errors->has('title')): ?>
	                                    <span class="invalid-feedback">
	                                        <strong><?php echo e($errors->first('title')); ?></strong>
	                                    </span>
	                                <?php endif; ?>
									<div class="form-group row"><label for="image" class="col-md-4 col-form-label">Image</label>
										<div class="col-md-6"> <input id="image" name="image" value="" required="required" autofocus="autofocus" class="form-control" type="file"></div>
									</div>
									<?php if($errors->has('image')): ?>
	                                    <span class="invalid-feedback">
	                                        <strong><?php echo e($errors->first('image')); ?></strong>
	                                    </span>
	                                <?php endif; ?>
	                                
	                                <div class="form-group row"><label for="dob" class="col-md-4 col-form-label">Type</label>
										<div class="col-md-6 ">
											<select name="type" value="" class="col-md-6 form-control banner_type">
												<option name="type" value="">Select Type</option>
												<option name="type" value="Album" <?php if($banner->type == 'Album'): ?> selected <?php endif; ?>>Album</option>
												<option name="type" value="Artist" <?php if($banner->type == 'Artist'): ?> selected <?php endif; ?>>Artist</option>
												<option name="type" value="Playlist" <?php if($banner->type == 'Playlist'): ?> selected <?php endif; ?>>Playlist</option>
												<option name="type" value="Music" <?php if($banner->type == 'Music'): ?> selected <?php endif; ?>>Music</option>
												<option name="type" value="News" <?php if($banner->type == 'News'): ?> selected <?php endif; ?>>News</option>
												<option name="type" value="Video" <?php if($banner->type == 'Video'): ?> selected <?php endif; ?>>Video</option>
												<option name="type" value="Video" <?php if($banner->type == 'LiveStream'): ?> selected <?php endif; ?>>Live</option>
											</select>
										</div> 
									</div>
									<div class="form-group row"><label for="dob" class="col-md-4 col-form-label">Items</label>
										<div class="col-md-6">
											<select name="item" value="" class="col-md-6 form-control banner_item">
											<?php $__currentLoopData = $banner->bannerable->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $obj): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<option name="item" value="<?php echo e($obj->id); ?>" <?php if($banner->bannerable->id == $obj->id): ?> selected <?php endif; ?>><?php echo e($obj->title?:$obj->name); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</select>
											<!-- <div class="d-inline-block bg-info pl-2 pr-2 mb-2 mt-2 text-light"><?php echo e($banner->bannerable->title?:$banner->bannerable->name); ?> 
											</div> -->
											
										</div>
									</div>
										<div class="form-group row mb-0 user_form_submit">
											<div class="col-md-6 offset-md-4">
												<input type="hidden" name="banner_id" value="<?php echo e($banner->id); ?>" /> 
												<a class="btn btn-primary update_banner_btn">Update Banner</a>
											</div>
										</div>
									</form>
								</div>
								<div class="col-md-4">
									<div class="card">
				                            <div class="card-image">
				                                <img src="<?php echo e($banner->image); ?>" class="artist_image">
				                            </div>
				                            <div class="card-body">
				                            		<h3 class="title"><?php echo e($banner->title); ?></h3>
			                                      
				                                	<div class="d-inline-block bg-info pl-2 pr-2 mb-2 text-light"><?php echo e($banner->bannerable->title?:$banner->bannerable->name); ?></div>
				                                	
				                            </div>
				                            <hr>
				                    </div>
								</div>
							</div>
						</div>
					</div>
					
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>