<?php $__env->startSection('main_content'); ?>
<!-- Header-->

        <!-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Artists</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="breadcrumbs navbar-top">
                        <div class="card">
                            <div class="card-header border-header">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <strong class="card-title artist-name">Add Artist</strong>
                                        </div>
                                        <div class="col-md-9">
                                            <a href="<?php echo e(url('admin/artists')); ?>" class="btn btn-primary float-right backbtn"><i class="fa fa-arrow-left"></i> Artist List</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>

        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <?php if(session()->has('message')): ?>
                        <div class="alert alert-success">
                            <?php echo e(session()->get('message')); ?>

                        </div>
                    <?php endif; ?>
                    <div class="card"> 
                       <!--  <div class="card-header border-header">
                            <strong class="card-title artist-name">Add Artist</strong>
                            <a href="<?php echo e(url('admin/artists')); ?>" class="btn btn-primary float-right backbtn"><i class="fa fa-arrow-left"></i> Artist List</a>
                        </div> -->
                        <div class="card-body">
                             <?php if($errors->any()): ?>
                                  <div class="alert alert-danger">
                                      <ul>
                                          <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                              <li><?php echo e($error); ?></li>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      </ul>
                                  </div><br/>
                              <?php endif; ?>
                            <form method="POST" action="<?php echo URL::to('/admin/artist/create'); ?>" enctype="multipart/form-data" class="add_artist">
                             <?php echo e(csrf_field()); ?>

                                <div class="form-group row"><label for="name" class="col-md-2 col-form-label">Name</label>
                                    <div class="col-md-6"><input id="name" name="name" value="" required="required" autofocus="autofocus" class="form-control" type="text"></div>
                                </div>
                                <?php if($errors->has('name')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                                
                                <div class="form-group row"><label for="bio" class="col-md-2 col-form-label">Bio</label>
                                    <div class="col-md-6"><textarea rows="4" cols="50" id="bio" name="bio" class="form-control txtbio"></textarea></div> 
                                </div>
                                <?php if($errors->has('bio')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('bio')); ?></strong>
                                    </span>
                                <?php endif; ?>
                                <div class="form-group row"><label for="image" class="col-md-2 col-form-label">Image</label>
                                    <div class="col-md-6"><input id="image" name="image" value="" required="required" autofocus="autofocus" class="form-control" type="file"></div>
                                </div>
                                <?php if($errors->has('image')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('image')); ?></strong>
                                    </span>
                                <?php endif; ?>
                                <div class="form-group row"><label for="dob" class="col-md-2 col-form-label">Date of Birth</label>
                                    <div class="col-md-6"><input type="text" class="form-control" id="dob" name="dob" autocomplete="off"></div>
                                </div>
                                <?php if($errors->has('dob')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('dob')); ?></strong>
                                    </span>
                                <?php endif; ?>
                                
                                <div class="form-group row"><label for="dob" class="col-md-2 col-form-label">Is Featured</label>
                                    <div class="col-md-6"><input value="1" type="checkbox" id="isFeatured" name="isFeatured"></div> 
                                </div>
                                <div class="form-group row"><label for="dob" class="col-md-2 col-form-label">Country</label>
                                    <div class="col-md-6">
                                    	<select class="form-control" name="country" value="">
                                    		<option value="">Please select Country</option>
											<?php
												foreach ($countries as $single) { ?>
													<option value="<?php echo $single->name; ?>"><?php echo $single->name; ?></option>
											<?php 	}
											?>
										</select>
                                    </div>
                                </div>
                                <?php if($errors->has('country')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('country')); ?></strong>
                                    </span>
                                <?php endif; ?>
                               
                                <div class="form-group row"><label for="name" class="col-md-2 col-form-label">Tags</label>
                                    <div class="col-md-6"><input id="artist_tags" name="artist_tags" value="" required="required" autofocus="autofocus" placeholder="Enter comma(,) separated Tags" class="form-control" type="text"></div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <a class="btn btn-primary add_artist_btn">Save</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
        
    
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>