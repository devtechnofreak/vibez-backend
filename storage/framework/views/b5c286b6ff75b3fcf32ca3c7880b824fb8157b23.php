<?php $__env->startSection('main_content'); ?>

		<style type="text/css">
			.ds-none {
				display: none;
			}
			.csvGenerate {
				padding: 6px 30px;
			    border: 2px solid #616161;
			    margin-left: 15px;
			    cursor: pointer;
			    font-size: 16px;
			}
			.loader {
			  	border: 16px solid #f3f3f3;
			  	border-radius: 50%;
			  	border-top: 3px solid blue;
			  	border-right: 3px solid green;
			  	border-bottom: 3px solid red;
			  	border-left: 3px solid pink;
			  	width: 35px;
			  	height: 35px;
			  	-webkit-animation: spin 1s linear infinite;
			  	animation: spin 1s linear infinite;
			  	margin-left: 10px;
			}

			@-webkit-keyframes spin {
			  0% { -webkit-transform: rotate(0deg); }
			  100% { -webkit-transform: rotate(360deg); }
			}

			@keyframes  spin {
			  0% { transform: rotate(0deg); }
			  100% { transform: rotate(360deg); }
			}
		</style>

        <div class="breadcrumbs navbar-top">
                        <div class="card">
	                        <div class="card-header border-header">
	                        	<div class="col-sm-12">
	                        		<div class="row">
			                        	<div class="col-sm-3">
			                        		<strong class="card-title user-name">Staff Users List</strong>
			                        	</div>
			                            <div class="col-sm-4">
			                            	<form id="myCsvForm" class="form-inline" action="<?php echo e(url('admin/user/export/csv')); ?>" method="post">
			                            		<?php echo csrf_field(); ?>                            			
			                            		<label>
					                        		<select name="csv_filter" class="form-control form-control-sm">
					                        			<option value="all">All users</option>
					                        			<option value="verify">Verified Users</option>
					                        			<option value="monthly">Active Monthly Users</option>
					                        			<option value="yearly">Active Yearly Users</option>
					                        			<option value="expired">Expired Subscriptions</option>
					                        		</select>
			                            			<a class="csvGenerate">Generate csv</a>	
			                            			<div class="loader"></div>
				                        		</label>
			                            	</form>
			                            </div>
			                            <div class="col-sm-5">
			                            	<a class="btn btn-primary float-right ml-2" href="<?php echo URL::to('/admin/user/add'); ?>"><strong class="card-title"><i class="fa fa-plus"></i> Add New</strong></a>
				                            <a href="<?php echo e(url('admin/users/trash')); ?>" class="btn btn-secondary float-right ml-2"><strong class="card-title"><i class="fa fa-trash"></i> View Trash (<?php echo e(App\User::onlyTrashed()->count()); ?>)</strong></a>
				                            <a href="<?php echo e(url('admin/users/suspended')); ?>" class="btn btn-secondary float-right ml-2"><strong class="card-title"><i class="fa fa-trash"></i> View Suspended Users (<?php echo e(App\User::where('user_suspend',1)->count()); ?>)</strong></a>
			                            </div>
			                        </div>
	                        	</div>
	                    	</div>
            			</div>
        </div>

        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <strong class="card-title">Staff Users List</strong>
                        </div> -->
                        <div class="card-body">
                        	<div class="video_bulk_action">
	                        	<label>Bulk Action 
	                        		<select name="bulk_action" class="form-control form-control-sm">
	                        			<option value="">Select action</option>
	                        			<option value="trash">Move to trash</option>
	                        			<option value="permenentt">Permanently delete</option>
	                        			<option value="suspend">Suspend the user</option>
	                        		</select>
	                        	</label>
	                        	<a class="apply_action">Apply</a>
	                        </div>
							<table id="users" class="table table-striped table-bordered usertb" style="width:100%">
								<thead>
									<tr>
										<th><input type="checkbox" class="deleteall delete_row"></th>
										<th>IMAGE</th>
										<th>Name</th>
										<th>VIBEZ ID</th>
										<th>Subscription Type</th>
										<th>Expiry Date</th>
										<th>Email</th>
										<th>Created_At</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th><input type="checkbox" class="deleteall delete_row"></th>
										<th>IMAGE</th>
										<th>Name</th>
										<th>VIBEZ ID</th>
										<th>Subscription Type</th>
										<th>Expiry Date</th> 
										<th>Email</th>
										<th>Created_At</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
                        </div>
                    </div>
                </div>
				<input type="hidden" class="csrf_token" value="<?php echo e(csrf_token()); ?>">
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	<script src="<?php echo e(asset('admin_assets/js/lib/data-table/datatables.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/dataTables.buttons.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/buttons.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/jszip.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/pdfmake.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/vfs_fonts.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/buttons.html5.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/buttons.print.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/buttons.colVis.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/js/lib/data-table/datatables-init.js')); ?>"></script>


<script type="text/javascript">
	$(document).ready(function() {

		$('.csvGenerate').click(function(event) {
			/* Act on the event */

			$('#myCsvForm').submit();
		});

		$('[name="csv_filter"]').change(function(event) {
			/* Act on the event */
			$('.loader').removeClass('ds-none');
			UserDataTable.draw();
		});

		$('.deleteall').click(function(){
			$('#users tbody .delete_row').each(function(){
				if($(this).is(':checked')){
					$(this).prop('checked',false);	
				}else{
					$(this).prop('checked',true);
				}
				
			})
		});
		$('.apply_action').click(function(){
			var action = "";
			if($('.video_bulk_action select').val() == "trash"){
				var r = confirm("Are you sure want to move to trash selected users!");
			    if (r == true) {
			        action = "trash";
			    } else {
			     return false
			    }
			}
			if($('.video_bulk_action select').val() == "permenentt"){
				var r = confirm("Are you sure want to permenently delete selected users!");
			    if (r == true) {
			        action = "delete";
			    } else {
			     return false
			    }
			}
			if($('.video_bulk_action select').val() == "suspend"){
				var r = confirm("Are you sure want to suspend selected users!");
			    if (r == true) {
			        action = "suspend";
			    } else {
			     return false
			    }
			}
			if(action != ""){
				var users = [];
				$('#users').find('.delete_bulk').each(function(){ 
					if($(this).is(':checked')){
						users.push($(this).val());
					}
				});
				var csrf_token = jQuery(document).find('.csrf_token').val();
				$.ajax({
		       	    beforeSend: function(xhrObj){
			                xhrObj.setRequestHeader("X-CSRF-TOKEN",csrf_token); 
			        },
				   url: site_url+"/admin/user/bulk/action",
				   data: {action	: action,users : users},
				   success: function(data) {
				   	window.location.reload();
				   },
				   type: 'POST'
				});
			}
		})
		var csrf_token = jQuery(document).find('.csrf_token').val();
		var UserDataTable = $('#users').DataTable( {
			"processing": true,
			"serverSide": true,
			"bAutoWidth": false,
			"pageLength": 50,
			"ajax": {
				"url": "user/getuserdata",
				"type": "POST",
				"dataType": 'json',
                "error": function(data){
                        console.log(data);
                    },
				"headers" : {
						'X-CSRF-TOKEN': csrf_token
				},
				"data":function(data) {
				    data.csv_filter = $.trim($('[name="csv_filter"]').val());
				}
			},
			"columns": [
				{ "data": "id", "orderable": false,    render: function ( data, type, row ) {
						return '<input type="checkbox" class="delete_bulk delete_row" value="'+data+'">';
					return data;
				} },
				{ "data": "image" ,     render: function ( data, type, row ) {
						
						var aws_url = "<?php echo e(env('AWS_URL')); ?>";
						var APP_URL = "<?php echo e(env('APP_URL')); ?>";
						
						var new_img = APP_URL+data;

						if(new_img == APP_URL)
						{
							var new_img = APP_URL+'public/assets/img/favicon.png';
						}
						else
						{
							var new_img = new_img;	
						}


						return '<img src="'+data+'" class="img-thumbnail img-responsive"/>';
					return data;
				},"width": "10%" },
				{ "data": "name" },
				{ "data": "vibez_id","width": "200px"},
				{ "data": "plan_type","width": "200px"},
				{ "data": "end_date","width": "200px"},
				{ "data": "email","width": "200px"},
				{ "data": "created_at" },
				{ "data": "id",     render: function ( data, type, row ) {
						// console.log(data);
						var editlink = "<?php echo URL::to('/admin/user/edit/"+data+"'); ?>";
						var deletelink = "<?php echo URL::to('/admin/user/delete/"+data+"'); ?>";
						return '<li class="libtnlink"><a class="edit_link" href="'+editlink+'"><i class="fa fa-pencil"></i></a></li><li class="libtnlink"><a href="#confirmationDelete" class="btn btn-danger delete-smt-btn delete_link text-danger" data-toggle="modal" data-id="'+data+'" data-url="'+deletelink+'" data-target="#confirmationDelete"><i class="fa fa-trash"></i></a></li>';
					
					return data;
				}}
			]
		} );

		UserDataTable.on( 'draw', function () {
		    $('.loader').addClass('ds-none');
		});
		
		
    } );
</script>
<script type="text/javascript">
$(document).ready(function(){
	$('#users').on('click', '.delete-smt-btn', function(e){
		$('#confirmationDelete').modal("show");
		var id = $(this).attr('data-id');
		var url = $(this).attr('data-url');
		// $(".remove-record-model").attr("action",url);
		$(".del-link").attr("href",url);
	});
});
</script>
<!-- Delete Model -->

    <div id="confirmationDelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;top: 25% !important;">
        <div class="modal-dialog modal-md modal-notify modal-danger" style="width:55%;">
            <div class="modal-content">
                <div class="modal-header">
                	<h4 class="modal-title" id="custom-width-modalLabel">Are you sure you want to delete this record?</h4>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect remove-data-from-delete-form" data-dismiss="modal">Close</button>

                    <a class="btn btn-default text-danger del-link" href="<?php echo URL::to('/admin/user/delete/'); ?>">Delete</a>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>