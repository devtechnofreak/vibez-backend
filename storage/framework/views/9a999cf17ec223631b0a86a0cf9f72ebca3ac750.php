<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Vibez</title>

		<link rel="shortcut icon" type="image/png" href="/assets/img/favicon.png"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

		<style>
			@font-face {
				font-family: 'helveticaneuelight';
				src: url('/assets/fonts/helveticaneue_light-webfont.woff2') format('woff2'),
					 url('/assets/fonts/helveticaneue_light-webfont.woff') format('woff');
				font-weight: normal;
				font-style: normal;

			}
			body{
				/* background-color: #ED213A; */
				/* height: 97vh;
				display: table; */
				
			}
		
			div.vibez_logo{
			    display: table-cell;
				vertical-align: middle;
				width: 100vw;
				text-align: center;
			}
			div.bottom_text{
				font-family: 'helveticaneuelight';
				position: fixed;
				bottom : 93px;
				width : 100%;
				color:  #fff;
				left : 0px;
				text-align : center;
				letter-spacing : 1px 
			}
			@media  only screen and (max-width: 797px) {
				div.vibez_logo img{
					width : 70%;
				}
			}

			@font-face {
				font-family: 'HelveticaNeue';
				src: url('assets/fonts/HelveticaNeueBold.woff2') format('woff2'),
					url('assets/fonts/HelveticaNeueBold.woff') format('woff'),
					url('assets/fonts/HelveticaNeueBold.ttf') format('truetype'),
					url('assets/fonts/HelveticaNeueBold.svg#HelveticaNeueBold') format('svg');
				font-weight: 100;
				font-style: normal;
			}

			@font-face {
				font-family: 'HelveticaNeueRegular';
				src: url('assets/fonts/HelveticaNeueRegular.woff2') format('woff2'),
					url('assets/fonts/HelveticaNeueRegular.woff') format('woff'),
					url('assets/fonts/HelveticaNeueRegular.ttf') format('truetype'),
					url('assets/fonts/HelveticaNeueRegular.svg#HelveticaNeueRegular') format('svg');
				font-weight: 100;
				font-style: normal;
			}

			@font-face {
				font-family: 'HelveticaNeueLight';
				src: url('assets/fonts/HelveticaNeueLight.woff2') format('woff2'),
					url('assets/fonts/HelveticaNeueLight.woff') format('woff'),
					url('assets/fonts/HelveticaNeueLight.ttf') format('truetype'),
					url('assets/fonts/HelveticaNeueLight.svg#HelveticaNeueLight') format('svg');
				font-weight: 100;
				font-style: normal;
			}

			body{
				font-family: 'HelveticaNeue';
				font-size: 16px;
			}
			h1,h2, h3, h4, h5, h6{
				font-family: 'HelveticaNeue';
			}
			p{
				font-family: 'HelveticaNeueLight';
				font-weight: 600;
			}
			.top-content {
				width: 100%;
				float: left;
				background: #ed213a;
				height: 870px;
			}
			.header-logo {
				text-align: center;
				padding: 50px 0;
			}
			.vibez-top-content {
				text-align: right;
			}
			.vibez-top-content h1 {
				color: #fff;
				font-weight: 800;
				line-height: 40px;
				margin-bottom: 30px;
				font-size: 44px;
			}
			.vibez-top-content p {
				font-size: 22px;
				color: #fff;
				margin-bottom: 30px;
				font-weight: 400;
			}
			.vibez-top-content a {
				margin-bottom: 20px; 
				display: block;
			}
			.vibez-second-content {
				width: 100%;
				float: left;
				padding: 100px 0;
			}
			.vibez-second-content .second-wrapper-left{
				padding-right: 0px;
				border-bottom: 2px solid #7a7a7a;
			}
			.vibez-second-content .second-wrapper-right{
				padding-left: 0px;
				/* border-bottom: 2px solid #7a7a7a; */
			}
			.vibez-mobile-right-icon {
			    width: 100%;
			    float: left;
			    position: absolute;
			    bottom: 0;
			    border-bottom: 1px solid #a7a0a0;
			}
			.vibez-mobile-right-icon img {
				width: 100%;
			}
			.row-eq-height {
				display: -webkit-box;
				display: -webkit-flex;
				display: -ms-flexbox;
				display: flex;
			}
			.vibez-second-left-content h3{
				font-weight: 700;
				margin-top: 0px;
				margin-bottom: 10px;
				color: #000;
			}
			.vibez-second-left-content p {
				font-size: 20px;
			}
			.vibez-second-left-content {
				width: 100%;
				float: left;
				margin-top: 80px;
			}
			.vibez-second-left-content-column {
				width: 100%;
				float: left;
				margin-bottom: 55px;
			}
			.vibez-third-section {
				width: 100%;
				float: left;
				text-align: center;
				padding: 0px 0 100px;
			}
			.vibez-third-section h3{
				font-size: 40px;
				font-weight: 700;
				margin-bottom: 40px;
			}
			.vibez-third-section p {
				font-size: 20px;
			}
			.vibez-third-section .great-idea {
				background: #ed213a;
				color: #fff;
				padding: 15px;
				display: table;
				margin: 30px auto 0;
				border-radius: 10px;
				font-size: 20px;
				cursor:pointer;
			}
			.vibez-third-section .great-idea:hover{
				text-decoration: none;
				color: #fff;
			}
			footer{
				width: 100%;
				float: left;
				background: #000;
				padding: 60px 0;
			}
			footer p {
				color: #fff;
				font-weight: 400;
			}

			#myModal{
				background: rgba(0, 0, 0, 0.90);
			}
			.modal-content {
				background: transparent;
			}
			.modal-header {
				border-bottom: none;
			}

			#myModal .form-control {
				background-color: transparent;
				border: none;
				border-bottom: 1px solid;
				color: #fff;
				border-radius: 0;
				font-family: 'HelveticaNeueLight';
    			font-weight: 700;
			}
			#myModal .form-group .btn-default {
				background-color: #ed213a;
				font-family: 'HelveticaNeueLight';
    			font-weight: 700;
				color: #fff;
				border-radius: 0;
				border: none;
				font-size: 18px;
				letter-spacing: 1px;
			}
			#myModal .close {
				color: #fff;
				opacity: 1;
				font-size: 32px;
			}
			#myModal textarea.form-control {
				border: 1px solid;
				height: 130px;
			}

			.vibez-top-content strong {
				font-family: 'HelveticaNeue';
			}
			#loginModal .modal-header {
				display: table;
			    text-align: right;
			    position: absolute;
			    right: 0;
			    z-index: 100;
			}
			#loginModal .modal-header .close{
				font-size: 32px;
			}
			#loginModal .btn-default {

			}
			#loginModal .extra-links {
				display: inline-block;
			    vertical-align: bottom;
			    margin-left: 5px;
			}
			#forgot-pass {
				display: block;
    			font-size: 12px;
    			color: #000;
			}
			#loginModal .extra-links span {
				font-size: 12px;
			}
			#loginModal .extra-links #new-register {
				font-size: 12px;
				color: #000;
			}
			.login-btn {
				background: #ed213a;
			    color: #fff;
			    border: none;
			    border-radius: 0;
			}

			
			
			

			@media  only screen and (max-width: 40em) {

				.container {
					padding-right: 30px;
    				padding-left: 30px;
				}

				.header-logo img {
					width: 300px;
				}
				.top-content .hero-banner img{
					width: 100%;
				}

				.top-content {
					height: auto;
				}

				.top-content .col-sm-8 img {
					width: 100%;
				}
				.vibez-top-content {
					text-align: center;
				}
				.vibez-top-content h1 {
					text-align: center;
					line-height: normal
				}
				.vibez-top-content p {
					text-align: center;
				}

				.vibez-top-content a {
					margin-bottom: 80px;
					display: inline-block;
					margin-right: 10px;
					width: 45%;
				}

				.vibez-top-content a img{
					width: 100%;
				}

				.vibez-second-content .row-eq-height{
					display: block;
				}
				.vibez-second-left-content {
					text-align: center;
				}
				.vibez-second-content .col-sm-8 {
					border-bottom: none;
					padding-right: 15px;
				}
				.vibez-second-content {
					padding: 0px;
				}
				.vibez-second-left-content h3 {
					margin: 28px 0 10px;
				}
				.vibez-second-content .col-sm-4 {
					padding-left : 15px;
				}
				.vibez-mobile-right-icon {
					margin-bottom: 50px;
				}
				.vibez-mobile-right-icon img{ 
					max-width: 100%;
    			height: auto;
				}

				    
				.vibez-third-section p {
					font-size: 16px;
				}
				footer {
					text-align: center;
				}
				.vibez-second-left-content p {
					font-size: 16px;
				}
				.vibez-second-content .second-wrapper-left {
					border-bottom: 0;
				}
				.vibez-second-content .second-wrapper-right {
					padding-left: 15px;
				}
				.second-wrapper-right .vibez-mobile-right-icon {
					margin-bottom: 0;
				}
				.vibez-second-content .second-wrapper-left {
					padding-right: 15px;
				}
				.vibez-second-content .second-wrapper-right {
					padding-left: 15px;
				}
				.vibez-mobile-right-icon {
					position: relative;
				}


				
			
			}

			/* min-width 641px and max-width 1024px, use when QAing tablet-only issues */ 
			@media  only screen and (min-width: 40.063em) and (max-width: 64em) { 
				.top-content {
					height: auto;
				}
				.vibez-top-content {
					text-align: center;
				}
				.vibez-second-content .row{
					display:block;
				}
				.vibez-mobile-right-icon {
					text-align: center;
				}
				.top-content .hero-banner img{
					width: 100%;
				}
				.vibez-second-content .second-wrapper-left {
					border-bottom: 0;
					padding-right: 15px;
				}
				.vibez-second-content .second-wrapper-right {
					padding-left: 15px;
				}
				.vibez-mobile-right-icon {
					position: relative;
				}



			} 

		</style>
	</head>
	<body>
		<!-- first section -->
		<div class="top-content">
      <div class="container">
			<div class="row">
        <div class="header-logo">
          <img src="/assets/img/vibezlogo.png" />
        </div>
        <div class="col-sm-12 col-md-9 hero-banner">
          <img src="/assets/img/viber-hero.png" />
        </div>
        <div class="col-sm-12 col-md-3">
          <div class="vibez-top-content">
              <h1>Join Africa's 
              #1 music 
              community</h1>
              <p><strong>Vibe with</strong> the hottest artists in Africa.
              Stay updated with the latest music,
              news, videos, events and lots more!</p>
              <a target="_blank" href="https://play.google.com/store/apps/details?id=com.ostech.vibez" class="andorid app-icon">
                <img src="/assets/img/android-icon.png">
              </a>
              <a target="_blank" href="https://itunes.apple.com/ng/app/vibez-africa-to-the-world/id1445991220?mt=8" class="ios app-icon">
                <img src="/assets/img/ios-icon.png">
              </a>
          </div>
					</div>
        </div>
        </div>
    </div>

    <!-- second section -->
    <div class="vibez-second-content">

        <div class="container">
          <div class="row row-eq-height">
            <div class="col-xs-12 col-sm-12 col-md-8 second-wrapper-left">

              <div class="vibez-second-left-content">

                  <div class="vibez-second-left-content-column">
                    <div class="col-sm-2">
                      <div class="vbz-icon">
                        <img src="/assets/img/your-music.png">
                      </div>
                    </div>
                    <div class="col-sm-10">
                        <h3>Your music, your way</h3>
                        <p>Organise your music into playlists according
                          to your mood and vibe whenever, wherever!
                          </p>
                    </div>
                  </div>

                  <div class="vibez-second-left-content-column">
                    <div class="col-sm-2">
                      <div class="vbz-icon">
                        <img src="/assets/img/video-chat.png">
                      </div>
                    </div>
                    <div class="col-sm-10">
                        <h3>No more FOMO!</h3>
                        <p>Stay updated on all happenings in the
                          African music space. Watch streams or
                          highlights of the biggest events in Africa.
                          </p>
                    </div>
                  </div>

                  <div class="vibez-second-left-content-column">
                    <div class="col-sm-2">
                      <div class="vbz-icon">
                        <img src="/assets/img/make-friend.png">
                      </div>
                    </div>
                    <div class="col-sm-10">
                        <h3>Make Friends</h3>
                        <p>Connect with other African music lovers.
                          Check out their music preferences and
                          discover new vibez!
                          </p>
                    </div>
                  </div>

              </div>
                
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 second-wrapper-right">
                <div class="vibez-mobile-right-icon">
                  <img src="/assets/img/vibez-mobile-latest.png">
                </div>
            </div>

          </div>



        </div>
      
    </div>

    <!-- third section -->

    <div class="vibez-third-section">
        <div class="container">

            <h3>A journey of constant improvement!</h3>

            <p>The goal is to connect the best of African music across the continent to African music fans
								around the world!</p>

            <p>We believe we have built a beautiful platform for starters, but we know it can and will always
								be improved. You have a say in this.</p>

            <p>Feel free to hit us up with suggestions on how you think we can make Vibez even better for
								You!</p>

          <a data-toggle="modal" class="great-idea button">I’ve got a great idea!</a>
          <a style="display: none;" data-toggle="modal" class="login button">I’ve got a great idea!</a>
          

					<?php if(session()->has('message')): ?>
						<div class="alert alert-success" style="margin-top: 50px;font-family: 'HelveticaNeueLight';font-weight: 700;">
							<?php echo e(session()->get('message')); ?>

						</div>
					<?php endif; ?>	
          
        </div>
    </div>
  

  <footer>
    <div class="container">
      <div class="row">
        <div class="col-sm-9">
          <p>Copyright <?php echo date('Y') ?> OS Digital Technologies LTD - Vibez.<br>
  All rights reserved.</p>
        </div>
        <div class="col-sm-3">
          <img src="/assets/img/footer-icon.png">
        </div>
      </div>
  </div>
  </footer>


		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						
						
								
								<form class="form-horizontal" role="form" action="<?php echo e(route('support_popup_mail')); ?>" method="post">
									<?php echo e(csrf_field()); ?>


									<h3 style="text-align:center;color:#fff;font-family: 'HelveticaNeueLight';">A Vibe Idea!</h3>
									<div class="form-group">
										<div class="col-sm-12">
											<input type="text" class="form-control" id="name" placeholder="Name" name="name" required="required">
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-12">          
											<input type="email" class="form-control" id="email" placeholder="Email" name="email" required="required">
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-12">          
											<input type="text" class="form-control" id="phone" placeholder="Phone" name="phone" required="required" maxlength="15" onkeypress="return isNumber(event)">
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-12">          
											<textarea class="form-control" name="message" required="required" placeholder="Message"></textarea>
										</div>
									</div>
									
									<div class="form-group">        
										<div class="col-sm-12">
											<button type="submit" class="btn btn-default">Send</button>
										</div>
									</div>
								</form>
					</div>
				</div>

			</div>
		</div>


		<!-- login -->

		<div id="loginModal" class="modal fade" role="dialog" style="display: none;">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content" style="background-color: #fff;">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						
						<form class="form-horizontal" role="form" method="post">
							<?php echo e(csrf_field()); ?>


							<h3 style="text-align:left;color:#000;font-family: 'HelveticaNeueLight';">Login</h3>

							<div class="form-group">
								<div class="col-sm-12">
									<label>Vibez ID</label>
									<input type="text" class="form-control" id="vibez_id" name="name" required="required">
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-12">          
									<label>Password</label>
									<input type="password" class="form-control" id="password" name="password" required="required">
								</div>
							</div>

							<div class="form-group">        
								<div class="col-sm-12">
									<button type="submit" class="btn btn-default login-btn">Login</button>
									<div class="extra-links">
										<a id="forgot-pass"><strong>Forgot Password?</strong></a>
										<span>New User?</span> <a id="new-register"><strong>Register!</strong></a>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>

			</div>
		</div>



		<script>

			jQuery(document).on('click','.great-idea', function(){
				jQuery('#myModal').modal({backdrop: 'static'})  
			});

			jQuery(document).on('click','.login', function(){
				jQuery('#loginModal').modal({backdrop: 'static'})  
			});

			function isNumber(evt) {
			    evt = (evt) ? evt : window.event;
			    var charCode = (evt.which) ? evt.which : evt.keyCode;
			    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			        return false;
			    }
			    return true;
			}
			
		</script>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-138613820-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-138613820-1');
		</script>
		

	</body>
</html>