
<?php $__env->startSection('main_content'); ?>
<!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Artists</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
					<?php if(session()->has('message')): ?>
						<div class="alert alert-success">
							<?php echo e(session()->get('message')); ?>

						</div>
					<?php endif; ?>
                    <div class="card"> 
                        <div class="card-header">
                            <strong class="card-title">Import Artists</strong>
                            <a href="<?php echo e(url('admin/artists')); ?>" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Back</a>
                            <a href="<?php echo e(url('sample/import_artists.csv')); ?>" class="btn btn-success float-right ml-2"><i class="fa fa-download"></i> Download Sample Csv</a>
                            <?php if(isset($csv)): ?>
                            <a href="<?php echo e(url('admin/artist/import/bulk')); ?>" class="btn btn-success float-right ml-2"><i class="fa fa-refresh"></i></a>
                            <?php endif; ?>
                        </div>
						<div class="card-body">
							 <?php if($errors->any()): ?>
								  <div class="alert alert-danger">
									  <ul>
										  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											  <li><?php echo e($error); ?></li>
										  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									  </ul>
								  </div><br/>
							  <?php endif; ?>
							<form method="POST" action="<?php echo e(url('admin/artist/import/bulk')); ?>" enctype="multipart/form-data" class="form-inline">
							 <?php echo e(csrf_field()); ?>

							<?php if(isset($csv)): ?>
							<table class="table">
							  <thead>
							    <tr>
							      <th scope="col">#</th>
							      <th scope="col">Name</th>
							      <th scope="col">Bio</th>
							      <th scope="col">Is Featured</th>
							      <th scope="col">Date of Birth</th>
							      <th scope="col">Tags</th>
							      <th scope="col">Image</th>
							      <th scope="col">Country</th>
							    </tr>
							  </thead>
							  <tbody>
							<?php $__currentLoopData = $csv; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							    <tr>
							      <th scope="row"><?php echo e($key+1); ?></th>
							      <td><input name="artist[<?php echo e($key); ?>][name]" required="required" autofocus="autofocus" class="form-control" value="<?php echo e($data['name']); ?>" type="text"></td>
							      <td><textarea rows="8" cols="20" name="artist[<?php echo e($key); ?>][bio]" class="form-control"><?php echo e($data['bio']); ?></textarea></td>
							      <td><input value="1" type="checkbox" name="artist[<?php echo e($key); ?>][isFeatured]" <?php echo e($data['is_featured'] ?'checked' : ''); ?>></td>
							      <td><input class="release_date" type="text" name="artist[<?php echo e($key); ?>][dob]" value="<?php echo e($data['dob']); ?>" autocomplete="off"></td>
							      <td><input name="artist[<?php echo e($key); ?>][artist_tags]" value="<?php echo e($data['tags']); ?>" placeholder="Enter comma(,) separated Tags" autofocus="autofocus" class="form-control mb-2" type="text"></td>
							      <td><input name="artist[<?php echo e($key); ?>][image]" class="form-control" value="" type="file"/></td>
							      <td>
							      	<select class="form-control" style="width: 200px" name="artist[<?php echo e($key); ?>][country]" value="">
										<option value="">Please select Country</option>
										<?php
											foreach ($countries as $single) { ?>
												<option value="<?php echo $single->name; ?>" <?php echo ($data['country'] == $single->name) ? 'selected' : ''; ?>><?php echo $single->name; ?></option>
										<?php 	}
										?>
									</select>
							      	</td>
							    </tr>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							  </tbody>
							</table>
							<?php else: ?>
							  <div class="form-group mx-sm-3 mb-2">
							    <label for="inputfile" class="sr-only">Password</label>
							    <input type="file" class="form-control-file" id="inputfile" name="csv_import" placeholder="Csv File" required>
							  </div>
							  <?php if($errors->has('name')): ?>
								<span class="invalid-feedback">
									<strong><?php echo e($errors->first('csv_import')); ?></strong>
								</span>
							  <?php endif; ?>
							<?php endif; ?>
							  <button type="submit" class="btn btn-primary mb-2">Import</button>
							</form>
						</div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
	
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>