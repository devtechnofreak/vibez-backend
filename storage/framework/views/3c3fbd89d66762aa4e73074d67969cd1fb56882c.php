<tr>
    <td style="background-color: #ED213A;">
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0">
            <tr>
                <td class="content-cell" align="center" style="color : #fff !important;">
                    <p style="font-family: Avenir,Helvetica,sans-serif; box-sizing: border-box;line-height: 1.5em;margin-top: 0;color: #fff;font-size: 12px;text-align: center;">© <?php echo e(date('Y')); ?> Vibez. All rights reserved.</p>
                </td>
            </tr>
        </table>
    </td>
</tr>
