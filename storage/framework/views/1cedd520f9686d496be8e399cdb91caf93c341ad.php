<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="/assets/img/favicon.png"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <?php 
    	$flag = 0;
    ?>
	<?php if(isset($model)): ?>
	<meta property="fb:app_id" content="661917537496563" />
	<?php if(get_class($model) == 'App\Playlist'): ?>
		<?php 
			$flag 			= 1;
		?>
		<meta property="og:type" content="music.playlist">
		<meta property="og:music:song:url" content="<?php echo e($model->musics[0]->audio); ?>">
		<meta property="og:url" content="<?php echo e(url('share/Playlist/'.$model->id)); ?>">
		<meta property="og:description" content="<?php echo e($model->description); ?>">
		<meta property="og:playlist:id" content="<?php echo e($model->id); ?>">
		<meta property="og:title" content="<?php echo e('Vibez Playlist: '.$model->title.' '.$model->user_name); ?>">
		<meta property="og:image" content="<?php echo e($model->thumb); ?>">
	<?php endif; ?> 
	<?php if(get_class($model) == 'App\Music'): ?>
		<?php 
			$flag = 1;
			$artists = array();
			foreach($model->artists as $single_artist){
				$artists[] = $single_artist->name;
			}

			$arName = '';
			if (!empty($artists)) {
				$arName = ' by '.implode(",", $artists);
			}
		?>
		<meta property="og:type" content="music.song">
		<meta property="og:music:duration" content="<?php echo e((int)$model->duration); ?>">
		<meta property="og:url" content="<?php echo e(url('share/Music/'.$model->id)); ?>">
		<meta property="og:music:id" content="<?php echo e($model->id); ?>">
		<meta property="og:music:album:id" content="<?php echo e($model->album[0]->id); ?>">
		<meta property="og:title" content="<?php echo e('Vibez Music: '.$model->title.$arName); ?>">
		<meta property="og:image" content="<?php echo e($model->thumb); ?>">
	<?php endif; ?>
	<?php if(get_class($model) == 'App\Video'): ?>
		<?php 
			$flag = 1;

			$artists = array();
			foreach($model->artists as $single_artist){
				$artists[] = $single_artist->name;
			}

			$arName = '';
			if (!empty($artists)) {
				$arName = ' by '.implode(",", $artists);
			}
		?>
		<meta property="og:type" content="video.other">
		<meta property="og:music:duration" content="<?php echo e((int)$model->duration); ?>">
		<meta property="og:url" content="<?php echo e(url('share/Video/'.$model->id)); ?>">
		<meta property="og:video:id" content="<?php echo e($model->id); ?>">
		<meta property="og:title" content="<?php echo e('Vibez Video: '.$model->title.$arName); ?>">
		<meta property="og:image" content="<?php echo e($model->thumb); ?>">
	<?php endif; ?> 
	<?php if(get_class($model) == 'App\User'): ?>
		<?php 
			$flag = 1;
		?>
		<meta property="og:type" content="profile">
		<meta property="og:profile:first_name" content="<?php echo e($model->name); ?>">
		<meta property="og:url" content="<?php echo e(url('share/User/'.$model->id)); ?>">
		<meta property="og:profile:gender" content="<?php echo e($model->gender); ?>">
		<meta property="og:profile:username" content="<?php echo e($model->vibez_id); ?>">
		<meta property="og:title" content="<?php echo e($model->name); ?>">
		<meta property="og:image" content="<?php echo e($model->image); ?>">
	<?php endif; ?>
	
	<?php if(get_class($model) == 'App\News'): ?>
		<?php 
			$flag = 1;
			$arName = 'Vibez Update:';
					
		?>
		<meta property="og:type" content="news">
		<meta property="og:url" content="<?php echo e(url('share/News/'.$model->id)); ?>">
		<meta property="og:news:id" content="<?php echo e($model->id); ?>">
		<meta property="og:title" content="<?php echo e($arName.' '.$model->title); ?>">
		<meta property="og:image" content="<?php echo e($model->thumb); ?>">
	<?php endif; ?>


	<?php if(get_class($model) == 'App\Album'): ?>
		<?php 
			$flag = 1;
		?>
		<meta property="og:type" content="album">
		<meta property="og:url" content="<?php echo e(url('share/Album/'.$model->id)); ?>">
		<meta property="og:album:id" content="<?php echo e($model->id); ?>">
		<meta property="og:title" content="<?php echo e($model->title.' '.$model->user_name); ?>">
		<meta property="og:image" content="<?php echo e($model->thumb); ?>">
	<?php endif; ?>

	<?php if(get_class($model) == 'App\LiveStream'): ?>
		<?php 
			$flag = 1;
		?>
		<meta property="og:type" content="livestream">
		<meta property="og:url" content="<?php echo e(url('share/LiveStream/'.$model->id)); ?>">
		<meta property="og:livestream:id" content="<?php echo e($model->id); ?>">
		<meta property="og:title" content="<?php echo e($model->title); ?>">
		<meta property="og:image" content="<?php echo e($model->thumb); ?>">
	<?php endif; ?>
		
    <?php endif; ?>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Vibez')); ?></title>

    <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <?php if($flag == 1){ ?>
    	<link href="<?php echo e(asset('css/custom_share.css')); ?>" rel="stylesheet">
    <?php } ?>
</head>
<body>
    <div id="app">
    	<?php if($flag == 1){ 
    		
    		$class = get_class($model);
			$content = '';
			switch ($class) {
			    case "App\Playlist":
					$content = '<div class="share_url playlist_wrapper">
									<div class="container">
									<div class="common-inner-wrapper">
										<div class="row">
												<div class="col-sm-5 col-md-5 col-lg-offset-2 col-lg-4">
													<h2>Playlist</h2>
													<div class="column left-content">
														<div class="img_box">
															<img src="'.$model->thumb.'">
														</div>
													</div>
												</div>
												<div class="col-sm-7 col-md-7 col-lg-4">
													<div class="column right-content">
														<div class="name">
															<h1 class="value">'. $playlist_title = str_limit( $model->title, 65) .'</h1>
															<p class="value">'. $playlist_desc = str_limit( $model->description, 130 , '[...]') .'</p>
														</div>
													</div>
													<div class="footer-common-app-link">
														<p>Download the app to listen</p>
														<a class="android app-icon" href="https://play.google.com/store/apps/details?id=com.ostech.vibez" target="_blank">
															<img src="/assets/img/android-icon.png">
														</a>
														<a class="ios app-icon" href="https://itunes.apple.com/ng/app/vibez-africa-to-the-world/id1445991220?mt=8" target="_blank">
															<img src="/assets/img/ios-icon.png">
														</a>
													</div>
												</div>
												
											</div>
										</div>
									</div>
			        			</div>';
			        break;
				case "App\Music":

				$artists = array();
				foreach($model->artists as $single_artist){
					$artists[] = $single_artist->name;
				}
				$content = '<div class="share_url music_wrapper">
								<div class="container">
								<div class="common-inner-wrapper">
									<div class="row">
											<div class="col-sm-5 col-md-5 col-lg-offset-2 col-lg-4">
												<h2>Music</h2>
												<div class="column left-content">
													<div class="img_box">
														<img src="'.$model->image.'">
													</div>
												</div>
											</div>
											<div class="col-sm-7 col-md-7 col-lg-4">
												<div class="column right-content">
													<div class="name">
														<p class="label">Track Title</p>
														<h1 class="value">'. $playlist_title = str_limit( $model->title, 65) .'</h1>
														<p class="label artist-label">Artist</p>
														<p class="value">'. $playlist_desc = implode(",",$artists) .'</p>
													</div>
												</div>
												<div class="footer-common-app-link">
													<p>Download the app to listen</p>
													<a class="android app-icon" href="https://play.google.com/store/apps/details?id=com.ostech.vibez" target="_blank">
														<img src="/assets/img/android-icon.png">
													</a>
													<a class="ios app-icon" href="https://itunes.apple.com/ng/app/vibez-africa-to-the-world/id1445991220?mt=8" target="_blank">
														<img src="/assets/img/ios-icon.png">
													</a>
												</div>
											</div>
											
										</div>
									</div>
								</div>
							</div>';
			        break;
			    case "App\Video":
			    	$artists = array();
					foreach($model->artists as $single_artist){
						$artists[] = $single_artist->name;
					}
			        $content = '<div class="share_url news_wrapper">
									<div class="container">
										<div class="row">
											<div class="common-inner-wrapper">
												<div class="col-sm-5 col-md-5 col-lg-offset-2 col-lg-4">
													<h2>Video</h2>
													<div class="column left-content">
														<div class="img_box">
															<img src="'.$model->thumb.'">
														</div>
													</div>
												</div>
												<div class="col-sm-7 col-md-7 col-lg-4">
													<div class="column right-content">
														<div class="name">
															<h1 class="value">'. implode(",",$artists) .' - '. str_limit( $model->title, 65) .'</h1>
															<p class="value">'. $news_desc = str_limit( $model->description, 130 , '[...]') .'</p>
															
														</div>
													</div>
												</div>
												<div class="footer-common-app-link">
													<p>Download the app to watch the video</p>
													<a class="android app-icon" href="https://play.google.com/store/apps/details?id=com.ostech.vibez" target="_blank">
														<img src="/assets/img/android-icon.png">
													</a>
													<a class="ios app-icon" href="https://itunes.apple.com/ng/app/vibez-africa-to-the-world/id1445991220?mt=8" target="_blank">
														<img src="/assets/img/ios-icon.png">
													</a>
												</div>
												
											</div>
											
										</div>
									</div>
			        			</div>';
			        break;
			        case "App\LiveStream":
			        	$content = '<div class="share_url news_wrapper">
									<div class="container">
										<div class="row">
											<div class="common-inner-wrapper">
												<div class="col-sm-5 col-md-5 col-lg-offset-2 col-lg-4">
													<h2>LiveStream</h2>
													<div class="column left-content">
														<div class="img_box">
															<img src="'.$model->thumb.'">
														</div>
													</div>
												</div>
												<div class="col-sm-7 col-md-7 col-lg-4">
													<div class="column right-content">
														<div class="name">
															<h1 class="value">'. $news_title = str_limit( $model->title, 65) .'</h1>
															<p class="value">'. $news_desc = str_limit( $model->description, 130 , '[...]') .'</p>
														</div>
													</div>
												</div>
												<div class="footer-common-app-link">
													<p>Download the app to read details</p>
													<a class="android app-icon" href="https://play.google.com/store/apps/details?id=com.ostech.vibez" target="_blank">
														<img src="/assets/img/android-icon.png">
													</a>
													<a class="ios app-icon" href="https://itunes.apple.com/ng/app/vibez-africa-to-the-world/id1445991220?mt=8" target="_blank">
														<img src="/assets/img/ios-icon.png">
													</a>
												</div>
												
											</div>
											
										</div>
									</div>
			        			</div>';
			        break;
				case "App\User":
					if($model->image =='')
					{
						$newimage = 'https://www.thevibez.net/public/assets/img/favicon.png';
						$style = 'style = width:50%';
					}
					else
					{
						$newimage = $model->image;
						$style = '';
					}
					$content = '<div class="share_url news_wrapper">
									<div class="container">
										<div class="row">
											<div class="common-inner-wrapper">
												<div class="col-sm-5 col-md-5 col-lg-offset-2 col-lg-4">
													<h2>User</h2>
													<div class="column left-content">
														<div class="img_box">
															<img src="'.$newimage.'" '.$style.'>
														</div>
													</div>
												</div>
												<div class="col-sm-7 col-md-7 col-lg-4">
													<div class="column right-content">
														<div class="name">
															<h1 class="value">VIBEZ ID</h1>
															<p class="value">'.$model->vibez_id.'</p>
														</div>
													</div>
												</div>
												<div class="footer-common-app-link">
													<p>Download the app to read details</p>
													<a class="android app-icon" href="https://play.google.com/store/apps/details?id=com.ostech.vibez" target="_blank">
														<img src="/assets/img/android-icon.png">
													</a>
													<a class="ios app-icon" href="https://itunes.apple.com/ng/app/vibez-africa-to-the-world/id1445991220?mt=8" target="_blank">
														<img src="/assets/img/ios-icon.png">
													</a>
												</div>
												
											</div>
											
										</div>
									</div>
			        			</div>';
			        break;
				case "App\News":
					$content = '<div class="share_url news_wrapper">
									<div class="container">
										<div class="row">
											<div class="common-inner-wrapper">
												<div class="col-sm-5 col-md-5 col-lg-offset-2 col-lg-4">
													<h2>News</h2>
													<div class="column left-content">
														<div class="img_box">
															<img src="'.$model->thumb.'">
														</div>
													</div>
												</div>
												<div class="col-sm-7 col-md-7 col-lg-4">
													<div class="column right-content">
														<div class="name">
															<h1 class="value">'. $news_title = str_limit( $model->title, 65) .'</h1>
															<p class="value">'. $news_desc = str_limit( $model->description, 130 , '[...]') .'</p>
														</div>
													</div>
												</div>
												<div class="footer-common-app-link">
													<p>Download the app to read details</p>
													<a class="android app-icon" href="https://play.google.com/store/apps/details?id=com.ostech.vibez" target="_blank">
														<img src="/assets/img/android-icon.png">
													</a>
													<a class="ios app-icon" href="https://itunes.apple.com/ng/app/vibez-africa-to-the-world/id1445991220?mt=8" target="_blank">
														<img src="/assets/img/ios-icon.png">
													</a>
												</div>
												
											</div>
											
										</div>
									</div>
			        			</div>';
			        break;
		        case "App\Album":
				

					$artists = array();
					foreach($model->artists as $single_artist){
						$artists[] = $single_artist->name;
					}
					$content = '<div class="share_url music_wrapper">
									<div class="container">
									<div class="common-inner-wrapper">
										<div class="row">
												<div class="col-sm-5 col-md-5 col-lg-offset-2 col-lg-4">
													<h2>Album</h2>
													<div class="column left-content">
														<div class="img_box">
															<img src="'.$model->image.'">
														</div>
													</div>
												</div>
												<div class="col-sm-7 col-md-7 col-lg-4">
													<div class="column right-content">
														<div class="name">
															<p class="label">Album Title</p>
															<h1 class="value">'. $playlist_title = str_limit( $model->title, 65) .'</h1>
															<p class="label artist-label">Artist</p>
															<p class="value">'. $playlist_desc = implode(",",$artists) .'</p>
														</div>
													</div>
													<div class="footer-common-app-link">
														<p>Download the app to listen</p>
														<a class="android app-icon" href="https://play.google.com/store/apps/details?id=com.ostech.vibez" target="_blank">
															<img src="/assets/img/android-icon.png">
														</a>
														<a class="ios app-icon" href="https://itunes.apple.com/ng/app/vibez-africa-to-the-world/id1445991220?mt=8" target="_blank">
															<img src="/assets/img/ios-icon.png">
														</a>
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>';
						break;		
			        
			    default:
			        $content = '';
			    break;    
			}	
			
    	?>
    				
				<div class="header_logo">
					<div class="container">
						<div class="row">
							<div class="col-lg-offset-2 col-lg-12">
								<a href="https://www.thevibez.net">
									<img src="/assets/img/vibezlogo.png">
								</a>
							</div>
						</div>
					</div>
				</div>

				<?php echo $content; ?>
				
				<footer>
					<div class="footer">
						<div class="container">
							<div class="row">
								<div class="col-xs-12 col-md-8 footer-left">
									<h4><strong>Vibe with</strong> the hottest artist in Africa.
									Stay updated with the latest music, news, videos, 
									events and lots more!</h4>
								</div>
								<div class="col-xs-12 col-md-4 footer-right">
									<img src="/assets/img/vibez-mobile-latest.png">
								</div>
							</div>	
						</div>
					</div>
					<div class="footer-bottom">
						<div class="container">
							
								<div class="col-xs-12 col-sm-12 col-md-9">
								<p>Copyright 2019 OS Digital Technologies LTD - Vibez.<br>
						All rights reserved.</p>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-3">
								<img src="/assets/img/footer-icon.png">
								</div>
							
						</div>
					</div>
				</footer>

				
				
		<?php }else{ ?>
			<style type="text/css">
			body {
				font-size: 1.5rem;
			}
			.btn
			{
				font-size: 1.5rem;
			}
			.form-control
			{
				font-size: 1.5rem;
			}
			</style>
			<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
	            <div class="container">
	                <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
	                    <?php echo e(config('app.name', 'Laravel')); ?>

	                </a>
	                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	                    <span class="navbar-toggler-icon"></span>
	                </button>
	
	                <div class="collapse navbar-collapse" id="navbarSupportedContent">
	                    <!-- Left Side Of Navbar -->
	                    <ul class="navbar-nav mr-auto">
	
	                    </ul>
	
	                    <!-- Right Side Of Navbar -->
	                    <ul class="navbar-nav ml-auto">
	                        <!-- Authentication Links -->
	                        <?php if(auth()->guard()->guest()): ?>
	                            <li><a class="nav-link" href="<?php echo e(route('admin.login')); ?>"><?php echo e(__('Login')); ?></a></li>
	                            <!-- <li><a class="nav-link" href="<?php echo e(route('admin.register')); ?>"><?php echo e(__('Register')); ?></a></li> -->
	                        <?php else: ?>
	                            <li class="nav-item dropdown">
	                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
	                                    <?php echo e(Auth::user()->name); ?> <span class="caret"></span>
	                                </a>
	
	                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	                                    <a class="dropdown-item" href="<?php echo e(route('admin.logout')); ?>"
	                                       onclick="event.preventDefault();
	                                                     document.getElementById('logout-form').submit();">
	                                        <?php echo e(__('Logout')); ?>

	                                    </a>
	
	                                    <form id="logout-form" action="<?php echo e(route('admin.logout')); ?>" method="POST" style="display: none;">
	                                        <?php echo csrf_field(); ?>
	                                    </form>
	                                </div>
	                            </li>
	                        <?php endif; ?>
	                    </ul>
	                </div>
	            </div>
	        </nav>
	
	        <main class="py-4">
	            <?php echo $__env->yieldContent('content'); ?>
	        </main>	
		<?php } ?>
        
    </div>
</body>
</html>
