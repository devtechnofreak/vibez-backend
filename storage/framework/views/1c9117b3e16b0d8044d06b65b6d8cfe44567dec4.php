<?php $__env->startSection('main_content'); ?>
<!-- Header-->

        <!-- <div class="breadcrumbs">
            <div class="col-sm-12">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Albums</h1>
                    </div>
                </div>
            </div>
        </div> -->
         <div class="breadcrumbs navbar-top">
                        <div class="card">
                            <div class="card-header border-header">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <?php if(isset($artists)): ?>
				                        	<strong class="card-title">Select Artist</strong>
				                        	<?php else: ?>
				                            <strong class="card-title">Add Album</strong>
				                            <?php endif; ?>
                                        </div>
                                        <div class="col-md-9">
                                            <a href="<?php echo e(url('admin/albums')); ?>" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Album List</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>
        <div class="content mt-5">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
					<?php if(session()->has('message')): ?>
						<div class="alert alert-success">
							<?php echo e(session()->get('message')); ?>

						</div>
					<?php endif; ?>
                    <div class="card"> 
                        <!-- <div class="card-header">
                        	<?php if(isset($artists)): ?>
                        	<strong class="card-title">Select Artist</strong>
                        	<?php else: ?>
                            <strong class="card-title">Add Album</strong>
                            <?php endif; ?>
                            <a href="<?php echo e(url('admin/albums')); ?>" class="btn btn-primary float-right ml-2"><i class="fa fa-arrow-left"></i> Album List</a>
                        </div> -->
						<div class="card-body">
							 <?php if($errors->any()): ?>
								  <div class="alert alert-danger">
									  <ul>
										  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											  <li><?php echo e($error); ?></li>
										  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									  </ul>
								  </div><br/>
							  <?php endif; ?>
							 <?php if(isset($artists)): ?>
							 <form method="POST" action="<?php echo URL::to('/admin/album/redirect'); ?>" enctype="multipart/form-data" class="add_playlist">
							 	<?php echo e(csrf_field()); ?>

                             <div class="form-group row justify-content-md-center">
									<div class="col-md-6 justify-content-md-center">
										<select name="artist" value="" class="col-md-6 form-control banner_type">
											<option value="">Select Artist</option>
											<?php $__currentLoopData = $artists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $artist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($artist->id); ?>"><?php echo e($artist->name); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
									</div> 
									<div class="col-md-6">
										<a class="btn btn-success add_playlist_btn">Create Album <i class="fa fa-arrow-right"></i></a>
									</div>
								</div> 
							</form>
							 <?php else: ?>
							<form method="POST" action="<?php echo URL::to('/admin/album/create'); ?>" enctype="multipart/form-data" class="add_playlist">
							 	<?php echo e(csrf_field()); ?>

								
								<div class="form-group row"><label for="name" class="col-md-2 col-form-label">Title</label>
									<div class="col-md-6"><input id="title" name="title" value="" required="required" autofocus="autofocus" class="form-control" type="text"></div>
								</div>
								<?php if($errors->has('title')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('title')); ?></strong>
                                    </span>
                                <?php endif; ?>
                                <div class="form-group row"><label for="description" class="col-md-2 col-form-label">Description</label>
									<div class="col-md-6"><textarea name="description" class="form-control txtbio" placeholder="Description" required="required"></textarea></div>
								</div>
								<div class="form-group row"><label for="image" class="col-md-2 col-form-label">Image</label>
									<div class="col-md-6"><input id="image" name="image" value="" required="required" autofocus="autofocus" class="form-control" type="file"></div>
								</div>
								<?php if($errors->has('image')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('image')); ?></strong>
                                    </span>
                                <?php endif; ?>
                                <div class="form-group row">
                                	<label for="album_tags" class="col-md-2 col-form-label">Tags</label>
										<div class="col-md-6">
											<input id="album_tags" name="tags" value="" placeholder="Enter comma , separated Tags" autofocus="autofocus" class="form-control mb-2" type="text">
										</div>
								</div>

								<div class="form-group row">
                                	<label for="album_tags" class="col-md-2 col-form-label">Artist</label>
										<div class="col-md-6">
											<?php echo e($artist_names); ?>

										</div>
								</div>

								<?php if(isset($artist)): ?>
				                <input type="hidden" value="<?php echo e($artist->id); ?>" name="artist_id"/>
				                <?php endif; ?>

				                <div class="form-group row">
                                	<label for="album_tags" class="col-md-2 col-form-label">Artists Featured</label>
										<div class="col-md-6">
											<select name="featured_artist_id[]" value="" class="js-example-basic-multiple form-control" multiple="multiple">
												<option value="">Select Artist</option>
												<?php $__currentLoopData = $all_artists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $artist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<option value="<?php echo e($artist->id); ?>"><?php echo e($artist->name); ?></option>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</select>
										</div>
								</div>

								<div class="form-group row">
                                	<label for="album_tags" class="col-md-2 col-form-label">Release Year</label>
										<div class="col-md-6">
											<select name="release_year" class="form-control mb-2">
												<option value="">-Please select-</option>
												<?php 
	                            				$firstYear = (int)date('Y') - 60;
												$lastYear = (int)date('Y');
												for($i=$firstYear;$i<=$lastYear;$i++)
												{ ?><option value="<?php echo $i; ?>"><?php echo $i; ?></option>
												<?php } ?>
											</select>
										</div>
								</div>

				                
								<div class="form-group row mb-0">
									<div class="col-md-6 offset-md-4">
										<a class="btn btn-primary add_playlist_btn">Add Tracks</a>
									</div>
								</div>
							</form>
							<?php endif; ?>
						</div>
                    </div>

                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
		
<?php $__env->stopSection(); ?>
<?php $__env->startSection('headscripts'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('admin_assets/plugins/select2/select2.min.css')); ?>">
<script src="<?php echo e(asset('admin_assets/js/vendor/jquery-2.1.4.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin_assets/plugins/select2/select2.min.js')); ?>"></script>

<script type="text/javascript">
    $(document).ready(function() {
	    $('.js-example-basic-multiple').select2({
	    	placeholder: "Select an Artist",
    		allowClear: true
	    });
	});
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>