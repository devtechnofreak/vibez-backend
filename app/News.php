<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\CommentableTrait;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
use Auth;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
	use SoftDeletes, CommentableTrait, CanBeLiked, SearchableTrait;
	
	protected $dates = ['deleted_at'];
	
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'news.title' => 10,
           // 'news.description' => 2,
        ]
    ];
	protected $appends = array('thumb','is_liked','view_link','total_comments','db_image','db_thumb_image', 'total_likes');
 	protected $fillable = [
        'title', 'image', 'video', 'audio', 'description', 'is_vip', 'is_noti', 'is_featured', 'is_pushed', 'feature_image', 'created_date','modified_by', 'modified_date', 'status','created_at','short_codes','related_artist'
    ];
	
	protected $hidden =[
		'video','audio','is_vip','is_noti','feature_image','created_by','modified_date','created_date','modified_by','status'
	];

	public function getTotalLikesAttribute()
    {
        return $this->likers()->count();
    }
	
	public function getImageAttribute($value)
	{
		return getCloudFrontPrivateUrl($value);
	}

	public function artists(){
		return $this->belongsToMany('App\Artist','artist_news')->where('is_main_artist','=','1')->withTimestamps();
	}

	public function related_artist()
    {
        return $this->belongsToMany('App\Artist','artist_news')->where('is_main_artist','=','0')->withTimestamps();
    }
	
	public function artistNames(){
		return $this->belongsToMany('App\Artist','artist_news')->where('is_main_artist','=','1')->pluck('name');
	}
	
	public function getThumbAttribute()
	{
		$path = pathinfo($this->getOriginal('image'));
		return getCloudFrontPrivateUrl($path['dirname'].'/'.$path['filename'].'_thumb.'.$path['extension']);
	}
	
    public function banners()
    {
        return $this->morphMany('App\Banner', 'bannerable');
    }
	
	public function getContent()
	{
		
		// if(\Cache::has('news_description_array_'.$this->id)){
		// 	return \Cache::get('news_description_array_'.$this->id);
		// }

		$searchArray = array("<strong>", "</strong>", "&lrm;", "<em>", "</em>", "<u>", "</u>"); 
		$replaceArray = array("*", "*", "", "_", "_", "~", "~");
		// $searchArray = array("<strong>", "</strong>", "&lrm;"); 
		// $replaceArray = array("*", "*", "");

		// $content = str_replace('&lrm;', '', $this->description);
		
		$content = str_replace($searchArray, $replaceArray, $this->description);
		
		$domearray = array();
		$c=0;
		if(trim($content)){
			
			$dom = new \DOMDocument;
			libxml_use_internal_errors(true);
	   		$dom->loadHTML($content);
			libxml_clear_errors();
			$i = 0;
			
			foreach($dom->getElementsByTagName('*') as $element ){
				$domelement= new \stdClass;
				
				switch ($element->tagName) {
					case 'p':
						if(!empty($element->nodeValue)){
							$domelement->type = "text";
							$domelement->data = $this->remove_bs($element->ownerDocument->saveHTML($element));
							$c++;
							
							$domearray[]= $domelement;
						}
						break;
					case 'img':
						$domelement->type = "image";
						$domelement->data = $element->getAttribute('src');
						$c++;
						$domearray[]= $domelement;
						break;
					case 'iframe':
						$keys = parse_url($element->getAttribute('src'));
					    $path = explode("/", $keys['path']);
						$domelement->type = "video";
						$domelement->data = end($path);
						$domearray[] = $domelement;
						$c++;
						break;
					
					case 'html':
						break;
					
					case 'body':
						break;
						
					case 'footer':
						break;
					
					default:
						$domelement->type = 'text';
						$domelement->data = $this->remove_bs($element->nodeValue);
						if($element->tagName != "em" && $element->tagName != "a" && $element->tagName != "b" && $element->tagName != "span" ){
							$domearray[]= $domelement;	
						}
						$c++;
						break;
				}
				$i++;
			}
		}
		$expiresAt = now()->addDays(24);
		// \Cache::put('news_description_array_'.$this->id, $domearray, $expiresAt);
		return $domearray;		
	}

	 public function remove_bs($orig_text) {
	   
	     $text = $orig_text;
	     // Single letters
	     $text = preg_replace("/[∂άαáàâãªä]/u",      "", $text);
	     $text = preg_replace("/[∆лДΛдАÁÀÂÃÄ]/u",     "", $text);
	     $text = preg_replace("/[ЂЪЬБъь]/u",           "", $text);
	     $text = preg_replace("/[βвВ]/u",            "", $text);
	     $text = preg_replace("/[çς©с]/u",            "", $text);
	     $text = preg_replace("/[ÇС]/u",              "", $text);        
	     $text = preg_replace("/[δ]/u",             "", $text);
	     $text = preg_replace("/[éèêëέëèεе℮ёєэЭ]/u", "", $text);
	     $text = preg_replace("/[ÉÈÊË€ξЄ€Е∑]/u",     "", $text);
	     $text = preg_replace("/[₣]/u",               "", $text);
	     $text = preg_replace("/[НнЊњ]/u",           "", $text);
	     $text = preg_replace("/[ђћЋ]/u",            "", $text);
	     $text = preg_replace("/[ÍÌÎÏ]/u",           "", $text);
	     $text = preg_replace("/[íìîïιίϊі]/u",       "", $text);
	     $text = preg_replace("/[Јј]/u",             "", $text);
	     $text = preg_replace("/[ΚЌК]/u",            '', $text);
	     $text = preg_replace("/[ќк]/u",             '', $text);
	     $text = preg_replace("/[ℓ∟]/u",             '', $text);
	     $text = preg_replace("/[Мм]/u",             "", $text);
	     $text = preg_replace("/[ñηήηπⁿ]/u",            "", $text);
	     $text = preg_replace("/[Ñ∏пПИЙийΝЛ]/u",       "", $text);
	     $text = preg_replace("/[óòôõºöοФσόо]/u", "", $text);
	     $text = preg_replace("/[ÓÒÔÕÖθΩθОΩ]/u",     "", $text);
	     $text = preg_replace("/[ρφрРф]/u",          "", $text);
	     $text = preg_replace("/[®яЯ]/u",              "", $text); 
	     $text = preg_replace("/[ГЃгѓ]/u",              "", $text); 
	     $text = preg_replace("/[Ѕ]/u",              "", $text);
	     $text = preg_replace("/[ѕ]/u",              "", $text);
	     $text = preg_replace("/[Тт]/u",              "", $text);
	     $text = preg_replace("/[τ†‡]/u",              "", $text);
	     $text = preg_replace("/[úùûüџμΰµυϋύ]/u",     "", $text);
	     $text = preg_replace("/[√]/u",               "", $text);
	     $text = preg_replace("/[ÚÙÛÜЏЦц]/u",         "", $text);
	     $text = preg_replace("/[Ψψωώẅẃẁщш]/u",      "", $text);
	     $text = preg_replace("/[ẀẄẂШЩ]/u",          "", $text);
	     $text = preg_replace("/[ΧχЖХж]/u",          "", $text);
	     $text = preg_replace("/[ỲΫ¥]/u",           "", $text);
	     $text = preg_replace("/[ỳγўЎУуч]/u",       "", $text);
	     $text = preg_replace("/[ζ]/u",              "", $text);
	 
	     // Punctuation
	     $text = preg_replace("/[‚‚]/u", ",", $text);        
	     $text = preg_replace("/[`‛′’‘]/u", "'", $text);
	     $text = preg_replace("/[″“”«»„]/u", '"', $text);
	     $text = preg_replace("/[—–―−–‾⌐─↔→←]/u", '', $text);
	     $text = preg_replace("/[  ]/u", ' ', $text);
	 
	     $text = str_replace("…", "...", $text);
	     $text = str_replace("≠", "!=", $text);
	     $text = str_replace("≤", "<=", $text);
	     $text = str_replace("≥", ">=", $text);
	     $text = preg_replace("/[‗≈≡]/u", "=", $text);
	 
	 
	     // Exciting combinations    
	     $text = str_replace("ыЫ", "bl", $text);
	     $text = str_replace("℅", "c/o", $text);
	     $text = str_replace("₧", "Pts", $text);
	     $text = str_replace("™", "tm", $text);
	     $text = str_replace("№", "No", $text);        
	     $text = str_replace("Ч", "4", $text);                
	     $text = str_replace("‰", "%", $text);
	     $text = preg_replace("/[∙•]/u", "*", $text);
	     $text = str_replace("‹", "<", $text);
	     $text = str_replace("›", ">", $text);
	     $text = str_replace("‼", "!!", $text);
	     $text = str_replace("⁄", "/", $text);
	     $text = str_replace("∕", "/", $text);
	     $text = str_replace("⅞", "7/8", $text);
	     $text = str_replace("⅝", "5/8", $text);
	     $text = str_replace("⅜", "3/8", $text);
	     $text = str_replace("⅛", "1/8", $text);        
	     $text = preg_replace("/[‰]/u", "%", $text);
	     $text = preg_replace("/[Љљ]/u", "Ab", $text);
	     $text = preg_replace("/[Юю]/u", "IO", $text);
	     $text = preg_replace("/[ﬁﬂ]/u", "fi", $text);
	     $text = preg_replace("/[зЗ]/u", "3", $text); 
	     $text = str_replace("£", "(pounds)", $text);
	     $text = str_replace("₤", "(lira)", $text);
	     $text = preg_replace("/[‰]/u", "%", $text);
	     $text = preg_replace("/[↨↕↓↑│]/u", "|", $text);
	     $text = preg_replace("/[∞∩∫⌂⌠⌡]/u", "", $text);
	 
	 
	     //2) Translation CP1252.
	     $trans = get_html_translation_table(HTML_ENTITIES);
	     $trans['f'] = '&fnof;';    // Latin Small Letter F With Hook
	     $trans['-'] = array(
	         '&hellip;',     // Horizontal Ellipsis
	         '&tilde;',      // Small Tilde
	         '&ndash;'       // Dash
	         );
	     $trans["+"] = '&dagger;';    // Dagger
	     $trans['#'] = '&Dagger;';    // Double Dagger         
	     $trans['M'] = '&permil;';    // Per Mille Sign
	     $trans['S'] = '&Scaron;';    // Latin Capital Letter S With Caron        
	     $trans['OE'] = '&OElig;';    // Latin Capital Ligature OE
	     $trans["'"] = array(
	         '&lsquo;',  // Left Single Quotation Mark
	         '&rsquo;',  // Right Single Quotation Mark
	         '&rsaquo;', // Single Right-Pointing Angle Quotation Mark
	         '&sbquo;',  // Single Low-9 Quotation Mark
	         '&circ;',   // Modifier Letter Circumflex Accent
	         '&lsaquo;'  // Single Left-Pointing Angle Quotation Mark
	         );
	 
	     $trans['"'] = array(
	         '&ldquo;',  // Left Double Quotation Mark
	         '&rdquo;',  // Right Double Quotation Mark
	         '&bdquo;',  // Double Low-9 Quotation Mark
	         );
	 
	     $trans['*'] = '&bull;';    // Bullet
	     $trans['n'] = '&ndash;';    // En Dash
	     $trans['m'] = '&mdash;';    // Em Dash        
	     $trans['tm'] = '&trade;';    // Trade Mark Sign
	     $trans['s'] = '&scaron;';    // Latin Small Letter S With Caron
	     $trans['oe'] = '&oelig;';    // Latin Small Ligature OE
	     $trans['Y'] = '&Yuml;';    // Latin Capital Letter Y With Diaeresis
	     $trans['euro'] = '&euro;';    // euro currency symbol
	     ksort($trans);
	 
	     foreach ($trans as $k => $v) {
	         $text = str_replace($v, $k, $text);
	     }
	 
	     $text = strip_tags($text);
	 
	     $text = html_entity_decode($text);
	 
	     return $text;
	 }

    public function getIsLikedAttribute()
    {
        return Auth::user()->hasLiked($this);
    }
	
    public function getTotalCommentsAttribute()
    {
        return count($this->comments()->get()->filter(function($item) {
							    return $item->author && $item->author->is_blocked === false;
							 }));
    }
	public function getViewLinkAttribute()
    {
    	$title = urlencode($this->title);
        return url('share/News/'.$title.'/'.$this->id);
    }

    public function getDbImageAttribute()
    {
    	$url = str_replace(" ","%20",$this->getOriginal('image'));
    	return env('AWS_URL').$url;
    }

    public function getDbThumbImageAttribute()
    {
    	$filePath = pathinfo($this->getOriginal('image'));
		if($this->getOriginal('image')){
			$n_u = str_replace(" ","%20",($filePath['dirname'].'/'.$filePath['filename'].'_thumb.'.$filePath['extension']));
			return env('AWS_URL').$n_u;
		} else {
			return '';
		}
    }
	
}
