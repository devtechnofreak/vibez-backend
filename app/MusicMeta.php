<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MusicMeta extends Model
{
    //
    protected $table = 'music_meta';
     protected $fillable = [
        'music_id','title','artist','album_artist','album','genre','track','account_id'
    ];
}
