<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatInvitation extends Model
{
    protected $fillable = ['from_user', 'to_user','accepted'];
	
	protected $hidden = ['created_at', 'updated_at'];
	
	protected $appends = ['chat_user'];
	
	public function invited()
	{
		return $this->belongsTo('App\User','to_user');
	}
	
	public function invitee()
	{
		return $this->belongsTo('App\User','from_user');
	}
	
	public function getChatUserAttribute()
	{
		return $this->invited->id == auth()->id()? $this->invitee : $this->invited;
	}
}
