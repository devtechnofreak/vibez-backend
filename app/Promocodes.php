<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promocodes extends Model
{
    //
    protected $table = 'promocodes';
     protected $fillable = [
        'code','artist_id','start_date','end_date'
    ];
	public function user_subscriptions()
    {
        return $this->belongsTo('App\UserSubscription');
    }
}
