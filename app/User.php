<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Overtrue\LaravelFollow\Traits\CanLike;
use App\Notifications\ApiResetPasswordNotification;
use App\Notifications\AddSubscriptionNotification;
use App\Notifications\UserWelcomeEmail;
use App\Notifications\ApiVerifyEmailNotification;
use Overtrue\LaravelFollow\Traits\CanFollow;
use Overtrue\LaravelFollow\Traits\CanBeFollowed;
use Auth;
use Nicolaslopezj\Searchable\SearchableTrait;
use JamesMills\Watchable\Models\Watch;
use App\Traits\CanBlock;
use App\Traits\CanBeBlocked;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements JWTSubject
{
    use SoftDeletes, Notifiable, CanLike, CanFollow, CanBeFollowed, SearchableTrait, CanBlock, CanBeBlocked;
	
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'users.name' => 10,
            'users.vibez_id' => 5,
        ]
    ];
	protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $appends =['is_followed','followers','followings','is_blocked','view_link'];
    protected $fillable = [
        'name', 'email', 'password','dob','vibez_id','gender','image','phone','fcm_token','device_type','device_id','design','user_suspend','email_verify','email_otp','is_social_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','fcm_token','device_type','created_at','updated_at','deleted_at','device_id'
    ];


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return ['vibez_id'=>$this->vibez_id,'device_id'=>$this->device_id];
    }
	
	public function routeNotificationForFcm() {
	    return $this->fcm_token;
	}
	
	public function role()
	{
		return $this->belongsTo('App\Role');
	}
	
	public function playlists()
	{
		return $this->hasMany('App\Playlist');
	}
	
	public function comments()
	{
		return $this->hasMany('App\Comment');
	}
	
	public function token()
	{
		return $this->hasOne('App\ApiToken');
	}
	
	public function keywords()
	{
		return $this->hasMany('App\SearchKeyword')->orderBy('updated_at','DESC');
	}

	public function watchedData()
	{
		return $this->hasMany(Watch::class);
	}
	
	public function recentlyPlayedMusics()
	{
		return $this->morphedByMany(Music::class, 'watchable','watch')
                    ->withPivot('watchable_type', 'created_at','updated_at')->distinct()->orderBy('pivot_created_at','DESC')->take(20);
	}
	
	public function recentlyPlayedVideos()
	{
		return $this->morphedByMany(Video::class, 'watchable','watch')
                    ->withPivot('watchable_type', 'created_at')->orderBy('pivot_created_at','DESC');
	}
	
	public function getPlaylists()
	{
		return $this->hasMany('App\Playlist')->get();
	}
	
	public function library(){
		return $this->belongsToMany('App\Music','libraries')->withTimestamps()->distinct()->orderBy('pivot_created_at','DESC');
	}
	/*
	public function getRecentSongs()
	{
		return Music::
		
			join('playlist_musics', 'musics.id', '=', 'playlist_musics.music_id')
		
		    ->join('playlists', 'playlist_musics.playlist_id', '=', 'playlists.id')
		
		    ->join('users', 'playlists.user_id', '=', 'users.id')
		
		    ->where('users.id', $this->id)
			
			->select('musics.*','playlist_musics.created_at as added_at')
			
			//->distinct()
			
			->orderBy('playlist_musics.created_at', 'desc')
			
			->paginate(10);
	}
	 * 
	 */
	/* public function enddate()
	{
		return $this->hasOne('App\UserSubscription','user_id','id')->orderBy('id','DESC');;
	} */

	public function getIsBlockedAttribute()
	{
		if (Auth::check()) {
			return $this->isBlockedBy(Auth::user());	
		}else{
			return false;
		}
	}
	
	public function getImageAttribute($value)
	{
		return $value?env('APP_URL').$value:'';
	}
	
	public function getPhoneAttribute($value)
	{
		return $value?:'';
	}
	
	public function getFavouriteData()
	{
		$data = array(
		'musics'=>$this->likes(Music::class)->get(),
		'playlists'=>$this->likes(Playlist::class)->get(),
		'news'=>$this->likes(News::class)->get(),
		'videos'=>$this->likes(Video::class)->get()
		);
		return $data;
	}
    public function getIsFollowedAttribute()
    {
        return $this->isFollowedBy(Auth::user());
    }
	public function getFollowingsAttribute()
	{
		return $this->followings(User::class)->count();
	}
	
	public function getFollowedArtist()
	{
		return $this->followings(Artist::class)->OrderBy('is_featured','DESC')->get();
	}

    public function getFollowersAttribute()
    {
        return $this->followers()->count();
    }
		
	public function sendApiVerifyEmailNotification($token){
		$this->notify(new ApiVerifyEmailNotification($token));
	}
	
	public function sendUserWelcomeEmail($vibez_id){
		$this->notify(new UserWelcomeEmail($vibez_id));
	}	
    public function sendApiPasswordResetNotification($token)
    {
        $this->notify(new ApiResetPasswordNotification($token));
    }
	
	public function sendAddSubscriptionNotification($token){
		$this->notify(new AddSubscriptionNotification($token));
	}
	
    public function getViewLinkAttribute()
    {
    	$vibez_id = urlencode($this->vibez_id);
        return url('share/User/'.$vibez_id.'/'.$this->id);
    }

	public function getChatInvitation()
	{
		$invitation = ChatInvitation::where(function($query){
			$query->where('from_user',Auth::user()->id);
			$query->where('to_user',$this->id);
		})->orWhere(function($query){
			$query->where('to_user',Auth::user()->id);
			$query->where('from_user',$this->id);
		});
		
		return $invitation->exists()?$invitation->first():new \stdClass();
	}
}
