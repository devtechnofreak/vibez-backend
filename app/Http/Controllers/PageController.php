<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = DB::select("select * from pages WHERE slug='privacy_policy'");
        return view('privacy',['data'=> $users]);
    }

    public function terms()
    {
        $users = DB::select("select * from pages WHERE slug='terms_conditions'");
        return view('terms',['data'=> $users]);
    }
}
