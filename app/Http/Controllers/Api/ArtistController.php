<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\User;
use App\Playlist;
use App\Music;
use App\Video;
use App\SearchKeyword;
use App\Artist;
use App\Album;
use App\News;
use App\Message;
use App\Conversation;
use App\LiveStream;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator, DB, Hash, Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Events\MessageSent;
use Illuminate\Validation\Rule;
use Exception;

class ArtistController extends Controller
{	
	public function followArtist(Request $request)
	{
        $rules = [
            'artist_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$artist = Artist::find($request->artist_id);	
		if(!$artist){
			return response()->json(['code' => 200,'message'=>'Artist does not exist!']);
		}
		Auth::user()->follow($artist);
		return response()->json(['code' => 200, 'follow_count'=>$artist->follow_count, 'message' => 'You followed this Artist successfully.']);
	}
	
	public function unfollowArtist(Request $request)
	{
        $rules = [
            'artist_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$artist = Artist::find($request->artist_id);	
		if(!$artist){
			return response()->json(['code' => 200,'message'=>'Artist does not exist!']);
		}
		Auth::user()->unfollow($artist);
		return response()->json(['code' => 200, 'follow_count'=>$artist->follow_count, 'message' => 'You unfollowed this Artist successfully.']);
	}
	
	public function getArtistInfo(Request $request)
	{
        $rules = [
            'artist_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		
		$artist = Artist::find($request->artist_id);	
		if($artist->dob == null){
				$artist->dob = "";
		}
		if(!$artist){
			return response()->json(['code' => 200,'message'=>'Artist does not exist!']);
		}	

		//$album = collect($artist->albums)->all();
		return response()->json(['code' => 200,'data'=>['artist'=>$artist,'albums'=>$artist->getLatestAlbums(),'trending_musics'=>$artist->getTrendingMusics(),'latest_videos'=>$artist->getLatestVideos(),'latest_news'=>$artist->getLatestNews(),'livestreams'=>$artist->getLivestreams()]]);	
	}
	
	public function getFeaturedArtists(Request $request)
	{
		$page = $request->input('page')?:1;
    	try {
			$artistList = Artist::where('is_featured',1)->OrderBy('updated_at','DESC')->get();
			//$artistList = Artist::get();
			for ($i=0; $i < sizeof($artistList); $i++) { 
				if($artistList[$i]->dob == null){
					$artistList[$i]->dob = "";
				}
			}
	        return response()->json(['code' => 200, 'data'=> [ 'artist_list' =>['data'=>$artistList->sortByDesc('is_followed')->forPage($page,20)->values()]]]);
        } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message'=>$e->getMessage()]); 
        }	
	}

	public function getArtistsMore(Request $request)
	{
		$page = $request->input('page')?:1;
    	try {
			
			$followed_artist = Auth::user()->getFollowedArtist();

			$FeaturedArtistList = Artist::where('is_featured',1)->OrderBy('updated_at','DESC')->get();
			$NonFeaturedArtistList = Artist::where('is_featured',0)->OrderBy('updated_at','DESC')->get();
					
			$new_collection = $followed_artist->merge($FeaturedArtistList)->merge($NonFeaturedArtistList);
			for ($i=0; $i < sizeof($new_collection); $i++) { 
				if($new_collection[$i]->dob == null){
					$new_collection[$i]->dob = "";
				}
			}
			return response()->json(['code' => 200, 'data'=> [ 'artist_list' =>['data'=>$new_collection->forPage($page,20)->values()]]]);
        } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message'=>$e->getMessage()]); 
        }	
	}

	public function likeArtist(Request $request)
	{
        $rules = [
            'artist_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$artist = Artist::find($request->artist_id);	
		if(!$artist){
			return response()->json(['code' => 200,'message'=>'Artist does not exist!']);
		}	
		Auth::user()->like($artist);
		return response()->json(['code' => 200, 'message' => 'You liked Artist successfully.']);
	}
	
	public function unlikeArtist(Request $request)
	{
        $rules = [
            'artist_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$artist = Artist::find($request->artist_id);
		if(!$artist){
			return response()->json(['code' => 200,'message'=>'Artist does not exist!']);
		}		
		Auth::user()->unlike($artist);
		return response()->json(['code' => 200, 'message' => 'You unliked Artist successfully.']);
	}
	
	public function getAllArtists()
	{
		$artist = Artist::all();
		for ($i=0; $i < sizeof($artist); $i++) { 
			if($artist[$i]->dob == null){
				$artist[$i]->dob = "";
			}
		}
		return response()->json(['code' => 200,'data'=>['artists'=>$artist]]);
	}
	
	public function bulkFollowArtists(Request $request)
	{
        $rules = [
            'artist_ids' => 'required|json'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$artist_ids = json_decode($request->artist_ids,true);
		foreach ($artist_ids as $artist_id) {
			$artist = Artist::find($artist_id);
			Auth::user()->follow($artist);
		}	
		return response()->json(['code' => 200, 'message' => 'Artists followed successfully.']);
	}

}