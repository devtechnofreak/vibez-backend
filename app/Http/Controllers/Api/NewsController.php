<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\User;
use App\News;
use App\Artist;
use App\Comment;
use App\Shortcode;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator, DB, Hash, Mail,Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use Illuminate\Routing\Controller;
use Exception;

class NewsController extends Controller
{
	public function getHomeNews(Request $request){
		$page = $request->input('page')?:1;
		try {
			/*$collection = News::orderBy('created_at','DESC')->get();
			$following_artists = Auth::user()->followings(Artist::class)->get()->pluck('id')->toArray();
			$newsList = $collection->sortByDesc(function($news) use($following_artists){
				return count(array_intersect($news->artists->pluck('id')->toArray(), $following_artists)); 
			});
			*/
			$newsList = News::orderBy('created_at', 'DESC')
								->paginate(20);				
	        return response()->json(['code' => 200, 'data'=> ['news' => $newsList]]);
        } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message' => $e->getMessage()]); 
        }
	}	
	public function getNewsBanners(Request $request){
		$page = $request->input('page')?:1;
		try {
			/*$collection = News::orderBy('created_at','DESC')->get();
			$following_artists = Auth::user()->followings(Artist::class)->get()->pluck('id')->toArray();
			$newsList = $collection->sortByDesc(function($news) use($following_artists){
				return count(array_intersect($news->artists->pluck('id')->toArray(), $following_artists)); 
			});
			*/
			$newsList = News::orderBy('is_featured', 'DESC')
    							->orderBy('created_at', 'DESC')
								->paginate(10);
	        return response()->json(['code' => 200, 'data'=> ['news' => $newsList]]);
        } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message' => $e->getMessage()]); 
        }
	}
	public function getNews(Request $request)
    {
    	
    	$page = $request->input('page')?:1;
		try {
			/*$collection = News::orderBy('created_at','DESC')->get();
			$following_artists = Auth::user()->followings(Artist::class)->get()->pluck('id')->toArray();
			$newsList = $collection->sortByDesc(function($news) use($following_artists){
				return count(array_intersect($news->artists->pluck('id')->toArray(), $following_artists)); 
			});
			*/
			$newsList = News::orderBy('created_at', 'DESC')
								->paginate(20);				
	        return response()->json(['code' => 200, 'data'=> ['news' => $newsList]]);
        } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message' => $e->getMessage()]); 
        }
		 
		/*
    	try {
			$newsList = News::orderBy('created_at','DESC')->paginate(10);
			foreach ($newsList as $key => $value) {
					$newsList[$key]->content_array = $value->getContent();
			}
	        return response()->json(['code' => 200, 'data'=> ['news' =>  $newsList]]);
        } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message' => $e->getMessage()]); 
        }
		 * 
		 */
        
    }

	public function getNewsDetail(Request $request,$id){
		$page = $request->input('page')?:1;
		$news  = News::find($id);
		if($news == NULL){
			return response()->json(['code' => 200, 'message'=> 'News does not exist!']);
		}
		$comments = $news->comments()->orderBy('created_at','DESC')->get()->filter(function($item) {
							    return $item->author->is_blocked === false;
							 })->forPage($page, 50)->values();
		$content_array = $news->getContent();
		
		$tra_array = json_decode(json_encode($content_array), True);

		$ss_string = 'vbz_sh_';
		 foreach ($tra_array as $key => $value) {
		 	
		 	$haystack = $value['data'];
		 	$needle   = "vbz_sh_";
			if( strpos( $haystack, $needle ) !== false) 
			{
				$find_string = $value['data'];
				
				$user = DB::table('shortcode')->where('code', $find_string)->first();
				
				$tra_array[$key]['type']  = 'short_code';
    			$tra_array[$key]['data']  = '<html>'.base64_decode($user->text).'</html>';
			}
		}

		$time  = $news->title;
		$object =  new \stdClass;
		$object->type = "text";
		$object->data = $time;
		$first_line_array = array($object);
		$news->content_array = array_merge($first_line_array,$tra_array);
		$news->posted_time = time_elapsed_string($news->created_at);


		$feature_artist_ids = array();
		if($news->related_artist != ""){
			$feature_artist_ids = explode(',', $news->related_artist);
			$artist_ids = array_merge($feature_artist_ids);
			$ids_ordered = implode(',', $artist_ids);
			$feature_artist = Artist::whereIn('id', $artist_ids)->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))->get();
			$arr = array();
			$feature_artist = $feature_artist->map(function ($employee) {
				$arr = array(
					"id" => $employee['id'],
					"name" => $employee['name'],
					"thumb" => $employee['thumb'],
					"country_flag" => $employee['country_flag'],
					"is_followed" => $employee['is_followed'],
					"is_liked" => $employee['is_liked'],
				);
			    return $arr;
			});
		}
		else
		{
			$feature_artist  = array();
		}

		if(is_null($news->related_artist))
		{
			$news->related_artist = "";
		}
		
		return response()->json(['code' => 200, 'data'=> ['news' =>  $news,'feature_artist' => $feature_artist,'comments'=>['data'=>$comments]]]);
	}

	public function addComment(Request $request)
	{
        $rules = [
            'comments' => 'required',
            'news_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$news  = News::find($request->news_id);	
		
		$news->addComment($request->comments, Auth::user());
		
		$total = count($news->comments()->get()->filter(function($item) {
							    return $item->author->is_blocked === false;
							 }));
		
		return response()->json(['code' => 200,'comment'=>$news->comments()->orderBy('created_at', 'desc')->first(),'total_comments'=>$news->comments()->count(), 'message' => 'Comment has been posted successfully.']);
	}
	
	public function likeComment(Request $request)
	{
        $rules = [
            'comment_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$comment = Comment::find($request->comment_id);	
		Auth::user()->like($comment);
		return response()->json(['code' => 200,'total_likes'=>$comment->total_likes ,'message' => 'You liked comment successfully.']);
	}
	
	public function unlikeComment(Request $request)
	{
        $rules = [
            'comment_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$comment = Comment::find($request->comment_id);	
		Auth::user()->unlike($comment);
		return response()->json(['code' => 200,'total_likes'=>$comment->total_likes , 'message' => 'You unliked comment successfully.']);
	}
	
	public function likeNews(Request $request)
	{
        $rules = [
            'news_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$news = News::find($request->news_id);	
		Auth::user()->like($news);
		// return response()->json(['code' => 200, 'message' => 'You liked News successfully.']);
		$newsLikes = $news->total_likes;	
		return response()->json(['code' => 200, 'message' => 'You liked News successfully.', 'total_likes' => $newsLikes]);
	}
	
	public function unlikeNews(Request $request)
	{
        $rules = [
            'news_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$news = News::find($request->news_id);	
		Auth::user()->unlike($news);
		// return response()->json(['code' => 200, 'message' => 'You unliked News successfully.']);
		$newsLikes = $news->total_likes;
		return response()->json(['code' => 200, 'message' => 'You unliked News successfully.', 'total_likes' => $newsLikes]);
	}
	
	public function removeComment(Request $request)
	{
        $rules = [
            'comment_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$comment = Comment::find($request->comment_id);	
		
		if(!$comment){
			return response()->json(['code' => 401,'message'=>'Comment does not exists']);
		}
		try {
			$comment->delete();	
		} catch(Exception $e)
		{
			return response()->json(['code' => 401, 'message' => $e->getMessage()]);
		}
			
		
		return response()->json(['code' => 200, 'message' => 'Comment has been removed successfully.']);
	}
}
