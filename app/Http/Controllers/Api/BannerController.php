<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\User;
use App\Banner;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator, DB, Hash, Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Exception;

use App\BannerView;

class BannerController extends Controller
{	
	public function getBanner(Request $request)
    {
    	try {
    		
			$banners = Banner::with('bannerable')->orderBy('order_id','ASC')->orderBy('created_at','DESC')->paginate(10);
	        return response()->json(['code' => 200, 'data'=> ['banners' =>  $banners]]);
	        
        } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message'=>$e->getMessage()]); 
        }
        
    }

    public function bannerWatch(Request $request) {

        $rules = [
            'banner_id' => 'required|integer'
        ];

        $validator = \Validator::make($request->all(), $rules);

        if($validator->fails()){
            $errors = $validator->errors();
            return response()->json(['code' => 401, 'message'=>implode(', ', $errors->all())]);
        }

        $bannerData = Banner::find($request->banner_id);

        if (is_null($bannerData)) {
            return response()->json(['code' => 401, 'message' => 'Banner not found']); 
        }

        $insArr = array(
            'user_id'         => $request->user()->id,
            'banner_id'       => $request->banner_id,
            'bannerable_id'   => $bannerData->bannerable_id,
            'bannerable_type' => $bannerData->bannerable_type
        );

        // echo '<pre>'; print_r($insArr);

        BannerView::create($insArr);

        return response()->json(['code' => 200, 'message' => 'Data updated successfully.' ]);
    }

}
