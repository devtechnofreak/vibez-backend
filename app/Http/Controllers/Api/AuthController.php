<?php 
namespace App\Http\Controllers\Api; 
use Illuminate\Http\Request; 
use App\User; 
use App\Playlist; 
use App\Music; 
use App\Video; 
use App\SearchKeyword; 
use App\SearchContent; 
use App\Artist; 
use App\Album; 
use App\Page; 
use App\News; 
use App\ChatInvitation; 
use App\Message; 
use App\Conversation; 
use App\Report;
use App\userChat; 
use JWTAuth; 
use Tymon\JWTAuth\Exceptions\JWTException; 
use Validator, DB, Hash, Mail; 
use Illuminate\Support\Facades\Password; 
use Illuminate\Routing\Controller; 
use Illuminate\Support\Facades\Auth; 
use App\Events\MessageSent; 
use Illuminate\Validation\Rule; 
use Cache; 
use App\Notifications\PushNotification; 
use App\Notifications\SilentPushNotification;
use App\Promocodes;
use App\EventCodes;
use App\UserSubscription;
use App\UserSubscriptionHistory;
use App\UserManualPayment;
use App\UserHelp;
use App\UserHelpFeedback;
use URL;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller {
	public function register(Request $request)
    {
        
        $credentials = $request->only('name', 'email', 'password','dob', 'gender', 'image','vibez_id','device_id');
        
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'password' => 'required',
            'dob' => 'required',
            'gender' => ['required',Rule::in(['Male', 'Female','None'])],
            'vibez_id' => 'required|unique:users',
            'device_id'=> 'required',
        ];
        $validator = Validator::make($credentials, $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		
        if($request->hasfile('image')) {
        	$image = $request->file('image');
        	$imageName = time().'.'.$image->getClientOriginalExtension();
        	 $image->move(public_path('images/users'), $imageName);
			 $image = 'images/users/'.$imageName;
        } else {
        	$image = '';
        }
		
    	$data = array(
				'name' => $request->name,
				'email' => $request->email,
				'password' => Hash::make($request->password),
				'dob' => date('Y-m-d',strtotime($request->dob)),
				'image' => $image,
				'vibez_id' => $request->vibez_id,
				'gender' => $request->gender,
				'phone' => $request->input('phone'),
				'fcm_token' => $request->input('fcm_token'),
				'device_type' => $request->input('device_type'),
				'device_id' => $request->device_id,
				'email_verify'	=> 0
            );
        $user = User::create($data);
		
		
		
		
		$start_date = date('Y-m-d');
		$date = strtotime("+7 day");
		//$date = strtotime("+1 year");
		$end_date =  date('Y-m-d', $date);
		$unique = md5(time());
		$result = '';
	    for($i = 0; $i < 20; $i++) {
	        $result .= mt_rand(0, 9);
	    }
		$subvscriptionHistory = array(
									'user_id'				=> $user->id,
									'subscription_platform'	=> $request->device_type,
									'subscription_amount'	=> 0,
									'subscription_gateway'	=> isset($request->subscription_gateway) ? $request->subscription_gateway : "",
									'receipt'				=> $unique,
									'plan'					=> 'Trial Period',
									'user_status'			=> 1,
									'transaction_id'		=> $result,
									'date'					=> date('Y-m-d'),
									'promocode'				=> 0
								);
			UserSubscriptionHistory::create($subvscriptionHistory);
		$userSubscription = array(
								'user_id'				=> $user->id,
								'start_date'			=> $start_date,
								'end_date'				=> $end_date,
								'user_status'			=> 1,
								'promocode'				=> 0,
								'receipt'				=> md5(time()),
								'subscription_platform'	=> $request->device_type,
								'subscription_gateway'	=> isset($request->subscription_gateway) ? $request->subscription_gateway : "",
								'transaction_id'		=> $result,
								'subscription_amount'	=> 0,
								'plan'					=> 'Trial Period'
							);
		UserSubscription::create($userSubscription);
		
        $login_credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($login_credentials)) {
                return response()->json(['code' => 401, 'message' => 'Account not found with this credentials. Please enter valid credentials.']);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['code' => 401, 'message' => 'Failed to login, please try again.']);
        }
        // all good so return the token
        
        $email_verifytoken = mt_rand(10000, 99999);
		$user->email_otp = $email_verifytoken;
		$user->save(); 
		
    	$user->sendApiVerifyEmailNotification($email_verifytoken);
        
        return response()->json(['code' => 200, 'data'=> [ 'token' => 'Bearer '.$token,'user'=>Auth::user()]]);
        
	}
	public function getSubscription(){
		$checkIfSubscription = UserSubscription::where('user_id',Auth::user()->id)->count();
		
		if($checkIfSubscription == 1){
			$subscription = UserSubscription::where('user_id',Auth::user()->id)->first();
			$history = UserSubscriptionHistory::where('user_id',Auth::user()->id)->get();
			return response()->json(['code' => 200, 'data'=> ['subscription'	=> $subscription,'history'	=> $history],'message'	=> 'subscription has listed successfully!']);
		}else{
			return response()->json(['code' => 200, 'data'=> [],'message'	=> 'you have no subscription yet for vibez!']);
		}
	}
	
	public function resendEmailVerification(){
		$email_verifytoken = mt_rand(10000, 99999);
		$user = User::find(Auth::user()->id);
		$user->email_otp = $email_verifytoken;
		$user->save(); 
    	$user->sendApiVerifyEmailNotification($email_verifytoken);
		return response()->json(['code' => 200, 'data'=> [],'message'	=> 'OTP is sent to registered email id!']);
	}
	
	public function verifyEmailOTP(Request $request){
		$rules = [
            'otp' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
        try {
           
		   $otp = $request->otp;
		   if($otp && $otp != ""){
		   		$user_otp = Auth::user()->email_otp;
			   if($otp == $user_otp){
				   	$user = User::find(Auth::user()->id);
					$user->email_verify = 1;
					$user->save();
					$user->sendUserWelcomeEmail($user->name);
					return response()->json(['code' => 200, 'message'	=> 'your email is now verified!']);
				}else{
			   		return response()->json(['code' => 401, 'message'	=> 'OTP you entered is wrong, please enter correct OTP!']);
			   }
		   }
		   
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['message' => 'there is an error in veryfy email, please try again later!.','code' => 401]);
        }
	}
    public function login(Request $request)
    {
        $credentials = $request->only('vibez_id', 'password');
        
        $rules = [
            'vibez_id' => 'required',
            'password' => 'required',
            'device_id'=> 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::claims(['device_id' => $request->input('device_id')])->attempt($credentials)) {
                return response()->json(['message' => 'Account not found with this credentials. Please enter valid credentials.','code' => 401]);
            }
			
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['message' => 'Failed to login, please try again.','code' => 401]);
        }
		
		if(!$request->input('force_login')){
			if(Auth::user()->device_id){
				return response()->json(['message' => 'Hey, you’re already logged in from another device. Do you want to proceed with this device?','code' => 402]);
			}
		}	
		
		

		

		User::where('id','!=',Auth::user()->id)->where('fcm_token', $request->input('fcm_token'))->update(['fcm_token' => '']);
		
		Auth::user()->fcm_token = $request->input('fcm_token');
		Auth::user()->device_type = $request->input('device_type');
		Auth::user()->device_id = $request->input('device_id');
		Auth::user()->save();
		
        // all good so return the token
        return response()->json(['code' => 200, 'data'=> [ 'token' => 'Bearer '.$token ,'user'=>User::find(Auth::user()->id)]]);
    }


    public function checkSubscriptionIos($receipt,$is_sandbox){
		//$sandbox should be TRUE if you want to test against itunes sandbox servers
		if ($is_sandbox)
			$verify_host = "ssl://sandbox.itunes.apple.com";
		else
			$verify_host = "ssl://buy.itunes.apple.com";

		$json='{"receipt-data" : "'.$receipt.'" ,"password" : "e698a3ab20094c6189f9fb352c237fc9"}';
		//opening socket to itunes
		$fp = fsockopen ($verify_host, 443, $errno, $errstr, 30);
		if (!$fp) 
		{
			// HTTP ERROR
			return false;
		} 
		else
		{ 
			//iTune's request url is /verifyReceipt     
			$header = "POST /verifyReceipt HTTP/1.0\r\n";
			$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
			$header .= "Content-Length: " . strlen($json) . "\r\n\r\n";
			fputs ($fp, $header . $json);
			$res = '';
			while (!feof($fp)) 
			{
				$step_res = fgets ($fp, 1024);
				$res = $res . $step_res;
			}
			fclose ($fp);
			//taking the JSON response
			$json_source = substr($res, stripos($res, "\r\n\r\n{") + 4);
			//decoding
			$app_store_response_map = json_decode($json_source);
			$app_store_response_status = $app_store_response_map->{'status'};
			if ($app_store_response_status == 0)//eithr OK or expired and needs to synch
			{
				//here are some fields from the json, btw.
				/*$json_receipt = $app_store_response_map->{'receipt'};
				$transaction_id = $json_receipt->{'transaction_id'};
				$original_transaction_id = $json_receipt->{'original_transaction_id'};
				$json_latest_receipt = $app_store_response_map->{'latest_receipt_info'};
				return true;*/

				/*$json_receipt = $app_store_response_map->latest_receipt_info[0]->expires_date_ms;
				$micro_date = $json_receipt;
				$date_array = $micro_date / 1000;
				$date = date("Y-m-d",$date_array);
				return $date;*/
				$new_last_element = end($app_store_response_map->latest_receipt_info);
				$json_receipt = $new_last_element->expires_date_ms;
				$product_id = $new_last_element->product_id;
				$micro_date = $json_receipt;
				$date_array = $micro_date / 1000;
				$date = date("Y-m-d",$date_array);
				return array('date'=>$date,'product_id'=> $product_id,'time'=>$date_array);
			}
			else
			{
				return false;
			}
		}
	}

	public function UpdateStausInApp(Request $request){
		$rules = [
            'receipt' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}

		try {
			$user_id = Auth::user()->id;
			$checkIfSubscription = UserSubscription::where('user_id',$user_id)
												->get()->first();
			
				$checkIfSubscription->receipt = $request->receipt;
				$checkIfSubscription->save();
				return response()->json(['code' => 200,'message'=> 'Subscription updated']);
			
		}
		catch (JWTException $e) {
	            // something went wrong whilst attempting to encode the token
	            return response()->json(['message' => 'Something going wrong in inappupdate.','code' => 401]);
	        }
	}

	public function checkSubscriptionCronjob(){
		$checkIfSubscription = UserSubscription::where('end_date',date('Y-m-d',strtotime("-1 days")))
												->where('plan','!=','Trial Period')
												->get()->toArray();
		foreach ($checkIfSubscription as $single_subscription) {
			if($single_subscription['subscription_gateway'] == "apple" || $single_subscription['subscription_platform'] == "ios"){
				$checkReceipt = $this->checkSubscriptionIos($single_subscription['receipt'],true);	
				if($checkReceipt != false){
					$subscription = UserSubscription::find($single_subscription['id']);
					$enddate = $single_subscription['end_date'];
					if($single_subscription['plan'] == "yearly subscription"){
						//$new_date = date('Y-m-d', strtotime("+1 year", strtotime($date)));
						$new_date = $checkReceipt;
						$subscription->end_date = $new_date;
						$subscription->save();
					}
					if($single_subscription['plan'] == "monthly subscription"){
						//$new_date = date('Y-m-d', strtotime("+1 months", strtotime($date)));
						$new_date = $checkReceipt;
						$subscription->end_date = $new_date;
						$subscription->save();
					}
				}
			}
			if($single_subscription['subscription_gateway'] == "playstore" && $single_subscription['receipt'] == true){
				$subscription = UserSubscription::find($single_subscription['id']);
				$enddate = $single_subscription['end_date'];
				if($single_subscription['plan'] == "yearly subscription"){
					$new_date = date('Y-m-d', strtotime("+1 year", strtotime($enddate)));
					$subscription->end_date = $new_date;
					$subscription->save();
				}
				if($single_subscription['plan'] == "monthly subscription"){
					$new_date = date('Y-m-d', strtotime("+1 months", strtotime($enddate)));
					$subscription->end_date = $new_date;
					$subscription->save();
				}
			}
		}
	}

	public function checkSubscriptionCronjobPaystack(){
		/*$checkIfSubscription = UserSubscription::where('end_date',date('Y-m-d',strtotime("-1 days")))
												->where('plan','!=','Trial Period')->where('subscription_gateway','=','paystack')->get()->toArray();*/
		$checkIfSubscription = UserSubscription::where('end_date',date('Y-m-d'))->where('plan','!=','Trial Period')->where('subscription_gateway','=','paystack')->get()->toArray();
		foreach ($checkIfSubscription as $single_subscription) {
			$result_new = array();
			//Set other parameters as keys in the $postdata array
			$url = "https://api.paystack.co/subscription/".$single_subscription['transaction_id'];
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			$headers = [
			  'Authorization: Bearer sk_live_768a55f7e48b65852679f99426a03da8bce17a39',
			  'Content-Type: application/json',
			];
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
			$request_new_dd = curl_exec ($ch);
			curl_close($ch);
			
			$result = json_decode($request_new_dd, true);

			if ($result['status']) {
				
				$sub_status = end($result['data']['invoices']);

				$next_payment_date = date_format(date_create($result['data']['next_payment_date']), 'Y-m-d');

				$today   = date('Y-m-d', strtotime("-1 days"));			
				
				if($sub_status['status'] == 'success')  {
					$newDate = date('Y-m-d', strtotime($next_payment_date));
					UserSubscription::where('id', $single_subscription['id'])->update(['end_date' => $newDate]);
				}
			}			
		}
	}

	public function checkSubscription(){
		$checkIfSubscription = UserSubscription::where('user_id',Auth::user()->id)->get()->toArray();
		$status = 'active';
		if(Auth::user()->user_suspend == 1){
			$status = 'suspend';
		}
		if(is_array($checkIfSubscription) && sizeof($checkIfSubscription) > 0){
			$enddate = $checkIfSubscription[0]['end_date'];
			$enddate = date('Y-m-d 11:59:59',strtotime($enddate));
			$date = strtotime($enddate);
			$now = time();
			
			/*if($checkIfSubscription[0]['subscription_platform'] == "ios"){
				//$checkReceipt = $this->checkSubscriptionIos($checkIfSubscription[0]['receipt'],true);
				
			}else{
				
			}*/
			if($checkIfSubscription[0]['subscription_platform'] == "ios" || $checkIfSubscription[0]['subscription_gateway'] == "apple"){
				if($date < $now) {
					$checkReceipt = $this->checkSubscriptionIos($checkIfSubscription[0]['receipt'],false);
					if($checkReceipt['time'] > $now)
					{
						$name = $checkReceipt['product_id'];
						$n_1 = explode('.', $name);
						$finalname = $n_1[3];
						if($finalname =='Monthly'){ $product_name = 'yearly subscription'; }
						else if($finalname =='Montly_ArtistCode'){ $product_name = 'monthly subscription'; }
						else if($finalname =='Yearly'){ $product_name = 'yearly subscription'; }
						else if($finalname =='Yearly_ArtistCode'){ $product_name = 'yearly subscription'; }
						else{ $product_name = 'Trial Period'; }	

						$subscription = UserSubscription::find($checkIfSubscription[0]['id']);
						$new_date = $checkReceipt['date'];
						$subscription->end_date = $new_date;
						$subscription->plan = $product_name;
						$subscription->updated_at = date('Y-m-d H:i:s');
						$subscription->save();
						return response()->json(['code' => 200, 'expiry_message'=>'','message'=> 'Subscription is running','user_status'	=> $status ]);	
					}
					else
					{
						return response()->json(['code' => 200, 'expiry_message'=>'Your subscription has expired','message'=> 'Subscription has been expire!','user_status'	=> $status]);		
					}
				}
			}
			
			if($date < $now) 
			{
				if(isset($checkReceipt))
				{
					return response()->json(['code' => 200, 'isContentAccess'=>true,'expiry_message'=>'','message'=> 'Subscription is running','user_status'	=> $status ]);
				}
				else
				{
					if($checkIfSubscription[0]['plan'] == 'Trial Period')
					{
						return response()->json(['code' => 200, 'isContentAccess'=>true,'expiry_message'=>'Your trial period has expired','message'=> 'Subscription not found or expire!','user_status'	=> $status]);		
					}
					else
					{
						return response()->json(['code' => 200, 'isContentAccess'=>true,'expiry_message'=>'Your subscription has expired','message'=> 'Subscription has been expire!','user_status'	=> $status]);		
					}
					
				}
			}else{
				return response()->json(['code' => 200, 'isContentAccess'=>true,'expiry_message'=>'','message'=> 'Subscription is running','user_status'	=> $status ]);
			}
		}else{
				return response()->json(['code' => 200, 'isContentAccess'=>true,'expiry_message'=>'Your trial period has expired','message'=> 'Subscription not found or expire','user_status'	=> $status ]);
		}
	}


	public function checkSubscriptionBySiddhesh(){
		$checkIfSubscription = UserSubscription::where('user_id',Auth::user()->id)->get()->toArray();
		$status = 'active';
		if(Auth::user()->user_suspend == 1){
			$status = 'suspend';
		}
		if(is_array($checkIfSubscription) && sizeof($checkIfSubscription) > 0){
			$enddate = $checkIfSubscription[0]['end_date'];
			$date = strtotime($enddate);
			$now = time();
			
			if($checkIfSubscription[0]['subscription_platform'] == "ios" || $checkIfSubscription[0]['subscription_gateway'] == "apple"){
				if($date < $now) {
					$checkReceipt = $this->checkSubscriptionIos($checkIfSubscription[0]['receipt'],false);
					if($checkReceipt['time'] > $now)
					{
						$name = $checkReceipt['product_id'];
						$n_1 = explode('.', $name);
						$finalname = $n_1[3];
						if($finalname =='Monthly'){ $product_name = 'yearly subscription'; }
						else if($finalname =='Montly_ArtistCode'){ $product_name = 'monthly subscription'; }
						else if($finalname =='Yearly'){ $product_name = 'yearly subscription'; }
						else if($finalname =='Yearly_ArtistCode'){ $product_name = 'yearly subscription'; }
						else{ $product_name = 'Trial Period'; }	

						$subscription = UserSubscription::find($checkIfSubscription[0]['id']);
						$new_date = $checkReceipt['date'];
						$subscription->end_date = $new_date;
						$subscription->plan = $product_name;
						$subscription->updated_at = date('Y-m-d H:i:s');
						$subscription->save();
						return response()->json(['code' => 200, 'expiry_message'=>'','message'=> 'Subscription is running','user_status'=> $status ]);	
					}
					else
					{
						return response()->json(['code' => 200, 'expiry_message'=>'Your subscription has expired','message'=> 'Subscription has been expire!','user_status'	=> $status]);		
					}
				}
			}
			
			if($date < $now) 
			{
				if(isset($checkReceipt))
				{
					return response()->json(['code' => 200, 'expiry_message'=>'','message'=> 'Subscription is running','user_status'	=> $status ]);
				}
				else
				{
					if($checkIfSubscription[0]['plan'] == 'Trial Period')
					{
						return response()->json(['code' => 200, 'expiry_message'=>'Your trial period has expired','message'=> 'Subscription not found or expire!','user_status'	=> $status]);		
					}
					else
					{
						return response()->json(['code' => 200, 'expiry_message'=>'Your subscription has expired','message'=> 'Subscription has been expire!','user_status'	=> $status]);		
					}
					
				}
			}else{
				return response()->json(['code' => 200, 'expiry_message'=>'','message'=> 'Subscription is running','user_status'	=> $status ]);
			}
		}else{
				return response()->json(['code' => 200, 'expiry_message'=>'Your trial period has expired','message'=> 'Subscription not found or expire','user_status'	=> $status ]);
		}
	}

	public function checkAppVersion(){
		$data = DB::table('app_releases')->get()->toArray();
		return response()->json(['code' => 200, 'data'=> $data[0]]);
	}
	
	public function getUserHelp(){
		$userHelp = UserHelp::where('user_id',Auth::user()->id)->get()->toArray();
		$i = 0;
		foreach ($userHelp as $single) {
			$feedback = UserHelpFeedback::where('help_id',$single['id'])->get()->toArray();
			$userHelp[$i]['feedback'] =$feedback; 
			$i++;
		}
		return response()->json(['code' => 200, 'data'=> $userHelp]);
	}
	public function addFeedback(Request $request){
		$rules = [
            'help_id' => 'required',
            'feedback' => 'required',
        ];
		$validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		$data = array(
					'help_id'	=> $request->help_id,
					'feedback'	=> $request->feedback,
					'feedback_from'	=> "User"
				);
		UserHelpFeedback::create($data);
		return response()->json(['code' => 200,'message'=>'Your request has been sent successfully. Our team will get back to you on registered Email ID.']);
	}
	public function addUserHelp(Request $request){
		$rules = [
            'help' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		$data = array(
					'help'		=> $request->help,
					'user_id'	=> Auth::user()->id,
					'status'	=> 0
				);
		UserHelp::create($data);
		return response()->json(['code' => 200,'message'=>'Your request has been sent successfully. Our team will get back to you on registered Email ID.']);
	}
	public function refreshFcm(Request $request){
        $rules = [
            'fcm_token' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		//if(isset($user->DeviceToken))
		Auth::user()->fcm_token = $request->fcm_token;
		Auth::user()->save();
		$data = array(
						'message' => 'FCM has been updated successfully.',
						'code'=>200,
					  );
		return response()->json($data);
	}
	public function socialLogin(Request $request)
	{
        $rules = [
        	'auth_key'=>'required|in:email,phone',
            'auth_value'=>'required',
            'social_id'=>'required',
            'device_id'=> 'required',
        ];
		
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		//return response()->json(['message' => $request->auth_value,'code' => 401]);
		$user = User::where($request->auth_key,$request->auth_value)->first();
		if($user){
			$login_cred = array(
				'vibez_id' => $user->vibez_id,
				'password' => $request->social_id,
            );
	        try {
	            // attempt to verify the credentials and create a token for the user
	            if (! $token = JWTAuth::claims(['device_id' => $request->input('device_id')])->attempt($login_cred)) {
	            	$newuser=User::where('email',$user->email)->first();
					if($newuser){
						if($newtoken=JWTAuth::fromUser($newuser)){
							$getUser = User::find($newuser->id);
							$getUser->fcm_token = $request->input('fcm_token');
							$getUser->device_type = $request->input('device_type');
							$getUser->device_id = $request->input('device_id');
							$getUser->email_verify = 1;
							$getUser->save();
							return response()->json(['code' => 200, 'data'=> [ 'token' => 'Bearer '.$newtoken,'user'=>User::find($newuser->id) ]]);
						}
					}
					return response()->json(['message' => 'You are already registered, try login with Vibez Id and Password.','code' => 401]);
	            }
	        } catch (JWTException $e) {
	            // something went wrong whilst attempting to encode the token
	            return response()->json(['message' => 'Failed to login, please try again.','code' => 401]);
	        }
			
			if(!$request->input('force_login')){
				if(Auth::user()->device_id){
					return response()->json(['message' => 'Hey, you’re already logged in from another device. Do you want to proceed with this device?','code' => 402]);
				}
			}
			
			Auth::user()->fcm_token = $request->input('fcm_token');
			Auth::user()->device_type = $request->input('device_type');
			Auth::user()->device_id = $request->input('device_id');
			Auth::user()->email_verify = 1;
			Auth::user()->save();
			
			return response()->json(['code' => 200, 'data'=> [ 'token' => 'Bearer '.$token,'user'=>User::find(Auth::user()->id) ]]);
		}
		
		return response()->json(['code' => 403,'message'=>'Registration required.']);
	}
	public function socialSignup(Request $request)
	{
		if(isset($request->phone)){
			$user=User::where('email',$request->email)->orWhere('phone', $request->phone)->first();	
		}else{
			$user=User::where('email',$request->email)->first();	
		}
		if($user){
			if($token=JWTAuth::fromUser($user)){
				return response()->json(['code' => 200, 'data'=> [ 'token' => 'Bearer '.$token,'user'=>Auth::user() ]]);
			}
		}
		
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required_without:phone|email|max:255|unique:users',
            'social_id'=>'required',
            'vibez_id' => 'required|unique:users',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'device_type'=>'required',
            'fcm_token'=>'required',
            'device_id'=>'required',
        ];
		
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
        if($request->hasfile('image')) {
        	$image = $request->file('image');
        	$imageName = time().'.'.$image->getClientOriginalExtension();
    	 	$image->move(public_path('images/users'), $imageName);
			$image = 'images/users/'.$imageName;
        } else {
        	$image = '';
        }
		
    	$data = array(
				'name' => $request->name,
				'email' => $request->input('email'),
				'password' => Hash::make($request->social_id),
				'dob' => date('Y-m-d',strtotime($request->input('dob'))),
				'image' => $image,
				'vibez_id' => $request->vibez_id,
				'gender' => isset($request->gender) ? $request->gender : 'None',
				'phone' => isset($request->phone) ? $request->phone : '',
				'fcm_token' => $request->input('fcm_token'),
				'device_type' => $request->input('device_type'),
				'device_id' => $request->device_id,
				'email_verify'		=> 1,
				'is_social_login'	=> 1
            );
		try {
        $user = User::create($data);
		} catch (\Exception $e) {
            return response()->json(['code' => 401, 'message' => $e->getMessage()]);
        }	
		$login_cred = array(
				'vibez_id' => $request->vibez_id,
				'password' => $request->social_id,
            );
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($login_cred)) {
                return response()->json(['code' => 401, 'message' => 'We cant find an account with this credentials.']);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['code' => 401, 'message' => 'Failed to login, please try again.']);
        }
        // all good so return the token
        
        $start_date = date('Y-m-d');
		$date = strtotime("+7 day");
		//$date = strtotime("+1 year");
		$end_date =  date('Y-m-d', $date);
		$unique = md5(time());
		$result = '';
	    for($i = 0; $i < 20; $i++) {
	        $result .= mt_rand(0, 9);
	    }
		$subvscriptionHistory = array(
									'user_id'				=> $user->id,
									'subscription_platform'	=> $request->device_type,
									'subscription_amount'	=> 0,
									'subscription_gateway'	=> isset($request->subscription_gateway) ? $request->subscription_gateway : "",
									'receipt'				=> $unique,
									'plan'					=> 'Trial Period',
									'user_status'			=> 1,
									'transaction_id'		=> $result,
									'date'					=> date('Y-m-d'),
									'promocode'				=> 0
								);
			UserSubscriptionHistory::create($subvscriptionHistory);
		$userSubscription = array(
								'user_id'				=> $user->id,
								'start_date'			=> $start_date,
								'end_date'				=> $end_date,
								'user_status'			=> 1,
								'promocode'				=> 0,
								'receipt'				=> md5(time()),
								'subscription_platform'	=> $request->device_type,
								'subscription_gateway'	=> isset($request->subscription_gateway) ? $request->subscription_gateway : "",
								'transaction_id'		=> $result,
								'subscription_amount'	=> 0,
								'plan'					=> 'Trial Period'
							);
		UserSubscription::create($userSubscription);
        
        
        return response()->json(['code' => 200, 'data'=> [ 'token' => 'Bearer '.$token,'user'=>Auth::user() ]]);
	}
	public function changePassword(Request $request)
	{
        $rules = [
            'oldPassword' => 'required',
            'newPassword' => 'required|max:12|min:4',
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		if(Hash::check($request->oldPassword, Auth::user()->password))
		{
			Auth::user()->password = Hash::make($request->newPassword);
			Auth::user()->save();
			return response()->json(['code' => 200, 'message' => 'Password has been changed successfully.']);
		} else {
			return response()->json(['code' => 401, 'message' => 'Sorry, Old Password doesn\'t match.']);
		}
	}
	
	public function updateProfile(Request $request)
	{
        $rules = [
            'name' => 'required|max:255',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'dob' => 'required',
            'user_id' => 'required',
            'gender' => ['required',Rule::in(['Male', 'Female','None'])],
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		
		$user = User::find($request->user_id);
        if($request->hasfile('image')) {
        	$image = $request->file('image');
        	$imageName = time().'.'.$image->getClientOriginalExtension();
        	 $image->move(public_path('images/users'), $imageName);
			 $image = 'images/users/'.$imageName;
        } else {
        	$image = $user->getOriginal('image');
        }
		
    	$data = array(
				'name' => $request->name,
				'password' => $request->input('password')?Hash::make($request->input('password')):$user->getOriginal('password'),
				'dob' => date('Y-m-d',strtotime($request->dob)),
				'image' => $image,
				'gender' => $request->gender,
				'phone' => $request->input('phone'),
            );
		$user->update($data);
		return response()->json(['code' => 200, 'message' => 'Profile updated successfully!','data'=>['user'=>$user]]);
	}

	public function sendPlaystoreApi(Request $request){
		$responsedata =  $request->responsedata;
		if(isset($responsedata) && $responsedata != ""){
			
			Mail::raw($responsedata, function ($message) {
			    $message->from('admin@thevibez.net', 'Vibez');
				$message->to('jeetb.wedowebapps@gmail.com')->subject('Vibez( Playstore response)');
			});
		}
	}

	public function cancelSubscription(Request $request){ 
		
		$checkIfSubscription = UserSubscription::where('user_id',Auth::user()->id)->get()->toArray();
		//$customer_id = $request->customer_id;
		 $curl = curl_init();
		 $url = 'https://api.ravepay.co/v2/gpx/subscriptions/query?seckey=FLWSECK-b84a144ca9eadf400e1718f7ea9ce9e5-X&email='.Auth::user()->email;
		  curl_setopt($curl, CURLOPT_URL, $url);
	   	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		      'Content-Type: application/json',
		   ));
	   	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	   	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	   	$result = curl_exec($curl);
	  	 curl_close($curl);
		 $result = json_decode($result);
		 
		 if(isset($result->data) && isset($result->data->plansubscriptions)){
		 	
		 	if(is_array($result->data->plansubscriptions) && sizeof($result->data->plansubscriptions) > 0){
		 		foreach ($result->data->plansubscriptions as $single_plan) {
					
					$plan_id = $single_plan->id;
					$status = $single_plan->status;
					if($status == "active"){
						$postcurl = curl_init();
						$url = 'https://api.ravepay.co/v2/gpx/subscriptions/'.$plan_id.'/cancel';
						$data = array(
								'seckey'	=> 'FLWSECK-b84a144ca9eadf400e1718f7ea9ce9e5-X'
								);
						$payload = json_encode($data);
						curl_setopt($postcurl, CURLOPT_URL, $url);
						curl_setopt($postcurl, CURLOPT_POST, 1);
						curl_setopt($postcurl, CURLOPT_POSTFIELDS, $payload);
					   	curl_setopt($postcurl, CURLOPT_HTTPHEADER, array(
						      'Content-Type: application/json',
						   ));
					   	curl_setopt($postcurl, CURLOPT_RETURNTRANSFER, 1);
					   	curl_setopt($postcurl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
					   	$result = curl_exec($postcurl);
					   	curl_close($postcurl);
						$result = json_decode($result);
					}
					$subscription = UserSubscription::find($checkIfSubscription[0]['id']);
					$subscription->user_status = 0;
					$subscription->save();
				 }
		 	}
			$subscription = UserSubscription::where('user_id',Auth::user()->id)->first();	
			return response()->json(['code' => 200, 'message' => 'Subscription has been canceled','data'=>['subscription'=>$subscription]]);
		 }else{
		 	$subscription = UserSubscription::where('user_id',Auth::user()->id)->first();
		 	return response()->json(['code' => 200, 'message' => 'Subscription not found!','data'=>['subscription'=>$subscription]]);		
		 }
		//
	}
	
	public function addSubscription(Request $request){
		
		 $rules = [
            'days' 				=> 'required',
            'plan'				=> 'required',
            'device_type'		=> 'required',
            'amount'			=> 'required',
            'receipt'			=> 'required',
            'currency'			=> 'required',
            'transaction_id'	=> 'required'
        ];
		$validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}

		Log::info("Add Scubscription");
		Log::info("Line 926: ".Auth::user()->id);
		Log::info("Days: ".$request->days);
		Log::info("plan: ".$request->plan);
		Log::info("device_type: ".$request->device_type);
		Log::info("amount: ".$request->amount);
		Log::info("receipt: ".$request->receipt);
		Log::info("currency: ".$request->currency);
		Log::info("transaction_id: ".$request->transaction_id);

		
		
		$checkIfSubscription = UserSubscription::where('user_id',Auth::user()->id)->count();
		
		if($checkIfSubscription == 1){
			Log::info("866:");
			$subscription = UserSubscription::where('user_id',Auth::user()->id)->first();
			
			$subvscriptionHistory = array(
									'user_id'				=> Auth::user()->id,
									'subscription_platform'	=> $request->device_type,
									'subscription_amount'	=> $request->amount,
									'receipt'				=> $request->receipt,
									'plan'					=> $request->plan,
									'transaction_id'		=> $request->transaction_id,
									'date'					=> date('Y-m-d'),
									'currency'				=> $request->currency,
									'user_status'			=> 1,
									'promocode'				=> isset($request->promocode) ? $request->promocode : 0,
									'subscription_gateway'	=> isset($request->subscription_gateway) ? $request->subscription_gateway : ""
								);
			UserSubscriptionHistory::create($subvscriptionHistory);
			$days = $request->days;
			$date = strtotime(date('Y-m-d'));
			$date = strtotime("+".$days." day", $date);
			$end_date =  date('Y-m-d', $date);
			$data = array(
					'receipt'				=> $request->receipt,
					'plan'					=> $request->plan,
					'end_date'				=> $end_date,
					'promocode'				=> isset($request->promocode) ? $request->promocode : 0,
					'subscription_platform'	=> $request->device_type,
					'subscription_amount'	=> $request->amount,
					'user_status'			=> 1,
					'subscription_gateway'	=> isset($request->subscription_gateway) ? $request->subscription_gateway : "",
					'transaction_id'		=> $request->transaction_id,
					'currency'				=> $request->currency,
					);
			
			$subscription->update($data);	
		}else{
			Log::info("866:");
			$days = $request->days;
			$date = strtotime("+".$days." day");
			$end_date =  date('Y-m-d', $date);
			$subvscriptionHistory = array(
									'user_id'				=> Auth::user()->id,
									'subscription_platform'	=> $request->device_type,
									'subscription_amount'	=> $request->amount,
									'subscription_gateway'	=> isset($request->subscription_gateway) ? $request->subscription_gateway : "",
									'receipt'				=> $request->receipt,
									'plan'					=> $request->plan,
									'transaction_id'		=> $request->transaction_id,
									'date'					=> date('Y-m-d'),
									'user_status'			=> 1,
									'currency'				=> $request->currency,
									'promocode'				=> isset($request->promocode) ? $request->promocode : 0
								);
			UserSubscriptionHistory::create($subvscriptionHistory);
			$data = array(
					'user_id'				=> Auth::user()->id,
					'receipt'				=> $request->receipt,
					'plan'					=> $request->plan,
					'start_date'			=> date('Y-m-d'),
					'end_date'				=> $end_date,
					'promocode'				=> isset($request->promocode) ? $request->promocode : 0,
					'subscription_platform'	=> $request->device_type,
					'subscription_amount'	=> $request->amount,
					'subscription_gateway'	=> isset($request->subscription_gateway) ? $request->subscription_gateway : "",
					'currency'				=> $request->currency,
					'user_status'			=> 1,
					'transaction_id'		=> $request->transaction_id,
					);
			UserSubscription::create($data);
		}
		$getEventCodes = EventCodes::where('user_id',0)->first();
		if($getEventCodes){
			$user = User::find(Auth::user()->id);
			$eventcode = EventCodes::find($getEventCodes->id);
			$eventcode->user_id = Auth::user()->id;
			$eventcode->save();
			//$user->sendAddSubscriptionNotification($getEventCodes->code);
			$userData = array(
						'user'			=> $user,
						'data'			=> $data,
						'event_code'	=> $eventcode
					);
			Mail::send('emails.addsubscription', compact('userData'), function ($message) use($userData){
			    $message->from('admin@thevibez.net', 'Vibez');
			    $message->to($userData['user']->email)->subject('Thank you for your subscription!');
			});	
		}
		
		 
		return response()->json(['code' => 200, 'message' => 'Subscription is successfully added!']);

	}

	
	public function subscriptionTransactionInstiallize(Request $request)
	{
		$rules = [
            'reference' 			=> 'required|max:255',
            'amount'				=> 'required',
            'email'					=> 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails())
		{
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		else
		{

			Log::info("Paystack Trasanction initialize");
			Log::info("Line 926: ".Auth::user()->id);
			Log::info($request->reference);
			Log::info($request->amount);
			Log::info($request->email);


			$result = array();
			//Set other parameters as keys in the $postdata array
			$postdata =  array('reference' => $request->reference, 'amount' => $request->amount,"email" => $request->email,'callback_url'=>'https://www.thevibez.net/api/addSubscriptionPayStack');
			
			Log::info(json_encode($postdata));


			$url = "https://api.paystack.co/transaction/initialize/";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($postdata));  //Post Fields
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$headers = [
			  'Authorization: Bearer sk_live_768a55f7e48b65852679f99426a03da8bce17a39',
			  'Content-Type: application/json',

			];
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$request_data_new = curl_exec ($ch);
			Log::info("Line 953: ".Auth::user()->id);
			Log::info("Line 954");
			Log::info(json_encode($request_data_new));

			curl_close ($ch);
			if ($request_data_new) 
			{
				$result = json_decode($request_data_new, true);
				if($result['status'] == false)
				{
					return response()->json(['code' => 401,'message'=>$result['message']]);
				}
				else
				{
					return response()->json(['code' => 200,'message'=>'Successfully created','data'=>$result['data']]);		
				}
			}
			else
			{
				return response()->json(['code' => 401,'message'=>'Api expression error']);
			}

		}
	}

	public function addSubscriptionPayStack_TestMode(Request $request){
		$rules = [
            'reference_url' 			=> 'required',
            'plan_id'					=> 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails())
		{
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		else
		{
			
			$new_geturl = $request->reference_url;
			$parts = parse_url($new_geturl);
			parse_str($parts['query'], $query);
			$trxref = $query['trxref'];
			$reference = $query['reference'];

			$result = array();
			//Set other parameters as keys in the $postdata array
			//$postdata =  array('reference' => $request->reference, 'amount' => $request->plan_amount,"email" => $request->user_email_id);
			$url = "https://api.paystack.co/transaction/verify/".$reference;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$headers = [
			  'Authorization: Bearer sk_test_58bcb94cfd0deef69e628d4cc586e39c17d4ef88',
			  'Content-Type: application/json',

			];
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$request_data_new = curl_exec ($ch);
			curl_close ($ch);
			if ($request_data_new) 
			{
				$result = json_decode($request_data_new, true);
				if($result['status'] == false)
				{
					return response()->json(['code' => 402,'message'=>$result['message']]);
				}
				else
				{	
					$main_data = $result['data'];

					if($main_data['status'] =="success")
					{
						$mycustomer_code = $main_data['customer']['customer_code'];
						$myplan_id = $request->plan_id;


						$result_new = array();
						//Set other parameters as keys in the $postdata array
						$postdata =  array('customer' => $mycustomer_code, 'plan' => $myplan_id);
						$url = "https://api.paystack.co/subscription/";
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL,$url);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($postdata));  //Post Fields
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$headers = [
						  'Authorization: Bearer sk_test_58bcb94cfd0deef69e628d4cc586e39c17d4ef88',
						  'Content-Type: application/json',

						];
						curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
						$request_new = curl_exec ($ch);
						curl_close ($ch);
						$result_of_suc = json_decode($request_new, true);
						// Do sucess stufff here
						return response()->json(['code' => 200,'message'=>$result['message'],'data'=>$result_of_suc,'customer_code'=>$mycustomer_code]);
					}
					else
					{
						return response()->json(['code' => 200,'message'=>"Verification failed"]);	
					}
				}
			}
			else
			{
				return response()->json(['code' => 401,'message'=>'Api expression error']);
			}
		}
	}

	public function addSubscriptionPayStack(Request $request){
		$rules = [
            'reference_url' => 'required',
            'plan_id'	=> 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails())
		{
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		else
		{

			Log::info("IN ADD Subscription PAYSTACK");
			Log::info($request->reference_url);
			Log::info($request->plan_id);

			$new_geturl = $request->reference_url;
			Log::info($new_geturl);


			$parts = parse_url($new_geturl);
			Log::info($parts);
			parse_str($parts['query'], $query);
			$trxref = $query['trxref'];
			Log::info($trxref);
			$reference = $query['reference'];
			Log::info($reference);
			
			$result = array();
			//Set other parameters as keys in the $postdata array
			//$postdata =  array('reference' => $request->reference, 'amount' => $request->plan_amount,"email" => $request->user_email_id);
			$url = "https://api.paystack.co/transaction/verify/".$reference;
			Log::info($url);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$headers = [
			  'Authorization: Bearer sk_live_768a55f7e48b65852679f99426a03da8bce17a39',
			  'Content-Type: application/json',

			];
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$request_data_new = curl_exec ($ch);
			Log::info("LINE 1107: ".Auth::user()->id);
			Log::info("LINE 1108 : ".json_encode($request_data_new));
			curl_close ($ch);
			if ($request_data_new) 
			{
				$result = json_decode($request_data_new, true);
				if($result['status'] == false)
				{
					return response()->json(['code' => 402,'message'=>$result['message']]);
				}
				else
				{	
					$main_data = $result['data'];

					Log::info("LINE 1123: ".json_encode($main_data));

					if($main_data['status'] =="success")
					{
						$mycustomer_code = $main_data['customer']['customer_code'];
						Log::info($mycustomer_code);
						$myplan_id = $request->plan_id;
						Log::info($myplan_id);


						$result_new = array();
						//Set other parameters as keys in the $postdata array
						$postdata =  array('customer' => $mycustomer_code, 'plan' => $myplan_id);
						
						Log::info($postdata);

						$url = "https://api.paystack.co/subscription/";
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL,$url);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($postdata));  //Post Fields
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$headers = [
						  'Authorization: Bearer sk_live_768a55f7e48b65852679f99426a03da8bce17a39',
						  'Content-Type: application/json',

						];
						curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
						$request_new = curl_exec ($ch);
						Log::info("USERID 1152: ".Auth::user()->id);
						Log::info("PAYSTACK 1153 : ".json_encode($request_new));
						curl_close ($ch);
						$result_of_suc = json_decode($request_new, true);
						// Do sucess stufff here
						return response()->json(['code' => 200,'message'=>$result['message'],'data'=>$result_of_suc,'customer_code'=>$mycustomer_code]);
					}
					else
					{
						Log::info("IN ADD Subscription PAYSTACK Verification FAILED");
						return response()->json(['code' => 200,'message'=>"Verification failed"]);	
					}
				}
			}
			else
			{
				Log::info("IN ADD Subscription PAYSTACK API EXPRESSSION ERROR");
				return response()->json(['code' => 401,'message'=>'Api expression error']);
			}
		}
	}

	public function cancelSubscriptionPayStack(Request $request){
		$rules = [
            'subscription_id' 			=> 'required|max:255',
            'email_token'				=> 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails())
		{
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		else
		{
			$checkIfSubscription = UserSubscription::where('user_id',Auth::user()->id)->get()->toArray();
			
			$result_new = array();
			//Set other parameters as keys in the $postdata array
			$postdata =  array('code' => $request->subscription_id, 'token' => $request->email_token);
			$url = "https://api.paystack.co/subscription/disable";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($postdata));  //Post Fields
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$headers = [
			  'Authorization: Bearer sk_live_768a55f7e48b65852679f99426a03da8bce17a39',
			  'Content-Type: application/json',

			];
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$request_new_dd = curl_exec ($ch);
			curl_close ($ch);
			if ($request_new_dd) 
			{
				$result_dt_nw = json_decode($request_new_dd, true);
				$subscription = UserSubscription::find($checkIfSubscription[0]['id']);
				$subscription->user_status = 0;
				$subscription->save();
				return response()->json(['code' => 200,'message'=>'Sucessfully','data'=>$result_dt_nw]);
			}
			else
			{
				return response()->json(['code' => 401,'message'=>'Api expression error']);
			}
		}
	}
	
	public function checkPromocode(Request $request){
		 $rules = [
            'promocode' => 'required|max:255',
            'current_date'		=> 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		
		$checkPromocode = Promocodes::where('code',$request->promocode)->get()->toArray();
		if(sizeof($checkPromocode) > 0){
			$checkifAlreadyUsed = UserSubscription::where('user_id',Auth::user()->id)->with('promocodes')->get()->toArray();
			
			if(sizeof($checkifAlreadyUsed) > 0 && isset($checkifAlreadyUsed[0]['promocodes'])){
				if($checkifAlreadyUsed[0]['promocodes']['code'] == $request->promocode){
					return response()->json(['code' => 401,'message'=>'You have already used this referral code once']);
				}
			}
			$given_date = strtotime(date('Y-m-d',strtotime($request->current_date)));
			$promocode_end_date = $checkPromocode[0]['end_date'];
			$promocode_start_date =$checkPromocode[0]['start_date'];
			if($promocode_end_date != ""){
				$promocode_end_date = strtotime($promocode_end_date);
				$promocode_start_date = strtotime($promocode_start_date);
				if($given_date < $promocode_start_date){
					return response()->json(['code' => 401,'message'=>'The entered referral code is invalid']);
				}elseif($given_date <= $promocode_end_date){
					return response()->json(['code' => 200,'message'=>'Congratulations, the referral code has been applied to your account']);
				}else{
					return response()->json(['code' => 401,'message'=>' The entered referral code has expired']);
				}
			}else{
				$promocode_start_date = strtotime($promocode_start_date);
				if($given_date >= $promocode_start_date){
					return response()->json(['code' => 200,'message'=>'Congratulations, the referral code has been applied to your account']);
				}else{
					return response()->json(['code' => 401,'message'=>'The entered referral code is invalid']);
				}
			}			
		}else{
			return response()->json(['code' => 401,'message'=>'The entered referral code does not exist']);
		}
	}
	
	public function changeThemeDesign(Request $request)
	{
		$rules = ['theme' => ['required',Rule::in(['dark', 'light'])]];
		
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		
		Auth::user()->design = $request->theme;
		Auth::user()->save();
		
		return response()->json(['code' => 200, 'message' => 'Design has been changed successfully!']);
	}
	public function updatePassword(Request $request)
	{
        $rules = [
            'verification_code' => 'required',
            'newPassword' => 'required|max:12|min:4',
            'email' => 'required|exists:users,email',
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
    	$user = User::where('email', $request->email)->first();
        if (!$user) {
            $error_message = "Your email address was not found.";
            return response()->json(['code' => 401, 'mesage' => $error_message]);
        }
		
		if(!$user->token){
			return response()->json(['code' => 401,'message'=>'Verfication code is not sent.']);
		}
		
		if($user->token->token == $request->verification_code)
		{
			$user->token()->delete();
			$user->password = Hash::make($request->newPassword);
			$user->save();
			return response()->json(['code' => 200, 'message' => 'Password has been changed successfully. Try Login now']);
		} else {
			return response()->json(['code' => 401, 'message' => 'Sorry, Verfication code does not match.']);
		}
	}
	
	/**
     * API Recover Password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recoverPassword(Request $request)
    {
        $rules = [
            'email' => 'required|exists:users,email',
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
    	$user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json(['code' => 401, 'mesage' => "Your email address was not found."]);
        }
        try {
            $token = mt_rand(10000, 99999);
			$user->token()->updateOrCreate(['user_id'=>$user->id],['token'=>$token]);
        	$user->sendApiPasswordResetNotification($token);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(),'code'=>401]);
        }
        return response()->json([
            'code' => 200, 'message'=> 'A Verification email has been sent! Please check your email.'
        ]);
    }
    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */
    public function logout(Request $request) {
			Auth::user()->fcm_token = '';
			Auth::user()->device_id = '';
			Auth::user()->save();
			Auth::guard('api')->logout(true);
        try {
            JWTAuth::invalidate($request->input('token'));
            return response()->json(['code' => 200, 'message'=> "You have successfully logged out."]);
        } catch (Exception $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['code' => 401, 'message' => 'Failed to logout, please try again.']);
        }
    }
	
	public function forceLogout(Request $request){
		 try {
		 	
            Auth::guard('api')->logout(true);
			JWTAuth::invalidate($request->input('token'));
            return response()->json(['code' => 200, 'message'=> "You have successfully logged out."]);
        } catch (Exception $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['code' => 401, 'message' => 'Failed to logout, please try again.']);
        }
	}
	
	public function manualPaymentSubmit(Request $request){
		$validation =$request->validate([
            'user_name' 			=> 'required',
            'date_paid' 			=> 'required',
            'bank_name'				=> 'required',
            'bank_account_number'	=> 'required',
            'transaction_id'	=> 'required'	
        ]);
		if (!is_array($validation)) {
			session_start();
			$_SESSION['errors'] = $validation->errors();
			$_SESSION['status'] = 0;
            return redirect()->back();
        }else{
			$userid = $request->user_id;
			$date_paid = $request->date_paid;
			$bank_name = $request->bank_name;
			$bank_account_number = $request->bank_account_number;
			$checkifAlreadyExist = UserManualPayment::where('user_id',$userid)->where('status', 0)->count();
			if($checkifAlreadyExist == 0){
				$userPaymentData = array(
								'user_id'				=> $request->user_id,
								'date_paid'				=> $request->date_paid,
								'bank_name'				=> $request->bank_name,
								'bank_account_number'	=> $request->bank_account_number,
								'transaction_id'		=> $request->transaction_id,
								'status'				=> 0,
							);
				UserManualPayment::create($userPaymentData);
				session_start();
				$_SESSION['errors'] = 'Thank you for submitting payment information, it will take 3-5 business days to update the subscription'; 
				$_SESSION['status'] = 1;
				return redirect()->back(); 
			}else{
				session_start();
				$_SESSION['errors'] = 'You have already submited the payment information';
				$_SESSION['status'] = 0;
				return redirect()->back();
			}
		}
	}
	
	public function manualPaymentInsert($email){
		if(isset($email) && $email != ""){
			$user = User::where('email',$email)->get()->toArray();
			$site_url = URL::to('/');
			if(sizeof($user) > 0){
				return view('payment',compact('user','site_url'));
			}else{
				return response()->json(['code' => 401, 'message' => 'The page you requested is not found!']);
			}
		}else{
			return response()->json(['code' => 401, 'message' => 'The page you requested is not found!']);
		}
	}
	public function manualPaymentGateway($email){
		if(isset($email) && $email != ""){
			$user = User::where('email',$email)->get()->toArray();
			$site_url = URL::to('/');
			if(sizeof($user) > 0){
				return view('payment_gateway',compact('user','site_url'));
			}else{
				return response()->json(['code' => 401, 'message' => 'The page you requested is not found!']);
			}
		}else{
			return response()->json(['code' => 401, 'message' => 'The page you requested is not found!']);
		}
	}
	public function manualPaymentInfo($email){
		if(isset($email) && $email != ""){
			$user = User::where('email',$email)->get()->toArray();
			$site_url = URL::to('/');
			if(sizeof($user) > 0){
				return view('payment_info',compact('user','site_url'));
			}else{
				return response()->json(['code' => 401, 'message' => 'The page you requested is not found!']);
			}
		}else{
			return response()->json(['code' => 401, 'message' => 'The page you requested is not found!']);
		}
	}
	
	public function share($class,$title,$id)
	{
		$model = \App::make('App\\'.$class)->find($id);
		return view('home',compact('model'));
	}
	
	public function searchAll(Request $request,$query)
	{
		$artists = Artist::search($query, null, true, true)->orderBy('updated_at','DESC')->orderBy('created_at','DESC')->paginate(3)->values();
		$albums = Album::search($query, null, true, true)->orderBy('updated_at','DESC')->orderBy('created_at','DESC')->paginate(3)->values();
		$musics = Music::search($query, null, true, true)->orderBy('updated_at','DESC')->orderBy('created_at','DESC')->paginate(3)->values();
		$videos = Video::search($query, null, true, true)->orderBy('updated_at','DESC')->orderBy('created_at','DESC')->paginate(3)->values();
		$playlists = Playlist::search($query, null, true, true)->get()->filter(function($item) {
							    return $item->user->is_blocked === false;
							 })->forPage(1, 3)->values();
		$news = News::search($query, null, true, true)->paginate(3)->values();
		$users = User::search($query, null, true, true)->where('id','<>',Auth::user()->id)->get()->filter(function($item) {
							    return $item->is_blocked === false;
							 })->forPage(1, 3)->values();
		foreach ($artists as $artist) {
			$musics = count($musics) < 3?$musics->merge($artist->musics)->unique('id'):$musics;
			$videos = count($videos) < 3?$videos->merge($artist->videos)->unique('id'):$videos;
			$news = count($news) < 3?$news->merge($artist->news)->unique('id'):$news;
			$albums = count($albums) < 3?$albums->merge($artist->albums)->unique('id'):$albums;
		}
		$result = response()->json(['code' => 200, 'data'=>['artists'=>$artists,'albums'=>$albums->forPage(1, 3),'musics'=>$musics->forPage(1, 3),'videos'=>$videos->forPage(1, 
3),'playlists'=>$playlists,'news'=>$news->forPage(1, 3),'users'=>$users],'query'=>$query]);
		return $result;
	}

	public function searchresult(Request $request)
	{
		$rules = [
            'content' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}


		/*DB::table('search_content')->insert(
		    ['user_id' => Auth::user()->id, 'content' => $request->content]
		);*/
		try
		{
			SearchContent::insert(['user_id' => Auth::user()->id, 'content' => $request->content, 'created_at'=> date('Y-m-d H:i:s'), 'updated_at'=> date('Y-m-d H:i:s')]);
			
			$data['mail']   = 'info@thevibez.net';
	        $data['content']   = $request->content;
	        $data['email']   = Auth::user()->email;
	        $data['name']    = Auth::user()->name;
	        
	        Mail::send('emails.letusknow',['user' => $data], function ($message) use($data){
                    $message->from('otoide@ostech.com.ng', $data['name'].' - '.$data['email'] );
                    $message->subject('Let Us Know');
                    $message->to($data['mail']);
                    $message->replyTo($data['email'], $data['name']);
        	});
			return response()->json(['code' => 200, 'message'=> "Successfully reported to admin."]);	
		}
		catch (Exception $e) {
            return response()->json(['code' => 401, 'message' => 'Failed, please try again.']);
        }
		
	}

	public function searchByType(Request $request,$type,$query)
	{
		$page = $request->input('page')?:1;
		switch ($type) {
			case 'musics':
					$musics = Music::search($query, null, true, true)->get();
					$artists = Artist::search($query, null, true, true)->get();
					foreach ($artists as $artist) {
						$musics = $musics->merge($artist->musics)->unique('id');
					}	
					$perpage = $musics->forPage($page, 20);
					$result = response()->json(['code' => 200, 'data'=>['musics'=>$perpage]]);
				break;
			case 'news':
					$news = News::search($query, null, true, true)->get();
					$artists = Artist::search($query, null, true, true)->get();
					foreach ($artists as $artist) {
						$news = $news->merge($artist->news)->unique('id');
					}	
					$result = response()->json(['code' => 200, 'data'=>['news'=>$news->forPage($page, 20)]]);
				break;
			case 'videos':
					$videos = Video::search($query, null, true, true)->get();
					$artists = Artist::search($query, null, true, true)->get();
					foreach ($artists as $artist) {
						$videos = $videos->merge($artist->videos)->unique('id');
					}	
					$result = response()->json(['code' => 200, 'data'=>['videos'=>$videos->forPage($page, 20)]]);
				break;
			case 'playlists':
					$playlists = Playlist::search($query, null, true, true)->get();
					$users = User::search($query, null, true, true)->get();
					foreach ($users as $user) {
						$playlists = $playlists->merge($user->playlists)->unique('id');
					}
					$result = response()->json(['code' => 200, 'data'=>['playlists'=>$playlists->forPage($page, 20)]]);
				break;
			case 'artists':
					$artists = Artist::search($query, null, true, true)->orderBy('is_featured', 'desc')->get();
					$result = response()->json(['code' => 200, 'data'=>['artists'=>$artists->forPage($page, 20)]]);
				break;
			case 'albums':
					$albums = Album::search($query, null, true, true)->get();
					$artists = Artist::search($query, null, true, true)->get();
					foreach ($artists as $artist) {
						$albums = $albums->merge($artist->albums)->unique('id');
					}
					$result = response()->json(['code' => 200, 'data'=>['albums'=>$albums->forPage($page, 20)]]);

					
				break;
			case 'users':
					$users = User::search($query, null, true, true)->get();
					$result = response()->json(['code' => 200, 'data'=>['users'=>$users->forPage($page, 20)]]);
				break;
			
			default:
				
				break;
		}
		return $result;
	}
/*
	public function searchMusics($query)
	{
		$musics = Music::search($query, null, true, true)->get();
		$artists = Artist::search($query, null, true, true)->get();
		foreach ($artists as $artist) {
			$musics = $musics->merge($artist->musics);
		}
		$result = response()->json(['code' => 200, 'data'=>['musics'=>$musics]]);
		return $result;
	}
 */
	public function searchedKeywords()
	{
		$recentSearch = Auth::user()->keywords->take(5);
		$date = new \DateTime();
		$date->modify('-12 hours');
		$formatted_date = $date->format('Y-m-d H:i:s');
		$trendingKeywords = DB::table('search_keywords')->select(DB::raw('count(*) as search_count, keyword'))
														->where('updated_at', '>',$formatted_date)
														->groupBy('keyword')
														->orderBy('search_count','DESC')
														->limit(5)
														->get();
		return response()->json(['code' => 200, 'data'=> ['recent_search' => $recentSearch,'trending_keywords' => $trendingKeywords]]);
	}
	
	public function followUser(Request $request)
	{
        $rules = [
            'user_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$user = User::find($request->user_id);
		if(!$user){
			return response()->json(['code' => 200,'message'=>'User does not exist!']);
		}
		$followBackFlag = FALSE;
		Auth::user()->follow($user);
		
		$toUser = User::find($request->user_id);
		// $checkIfFollowBack = $user->followers()->get()->toArray();
		$checkIfFollowBack = Auth::user()->followers()->get()->toArray();
		foreach ($checkIfFollowBack as $single) {
			
			if($single['id'] == $request->user_id){
				$flag = 0;
				$matchThese = ['to_user' => Auth::user()->id, 'from_user' => $request->user_id];
				$orThose = ['to_user' => $request->user_id, 'from_user' => Auth::user()->id];
				$authuserid = Auth::user()->id;
				$requestuserid = $request->user_id;
				$checkifinvitation = ChatInvitation::where(function($q) use ($authuserid, $requestuserid) {
					            $q->where(function($query) use ($authuserid, $requestuserid){
					                    $query->where('to_user', $authuserid)
					                          ->where('from_user', $requestuserid);
					                })
					              ->orWhere(function($query) use ($authuserid, $requestuserid) {
					                    $query->where('to_user', $requestuserid)
					                          ->where('from_user', $authuserid);
					                });
					            })->count();
				if($checkifinvitation != 0)
				{
					$flag = 1;					
				}
				if($flag == 0){
					ChatInvitation::firstOrCreate(['from_user'=>$request->user_id,'to_user'=>Auth::user()->id,'accepted'=> 1]);	
				}
				
			}
		}
		$message['title']= 'Vibez';
		$message['message']= Auth::user()->vibez_id.' has followed you.';
		$message['click_action']= 'vibez.user.follow';
		$message['user_id']= Auth::user()->id;
		$message['user'] = Auth::user();
		$user->notify((new PushNotification($message))->onQueue('audio')); 
		return response()->json(['code' => 200, 'follow_count'=>$user->followers, 'message' => 'You followed this User successfully.','chat_invitation_info'=> $toUser->getChatInvitation()]);
	}
	
	public function unfollowUser(Request $request)
	{
        $rules = [
            'user_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$user = User::find($request->user_id);
		if(!$user){
			return response()->json(['code' => 200,'message'=>'User does not exist!']);
		}
		Auth::user()->unfollow($user);
		return response()->json(['code' => 200, 'follow_count'=>$user->followers, 'message' => 'You unfollowed this User successfully.']);
	}
	public function blockUser(Request $request)
	{
        $rules = [
            'user_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$user = User::find($request->user_id);
		if(!$user){
			return response()->json(['code' => 200,'message'=>'User does not exist!']);
		}
		Auth::user()->block($user);
		$message['title']= 'Vibez';
		$message['message']= Auth::user()->name.' has blocked you for chat.';
		$message['click_action']= 'vibez.chat.block';
		$message['user_id']= Auth::user()->id;
		$user->notify((new SilentPushNotification($message))->onQueue('audio'));
		return response()->json(['code' => 200, 'message' => 'You blocked this User successfully.']);
	}
	
	public function unblockUser(Request $request)
	{
        $rules = [
            'user_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$user = User::find($request->user_id);
		if(!$user){
			return response()->json(['code' => 200,'message'=>'User does not exist!']);
		}
		Auth::user()->unblock($user);
		$message['title']= 'Vibez';
		$message['message']= Auth::user()->name.' has unblocked you for chat.';
		$message['click_action']= 'vibez.chat.unblock';
		$message['user_id']= Auth::user()->id;
		$user->notify((new SilentPushNotification($message))->onQueue('audio'));
		return response()->json(['code' => 200, 'message' => 'You unblocked this User successfully.']);
	}
	
	public function reportUser(Request $request)
	{
        $rules = [
            'user_id' => 'required|integer',
            'description'=> 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$user = User::find($request->user_id);
		if(!$user){
			return response()->json(['code' => 200,'message'=>'User does not exist!']);
		}
		Report::create(['report_from'=>Auth::user()->id,
						'report_to'=>$request->user_id,
						'description'=>$request->input('description'),
						'feedback'=>''
					  ]);
		return response()->json(['code' => 200, 'message' => 'Report has been sent to Vibez Admin successfully, shortly you will receive feedback mail.']);
	}
	
	
	public function likeAlbum(Request $request)
	{
        $rules = [
            'album_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$album = Album::find($request->album_id);
		if(!$album){
			return response()->json(['code' => 200,'message'=>'Album does not exist!']);
		}
		Auth::user()->like($album);
		return response()->json(['code' => 200, 'message' => 'You liked this Album successfully.']);
	}
	
	public function unlikeAlbum(Request $request)
	{
        $rules = [
            'album_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$album = Album::find($request->album_id);
		if(!$album){
			return response()->json(['code' => 200,'message'=>'Album does not exist!']);
		}
		Auth::user()->unlike($album);
		return response()->json(['code' => 200, 'message' => 'You unliked this Album successfully.']);
	}
	
	public function deleteUser(Request $request)
	{
        $rules = [
            'user_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$user = User::find($request->user_id);
		if(!$user){
			return response()->json(['code' => 200,'message'=>'user does not exist!']);
		}
		$user->playlists()->delete();
		$user->token()->delete();
		$user->comments()->delete();
		$user->keywords()->delete();
		$user->watchedData()->delete();
		//$user->likes()->delete();
		//$user->followings()->delete();
		$user->delete();
		return response()->json(['code' => 200, 'message' => 'User deleted successfully.']);
	}
	
	public function getUserInfo(Request $request)
	{
        $rules = [
            'user_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$user = User::find($request->user_id);
		if(!$user){
			return response()->json(['code' => 200,'message'=>'user does not exist!']);
		}
		
		return response()->json(['code' => 200, 'data'=> ['user'=>$user 
,'playlists'=>$user->getPlaylists(),'favourites'=>$user->getFavouriteData(),'recent_played'=>$user->recentlyPlayedMusics()->get(),'followed_artists'=>$user->getFollowedArtist(),'chat_invitation_info'=>$user->getChatInvitation()]]);
	}
	
	public function followAlbum(Request $request)
	{
        $rules = [
            'album_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$album = Album::find($request->album_id);
		if(!$album){
			return response()->json(['code' => 200,'message'=>'Album does not exist!']);
		}
		Auth::user()->follow($album);
		return response()->json(['code' => 200, 'follow_count'=>$album->follow_count, 'message' => 'You followed this Album successfully.']);
	}
	
	public function unfollowAlbum(Request $request)
	{
        $rules = [
            'album_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401, 'message'=>implode(', ', $errors->all())]);
		}	
		$album = Album::find($request->album_id);
		if(!$album){
			return response()->json(['code' => 200,'message'=>'Album does not exist!']);
		}
		Auth::user()->unfollow($album);
		return response()->json(['code' => 200, 'follow_count'=>$album->follow_count, 'message' => 'You unfollowed this Album successfully.']);
	}
	/*
	public function getMessageHistory(Request $request)
	{
        $rules = [
            'user_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		$conversation = Conversation::getConversation($request->user_id);
		if(!$conversation){
			
			$conversation = Conversation::create(['user1'=>Auth::user()->id,'user2'=>$request->user_id]);
		}
		$conversation->markAsRead();
		return response()->json(['code' => 200, 'data'=> ['history'=>$conversation->messages,'conversation_id'=>$conversation->id]]);
	}
	 *
	 */
	public function readAllMessage(){
		userChat::where('to_id',Auth::user()->id)->delete();
		return response()->json(['code' => 200,'message' => 'Message read successfully.']); 
	}
	public function sendMessage(Request $request)
	{
        $rules = [
            'user_id' => 'required|integer',
            'message'=>'required'
        ];
		
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		$message['title']= Auth::user()->vibez_id;
		$message['message']= $request->message;
		$message['user']=Auth::user();
		$message['click_action']= 'vibez.message.new';
		$user = User::find($request->user_id);
		$message['user']->is_blocked = false;
		
		$user->notify((new PushNotification($message))->onQueue('audio'));
		$user_chat = userChat::updateOrCreate(
				['from_id' => Auth::user()->id,'to_id'=> $request->user_id],
				[
					'from_id'	 	=> Auth::user()->id,
					'to_id'			=> $request->user_id
				]); 
		return response()->json(['code' => 200,'message' => 'Message sent successfully.']);
	}
	
	public function sendChatInvitation(Request $request)
	{
        $rules = [
            'user_id' => 'required|integer'
        ];
		
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		
		$user = User::find($request->user_id);
		//$user->unblock(Auth::user());
		
		if(Auth::user()->isBlockedBy($user))
		{
			return response()->json(['code' => 401,'message' => $user->name.' blocked you, invitation can not be sent.']);
		}
		
		if(ChatInvitation::where('to_user',Auth::user()->id)->where('from_user',$request->user_id)->exists())
		{
			return response()->json(['code' => 401,'message' => 'Chat invitation already sent by '.$user->name.'.']);
		}
		
		if(ChatInvitation::where('to_user',$request->user_id)->where('from_user',Auth::user()->id)->exists())
		{
			return response()->json(['code' => 401,'message' => 'Chat invitation already sent by you.']);
		}
		
		$invitation = ChatInvitation::firstOrCreate(['from_user'=>Auth::user()->id,'to_user'=>$request->user_id]);
		
		$message['title']= 'Vibez';
		$message['message']= Auth::user()->name.' has invited you for chat.';
		$message['click_action']= 'vibez.chat.invite';
		$message['invitation_id']= $invitation->id;
		$message['invitee']= Auth::user();
		$user->notify((new PushNotification($message))->onQueue('audio'));
		
		return response()->json(['code' => 200,'message' => 'Chat invitation sent successfully.']);
	}
	public function getChatList()
	{
		$invitations = ChatInvitation::where(function ($query){
			$query->where('from_user',Auth::user()->id);
			$query->orWhere('to_user',Auth::user()->id);
		})->where('accepted',1)->orderBy('created_at','DESC')->get()->filter(function($item) {
							    return !$item->invited->isBlockedBy($item->invitee) || !$item->invitee->isBlockedBy($item->invited);
							 })->values();
		return response()->json(['code' => 200,'data' => ['chat_list'=>$invitations]]);
	}
	
	public function acceptChatInvitation(Request $request)
	{
        $rules = [
            'invitation_id' => 'required|integer'
        ];
		
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		
		$invitation = ChatInvitation::find($request->invitation_id);
		
		if(Auth::user()->isBlockedBy($invitation->invitee)){
			return response()->json(['code' => 401,'message' => $invitation->invitee->name.' blocked you, invitation can not be accepted.']);
		}
		
		$invitation->accepted = 1;
		$invitation->save();
		
		return response()->json(['code' => 200,'message' => 'Invitation accepted successfully.']);
	}
	
	public function getAllNotifications()
	{
		$newNoti = array();	
		$notifications = Auth::user()->notifications->sortByDesc('created_at')->take(50)->toArray();
		
		for ($i=0; $i < sizeof($notifications); $i++) 
		{
			$notifications[$i] = (array)$notifications[$i];
			/*if (array_key_exists("playlist",$notifications[$i]['data']) && (int)$notifications[$i]['data']['playlist']['id'] > 0)
			{
				$playlist = Playlist::find($notifications[$i]['data']['playlist']['id']);
				$notifications[$i]['data']['playlist']['image'] = $playlist->db_image;
				$notifications[$i]['data']['playlist']['thumb'] = $playlist->db_thumb_image;
				
			} 

			if (array_key_exists("video",$notifications[$i]['data']) && (int)$notifications[$i]['data']['video']['id'] > 0)
			{
				$video = Video::find($notifications[$i]['data']['video']['id']);

				$notifications[$i]['data']['video']['image'] = $video->db_image;
				$notifications[$i]['data']['video']['thumb'] = $video->db_thumb_image;
				$notifications[$i]['data']['video']['url']  = $video->db_url;
			}

			if (array_key_exists("news",$notifications[$i]['data']) && (int)$notifications[$i]['data']['news']['id'] > 0)
			{
				$news = News::find($notifications[$i]['data']['news']['id']);
				$notifications[$i]['data']['news']['image'] = $news->db_image;
				$notifications[$i]['data']['news']['thumb'] = $news->db_thumb_image;
			}

			if (array_key_exists("album",$notifications[$i]['data']) && (int)$notifications[$i]['data']['album']['id'] > 0)
			{
				$album = Album::find($notifications[$i]['data']['album']['id']);
				$notifications[$i]['data']['album']['image'] = $album->db_image;
				$notifications[$i]['data']['album']['thumb'] = $album->db_thumb_image;
			}*/

			$notifications[$i]['time_string'] = time_elapsed_string($notifications[$i]['updated_at']);
			$notifications[$i]['description'] = 'Vibez';
			$newNoti[] = $notifications[$i];
		}
		$total = Auth::user()->unreadNotifications->count();
		return response()->json(['code' => 200, 'data'=> ['notifications'=>$newNoti,'total'=>$total]]);
		
		
	}
	public function getAllUnreadNotifications(){
		$total = Auth::user()->unreadNotifications->count();
		$chat_count=userChat::where('to_id',Auth::user()->id)->count();
		if($chat_count > 0){
			$chat_count = 1;
		}
		return response()->json(['code' => 200, 'data'=> ['notifications_count'=>$total,'chat_count'=>$chat_count]]);
	}
	public function markAsRead(){
		$notifications = Auth::user()->notifications->where('notifiable_id',Auth::user()->id)->toArray();
		/*$notification = Auth::user()->unreadNotifications->where('id',$id)->first();
		if(!$notification){
			return response()->json(['code' => 200,'message'=>'Notification does not exist or it\'s allready read!']);
		}
		if($notification)
		$notification->markAsRead();
		*/
		foreach ($notifications as $single_notification) {
			$notification = Auth::user()->notifications->where('id',$single_notification['id'])->first();
			$notification->markAsRead();
		}
		return response()->json(['code' => 200, 'message'=>'Notification read successfully.']);
		
	}
	public function removeNotification($id){
		
		$notification = Auth::user()->notifications->where('id',$id)->first();
		
		if(!$notification){
			return response()->json(['code' => 200,'message'=>'Notification does not exist!']);
		}
		
		if($notification)
		$notification->delete();
		
		return response()->json(['code' => 200, 'message'=>'Notification removed successfully.']);
	}
	public function removeAllNotification(Request $request){
		
		$notifications = Auth::user()->notifications->where('notifiable_id',Auth::user()->id)->toArray();
		foreach ($notifications as $single_notification) {
			$notification = Auth::user()->notifications->where('id',$single_notification['id'])->first();
			$notification->delete();
		}
		return response()->json(['code' => 200, 'message'=>'All notification removed successfully.']);
	}
	
	public function getUserFollowers(Request $request)
	{
        $rules = [
            'user_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$user = User::find($request->user_id);
		if(!$user){
			return response()->json(['code' => 200,'message'=>'user does not exist!']);
		}	
		
		return response()->json(['code' => 200, 'data'=> ['followers'=>$user->followers()->get()]]);
	}
	
	public function getUserFollowings(Request $request)
	{
        $rules = [
            'user_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$user = User::find($request->user_id);
		if(!$user){
			return response()->json(['code' => 200,'message'=>'user does not exist!']);
		}	
		
		return response()->json(['code' => 200, 'data'=> ['followers'=>$user->followings(User::class)->get()]]);
	}
	
	public function hasBlockedCurrent($id)
	{
		return response()->json(['code'=>200,'status'=>Auth::user()->isBlockedBy(User::find($id))]);
	}
	
	public function getPage(Request $request)
	{
        $rules = [
            'slug' => 'required'
        ];
		$page = Page::where('slug','like','%'.$request->slug.'%')->first();
		if(!$page){
			return response()->json(['code' => 200,'message'=>'Page does not exist!']);
		}	
		
		return response()->json(['code' => 200, 'data'=> ['page'=>$page]]);
	}
	
	public function getBlockedUser()
	{
		$users = Auth::user()->blocks(User::class)->get();
		return response()->json(['code' => 200, 'data'=> ['users'=>$users]]);
	}

	
	public function checkRcIos($receipt){
		
		//$sandbox should be TRUE if you want to test against itunes sandbox servers
		
		//$verify_host = "ssl://sandbox.itunes.apple.com";
		$verify_host = "ssl://buy.itunes.apple.com";
		$json='{"receipt-data" : "'.$receipt.'" ,"password" : "e698a3ab20094c6189f9fb352c237fc9"}';
		//opening socket to itunes
		$fp = fsockopen ($verify_host, 443, $errno, $errstr, 30);
		if (!$fp) 
		{
			// HTTP ERROR
			return false;
		} 
		else
		{ 
			//iTune's request url is /verifyReceipt     
			$header = "POST /verifyReceipt HTTP/1.0\r\n";
			$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
			$header .= "Content-Length: " . strlen($json) . "\r\n\r\n";
			fputs ($fp, $header . $json);
			$res = '';
			while (!feof($fp)) 
			{
				$step_res = fgets ($fp, 1024);
				$res = $res . $step_res;
			}
			fclose ($fp);
			//taking the JSON response
			$json_source = substr($res, stripos($res, "\r\n\r\n{") + 4);
			//decoding
			$app_store_response_map = json_decode($json_source);

			
			$app_store_response_status = $app_store_response_map->status;
			if ($app_store_response_status == 0)//eithr OK or expired and needs to synch
			{
				//here are some fields from the json, btw.
				$new_last_element = end($app_store_response_map->latest_receipt_info);
				$json_receipt = $new_last_element->expires_date_ms;
				$product_id = $new_last_element->product_id;
				$micro_date = $json_receipt;
				$date_array = $micro_date / 1000;
				$date = date("Y-m-d",$date_array);
				return array('date'=>$date,'product_id'=> $product_id,'time'=>$date_array);
				
				
				/*$transaction_id = $json_receipt->transaction_id;
				$original_transaction_id = $json_receipt->original_transaction_id;
				$json_latest_receipt = $app_store_response_map->latest_receipt_info;
				return $date;*/
			}
			else
			{
				return false;
			}
		}
	}
}