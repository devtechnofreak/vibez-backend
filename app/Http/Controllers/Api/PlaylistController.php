<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\User;
use App\Music;
use App\Playlist;
use App\Album;
use App\Video;
use App\News;
use App\PlaylistMusic;
use App\Artist;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator, DB, Hash, Mail, Auth, Notification;
use App\Notifications\PushNotification;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use Illuminate\Routing\Controller;
use Storage;
use Exception;
use \Gumlet\ImageResize;

class PlaylistController extends Controller
{
	public function getPlaylist()
	{
    	try {
			$playlists = Playlist::where('is_admin',1)->orderBy('order_id','ASC')->orderBy('created_at','DESC')->paginate(10);
	        return response()->json(['code' => 200, 'data'=> ['playlist' =>  $playlists]]);
        } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message'=>$e->getMessage()]); 
        } 		
	}

	public function getUserPlaylist()
	{
    	try {
			$playlists = Auth::user()->playlists()->with('musics')->orderBy('created_at','DESC')->paginate(20);
	        return response()->json(['code' => 200, 'data'=> ['playlist' =>  $playlists]]);
        } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message'=>$e->getMessage()]); 
        } 		
	}
	
	public function getRecentlyUpdatedPlaylist(Request $request)
	{
    	try {
			//$playlists = Auth::user()->playlists()->with('musics')->orderBy('updated_at','DESC')->paginate(20);
			$followed_playlists = Auth::user()->followings(Playlist::class)->orderBy('playlists.updated_at','DESC')->get();
			$playlist_array = array();
			for ($i=0; $i < sizeof($followed_playlists); $i++) { 
				$playlistuser_id = $followed_playlists[$i]->user_id;
				$current_id = Auth::user()->id;
				if($playlistuser_id != $current_id){
					$playlist_array[] = $followed_playlists[$i];
				}
			}
			$chunk = $followed_playlists->splice(6);
			$user_playlists = Auth::user()->playlists()->orderBy('updated_at','DESC')->get();
			$playlists = $chunk->merge($user_playlists);
			$sorted = $playlists->sortByDesc('updated_at')->slice(0, 50);
	        return response()->json(['code' => 200, 'data'=> ['followed_playlists'=>$playlist_array,'playlist' =>  $sorted->values()->all()]]);
        } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message'=>$e->getMessage()]); 
        } 		
	}
	
	public function getFavourites($type = null)
	{
    	try {
			$playlists = Auth::user()->likes(Playlist::class)->paginate(20);
			$musics = Auth::user()->likes(Music::class)->paginate(20);
			$videos = Auth::user()->likes(Video::class)->paginate(20);
			$news = Auth::user()->likes(News::class)->paginate(20);
			$data = ['playlist' =>  $playlists,'musics'=>$musics,'videos'=>$videos,'news'=>$news];
			if($type) {
				return response()->json(['code' => 200, 'data'=> [$type => $data[$type]]]);
			}
	        return response()->json(['code' => 200, 'data'=> $data]);
        } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message'=>$e->getMessage()]); 
        } 		


	}
	
	public function getSinglePlaylist(Request $request)
	{
		$rules = array('playlist_id' => 'required|integer');
		$validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		$playlist = Playlist::where('id',$request->playlist_id)->with('musics')->first();
		if(!$playlist){
			return response()->json(['code' => 401,'message'=>'Playlist does not exist!']);
		}


		$related_artist  = DB::table('playlist_musics AS A')
					->leftJoin('music_artists as B', 'A.music_id', '=', 'B.music_id')
					->leftJoin('artists as C', 'B.artist_id', '=', 'C.id')
					->select(DB::raw('group_concat(DISTINCT C.id) as id'))
					->where('A.playlist_id','=',$request->playlist_id)
					->groupBy('B.artist_id')
					->orderBy('C.name','asc')
					->get();

		$count = $related_artist->count();

		if($count > 0)
		{
			foreach ($related_artist as $key => $value) {
				if ($value->id)
					$all_ids[] = $value->id;	
			}
			$ids_ordered = implode(',', $all_ids);
			$feature_artist = Artist::whereIn('id', $all_ids)->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))->get();
			$arr = array();
			$related_artist = $feature_artist->map(function ($employee) {
				$arr = array(
					"id" => $employee['id'],
					"name" => $employee['name'],
					"thumb" => $employee['thumb'],
					"country_flag"=> $employee['country_flag'],
					"is_followed" => $employee['is_followed'],
					"is_liked" => $employee['is_liked'],
				);
			    return $arr;
			});	

			if(!empty($related_artist))
			{
				$new_related_artist = $related_artist;
			}
			else
			{
				$new_related_artist = array();
			}
		}
		else
		{
			$new_related_artist = array();
		}
		
		return response()->json(['code' => 200, 'data'=> ['feature_artist'=>$new_related_artist,'playlist' =>  $playlist]]);
		
	}

	public function getSingleAlbum(Request $request)
	{
		$rules = array('album_id' => 'required|integer');
		$validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		$album = Album::where('id',$request->album_id)->with('musics')->first();
		if(!$album){
			return response()->json(['code' => 401,'message'=>'Album does not exist!']);
		}

		$main_artist = $album->artists()->first();
		if(!is_null($main_artist)){
			$mian_artist_ar = array($main_artist->id);
		} else {
			$mian_artist_ar = array();
		}

		$feature_artist_ids = array();
		if($album->related_artist != ""){
			$feature_artist_ids = explode(',', $album->related_artist);
			$artist_ids = array_merge($mian_artist_ar, $feature_artist_ids);
			$ids_ordered = implode(',', $artist_ids);
			$feature_artist = Artist::whereIn('id', $artist_ids)->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))->get();
			$arr = array();
			$feature_artist = $feature_artist->map(function ($employee) {
				$arr = array(
					"id" => $employee['id'],
					"name" => $employee['name'],
					"thumb" => $employee['thumb'],
					"country_flag"=> $employee['country_flag'],
					"is_followed" => $employee['is_followed'],
					"is_liked" => $employee['is_liked'],
				);
			    return $arr;
			});
		} else {
			// $feature_artist  = array();
			$feature_artist  = array();
			$artist_ids = array_merge($mian_artist_ar);
			$ids_ordered = implode(',', $artist_ids);
			$feature_artist = Artist::whereIn('id', $artist_ids)->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))->get();
			$arr = array();
			$feature_artist = $feature_artist->map(function ($employee) {
				$arr = array(
					"id" => $employee['id'],
					"name" => $employee['name'],
					"thumb" => $employee['thumb'],
					"country_flag"=> $employee['country_flag'],
					"is_followed" => $employee['is_followed'],
					"is_liked" => $employee['is_liked'],
				);
			    return $arr;
			});
		}
		//$musics = $album->musics;
		
		return response()->json(['code' => 200, 'data'=> ['feature_artist' => $feature_artist,'album' =>  $album]]);
		
	}

	public function create(Request $request){
		$validation =$request->validate([
            'music_ids'=>'required|json'
        ]);
		
        if (!is_array($validation)) {
            $errors = $validation->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
        }else{
			if($request->hasfile('image')){
				$file = $request->file('image');
				$imageName = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				$thumbname = basename($imageName,'.'.$extension);
				$thumb = new ImageResize($file);
				Storage::disk('s3')->put('images/playlists/'.$imageName, file_get_contents($file->getRealPath()));
				$thumb->resizeToWidth(300);
				Storage::disk('s3')->put('images/playlists/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
				$image = 'images/playlists/'.$imageName;
			}else{
				$image ='';
			}
			
			
			$playlist = Playlist::create([
				'title' 		=> $request->input('title')?:'Untitled Playlist',
				'image'	 		=> $image?:'',
				'description'	=> $request->input('description')?:'',
				'user_id'		=> Auth::user()->id,
				'is_admin'		=> 0,
			]);
			$music_ids = json_decode($request->music_ids,true);
			$attachment = array();
			foreach ($music_ids as $key => $music_id) {
				$attachment[$music_id]['position'] = $key;
			}
			$playlist->musics()->attach($attachment);
			
			$message['title']= 'Vibez';
			$message['message']= 'Checkout this new Playlist.';
			$message['click_action']= 'vibez.playlist.new';
			$message['playlist']= $playlist->setAppends(['user_name'])->toArray();
			$users = Auth::user()->followers()->get();
			Notification::send($users, (new PushNotification($message))->onQueue('audio'));
			
			return response()->json(['code' => 200,'message'=>'Playlist created successfully!']);
		}
	}

	public function update(Request $request)
	{
		$validation =$request->validate([
            'playlist_id'=>'required|integer',
            'image'=>'image|mimes:jpeg,png,jpg,gif,svg'
        ]);	
        if (!is_array($validation)) {
            $errors = $validation->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
        }
		$playlist = Auth::user()->playlists()->where('id',$request->playlist_id)->first();	
		if(!$playlist){
			return response()->json(['code' => 200,'message'=>'Playlist does not exist!']);
		}
		if($request->hasfile('image')){
			$file = $request->file('image');
			$imageName = time().'.'.$file->getClientOriginalExtension();
			$extension = $file->getClientOriginalExtension();
			$thumbname = basename($imageName,'.'.$extension);
			$thumb = new ImageResize($file);
			Storage::disk('s3')->put('images/playlists/'.$imageName, file_get_contents($file->getRealPath()));
			$thumb->resizeToWidth(300);
			Storage::disk('s3')->put('images/playlists/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
			$image = 'images/playlists/'.$imageName;
		}else{
			$image = $playlist->getOriginal('image');
		}		
		$playlist->update([
				'title' 		=> $request->input('title')?:'Untitled Playlist',
				'image'	 		=> $image,
				'description'	=> $request->input('description')?:'',		
		]);
		
		return response()->json(['code' => 200,'message'=>'Playlist updated successfully!']);
	}

	public function addTracks(Request $request){
		$validation =$request->validate([
            'playlist_id' 	=> 'required|integer',
            'music_ids'		=>'required|json'
        ]);	
        if (!is_array($validation)) {
            $errors = $validation->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
        }	
		$playlist = Auth::user()->playlists()->where('id',$request->playlist_id)->first();	
		if(!$playlist){
			return response()->json(['code' => 200,'message'=>'Playlist does not exist!']);
		}
		$music_ids = json_decode($request->music_ids,true);
		$attachment = array();
		foreach ($music_ids as $key => $music_id) {
			$attachment[$music_id]['position'] = $key;
		}
		$playlist->musics()->attach($music_ids);
		$playlist->touch();
		$message['title'] = 'Vibez';
		$message['message'] = 'Checkout this updated Playlist.';
		$message['click_action'] = 'vibez.playlist.update';
		$message['playlist'] = $playlist->setAppends(['user_name'])->toArray();
		$users = $playlist->followers;
		Notification::send($users, (new PushNotification($message))->onQueue('audio'));			
		return response()->json(['code' => 200,'message'=>'Songs added to Playlist successfully!']);
	}
	
	public function changeOrderMusics(Request $request)
	{
		$validation =$request->validate([
            'playlist_id' 	=> 'required|integer',
            'music_ids'		=>'required|json'
        ]);	
        if (!is_array($validation)) {
            $errors = $validation->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
        }		
		$playlist = Auth::user()->playlists()->where('id',$request->playlist_id)->first();	
		if(!$playlist){
			return response()->json(['code' => 200,'message'=>'Playlist does not exist!']);
		}	
		$playlist->musics()->sync(json_decode($request->music_ids,true));
		$playlist->touch();	
		return response()->json(['code' => 200,'message'=>'Songs orders for Playlist changed successfully!']);
	}
	
	public function removeTrack(Request $request){
		$validation =$request->validate([
            'playlist_id' 	=> 'required|integer',
            'music_id'		=>'required|integer'
        ]);	
        if (!is_array($validation)) {
            $errors = $validation->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
        }		
		$playlist = Auth::user()->playlists()->where('id',$request->playlist_id)->first();
		if(!$playlist){
			return response()->json(['code' => 200,'message'=>'Playlist does not exist!']);
		}
		$playlist->musics()->detach($request->music_id);
		$playlist->touch();
		return response()->json(['code' => 200,'message'=>'Song removed from Playlist successfully!']);
	}
	
	public function delete($id){
		$playlist = Auth::user()->playlists()->where('id',$id)->first();
		if(!$playlist){
			return response()->json(['code' => 200,'message'=>'Playlist does not exist!']);
		}
		$path = pathinfo($playlist->getOriginal('image'));
		//Storage::disk('s3')->delete($playlist->getOriginal('image'));
		//Storage::disk('s3')->delete($path['dirname'].'/'.$path['filename'].'_thumb.'.$path['extension']);
		$playlist->musics()->detach();
		$playlist->delete();
		
		return response()->json(['code' => 200,'message'=>'Playlist Deleted Successfully!']);	 
	}
	
	public function likePlaylist(Request $request)
	{
        $rules = [
            'playlist_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$playlist = Playlist::find($request->playlist_id);	
		if(!$playlist){
			return response()->json(['code' => 200,'message'=>'Playlist does not exist!']);
		}
		Auth::user()->like($playlist);
		return response()->json(['code' => 200, 'message' => 'You liked this playlist successfully.']);
	}
	
	public function unlikePlaylist(Request $request)
	{
        $rules = [
            'playlist_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$playlist = Playlist::find($request->playlist_id);	
		if(!$playlist){
			return response()->json(['code' => 200,'message'=>'Playlist does not exist!']);
		}
		Auth::user()->unlike($playlist);
		return response()->json(['code' => 200, 'message' => 'You unliked this playlist successfully.']);
	}

	public function followPlaylist(Request $request)
	{
        $rules = [
            'playlist_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$playlist = Playlist::find($request->playlist_id);	
		if(!$playlist){
			return response()->json(['code' => 200,'message'=>'Playlist does not exist!']);
		}
		Auth::user()->follow($playlist);
		return response()->json(['code' => 200, 'follow_count'=>$playlist->follow_count, 'message' => 'You followed this playlist successfully.']);
	}
	
	public function unfollowPlaylist(Request $request)
	{
        $rules = [
            'playlist_id' => 'required|integer'
        ];
		
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$playlist = Playlist::find($request->playlist_id);	
		if(!$playlist){
			return response()->json(['code' => 200, 'message'=>'Playlist does not exist!']);
		}
		Auth::user()->unfollow($playlist);
		return response()->json(['code' => 200,'follow_count'=>$playlist->follow_count, 'message' => 'You unfollowed this playlist successfully.']);
	}

}
