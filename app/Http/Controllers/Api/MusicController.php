<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\User;
use App\Music;
use App\Artist;
use JWTAuth;
use App\View;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator, DB, Hash, Mail, Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use Illuminate\Routing\Controller;
use Exception;

class MusicController extends Controller
{	
	public function getMusic()
    {
    	try {
			$musicList = Music::orderBy('created_at', 'DESC')->paginate(10);
	        return response()->json(['code' => 200, 'data'=> [ 'music_list' =>  $musicList]]);
        } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message'=>$e->getMessage()]); 
        }
        
    }
	
	public function getTrendingMusic(Request $request)
	{
		
		$page = $request->input('page')?:1;
    	try {
			$collection = Music::where('is_trending',1)->OrderBy('updated_at','DESC')->get();
			$following_artists = Auth::user()->followings(Artist::class)->get()->pluck('id')->toArray();
			$musicList = $collection->sortByDesc(function($music) use($following_artists){
				return count(array_intersect($music->artists->pluck('id')->toArray(), $following_artists)); 
			});
	        return response()->json(['code' => 200, 'data'=> [ 'music_list' =>  ['data'=>$musicList->forPage($page,10)->values()]]]);
        } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message'=>$e->getMessage()]); 
        }		
	}
	
	
	public function getTrendingMusicNew(Request $request){
		$page = $request->input('page')?:0;
		$offset = 10*$page; 
		try {
			$list = DB::select('CALL GetTrendingMusic');				
			$list_size = sizeof($list);
			$music_ids = array();
			$trendingall = array();
			foreach ($list as $single_music) {
				$music_ids[] = $single_music->viewable_id;
			}
			if($list_size == 0 || $list_size < 20){
				$remaining = 20 - $list_size;
				$trendingall = DB::select('CALL GetTrendingMusicAll('.$remaining.')');
			}
			foreach ($trendingall as $single_music) {
				$music_ids[] = $single_music->viewable_id;
			}
			$ids_ordered = implode(',', $music_ids);
			if(sizeof($music_ids) > 0){
				$musics = Music::whereIn('id', $music_ids)->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))->get();	
			}else{
				$musics = array();
			}
			return response()->json(['code' => 200, 'data'=> [ 'music_list' =>  ['data'=>$musics]]]);
		 } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message'=>$e->getMessage()]); 
        }	
	}
	public function getLatestMusic()
	{
    	try {
			$musicList = Music::where('is_new',1)->OrderBy('updated_at','DESC')->paginate(10);
	        return response()->json(['code' => 200, 'data'=> [ 'music_list' =>  $musicList]]);
        } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message'=>$e->getMessage()]); 
        }		
	}
	
	
	public function getRecentlyAddedMusic()
	{
    	try {
			$musicList = Auth::user()->library()->paginate(20);
	        return response()->json(['code' => 200, 'data'=> [ 'music_list' =>  $musicList]]);
        } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message'=>$e->getMessage()]); 
        }		
	}
	
	public function getRecentlyPlayedMusic()
	{
    	try {
			$musicList = Auth::user()->recentlyPlayedMusics()->paginate(20);
	        return response()->json(['code' => 200, 'data'=> ['music_list' =>  $musicList]]);
        } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message'=>$e->getMessage()]); 
        } 		
	}
	
	public function playMusic(Request $request)
	{
        $rules = [
            'music_id' => 'required|integer'
        ];
		
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$music = Music::find($request->music_id);
		if(!$music){
			return response()->json(['code' => 200,'message'=>'Audio File does not exist!']);
		}

		//$music->addView();
		
		$view = View::updateOrCreate([
            'user_id' 		=> Auth::user()->id,
            'viewable_type'	=> 'App\Music',
            'viewable_id'	=> $request->music_id
        ],[
        	'user_id' 		=> Auth::user()->id,
            'viewable_type'	=> 'App\Music',
            'viewable_id'	=> $request->music_id,
            'count'=>DB::raw('count + 1')
        ]);
		$view->save();
		$music->unwatch();
		$music->watch();
		return response()->json(['code' => 200, 'message' => 'Music added to watch list successfully.']);
	}
			
	public function likeMusic(Request $request)
	{
        $rules = [
            'music_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$music = Music::find($request->music_id);	
		if(!$music){
			return response()->json(['code' => 200,'message'=>'Audio File does not exist!']);
		}
		Auth::user()->like($music);
		return response()->json(['code' => 200, 'message' => 'You liked this song successfully.']);
	}
	
	public function unlikeMusic(Request $request)
	{
        $rules = [
            'music_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$music = Music::find($request->music_id);
		if(!$music){
			return response()->json(['code' => 200,'message'=>'Audio File does not exist!']);
		}	
		Auth::user()->unlike($music);
		return response()->json(['code' => 200, 'message' => 'You unliked this song successfully.']);
	}
	
	public function addTracksLibrary(Request $request){
		
		$validation = $request->validate([
            'music_id'		=>'required'
        ]);	
		
        if (!is_array($validation)) {
            $errors = $validation->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
        }	
		Auth::user()->library()->detach($request->music_id);
		Auth::user()->library()->attach($request->music_id);
		
		return response()->json(['code' => 200,'message'=>'Songs added to Library successfully!']);
	}
	
	public function removeTracksLibrary(Request $request){
		
		$validation = $request->validate([
            'music_id'		=>'required'
        ]);	
		
        if (!is_array($validation)) {
            $errors = $validation->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
        }	
		
		Auth::user()->library()->detach($request->music_id);
		
		return response()->json(['code' => 200,'message'=>'Songs removed from Library successfully!']);
	}
	
	public function getLibrarySongs()
	{
		return response()->json(['code' => 200, 'data'=> [ 'library_songs' =>  ['data'	=> Auth::user()->library()->get()]]]); 
	}

}
