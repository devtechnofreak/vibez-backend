<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\User;
use App\Video;
use App\Artist;
use App\Category;
use App\LiveStream;
use JWTAuth;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator, DB, Hash, Mail, Notification;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use Illuminate\Routing\Controller;
use App\Notifications\PushNotification;
use App\Notifications\SilentPushNotification;
use Exception;

class LiveStreamController extends Controller
{
	public function addComment(Request $request)
	{
        $rules = [
            'comments' => 'required',
            'livestream_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$stream  = LiveStream::find($request->livestream_id);	
		if(!$stream){
			return response()->json(['code' => 401,'message'=>'Live Stream does not exists']);
		}		
		$stream->addComment($request->comments, Auth::user());
		
		$message['title']= 'Vibez';
		$message['message']= 'New comment added from user.';
		$message['click_action']= 'vibez.stream.comments';
		$message['livestream_id']=$stream->id;
		$message['comment']= $request->comments;
		$message['from_user']= Auth::user();
		Notification::send($stream->collectWatchers(), (new SilentPushNotification($message))->onQueue('audio'));
		
		return response()->json(['code' => 200,'comment'=>$stream->comments()->orderBy('created_at', 'desc')->first(),'total_comments'=>$stream->comments()->count(), 'message' => 'Comment has been posted successfully.']);
	}
	
	public function getLivestreamDetail(Request $request,$id){
		$stream  = LiveStream::find($id);
		$page = $request->input('page')?:1;
		if(!$stream){
			return response()->json(['code' => 401,'message'=>'Live Stream does not exists']);
		}
		$comments = $stream->comments()->orderBy('created_at','DESC')->get()->filter(function($item) {
							    return $item->author->is_blocked === false;
							 })->forPage($page, 50)->values();
		
		// return response()->json(['code' => 200, 'data'=> ['livestream' =>  $stream ,'comments'=>['data'=>$comments],'related_artist'=>$stream->artists]]);
		return response()->json(['code' => 200, 'data'=> ['livestream' =>  $stream ,'comments'=>['data'=>$comments],'related_artist'=>$stream->artists, 'related_videos' => Video::orderBy(DB::raw('RAND()'))->take(10)->get()]]);
		
	}
	
	public function playLivestream(Request $request){
        $rules = [
            'livestream_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		$stream  = LiveStream::find($request->livestream_id);
		if(!$stream){
			return response()->json(['code' => 401,'message'=>'Live Stream does not exists']);
		}	
		$stream->toggleWatch();
		$message['title']= 'Vibez';
		$message['message']= 'Total Playstream view cont.';
		$message['click_action']= 'vibez.stream.count';
		$message['livestream_id']=$stream->id;
		$message['count']= $stream->view_count;
		Notification::send($stream->collectWatchers(), (new SilentPushNotification($message))->onQueue('audio'));
		return response()->json(['code' => 200, 'message'=>'stream watch is toggled successfully']);
	}

}