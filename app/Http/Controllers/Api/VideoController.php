<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\User;
use App\Video;
use App\Artist;
use App\Category;
use App\LiveStream;
use JWTAuth;
use Auth;
use App\View;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator, DB, Hash, Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use Illuminate\Routing\Controller;
use Exception;

class VideoController extends Controller
{	
	public function getVideos(Request $request)
    {
    	$page = $request->input('page')?:1;
    	try {
			$videos = Video::orderBy('is_featured', 'DESC')
    							->orderBy('created_at', 'DESC')
								->take(10)
    							->get()
								->toArray();
			//$following_artists = Auth::user()->followings(Artist::class)->get()->pluck('id')->toArray();
			//$videos = $collection->sortByDesc(function($video) use($following_artists){
			//	return count(array_intersect($video->artists->pluck('id')->toArray(), $following_artists)); 
			//});
	        return response()->json(['code' => 200, 'data'=> ['videos' =>  ['data'=>$videos]]]);
        } catch (Exception $e) {
        	return response()->json(['code' => 401, 'message'=>$e->getMessage()]); 
        } 
    }
	
	public function getVideoDetail(Request $request,$id){
		$video  = Video::find($id);
		//echo $video->url;exit;
		 
		$page = $request->input('page')?:1;
		if(!$video){
			return response()->json(['code' => 401,'message'=>'video does not exists']);
		}
		$comments = $video->comments()->orderBy('created_at','DESC')->get()->filter(function($item) {
							    return $item->author->is_blocked === false;
							 })->forPage($page, 50)->values();
		$video->stream_url = $video->url;
		
		$video_artist_ids  = $video->artists->pluck('id')->toArray();
		$related_artist_ids = array();
		if($video->related_artist != ""){
			$related_artist_ids = explode(',', $video->related_artist);
		}
		$artist_ids = array_merge($video_artist_ids,$related_artist_ids);
		$ids_ordered = implode(',', $artist_ids);
		$feature_artist = Artist::whereIn('id', $artist_ids)->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))->get();
		$arr = array();
		$related_artist = $feature_artist->map(function ($employee) {
			$arr = array(
				"id" => $employee['id'],
				"name" => $employee['name'],
				"image" => $employee['image'],
				"thumb" => $employee['thumb'],
				"country_flag"=> $employee['country_flag'],
				"is_followed" => $employee['is_followed'],
				"is_liked" => $employee['is_liked'],
				"follow_count" => $employee['follow_count'],
				"bio" => $employee['bio'],
			);
		    return $arr;
		});
		if(!empty($related_artist))
		{
			$new_related_artist = $related_artist;
		}
		else
		{
			$new_related_artist = array();
		}

		$relatedVideos = [];
		if (!empty($artist_ids)) {
			// \DB::enableQueryLog();
			$query = \DB::select('SELECT group_concat(video_id) AS ids FROM `video_artists` WHERE `artist_id` IN ('.implode(',', $artist_ids).') AND `is_main_artist` = 1 AND video_id != '. $video->id );
			// echo '<pre>'; print_r(\DB::getQueryLog()); die;
			if ($query) {
				$relatedVideos = Video::whereIn('id' , explode(',', $query[0]->ids))->get();
				$relatedVideos = $relatedVideos->map(function($rvData) {
					$arr = array(
						"id" 			 => $rvData['id'],
						"title" 		 => $rvData['title'],
						"image" 		 => $rvData['image'],
						"thumb" 		 => $rvData['thumb'],
						"total_comments" => $rvData['total_comments'],
						"view_count" 	 => $rvData['view_count'],
						"total_likes" 	 => $rvData['total_likes'],
						"is_liked" 		 => $rvData['is_liked'],
						"artists" 		 => $rvData['artists'][0]['name']
					);
				    return $arr;
				});
			}
		}

		return response()->json(['code' => 200, 'data'=> ['video' =>  $video ,'comments'=>['data'=>$comments],'related_artist'=>$new_related_artist, 'related_videos' => $relatedVideos ]]);
	}
	
	public function playVideo(Request $request){
        $rules = [
            'video_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}
		$video  = Video::find($request->video_id);
		if(!$video){
			return response()->json(['code' => 401,'message'=>'video does not exists']);
		}	
		$view = View::updateOrCreate([
            'user_id' 		=> Auth::user()->id,
            'viewable_type'	=> 'App\Video',
            'viewable_id'	=> $request->video_id
        ],[
        	'user_id' 		=> Auth::user()->id,
            'viewable_type'	=> 'App\Video',
            'viewable_id'	=> $request->video_id,
            'count'=>DB::raw('count + 1')
        ]);
		$view->save();
		//$video->addView();	
		$video->watch();
		return response()->json(['code' => 200, 'message'=>'Video Played successfully']);
	}
		
	public function addComment(Request $request)
	{
        $rules = [
            'comments' => 'required',
            'video_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$video  = Video::find($request->video_id);	
		if(!$video){
			return response()->json(['code' => 401,'message'=>'video does not exists']);
		}		
		$video->addComment($request->comments, Auth::user());
		
		return response()->json(['code' => 200,'comment'=>$video->comments()->orderBy('created_at', 'desc')->first(), 'message' => 'Comment has been posted successfully.']);
	}
	
	public function likeVideo(Request $request)
	{
        $rules = [
            'video_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$video = Video::find($request->video_id);
		if(!$video){
			return response()->json(['code' => 401,'message'=>'video does not exists']);
		}	
		Auth::user()->like($video);
		// return response()->json(['code' => 200, 'message' => 'You liked Video successfully.']);
		$videoLikes = $video->total_likes;
		return response()->json(['code' => 200, 'message' => 'You liked Video successfully.', 'total_likes' => $videoLikes]);
	}
	
	public function unlikeVideo(Request $request)
	{
        $rules = [
            'video_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$video = Video::find($request->video_id);	
		if(!$video){
			return response()->json(['code' => 401,'message'=>'video does not exists']);
		}
		Auth::user()->unlike($video);
		// return response()->json(['code' => 200, 'message' => 'You unliked Video successfully.']);
		$videoLikes = $video->total_likes;
		return response()->json(['code' => 200, 'message' => 'You unliked Video successfully.', 'total_likes' => $videoLikes]);
	}
	
	public function getVideoCategories()
	{
		$categories = Category::orderByRaw('sort_order = 0, sort_order')->get(); 
		foreach ($categories as $category) {
			$category->append('videos_page');
		}
		$livestreams = LiveStream::where('end_time','>=',date('Y-m-d').' 00:00:00')->orderBy('start_time','DESC')->get();
		return response()->json(['code' => 200, 'data'=> ['categories' =>  $categories,'live_stream'=>$livestreams]]);
	}
	
	public function getSingleVideoCategory($id)
	{
		$category = Category::find($id);
		$category->append('videos_page');
		return response()->json(['code' => 200, 'data'=> ['category' =>  $category]]);
	}

	public function likeCategory(Request $request)
	{
        $rules = [
            'category_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$category = Category::find($request->category_id);
		if(!$category){
			return response()->json(['code' => 401,'message'=>'channel does not exists']);
		}	
		Auth::user()->like($category);
		// return response()->json(['code' => 200, 'message' => 'You liked Video successfully.']);
		$categoryLikes = $category->total_subscriber;
		return response()->json(['code' => 200, 'message' => 'You subscribed channel successfully.', 'total_subscriber' => $categoryLikes]);
	}
	
	public function unlikeCategory(Request $request)
	{
        $rules = [
            'category_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json(['code' => 401,'message'=>implode(', ', $errors->all())]);
		}	
		$category = Category::find($request->category_id);	
		if(!$category){
			return response()->json(['code' => 401,'message'=>'channel does not exists']);
		}
		Auth::user()->unlike($category);
		// return response()->json(['code' => 200, 'message' => 'You unliked Video successfully.']);
		$categoryLikes = $category->total_subscriber;
		return response()->json(['code' => 200, 'message' => 'You unsubscribed channel successfully.', 'total_subscriber' => $categoryLikes]);
	}

	public function getFeaturedVideoCategories()
	{
		$categories = Category::where('is_featured',1)->orderByRaw('sort_order = 0, sort_order')->get(); 
		foreach ($categories as $category) {
			$category->append('videos_page');
		}
		return response()->json(['code' => 200, 'data'=> ['categories' =>  $categories]]);
	}

}
