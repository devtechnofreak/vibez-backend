<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Mail;

class SupportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('support');
    }

    public function mail(Request $request)
    {
        
        $data['mail']   = 'otoide@ostech.com.ng';
        $data['name']   = $request->name;
        $data['subject']   = $request->subject;
        $data['email']   = $request->email;
        $data['message']   = $request->message;
        
        Mail::send('emails.support',['user' => $data], function ($message) use($data){
                    $message->from('otoide@ostech.com.ng','Support query');
                    $message->subject('Vibez Support Inquiry By '.$data['subject']);
                    $message->to($data['mail'], $data['name']);
        });
        return redirect()->back()->with('message','Submitted sucessfully'); 
        
    }

    public function popup_mail(Request $request)
    {
        
        $data['mail']   = 'otoide@ostech.com.ng';
        $data['name']   = $request->name;
        $data['email']   = $request->email;
        $data['phone']   = $request->phone;
        $data['message']   = $request->message;
        
        Mail::send('emails.idea',['user' => $data], function ($message) use($data){
                    $message->from($data['email'],'Vibez Idea');
                    $message->subject('Vibez app suggestion(s)');
                    $message->to($data['mail'], $data['name']);
        });

        return redirect()->back()->with('message','Submitted sucessfully'); 
        
    }
    

    
}
