<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\News;
use App\Artist;
use App\User;
use \Gumlet\ImageResize;
use DB,Notification;
use App\Notifications\PushNotification;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use App\Shortcode;

class NewsController extends Controller
{
    public function index(){
		return view('admin.news.index');
	}
	
    public function trash(){
		return view('admin.news.trash');
	}
	
	public function add(){
		$artists = Artist::all();
		return view('admin.news.add',compact('artists')); 
	}
	public function bulkTrashNewsAction(Request $request){
		$action = $request->action; 
		$news = $request->news;
		if($action == "restore"){
			if(sizeof($news) > 0){
				foreach ($news as $single_news) {
					$news = News::onlyTrashed()
                			->where('id', $single_news)->first();
					$news->restore();
					$news->banners()->restore();
					$news->comments()->restore();
				}
			}
		}
		if($action == "delete"){
			if(sizeof($news) > 0){
				foreach ($news as $single_news) {
					$news = News::onlyTrashed()
                			->where('id', $single_news)->first();
					
					if($news->getOriginal('image')):
						$path = pathinfo($news->getOriginal('image'));
						
						Storage::disk('s3')->delete($news->getOriginal('image'));
						Storage::disk('s3')->delete($path['dirname'].'/'.$path['filename'].'_thumb.'.$path['extension']);
					endif;
					$news->artists()->detach();
					$news->banners()->forceDelete();
					$news->comments()->forceDelete();
					$news->forceDelete();
				}
			}
		}
		return array("status"	=> 1);
	}
	/*public function sendPushNotification(Request $request){
		$news_id = $request->news_id;

		if(isset($news_id) && $news_id != ""){
			$news = News::find($news_id);

			$news_pay_arr = array(
				'id'		=> $news->id,
				'title'		=> $news->title,
				'image'		=> $news->db_image,
				'thumb'		=> $news->db_thumb_image,
				'view_link' => $news->view_link,
				'total_comments'=> $news->total_comments,
			);
			
			if(!empty($news->artists->toarray()))
			{

				$user_array = array();
				foreach ($news->artists as $artist) {

					
					$users = $artist->followers->pluck('id')->toArray();
					$user_array = array_merge($user_array,$users);
				}

				$user_array = array_unique($user_array);
				//$users = $artist->followers->pluck('id')->toArray();
				if(is_array($users) && sizeof($users) > 0){
					$selectedUser = User::whereNotIn('id', $user_array)->get();
				}else{
					$selectedUser = User::all();
				}
				//description logic 
				$description = substr(strip_tags($news->description),0,80).'...';
				$news_descptrion = implode(' ', array_slice(str_word_count(($description ? $description : '-'), 2), 0, 8)).'...';
				//description logic end
				$unicodeChar = '\uD83D\uDC4B';
				$new_uni_code = json_decode('"'.$unicodeChar.'"');
				$message = array();
				$message['title'] = 'Vibez';
				$message['message'] = $new_uni_code." Vibez Update: '".$news->title."'";
				$message['description'] = $news_descptrion;
				$message['click_action'] = 'vibez.news.new';
				$message['news'] = $news_pay_arr;
				Notification::send($selectedUser, (new PushNotification($message))->onQueue('audio')); 
				return redirect()->back()->with('message','Pushnotification sent successfully!');
			}
			else
			{
				$description = substr(strip_tags($news->description),0,80).'...';
				$news_descptrion = implode(' ', array_slice(str_word_count(($description ? $description : '-'), 2), 0, 8)).'...';
				$selectedUser = User::all();
				$unicodeChar = '\uD83D\uDC4B';
				$new_uni_code = json_decode('"'.$unicodeChar.'"');
				$message = array();
				$message['title'] = 'Vibez';
				$message['message'] = $new_uni_code." Vibez Update: '".$news->title."'";
				$message['description'] = $news_descptrion;
				$message['click_action'] = 'vibez.news.new';
				$message['news'] = $news_pay_arr;
				Notification::send($selectedUser, (new PushNotification($message))->onQueue('audio')); 
				return redirect()->back()->with('message','Pushnotification sent successfully!');
			}
			

			
		}

	}*/

	public function sendPushNotification(Request $request){
		$unicodeChar = '\uD83D\uDC4B';
		$new_uni_code = json_decode('"'.$unicodeChar.'"');
		$news_id = News::find($request->news_id);
		$notificationtype = $request->notificationtype;
		if($notificationtype == 'custom')
		{
			$news_title = $request->news_title;
			//description logic 
			$description = substr(strip_tags($request->news_description),0,80).'...';
			$news_descptrion = implode(' ', array_slice(str_word_count(($description ? $description : '-'), 2), 0, 8)).'...';
			//description logic end
		}
		else
		{
			$news_title = $new_uni_code." Vibez Update: ".$news_id->title;
			
			//description logic 
			$description = substr(strip_tags($news_id->description),0,80).'...';
			$news_descptrion = implode(' ', array_slice(str_word_count(($description ? $description : '-'), 2), 0, 8)).'...';
			//description logic end

		}
		
		
		if(isset($news_id) && $news_id != ""){
			$news = News::find($request->news_id);
			
			$news_pay_arr = array(
				'id'		=> $news->id,
				'title'		=> $news->title,
				'image'		=> $news->db_image,
				'thumb'		=> $news->db_thumb_image,
				'view_link' => $news->view_link,
				'total_comments'=> $news->total_comments,
			);
			
			if(!empty($news->artists->toarray()))
			{
				$user_array = array();
				foreach ($news->artists as $artist) {
					$users = $artist->followers->pluck('id')->toArray();
					$user_array = array_merge($user_array,$users);
				}

				$user_array = array_unique($user_array);
				//$users = $artist->followers->pluck('id')->toArray();
				if(is_array($users) && sizeof($users) > 0){
					$selectedUser = User::whereNotIn('id', $user_array)->get();
				}else{
					$selectedUser = User::all();
				}

				//$selectedUser = User::whereIn('id', ['480','9','366'])->get();
				
				$message = array();
				$message['title'] = 'Vibez';
				$message['message'] = $news_title;
				$message['description'] = $news_descptrion;
				$message['click_action'] = 'vibez.news.new';
				$message['news'] = $news_pay_arr;
				Notification::send($selectedUser, (new PushNotification($message))->onQueue('audio')); 
				return redirect()->back()->with('message','Pushnotification sent successfully!');
			}
			else
			{
				$selectedUser = User::all();
				$message = array();
				$message['title'] = 'Vibez';
				$message['message'] = $news_title;
				$message['description'] = $news_descptrion;
				$message['click_action'] = 'vibez.news.new';
				$message['news'] = $news_pay_arr;
				Notification::send($selectedUser, (new PushNotification($message))->onQueue('audio')); 
				return redirect()->back()->with('message','Pushnotification sent successfully!');
			}
		}
	}

	
	public function bulkNewsAction(Request $request){
		$action = $request->action; 
		$news = $request->news;
		if($action == "trash"){
			if(sizeof($news) > 0){
				foreach ($news as $single_news) {
					$news = News::find($single_news);
					$news->banners()->delete();
					$news->comments()->delete();
					$news->delete();
				}
			}
		}
		if($action == "delete"){
			if(sizeof($news) > 0){
				foreach ($news as $single_news) {
					$news = News::find($single_news);
					if($news->getOriginal('image')):
						$path = pathinfo($news->getOriginal('image'));
						
						Storage::disk('s3')->delete($news->getOriginal('image'));
						Storage::disk('s3')->delete($path['dirname'].'/'.$path['filename'].'_thumb.'.$path['extension']);
					endif;
					$news->artists()->detach();
					$news->banners()->forceDelete();
					$news->comments()->forceDelete();
					$news->forceDelete();
				}
			}
		}
		return array("status"	=> 1);
	}
	public function getNews(Request $request) {
		$search = $request->input( 'search' );
		 $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
         $order = $request->input ( 'order' );
		
		$column = 'created_at';
		$dir = 'DESC';
		if(is_array($order) && sizeof($order) > 0){
			if(isset($order[0]['column']) && isset($order[0]['dir'])){
				switch ($order[0]['column']) {
				    case "2":
				        $column = "title";
						$dir = $order[0]['dir'];
				        break;
				    default:
				        $column = "created_at";
						$dir = "DESC";
				}
			}
		}
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
            $news = News::select('news.title','news.image','news.id','news.created_at')->join('artist_news', 'artist_news.news_id', '=', 'news.id')->join('artists', 'artist_news.artist_id', '=', 'artists.id')->where('artist_news.is_main_artist', '=', "1")->where(function ($query) use ($searchTerm) {
									$query->where('title', 'like', "%".$searchTerm."%")
									;
								})
								->orderBy($column,$dir)
								->skip($start)->take($length)
								->get();
			$allnews = News::select('title','image','id','created_at')->where(function ($query) use ($searchTerm) {
									$query->where('title', 'like', "%".$searchTerm."%")
									;
								})
								->count();
           
        }else{
			$news = News::select('title','image','id','created_at')->orderBy($column,$dir)->skip($start)->take($length)->get();
			$allnews = News::select('title','image','id','created_at')->count();
		}
		
		$totalData = $allnews;            //Total record
        $totalFiltered = $news->count();      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
		
		$data = array ();
		
        foreach ( $news as $single_news ) {
			
            $nestedData = array ();
			$nestedData ['title'] = $single_news->title;
            $nestedData ['image'] = $single_news->thumb;
			$nestedData ['artists'] = $single_news->artistNames();
			$nestedData['created_at']= time_elapsed_string($single_news->created_at);
			$nestedData ['id'] = $single_news->id; 
			$data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalData ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;

	}

	public function getTrashedNews(Request $request) {
		$search = $request->input( 'search' );
		$start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
            $news = News::onlyTrashed()->select('title','image','id','created_at')->where(function ($query) use ($searchTerm) {
									$query->where('title', 'like', "%".$searchTerm."%")
									;
								})
								->orderBy('created_at', 'DESC')
								->skip($start)->take($length)
								->get();
			$allnews = News::onlyTrashed()->select('title','image','id','created_at')->where(function ($query) use ($searchTerm) {
									$query->where('title', 'like', "%".$searchTerm."%")
									;
								})
								->count();
           
        }else{
			$news = News::onlyTrashed()->select('title','image','id','created_at')->orderBy('created_at', 'DESC')->skip($start)->take($length)->get();
			$allnews = News::onlyTrashed()->select('title','image','id','created_at')->count();
		}
		
		$totalData = $allnews;            //Total record
        $totalFiltered = $news->count();      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        
		
		$data = array ();
		
        foreach ( $news as $single_news ) {
			
            $nestedData = array ();
			$nestedData ['title'] = $single_news->title;
            $nestedData ['image'] = $single_news->thumb;
			$nestedData ['artists'] = collect($single_news->artists)->pluck('name')->all();
			$nestedData['created_at']= time_elapsed_string($single_news->created_at);
			$nestedData ['id'] = $single_news->id; 
			$data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalData ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;

	}
	
	public function create(Request $request){
		$validation =$request->validate([
            'title' 	=> 'required|string|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'description' => 'required'
        ]);
		
        if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{

        	if($request->hasfile('image')){
				$file = $request->file('image');
				$imageName = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				$thumbname = basename($imageName,'.'.$extension);
				$thumb = new ImageResize($file); 
				Storage::disk('s3')->put('news/'.$imageName, file_get_contents($file->getRealPath()));
				$thumb->resizeToWidth(400);
				Storage::disk('s3')->put('news/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
				$image = 'news/'.$imageName;
			}
			$date = date_create($request->input('created_at'));

			$related_artist = "";	
			if($request->featured_artist_id && $request->featured_artist_id != ""){
				$related_artist = implode(',', $request->featured_artist_id);

			}

			$news = News::create([
				'title'			=> $request->title,
				'image'			=> $image?:'',
				'description'   => $request->input('description')?:'',
				'is_featured'   => $request->input('is_featured')?:0,
				'is_pushed'   => $request->input('is_pushed')?:0,
				'created_at'	=> date_format($date, 'Y-m-d H:i:s'),
				'related_artist'=> $related_artist
			]);


			if($request->featured_artist_id && $request->featured_artist_id != ""){
				foreach ($request->featured_artist_id as $key => $value) {
						$ins_array[] = 
							array(
								'news_id' 	=> $news->id, 
								'artist_id' =>$value,
								'is_main_artist'=>'0',
								'created_at'=>date('Y-m-d H:i:s'),
								'updated_at'=>date('Y-m-d H:i:s')
							);
					}
					DB::table('artist_news')->insert($ins_array);
			}	

			if($request->short_code_id[0] != '' && $request->short_code_id[0]!=NULL)
			{
				$sh_code = News::find($news->id);
				$my_codes = trim($request->short_code_id[0],',');
				//$my_codes = trim($request->short_code_id[0],',');
				$sh_code->short_codes = $my_codes;
				$sh_code->save();
			}
			else
			{
				$sh_code = News::find($news->id);
				$my_codes = '';
				$sh_code->short_codes = $my_codes;
				$sh_code->save();

			}


			//description logic 
				$description = substr(strip_tags($news->description),0,80).'...';
				$news_descptrion = implode(' ', array_slice(str_word_count(($description ? $description : '-'), 2), 0, 8)).'...';
			//description logic end


			$news_pay_arr = array(
				'id'		=> $news->id,
				'title'		=> $news->title,
				'image'		=> $news->db_image,
				'thumb'		=> $news->db_thumb_image,
				'view_link' => $news->view_link,
				'total_comments'=> $news->total_comments,
			);

			if(isset($request->artist_id)){
				$news->artists()->sync($request->artist_id);
			
				$message['title'] = 'Vibez';
				$message['message'] = "Vibez Update: '".$request->title."'";
				$message['description'] = $news_descptrion;
				$message['click_action'] = 'vibez.news.new';
				$message['news'] = $news_pay_arr;
				
				$users = collect([]);
				foreach ($news->artists as $artist) {
					$users = $users->merge($artist->followers);	
				}
				$users = $users->unique();
				if(!$request->is_pushed) {
					Notification::send($users, (new PushNotification($message))->onQueue('audio'));
				}
				
			}
			
			Cache::flush();
			return redirect()->back()->with('message', 'News created successfully!');
		}
	}


	public function newsimages(){

		/*$this->validate(request(), [
		    'upload' => 'mimes:jpeg,jpg,gif,png'
		]);

	    $file = request()->file('upload');*/
	    $file_name = $_FILES['upload']['name'];
      	$file_size =$_FILES['upload']['size'];
      	$file_tmp =$_FILES['upload']['tmp_name'];
      	$file_type=$_FILES['upload']['type'];
	    
	    $file_ext = explode(".",$file_name);
	    $imageName = time();
		$extension = $file_ext[1];
		$thumbname = basename($file_tmp,'.'.$extension);
		$thumb = new ImageResize($file_tmp);
		$imageName = $imageName.'.'.$extension;
		Storage::disk('s3')->put('news/'.$imageName, file_get_contents($file_tmp));
		$thumb->resizeToWidth(400);
		Storage::disk('s3')->put('news/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
		$image = 'https://s3.amazonaws.com/vibezapp/news/'.$imageName;

		return "<script>window.parent.CKEDITOR.tools.callFunction(1,'{$image}','')</script>";

	}
	
	public function edit($id){
			$news = News::find($id);
			$artists = Artist::all();
			if($news->short_codes != '') {
				$array = explode(',', $news->short_codes);
				foreach (array_reverse($array) as $key => $value) {
					$all_ids[] = $value;	
				}
				$ids_ordered = implode(',', $all_ids);
				$feature_artist = Shortcode::whereIn('id', $all_ids)->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))->get();
				$arr = array();
				$related_artist = $feature_artist->map(function ($employee) {
					$arr = array(
						"id" => $employee['id'],
						"type" => $employee['type'],
						"text" => $employee['text'],
						"code"=> $employee['code'],
					);
				    return (array) $arr;
				});	

				if(!empty($related_artist))
				{
					$short_codes = $related_artist;
				}
				else
				{
					$short_codes = '';
				}
			}
			else
			{
				$short_codes = '';
			}
			//$short_codes = Shortcode::all()->sortByDesc("created_at")->toArray();
			return view('admin.news.edit',compact('news','artists','short_codes'));
	}

	public function update(Request $request){
		$validation =$request->validate([
			'news_id' 	=> 'required|integer',
	        'title' 	=> 'required|string|max:255',
	        'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
	    ]);
	
	    if (!is_array($validation)) {
	        return redirect()->back()->withErrors($validation->errors());
	    }else{
	    	$news = News::find($request->news_id);
			
			if($request->hasfile('image')){
				$file = $request->file('image');
				$imageName = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				$thumbname = basename($imageName,'.'.$extension);
				$thumb = new ImageResize($file);
				Storage::disk('s3')->put('news/'.$imageName, file_get_contents($file->getRealPath()));
				$thumb->resizeToWidth(300);
				Storage::disk('s3')->put('news/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
				$image = 'news/'.$imageName;
			} else {
				$image = $news->getOriginal('image');
			}
			$date = date_create($request->input('created_at'));
			DB::table('artist_news')->where('news_id', '=', $request->news_id)->delete();
			if(isset($request->artist_id)){
				$news->artists()->sync($request->artist_id);
			}
			$related_artist = "";	
			if($request->featured_artist_id && $request->featured_artist_id != ""){
				$related_artist = implode(',', $request->featured_artist_id);
				foreach ($request->featured_artist_id as $key => $value) {
					$ins_array[] = 
						array(
							'news_id' 	=> $request->news_id, 
							'artist_id' =>$value,
							'is_main_artist'=>'0',
							'created_at'=>date('Y-m-d H:i:s'),
							'updated_at'=>date('Y-m-d H:i:s')
						);
				}
				DB::table('artist_news')->insert($ins_array);
			
			}
			$news_data = array(
				'title'	=> $request->title,
				'image'	=> $image,
				'description' => $request->input('description')?:'',
				'is_featured' => $request->input('is_featured')?:0,
				'created_at'	=> date_format($date, 'Y-m-d H:i:s'),
				'related_artist'=> $related_artist
			);
			$news->update($news_data);

			if($request->short_code_id[0] != '' && $request->short_code_id[0]!=NULL)
			{
				$sh_code = News::find($news->id);
				$my_codes = trim($request->short_code_id[0],',');
				$sh_code->short_codes = $my_codes;
				$sh_code->save();
			}
			else
			{
				$sh_code = News::find($news->id);
				$my_codes = '';
				$sh_code->short_codes = $my_codes;
				$sh_code->save();

			}
			Cache::flush();
		}		
		
		return redirect()->back()->with('message', 'News updated successfully!'); 
	}


	public function delete($id){
		$news = News::find($id);
		$news->banners()->delete();
		$news->comments()->delete();
		$news->delete();
		
		return redirect()->back()->with('message', 'News has been moved to trash successfully!');  
	}

	public function deletePermanentNewsData($id)
	{
		$news = News::onlyTrashed()
                ->where('id', $id)->first();
		if($news->getOriginal('image')):
			$path = pathinfo($news->getOriginal('image'));
			
			Storage::disk('s3')->delete($news->getOriginal('image'));
			Storage::disk('s3')->delete($path['dirname'].'/'.$path['filename'].'_thumb.'.$path['extension']);
		endif;
		$news->artists()->detach();
		$news->banners()->forceDelete();
		$news->comments()->forceDelete();
		$news->forceDelete();
		return redirect()->back()->with('message','News has been deleted permanently.');
	}
	
	public function restoreNews($id)
	{
		$news = News::onlyTrashed()
                ->where('id', $id)->first();
		$news->restore();
		$news->banners()->restore();
		$news->comments()->restore();
		return redirect()->action('Admin\NewsController@index')->with('message', 'News and related data restored Successfully!');
	}
	
	public function regenerateThumbnail()
	{
		$data = News::paginate(20);
		foreach ($data as $news) {
				$image = $news->getOriginal('image');
				$pathinfo = pathinfo($news->getOriginal('image'));
				$thumb = ImageResize::createFromString(file_get_contents($news->image));
				$thumb->resizeToWidth(300);
				Storage::disk('s3')->put('news/'.$pathinfo['filename'].'_thumb.'.$pathinfo['extension'], $thumb->getImageAsString());			
		}

		return response()->json(['message'=>'Image has been generated successfully!','data'=>$data]);
	}

}
