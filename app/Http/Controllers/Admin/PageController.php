<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Page;
use App\User;
use App\Artist;
use \Gumlet\ImageResize;
use Illuminate\Support\Facades\Storage;
use DB, Notification;
use App\Notifications\PushNotification;

class PageController extends Controller
{
    public function index(){
		return view('admin.page.index');
	}	
	
	public function add(){
		
		return view('admin.page.add'); 
	}
	
	public function edit($id){
		$page = Page::find($id);
		return view('admin.page.edit',compact('page'));
	}
	
	public function create(Request $request){
		$validation = $request->validate([
			'title'	=>'required',
            'content'=>'required',
            'slug'=>'required',
        ]);
		
        if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }	
		
		$page = Page::create([
			'title'			=> $request->title,
			'content' 		=> $request->content,
			'slug'			=> $request->slug,
		]);
			
		return redirect()->back()->with('message', 'Page added successfully!');
	}	

	public function update(Request $request){
		$validation =$request->validate([
			'title'	=>'required',
            'content'=>'required',
			'page_id' => 'required|integer',
        ]);
		
		if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }
    	$page = Page::find($request->page_id);

		$page_data = array(
			'title'			=> $request->title,
			'content' 		=> $request->content,
		);
		
		$page->update($page_data);
			
		return redirect()->back()->with('message', 'Page updated successfully!'); 
	}

	public function getPages(Request $request){
		$search = $request->input( 'search' );
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
 			$pages = Page::where('title','like',"%".$searchTerm."%")->get();
           
        }else{
				$pages = Page::get();
		}
		
		$totalData = $pages->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
		
		$data = array ();
        foreach ( $pages as $page ) {
			
            $nestedData = array ();
			$nestedData ['title'] = $page->title;
			$nestedData ['slug'] = $page->slug;
			$nestedData ['created_at'] = date('d/m/Y',strtotime($page->created_at));
			$nestedData ['id'] = $page->id; 
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}

	public function delete($id)
	{
		$page = Page::find($id);
		
		return redirect()->back()->with('message', 'Live Stream removed successfully!');
	}		
}
	