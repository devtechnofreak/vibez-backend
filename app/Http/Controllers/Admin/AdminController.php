<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Music;
use App\Video;
use App\News;
use App\Playlist;
use App\Artist;
use App\Album;
use App\User;
use App\UserHelp;
use DB; 
use Auth;
class AdminController extends Controller
{
    public function index(){
    	$total_music = Music::count();
		$total_video = Video::count();
		$total_news = News::count();
		$total_playlist = Playlist::where('user_id',1)->count();
		$total_artist = Artist::count();
		$total_album = Album::count();
		$total_user = User::where('role_id', 2)->count();
		
		$user_help = UserHelp::select('user_help.*','users.name as name','users.email as email','users.image as image')
										->join("users","users.id","=","user_help.user_id")
										->orderBy('id', 'DESC')->limit(5)->get();
		$latest_album = Album::where('album_status',1)->orderBy('id', 'DESC')->limit(5)->get();
		$latest_videos = Video::orderBy('id', 'DESC')->limit(5)->get();
		$latest_musics = Music::orderBy('id', 'DESC')->limit(5)->get();
		$latest_artist = DB::select("CALL GetArtistByFollowCount(20)");

		$trialPlanAllUser = $this->getCountByPlanSecond('trial',0);
		$trialPlanActiveUser = $this->getCountByPlanSecond('trial',1);
		$trialPlanExipredUser = $this->getCountByPlanSecond('trial',2);

		$monthPlanAllUser = $this->getCountByPlanSecond('month',0);
		$monthPlanActiveUser = $this->getCountByPlanSecond('month',1);
		$monthPlanExpiredUser = $this->getCountByPlanSecond('month',2);


		$yearPlanAllUser  = $this->getCountByPlanSecond('year',0);
		$yearPlanActiveUser = $this->getCountByPlanSecond('year',1);
		$yearPlanExpiredUser = $this->getCountByPlanSecond('year',2);


		$total_active_user = $this->getCountByPlanThird(1);
		$total_expired_user = $this->getCountByPlanThird(2);



		$data = array(
			'total_music'		=>	$total_music,
			'total_video'		=>	$total_video,
			'total_news'		=>	$total_news,
			'total_playlist'	=>	$total_playlist,
			'total_artist'		=>	$total_artist,
			'total_album'		=>	$total_album,
			'total_user'		=>	$total_user,
			'user_help'			=>  $user_help,
			'latest_videos'		=>  $latest_videos,
			'latest_album'		=>  $latest_album,
			'latest_musics'		=>  $latest_musics,
			'latest_artists'	=>  $latest_artist,

			'total_active_user'		=>	$total_active_user,
			'total_expired_user'	=>	$total_expired_user,


			'trialPlanAllUser'  	=> $trialPlanAllUser,		
			'trialPlanActiveUser'  	=> $trialPlanActiveUser,		
			'trialPlanExipredUser'  => $trialPlanExipredUser,		
			
			'monthPlanAllUser'  	=> $monthPlanAllUser,		
			'monthPlanActiveUser'  	=> $monthPlanActiveUser,		
			'monthPlanExpiredUser'  => $monthPlanExpiredUser,		
			
			'yearPlanAllUser'   	=> $yearPlanAllUser,
			'yearPlanActiveUser'   	=> $yearPlanActiveUser,
			'yearPlanExpiredUser'	=> $yearPlanExpiredUser,
		);
		return view('admin/index',['data' => $data]);
	}

	public function getCountByPlan($plan = '') {

		return DB::table('users as usr')
                ->join('user_subscriptions as usr_sub', 'usr.id', '=', 'usr_sub.user_id')
                ->select(array('usr.id', 'usr.name', 'usr.email', 'usr_sub.plan'))
                ->where('usr.role_id','=','2')
                ->where('usr_sub.plan', 'like', '%'.$plan.'%')
                ->whereNull('usr.deleted_at')
                ->whereDate('usr_sub.end_date', '>=', date('Y-m-d')) 
                // ->when($isDateFilterApply, function ($query) use ($date_from) {
                //     return $query->whereDate('B.created_at', '>=', $date_from);
                // })
                ->count();

	}

	public function getCountByPlanSecond($plan = '',$flag) {
		if($flag == '0')
		{
			$isFilterApply = false;	
		}	
		else
		{
			$isFilterApply = true;	
		}
		
		return DB::table('users as usr')
                ->join('user_subscriptions as usr_sub', 'usr.id', '=', 'usr_sub.user_id')
                ->select(array('usr.id', 'usr.name', 'usr.email', 'usr_sub.plan'))
                ->where('usr.role_id','=','2')
                ->where('usr_sub.plan', 'like', '%'.$plan.'%')
                ->whereNull('usr.deleted_at')
                ->when($isFilterApply, function ($query) use ($flag) {
                    if($flag == '1')
                	{
                		return $query->where('usr_sub.end_date', '>=', date('Y-m-d'));
                	}
                	else if($flag == '2')
                	{
                		return $query->where('usr_sub.end_date', '<', date('Y-m-d'));
                	}
                })
                ->count();
	}


	public function getCountByPlanThird($flag) {
		$isFilterApply = true;
		return DB::table('users as usr')
                ->join('user_subscriptions as usr_sub', 'usr.id', '=', 'usr_sub.user_id')
                ->select(array('usr.id', 'usr.name', 'usr.email', 'usr_sub.plan'))
                ->where('usr.role_id','=','2')
                ->whereNull('usr.deleted_at')
                ->when($isFilterApply, function ($query) use ($flag) {
                    if($flag == '1')
                	{
                		return $query->where('usr_sub.end_date', '>=', date('Y-m-d'));
                	}
                	else if($flag == '2')
                	{
                		return $query->where('usr_sub.end_date', '<', date('Y-m-d'));
                	}
                })
                ->count();
	}
}
