<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use App\Artist;
use App\Album;
use App\Tag;
use App\User;
use App\Video;
use App\VideoTag;
use App\VideoArtist;
use App\Playlist;


use DB,Notification;
use App\Notifications\PushNotification;
use Illuminate\Support\Facades\Log;

class NotificationController extends Controller
{
	/*public function sendPushNotification_News(Request $request){

		$news_id = $request->news_id;

		if(isset($news_id) && $news_id != "")
		{
			$news = News::find($news_id);
			$news_arr= $news->toArray();
			
			$old_news_image = $news_arr['image'];
			$old_news_thumb = $news_arr['thumb'];

			$news_arr['image'] = strtok($old_news_image,'?');
			$news_arr['thumb'] = strtok($old_news_thumb,'?');
			
			$description = substr(strip_tags($news->description),0,100).'...';
			
			$news_descptrion = implode(' ', array_slice(str_word_count(($description ? $description : '-'), 2), 0, 9)).'...';
			
			$selectedUser = User::whereIn('id', ['15','480'])->get();

			$news_pay_arr = array(
				'id'		=> $news->id,
				'title'		=> $news->title,
				'image'		=> $news->db_image,
				'thumb'		=> $news->db_thumb_image,
				'view_link' => $news->view_link,
				'total_comments'=> $news->total_comments,
			);

			$message = array();
			$message['title'] = 'Vibez';
			$message['message'] = "Vibez Update: '".$news->title."'";
			$message['description'] = $news_descptrion;
			$message['click_action'] = 'vibez.news.new';
			$message['news'] = $news_pay_arr;
			Notification::send($selectedUser, (new PushNotification($message))->onQueue('audio'));
			return redirect()->back()->with('message','Pushnotification sent successfully!');
			
		}
		
	}*/

	public function sendPushNotification_News(Request $request){
		
		$unicodeChar = '\uD83D\uDC4B';
		$new_uni_code = json_decode('"'.$unicodeChar.'"');
		$news_id = News::find($request->news_id_test);
		$notificationtype = $request->notificationtypetest;
		if($notificationtype == 'custom')
		{
			$news_title = $request->news_title_test;
			//description logic 
			$description = substr(strip_tags($request->news_description_test),0,80).'...';
			$news_descptrion = implode(' ', array_slice(str_word_count(($description ? $description : '-'), 2), 0, 8)).'...';
			//description logic end
		}
		else
		{
			$news_title = $new_uni_code." Vibez Update: ".$news_id->title;
			
			//description logic 
			$description = substr(strip_tags($news_id->description),0,80).'...';
			$news_descptrion = implode(' ', array_slice(str_word_count(($description ? $description : '-'), 2), 0, 8)).'...';
			//description logic end

		}
		
		
		if(isset($news_id) && $news_id != ""){
			$news = News::find($request->news_id_test);
			
			$news_pay_arr = array(
				'id'		=> $news->id,
				'title'		=> $news->title,
				'image'		=> $news->db_image,
				'thumb'		=> $news->db_thumb_image,
				'view_link' => $news->view_link,
				'total_comments'=> $news->total_comments,
			);
			
			if(!empty($news->artists->toarray()))
			{
				$user_array = array();
				foreach ($news->artists as $artist) {
					$users = $artist->followers->pluck('id')->toArray();
					$user_array = array_merge($user_array,$users);
				}

				$user_array = array_unique($user_array);
				//$users = $artist->followers->pluck('id')->toArray();
				if(is_array($users) && sizeof($users) > 0){
					//$selectedUser = User::whereNotIn('id', $user_array)->get();
					$selectedUser = User::whereIn('id', ['15','480'])->get();
				}else{
					//$selectedUser = User::all();
					$selectedUser = User::whereIn('id', ['15','480'])->get();
				}
				
				$message = array();
				$message['title'] = 'Vibez';
				$message['message'] = $news_title;
				$message['description'] = $news_descptrion;
				$message['click_action'] = 'vibez.news.new';
				$message['news'] = $news_pay_arr;
				Notification::send($selectedUser, (new PushNotification($message))->onQueue('audio')); 
				return redirect()->back()->with('message','Pushnotification sent successfully!');
			}
			else
			{
				//$selectedUser = User::all();
				$selectedUser = User::whereIn('id', ['15','480'])->get();
				$message = array();
				$message['title'] = 'Vibez';
				$message['message'] = $news_title;
				$message['description'] = $news_descptrion;
				$message['click_action'] = 'vibez.news.new';
				$message['news'] = $news_pay_arr;
				Notification::send($selectedUser, (new PushNotification($message))->onQueue('audio')); 
				return redirect()->back()->with('message','Pushnotification sent successfully!');
			}
		}
	}	


	public function sendPushNotification_Album(Request $request){

		$album_id = $request->album_id;
		if(isset($album_id) && $album_id != ""){
			$album = Album::find($album_id);
			$artist = $album->artists()->first();

			
			
			/*$users = $artist->followers->pluck('id')->toArray();
			if(is_array($users) && sizeof($users) > 0){
				$selectedUser = User::whereNotIn('id', $users)->get();
			}else{
				$selectedUser = User::all();
			}*/

			$album_pay_arr = array(
				'id'			=> $album->id,
				'title'			=> $album->title,
				'image'		  	=> $album->db_image,
				'description' 	=> $album->description,
				'user_name'		=> $album->user_name,
				'thumb'			=> $album->db_thumb_image,
			);
			
			$selectedUser = User::whereIn('id', ['363'])->get();
			$message = array();
			$message['title'] = 'Vibez';
			$message['message'] = "Music Update: ".$artist->name.' - '.$album->title;
			$message['click_action'] = 'vibez.album.new';
			$message['album'] = $album_pay_arr;

			/*$old_album_image = $message['album']['image'];
			$message['album']['image'] = strtok($old_album_image,'?');
			$message['album'] = $message['album'];*/
			Notification::send($selectedUser, (new PushNotification($message))->onQueue('audio'));
			return redirect()->back()->with('message','Pushnotification sent successfully!'); 
		}
		
	}


	public function sendPushNotification_Video(Request $request){
		$video_id = $request->video_id;
		$video = Video::find($video_id);
		$artist = $video->artists()->first();
		$notificationtype = $request->notificationtypetest;
		if($notificationtype == 'custom'){
			$video_title = $request->video_title_test;
		}
		else{
			$video_title = 'Video Update: '.$artist->name.' - '.$video->title;
		}

		if(isset($video_id) && $video_id != ""){
			
			
			/*$users = $video->followers->pluck('id')->toArray();*/

			/*if(is_array($users) && sizeof($users) > 0){
				$selectedUser = User::whereNotIn('id', $users)->get();
			}else{
				$selectedUser = User::all();
			}*/
			$video_pay_arr = array(
				'id'			=> $video->id,
				'title'			=> $video->title,
				'url'			=> $video->db_url,
				'image'			=> $video->db_image,
				'description'	=> $video->description,
				'thumb'			=> $video->db_thumb_image,
				'stream_url'	=> $video->stream_url,
			);

			//$selectedUser = User::all();
			$selectedUser = User::whereIn('id', ['366','8156','480'])->get();
			$message = array();
			$message['title'] = 'Vibez';
			$message['message'] = $video_title;
			$message['click_action'] = 'vibez.video.new'; 
			$message['video'] = $video_pay_arr;
			Notification::send($selectedUser, (new PushNotification($message))->onQueue('audio'));
			return redirect()->back()->with('message','Pushnotification sent successfully!');
		}
		
	}


	public function sendPushNotification_Playlist(Request $request){

		$playlist_id = $request->playlist_id;
		if(isset($playlist_id) && $playlist_id != ""){
			$playlist = Playlist::find($playlist_id);
			//$selectedUser = User::all();
			$selectedUser = User::whereIn('id', ['366'])->get();

			$playlist_pay_arr = array(
				'id'			=> $playlist->id,
				'user_id'		=> $playlist->user_id,
				'title'			=> $playlist->title,
				'image'			=> $playlist->db_image,		 
				'description' 	=> $playlist->description,
				'thumb'			=> $playlist->db_thumb_image,
				'user_name' 	=> $playlist->user_name,
			);
			$message = array();
			$message['title'] = 'Vibez';
			$message['message'] = 'Playlist updated: '.$playlist->title;
			$message['click_action'] = 'vibez.playlist.update'; 
			$message['playlist'] = $playlist_pay_arr;
			Notification::send($selectedUser, (new PushNotification($message))->onQueue('audio'));
			return redirect()->back()->with('message','Pushnotification sent successfully!');
		}
		
	}
}

?>