<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Announcement;
use DB,Notification;
use App\User; 
use App\Notifications\PushNotification;
class AnnouncementController extends Controller
{
	public function create(Request $request){
		$validation =$request->validate([
            'title' 	=> 'required|string|max:255',
            'image' 	=> 'image|mimes:jpeg,png,jpg,gif,svg',
            'content' => 'required'
        ]);
		if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
			if($request->hasfile('image')){
				$file = $request->file('image');
				//$fileName = $file->getClientOriginalName();
				$fileName = time().'.'.$file->getClientOriginalExtension(); 
				$file->move(public_path('import/music'), $fileName);
				$image = $fileName;
				$message['image'] = url('/')."/import/music/".$image;
			}
			else
			{
				$message['image'] = "";
			}
			
			$news = Announcement::create([
				'title'			=> $request->title,
				'image'			=> isset($image)?:'',
				'content'   => $request->input('content')?:''
			]);
			
			$message['title'] = $request->title;
			$message['message'] = $request->content;
			$message['image'] = $message['image'];
			$message['click_action'] = 'vibez.announcement.new';
			
			$users = User::all();
			//$users = User::whereIn('id', ['9','366'])->get();
			//$users = $users->unique();
			Notification::send($users, (new PushNotification($message))->onQueue('audio')); 
			return redirect()->back()->with('message', 'Announcement created successfully!');
		}
	}

	public function bulkAnnouncementAction(Request $request){
		$action = $request->action;
		$announcements = $request->announcement;
		if($action == "delete"){
			if(sizeof($announcements) > 0){
				foreach ($announcements as $single_announcement) {
					Announcement::where('id', $single_announcement)->delete();
				}
			}
		}
		return array("status"	=> 1); 
	}
	
	public function getaannouncement(Request $request){ 
		$search = $request->input( 'search' );
		$start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' ); 
        $order = $request->input ( 'order' );
        $column = 'id';
		$dir = 'DESC';
		if(is_array($order) && sizeof($order) > 0){
			if(isset($order[0]['column']) && isset($order[0]['dir'])){
				switch ($order[0]['column']) {
				    case "2":
				        $column = "title";
						$dir = $order[0]['dir'];
				        break;
				    default:
				        $column = "id";
						$dir = "DESC";
				}
			}
		}
		if (isset($search['value']) && $search['value'] != "") { 
			
			$searchTerm = $search['value'];
            $announcements = Announcement::select('title','content','image','id','created_at')->where(function ($query) use ($searchTerm) {
															$query->where('title', 'like', "%".$searchTerm."%");
														})
												->orderBy($column, $dir)
												->skip($start)->take($length)
												 ->get();
			 $allannouncement = Announcement::select('title','content','image','id','created_at')->where(function ($query) use ($searchTerm) {
															$query->where('title', 'like', "%".$searchTerm."%");
														})
												->orderBy('id', 'DESC')
												->count();
           
        }else{
			$announcements = Announcement::select('title','content','image','id','created_at')->orderBy($column, $dir)->skip($start)->take($length)->get();
			$allannouncement = Announcement::select('title','content','image','id','created_at')->count();
		}
		
        // Here are the parameters sent from client for paging 
          //  Get length record from start
		
		$data = array ();
        foreach ( $announcements as $announcement ) {
            $nestedData = array ();
            $nestedData ['title'] = $announcement->title;
			$nestedData ['content']= $announcement->content;
            $nestedData ['image'] = ($announcement->image != "") ? url('/')."/import/music/".$announcement->image: "";
			$nestedData ['created_at'] = $announcement->created_at->format('d / m / Y');
			$nestedData ['id'] = $announcement->id; 
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $allannouncement ), // total number of records
                "recordsFiltered" => intval ( $allannouncement ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}
	
	public function add(){
		return view('admin.announcement.add'); 
	}
	
    public function index(){
		return view('admin.announcement.index');
	}
}