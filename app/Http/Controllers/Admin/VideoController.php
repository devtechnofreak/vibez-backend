<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Filesystem\Filesystem;
use Pbmedia\LaravelFFMpeg\FFMpegFacade as FFMpeg;
use App\Video;
use App\Artist;
use App\Category;
use App\User;
use App\Tag;
use App\VideoTag;
use App\VideoArtist;
use \Gumlet\ImageResize;
use App\Jobs\VideoUpload;
use App\Jobs\ConvertVideoForStreaming;
use App\Jobs\CreateNewVideo;
use App\Jobs\CreateNewVideoNew;
use App\Jobs\VideoUploadDone;
//use App\Jobs\SendPushNotification;
use DB,Notification;
use App\Notifications\PushNotification;
use Storage;
use File;
use Illuminate\Support\Facades\Log;

class VideoController extends Controller
{
    public function index(){
		return view('admin.videos.index');
	}
	
    public function trash(){
		return view('admin.videos.trash');
	}	
	
	public function add(){
		$artists = Artist::all();
		$tags = Tag::all();
		$categories = Category::all();
		return view('admin.videos.add',compact('artists', 'tags', 'categories')); 
	}
	
	public function getVideos(Request $request) {
		$search = $request->input( 'search' );
		 $start = $request->input ( 'start' );  
        $length = $request->input ( 'length' ); 
		$order = $request->input ( 'order' );
		
		$column = 'id';
		$dir = 'DESC';
		if(is_array($order) && sizeof($order) > 0){
			if(isset($order[0]['column']) && isset($order[0]['dir'])){
				switch ($order[0]['column']) {
				    case "2":
				        $column = "title";
						$dir = $order[0]['dir'];
				        break;
				    case "5":
				        $column = "date_released";
						$dir = $order[0]['dir'];
				        break;
				    case "6":
				        $column = "duration";
						$dir = $order[0]['dir'];
				        break;	
				    default:
				        $column = "date_released";
						$dir = "DESC";
				}
			}
		}
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
            $videos = Video::select('videos.title','videos.image','videos.date_released','videos.duration','videos.id','videos.category_id')->join('video_artists', 'video_artists.video_id', '=', 'videos.id')->join('artists', 'video_artists.artist_id', '=', 'artists.id')->where('video_artists.is_main_artist', '=', "1")->where(function ($query) use ($searchTerm) {
															$query->where('videos.title', 'like', "%".$searchTerm."%")->orWhere('artists.name', 'like', "%".$searchTerm."%");
														})
								->orderBy($column, $dir)
								->skip($start)->take($length)
								->get();
           $allVideos = Video::select('title','image','date_released','duration','id','category_id')->where(function ($query) use ($searchTerm) {
															$query->where('title', 'like', "%".$searchTerm."%");
														})
								->count();
        }else{
        	$videos = Video::select('title','image','date_released','duration','id','category_id')->orderBy($column, $dir)->skip($start)->take($length)->orderBy('date_released', 'DESC')->get(); 
			$allVideos = Video::select('title','image','date_released','duration','id','category_id')->count();
		}
		
		$totalData = $allVideos;            //Total record
        $totalFiltered = $videos->count();      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
         //  Get length record from start
		
		$data = array ();
        foreach ( $videos as $video ) {

            $nestedData = array ();
			$nestedData ['title'] = $video->title;
			$nestedData ['tags']= collect($video->tags)->pluck('name')->all();
			$nestedData ['artists']= collect($video->artists)->pluck('name')->all();
			$nestedData ['related_artists'] = collect($video->related_artist)->pluck('name')->all();
            $nestedData ['image'] = $video->thumb;
            $nestedData ['category'] = $video->category;
			$nestedData ['date_released'] = date('d / m / Y',strtotime($video->date_released));
			$nestedData ['duration'] = $video->duration;
			$nestedData ['id'] = $video->id; 
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalData ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;

	}

	public function bulkTrashVideoAction(Request $request){
		$action = $request->action; 
		$videos = $request->videos;
		if($action == "restore"){
			if(sizeof($videos) > 0){
				foreach ($videos as $single_video) {
					$video = Video::onlyTrashed()
	                ->where('id', $single_video)->first();
					
					$video->restore();
					$video->banners()->restore();
					$video->comments()->restore();		
				}
			}
		}
		if($action == "delete"){
			if(sizeof($videos) > 0){
				foreach ($videos as $single_video) {
					$video = Video::onlyTrashed()
	                ->where('id', $single_video)->first(); 
					$imageName = $video->getOriginal('image');
					$filePath = pathinfo($imageName);
					//Storage::disk('s3')->delete($imageName);
					//Storage::disk('s3')->delete($filePath['dirname'].'/'.$filePath['filename'].'_thumb.'.$filePath['extension']);
					Storage::disk('s3')->delete($video->getOriginal('url'));
					$video->banners()->forceDelete();
					$video->comments()->forceDelete();
					$video->artists()->detach();
					$video->tags()->detach();	
					$video->forceDelete();
					
				}
			}
		}
		return array("status"	=> 1);
	}

	public function sendPushNotification(Request $request){
		$video_id = $request->video_id;
		$video = Video::find($video_id);
		$artist = $video->artists()->first();
		$notificationtype = $request->notificationtype;
		if($notificationtype == 'custom'){
			$video_title = $request->video_title;
		}
		else{
			$video_title = 'Video Update: '.$artist->name.' - '.$video->title;
		}

		if(isset($video_id) && $video_id != ""){
			/*$users = $video->followers->pluck('id')->toArray();*/

			/*if(is_array($users) && sizeof($users) > 0){
				$selectedUser = User::whereNotIn('id', $users)->get();
			}else{
				$selectedUser = User::all();
			}*/
			$video_pay_arr = array(
				'id'			=> $video->id,
				'title'			=> $video->title,
				'url'			=> $video->db_url,
				'image'			=> $video->db_image,
				'description'	=> $video->description,
				'thumb'			=> $video->db_thumb_image,
				'stream_url'	=> $video->stream_url,
			);

			$selectedUser = User::all();
			//$selectedUser = User::whereIn('id', ['480','9','366'])->get();
			$message = array();
			$message['title'] = 'Vibez';
			$message['message'] = $video_title;
			$message['click_action'] = 'vibez.video.new'; 
			$message['video'] = $video_pay_arr;
			Notification::send($selectedUser, (new PushNotification($message))->onQueue('audio'));
			return redirect()->back()->with('message','Pushnotification sent successfully!');
		}
	}

	public function sendPushNotificationForTest(Request $request){
			

		$video_id = $request->video_id;

		if(isset($video_id) && $video_id != ""){
			$video = Video::find($video_id);
			$artist = $video->artists()->first();
			/*$users = $video->followers->pluck('id')->toArray();*/

			/*if(is_array($users) && sizeof($users) > 0){
				$selectedUser = User::whereNotIn('id', $users)->get();
			}else{
				$selectedUser = User::all();
			}*/
			$video_pay_arr = array(
				'id'			=> $video->id,
				'title'			=> $video->title,
				'url'			=> $video->db_url,
				'image'			=> $video->db_image,
				'description'	=> $video->description,
				'thumb'			=> $video->db_thumb_image,
				'stream_url'	=> $video->stream_url,
			);
			
			$selectedUser = User::where('id', '=', '9')->get();
			$message = array();
			$message['title'] = 'Vibez';
			$message['message'] = 'Video Update: '.$artist->name.' - '.$video->title;
			$message['click_action'] = 'vibez.video.new'; 
			$message['video'] = $video_pay_arr;
			Notification::send($selectedUser, (new PushNotification($message))->onQueue('audio'));
			return redirect()->back()->with('message','Pushnotification sent successfully!');
		}
	}



	
	public function bulkVideoAction(Request $request){
		$action = $request->action;
		$videos = $request->videos;
		
		if($action == "trash"){
			if(sizeof($videos) > 0){
				foreach ($videos as $single_video) {
					$video = Video::find($single_video);
					$video->delete();
					$video->banners()->delete();	
					$video->comments()->delete();
				}
			}
		}
		if($action == "delete"){
			if(sizeof($videos) > 0){
				foreach ($videos as $single_video) {
					$video = Video::find($single_video); 
					$imageName = $video->getOriginal('image');
					$filePath = pathinfo($imageName);
					//Storage::disk('s3')->delete($imageName);
					//Storage::disk('s3')->delete($filePath['dirname'].'/'.$filePath['filename'].'_thumb.'.$filePath['extension']);
					Storage::disk('s3')->delete($video->getOriginal('url'));
					$video->banners()->forceDelete();
					$video->comments()->forceDelete();
					$video->artists()->detach();
					$video->tags()->detach();	
					$video->forceDelete();
				}
			}
		}
		return array("status"	=> 1);
	}

	public function getTrashVideos(Request $request) {
		$search = $request->input( 'search' );
		$start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
            $videos = Video::onlyTrashed()->select('title','image','date_released','duration','id','category_id')->where(function ($query) use ($searchTerm) {
															$query->where('title', 'like', "%".$searchTerm."%");
														})
								->orderBy('date_released', 'DESC')
								->skip($start)->take($length)
								->get();
			 $allVideos = Video::onlyTrashed()->select('title','image','date_released','duration','id','category_id')->where(function ($query) use ($searchTerm) {
															$query->where('title', 'like', "%".$searchTerm."%");
														})
								->count();
           
        }else{
			$videos = Video::onlyTrashed()->select('title','image','date_released','duration','id','category_id')->orderBy('date_released', 'DESC')->skip($start)->take($length)->get();
			$allVideos = Video::onlyTrashed()->select('title','image','date_released','duration','id','category_id')->orderBy('date_released', 'DESC')->count(); 
		}
		
		$totalData = $allVideos;            //Total record
        $totalFiltered = $videos->count();        // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        
		
		$data = array ();
        foreach ( $videos as $video ) {
            $nestedData = array ();
			$nestedData ['title'] = $video->title;
			$nestedData ['tags']= collect($video->tags)->pluck('name')->all();
            $nestedData ['image'] = $video->thumb;
            $nestedData ['category'] = $video->category;
			$nestedData ['date_released'] = date('d / m / Y',strtotime($video->date_released));
			$nestedData ['duration'] = $video->duration;
			$nestedData ['id'] = $video->id; 
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalData ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;

	}
	
	public function create(Request $request){
		
		$validation =$request->validate([
            'title' 	=> 'required|string|max:255',
            'image' => 'required_without:select_generated|image|mimes:jpeg,png,jpg,gif,svg',
            'date_released'	=> 'required',
            'video' => 'required|file|mimetypes:video/mp4,video/mpeg,video/x-matroska',
            'artist_id' => 'required|array',
            'category_id' => 'required'
        ]);
		
		if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
        	if($request->select_generated === true && $request->filename){
        		
        	}
        	$folder = substr($request->title,0,1);	
			FFMpeg::cleanupTemporaryFiles();
			$file = $request->file('video');
			$fileName = time().'.'.$file->getClientOriginalExtension();
			$filepath = 'videos/'.strtolower($folder).'/';
			if($request->select_generated === true && $request->filename){
        		$fileName = $request->filename.'.'.$file->getClientOriginalExtension();
        	} else {
        		$file->move(public_path('import/music'), $fileName);
        	}
			
			$ffprobe = \FFMpeg\FFProbe::create();
			$duration = $ffprobe->format(public_path('import/music').'/'.$fileName)->get('duration');
			 
			 	
			if($request->hasfile('image')) {
				$image = $request->file('image');
				$imageName = $image->getClientOriginalName();
				$extension = $image->getClientOriginalExtension();
				$thumbname = basename($imageName,'.'.$extension);
				
				$thumb = new ImageResize($image);
				Storage::disk('s3')->put('videos/'.strtolower($folder).'/'.$imageName, file_get_contents($image->getRealPath()));
				$thumb->resizeToWidth(400);
				Storage::disk('s3')->put('videos/'.strtolower($folder).'/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
				$imageName = 'videos/'.strtolower($folder).'/'.$imageName;
			} else {
				$imageName ='';
			}
			if($request->select_generated === true && $request->filename){
        		$imageName = $request->filename.'.jpg';
				$thumb = new ImageResize(public_path('import/music/'.$imageName));
				Storage::disk('s3')->put('videos/'.strtolower($folder).'/'.$imageName, file_get_contents(public_path('import/music/'.$imageName)));
				$thumb->resizeToWidth(400);
				Storage::disk('s3')->put('videos/'.strtolower($folder).'/'.$request->filename.'_thumb.jpg', $thumb->getImageAsString());
				$imageName = 'videos/'.strtolower($folder).'/'.$imageName;
        	} 
			$video_tags = array_filter(explode(',',$request->tag_id));
			$tagIds = array();
			foreach ($video_tags as $tagname) {
				$tagname = trim($tagname); 
				$tag = Tag::firstOrCreate([
							'name'	=> $tagname,
							'status'=> 1
						]);
				$tagIds[]= $tag->tag_id?:$tag->id;
			}
			$data = array(
					'title' 		=> $request->title,
					'image'	 		=> $imageName,
					'url'			=> $filepath.$fileName,
					'description'	=>	$request->input('description')?:'',
					'date_released'	=> date('Y-m-d',strtotime($request->date_released)),
					'duration'		=> $duration,
					'category_id'	=> $request->category_id,			
					'artist_id'		=> $request->artist_id,
					'tag_id'		=> $tagIds,
					'is_featured'   => $request->input('is_featured')?:0
					);
			
			Log::debug('An informational message.');
			VideoUpload::withChain([
			   // new ConvertVideoForStreaming($filepath,$fileName),
			   (new CreateNewVideoNew($data))->onQueue('audio')->delay(100)
			   //new CreateNewVideoNew($data),
			   //new SendPushNotification()
			])->dispatch($filepath,$fileName)->allOnQueue('audio')->delay(5);
			
			
			return redirect()->back()->with('message', 'Video created successfully, Sit back and relax while video is being uploaded!');
		}
	}

	public function generateThumbnail(Request $request)
	{
		$validation =$request->validate([
            'video' => 'required|file|mimetypes:video/mp4,video/mpeg,video/x-matroska',
        ]);	
		if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }	
		$file = $request->file('video');
		$time = time();
		$fileName = $time.'.'.$file->getClientOriginalExtension();
		$file->move(public_path('import/music'), $fileName);
		try{	
		$image = FFMpeg::fromDisk('uploads')
						    ->open($fileName)
						   	->getFrameFromSeconds(1)
						    ->export()
						    ->toDisk('uploads')
						    ->save($time.'.jpg');
		} catch (\Exception $e){
			return response()->json(['message'=> $e->getMessage()]);
		}
		$videpath = public_path('import/music/'.$fileName);
		$newpath = public_path('import/music/'.$time.'.mp4');
		$watermark = public_path('import/music/watermark.png');
		//$videoWatermark = "";
		//exec('/usr/bin/ffmpeg -i '.$videpath.' -i '.$watermark.' -filter_complex "[1:v]scale=-1:120*(1/1)[wm];[0:v][wm]overlay=W-w-100:100" '.$newpath.'');
		//sleep(5);
		//Storage::disk('uploads')->delete($filename); 
		return response()->json(['image'=>'import/music'.'/'.$time.'.jpg','filename'=>$time]);
	}
	
	public function generateThumbMultiple(Request $request){
		$filename =$request->filename;
		$time = time(); 
		try{	
		$image = FFMpeg::fromDisk('uploads')
						    ->open($filename)
						   	->getFrameFromSeconds(1)
						    ->export()
						    ->toDisk('uploads')
						    ->save($time.'.jpg');
		} catch (\Exception $e){
			return response()->json(['message'=> $e->getMessage()]); 
		}
			/*$videpath = public_path('import/music/'.$filename);
			$newpath = public_path('import/music/'.$time.'.mp4'); 
			$watermark = public_path('import/music/watermark.png');
			$videoWatermark = "";
			//exec('/usr/bin/ffmpeg -i '.$videpath.' -i '.$watermark.' -filter_complex "[1:v]scale=-1:120*(1/1)[wm];[0:v][wm]overlay=W-w-100:100" '.$newpath.'');
			 * 
			 */
			//Storage::disk('uploads')->delete($filename); 
		return response()->json(['image'=>'import/music'.'/'.$time.'.jpg','filename'=>$time,'new_video'=>$time.'.mp4']); 
	}
	
	public function setThumbnail($id)
	{
		$video = Video::find($id);	
		$time = time();
		$folder = substr($video->title,0,1);
		try{
					$image = FFMpeg::fromDisk('s3')
						    ->open($video->getOriginal('url'))
						   	->getFrameFromSeconds(1)
						    ->export()
						    ->toDisk('uploads')
						    ->save($time.'.jpg');
		} catch (\Exception $e){
			return redirect()->back()->with('message', $e->getMessage());
		}
							
		$imageName = $time.'.jpg';
		if(!file_exists(public_path('import/music/'.$imageName)))
		{
			return redirect()->back()->withErrors(['Thumbnail could not be generated.']);
		}	
		$thumb = new ImageResize(public_path('import/music/'.$imageName));
		Storage::disk('s3')->put('videos/'.strtolower($folder).'/'.$imageName, file_get_contents(public_path('import/music/'.$imageName)));
		$thumb->resizeToWidth(400); 
		Storage::disk('s3')->put('videos/'.strtolower($folder).'/'.$time.'_thumb.jpg', $thumb->getImageAsString());
		$imageName = 'videos/'.strtolower($folder).'/'.$imageName;
		$video->update(['image'=>$imageName]);
		return redirect()->back()->with('message', 'Thumbnail generated successfully');
	}
	
	public function edit($id){
		$video = Video::find($id);
		$artists = Artist::all();
		$tags = Tag::all();
		$categories = Category::all();
		return view('admin.videos.edit',compact('video','categories','artists','tags'));
	}
	
	public function update(Request $request){
		$validation =$request->validate([
            'title' 	=> 'required|string|max:255',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'date_released'	=> 'required',
            'artist_id' => 'required|array',
            'category_id' => 'required',
            'video_id'	=>'required'
        ]);	
		if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
        	$video = Video::find($request->video_id);
        	$folder = substr($request->title,0,1);	
			$path = pathinfo($video->getOriginal('url'));
			FFMpeg::cleanupTemporaryFiles();
			//$ffprobe = \FFMpeg\FFProbe::create();
			//$duration = $ffprobe->format(public_path('import/music').'/1531575387.mp4')->get('duration');
			//ConvertVideoForStreaming::dispatch($path['dirname'].'/',$path['basename'])->onQueue('audio');
			if($request->hasfile('image')) {
				$image = $request->file('image');
				$imageName = $image->getClientOriginalName();
				$extension = $image->getClientOriginalExtension();
				$thumbname = basename($imageName,'.'.$extension);
				
				$thumb = new ImageResize($image);
				Storage::disk('s3')->put('videos/'.strtolower($folder).'/'.$imageName, file_get_contents($image->getRealPath()));
				$thumb->resizeToWidth(400);
				Storage::disk('s3')->put('videos/'.strtolower($folder).'/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
				$imageName = 'videos/'.strtolower($folder).'/'.$imageName;
			} else {
				$imageName = $video->getOriginal('image');
			}
		}
		DB::table('video_artists')->where('video_id', '=', $request->video_id)->delete();
		$video->artists()->sync($request->artist_id);
		$related_artist = "";	
		if($request->featured_artist_id && $request->featured_artist_id != ""){
			$related_artist = implode(',', $request->featured_artist_id);
			foreach ($request->featured_artist_id as $key => $value) {
				$ins_array[] = 
					array(
						'video_id' => $request->video_id, 
						'artist_id' =>$value,
						'is_main_artist'=>'0',
						'created_at'=>date('Y-m-d H:i:s'),
						'updated_at'=>date('Y-m-d H:i:s')
					);
			}
			DB::table('video_artists')->insert($ins_array);
		
		}


		$videos_data = array(
			'title'			=> $request->title,
			'image'			=> $imageName,
			'description'	=> $request->input('description')?:'',
			'date_released'	=> date('Y-m-d',strtotime($request->date_released)),
			'category_id'	=> $request->category_id,
			'is_featured'   => $request->input('is_featured')?:0,
			'related_artist'=> $related_artist
			//'duration'		=>$duration
		);
		$video_tags = array_filter(explode(',',$request->tag_id));
		$tagIds = array();
		foreach ($video_tags as $tagname) {
			$tagname = trim($tagname); 
			$tag = Tag::firstOrCreate([
						'name'	=> $tagname,
						'status'=> 1
					]);
			$tagIds[]= $tag->tag_id?:$tag->id;
		}		
		$video->update($videos_data);
		$video->tags()->sync($tagIds);	
		return redirect()->back()->with('message', 'Video Updated Successfully!'); 
	}


	public function delete($id){
		
		$video = Video::find($id);
		$video->delete();
		$video->banners()->delete();	
		$video->comments()->delete();
		return redirect()->back()->with('message', 'Video has been moved to trash successfully!');  
	}
	
	public function restoreVideo($id)
	{
		$video = Video::onlyTrashed()
                ->where('id', $id)->first();
		
		$video->restore();
		$video->banners()->restore();
		$video->comments()->restore();

		return redirect()->action('Admin\VideoController@index')->with('message', 'Playlist and related data restored successfully!');
	}	
	
	public function deletePermanentVideoData($id){
		
		$video = Video::onlyTrashed()
                ->where('id', $id)->first();
		$imageName = $video->getOriginal('image');
		$filePath = pathinfo($imageName);
		//Storage::disk('s3')->delete($imageName);
		//Storage::disk('s3')->delete($filePath['dirname'].'/'.$filePath['filename'].'_thumb.'.$filePath['extension']);
		Storage::disk('s3')->delete($video->getOriginal('url'));
		$video->banners()->forceDelete();
		$video->comments()->forceDelete();
		$video->artists()->detach();
		$video->tags()->detach();	
		$video->forceDelete();
		
			
		return redirect()->back()->with('message', 'Video deleted permanently!');  
	}
	
	public function viewBulkImport()
	{
		$artists = Artist::all();
		$tags = Tag::all();
		$categories = Category::all();
		return view('admin.videos.import',compact('artists', 'tags', 'categories'));  
	}
	
	public function postBulkImport(Request $request)
	{
		$validation =$request->validate([
            'video' => 'required|array',
            'video.*.title' => 'required',
            'video.*.artist_id' => 'required|array',
            'video.*.category_id' => 'required',
            'video.*.date_released' => 'required',

        ]);
		if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
			foreach ($request->video as $key => $video) {
				if(file_exists(public_path('import/music/'.$video['filename']))){
					$folder = substr($video['title'],0,1);	
					FFMpeg::cleanupTemporaryFiles();
					$ffprobe = \FFMpeg\FFProbe::create();
					$duration = $ffprobe->format(public_path('import/music').'/'.$video['filename'])->get('duration');
					$filepath = 'videos/'.strtolower($folder).'/';	
					if(isset($video['image'])) {
						$image = $video['image'];
						$imageName = $image->getClientOriginalName();
						$extension = $image->getClientOriginalExtension();
						$thumbname = basename($imageName,'.'.$extension);
						$thumb = new ImageResize($image);
						Storage::disk('s3')->put('videos/'.strtolower($folder).'/'.$imageName, file_get_contents($image->getRealPath()));
						$thumb->resizeToWidth(400);
						Storage::disk('s3')->put('videos/'.strtolower($folder).'/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
						$imageName = 'videos/'.strtolower($folder).'/'.$imageName;
						$currentImage = $image->getRealPath();
					} else {
						if($video['generated_thumbnail']){
			        		$imageName = $video['generated_thumbnail'].'.jpg';
							$thumb = new ImageResize(public_path('import/music/'.$imageName));
							Storage::disk('s3')->put('videos/'.strtolower($folder).'/'.$imageName, file_get_contents(public_path('import/music/'.$imageName)));
							$thumb->resizeToWidth(400);
							Storage::disk('s3')->put('videos/'.strtolower($folder).'/'.$video['generated_thumbnail'].'_thumb.jpg', $thumb->getImageAsString());
							$imageName = 'videos/'.strtolower($folder).'/'.$imageName; 
							$currentImage = public_path('import/music/'.$imageName);
			        	}
					}
					
					if (array_key_exists("tags",$video))
					{
						$video_tags = array_filter(explode(',',$video['tags']));
						$tagIds = array();
						foreach ($video_tags as $tagname) {
							$tagname = trim($tagname); 
							$tag = Tag::firstOrCreate([
										'name'	=> $tagname,
										'status'=> 1
									]);
							$tagIds[]= $tag->tag_id?:$tag->id;
						}	
					}
					else
					{
						$tagIds = "";
					}

					$related_artist = "";
					if (array_key_exists("featured_artist_id",$video))
					{
						if($video['featured_artist_id'] && $video['featured_artist_id'] != ""){
							$related_artist = implode(',', $video['featured_artist_id']);
						}		
					}

					$data = array(
							'title' 			=> $video['title'],
							'image'	 			=> $imageName,
							'url'				=> $filepath.$video['filename'],
							'description'		=> $video['description']?:'',
							'date_released'		=> date('Y-m-d',strtotime($video['date_released'])),
							'duration'			=> $duration,
							'category_id'		=> $video['category_id'],			
							'artist_id'			=> $video['artist_id'],
							'related_artist'	=> $related_artist,
							'tag_id'			=> $tagIds,
							'is_featured'   	=> isset($video['is_featured'])?:0
							);

					Log::debug('An informational message 686.');
					VideoUpload::withChain([
					   // new ConvertVideoForStreaming($filepath,$fileName),
						(new CreateNewVideoNew($data))->onQueue('audio')->delay(10)
					    //new SendPushNotification($filepath,$video['filename'])
					])->dispatch($filepath,$video['filename'])->allOnQueue('audio')->delay(5);	 
				}
				
			}
			return redirect()->back()->with('message', 'Video created successfully, Sit back and relax while video is being uploaded!');
		}
		return redirect()->back()->with('message', 'Videos imported successfully!');
	}
	
	public function importVideoFiles(Request $request){
		if($request->hasfile('file')) {
			
			$file = $request->file('file');
			$time = time();
			//$fileName = $file->getClientOriginalName();
			$fileName = time().'.'.$file->getClientOriginalExtension();
			$file->move(public_path('import/music'), $fileName);
			
			return response()->json(['status'=>true,'filename'	=>$fileName]);
		}
		return response()->json(['status'=>true]);
		
		
	}
	
	public function deleteVideoFiles(Request $request)
	{
   		try{
			Storage::disk('uploads')->delete($request->filename);
		}catch(\Exception $e){
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		
		return response()->json(['status'=>true]);
	}
}
