<?php 
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Filesystem\Filesystem;
use DB,Notification;
use App\Notifications\PushNotification;
use Illuminate\Support\Facades\Log;
use App\Shortcode;

class ShortcodeController extends Controller
{
	public function create(Request $request){
		
		$type = $request->sh_type;
		if($type == 'instagram')
		{
			$text_nw = str_replace('<script async src="//www.instagram.com/embed.js"></script>','<script async src="https://www.instagram.com/embed.js"></script>',$request->sh_text);
		}
		else
		{
			$text_nw = $request->sh_text;
		}
		$text = base64_encode($text_nw);
		if($type == 'twitter')
		{
			$code = 'vbz_sh_tw';
		}
		elseif($type == 'instagram')
		{
			$code = 'vbz_sh_ig';
		}
		else
		{
			return 'Code not generated';

		}

		$shortcode_id = Shortcode::insertGetId([
									'type'	  		=> $type,
									'text'			=> $text,
									'created_at'	=> date('Y-m-d H:i:s'),
								]);  
		
		$findcode = Shortcode::find($shortcode_id);
		$code = $code.'_'.$shortcode_id;
		$findcode->code = $code;
		$findcode->save();
		return json_encode($findcode);
        

	}

	public function delete(Request $request){
		
		$delete_id = $request->delete_id;
		$findcode = Shortcode::find($delete_id);
		$findcode->delete();
		return json_encode($findcode);
        

	}
}
?>