<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Artist;
use App\Tag;
use App\ArtistTags;
use DB;
use App\Scopes\ActiveScope;
use Illuminate\Support\Facades\Storage;
use \Gumlet\ImageResize;

class ArtistController extends Controller
{
    public function index(){
		return view('admin.artists.index');
	}
	
    public function trash(){
		return view('admin.artists.trash')->withErrors(array('Removing Artist will remove all artist related data like albums, songs, videos etc'));
	}
	
	public function add(){
		$countries = DB::table('countries')->get()->toArray();
		return view('admin.artists.add',["countries"=>$countries]); 
	}
	
	public function view($id){
		$artist = Artist::withoutGlobalScope(ActiveScope::class)->find($id);
		$albums = $artist->albums()->orderBy('updated_at','DESC')->paginate(5);
		return view('admin.artists.view',compact('artist','albums'));	
	}
	public function viewBulkImport(){
		$countries = DB::table('countries')->get()->toArray();
		return view('admin.artists.import', compact('countries')); 
	}
	
	public function bulkTrashArtistAction(Request $request){
		$action = $request->action;
		$artists = $request->artists;
		
		if($action == "restore"){
			if(sizeof($artists) > 0){
				foreach ($artists as $single_artist) {
					$artist = Artist::withoutGlobalScope(ActiveScope::class)->onlyTrashed()
				                ->where('id', $single_artist)->first();
					$artist->restore();
					
					foreach ($artist->musics()->withTrashed()->get() as $music) {
						$music->restore();
						$music->banners()->restore();
					}
					
					foreach ($artist->videos()->withTrashed()->get() as $video) {
						$video->restore();
					}
					
					foreach ($artist->albums()->withTrashed()->get() as $album) {
						$album->restore();
					}
					
					foreach ($artist->news()->withTrashed()->get() as $news) {
						$news->restore();
					}
					
					$artist->banners()->restore();
				}
			}
			
		}
		if($action == "delete"){
			if(sizeof($artists) > 0){
				foreach ($artists as $single_artist) {
					$artist = Artist::withoutGlobalScope(ActiveScope::class)->onlyTrashed()
                			->where('id', $single_artist)->first();
					$artist->tags()->detach();
					$artist->videos()->forceDelete();
					foreach ($artist->musics as $music) {
						$music->banners()->forceDelete();
					}
					$artist->musics()->forceDelete();
					$artist->albums()->forceDelete();
					$artist->news()->forceDelete();
					$artist->banners()->forceDelete();
					$artist->forceDelete();
				}
			}
		}
		
	}
	
	public function bulkArtistAction(Request $request){
		$action = $request->action;
		$artists = $request->artists;
		
		if($action == "trash"){
			if(sizeof($artists) > 0){
				foreach ($artists as $single_artist) {
					$artist = Artist::withoutGlobalScope(ActiveScope::class)->find($single_artist);
		
					foreach ($artist->musics as $music) {
						$music->delete();
						$music->banners()->delete();
					}
					
					foreach ($artist->videos as $video) {
						$video->delete();
					}
					
					foreach ($artist->albums as $album) {
						$album->delete();
					}
					
					foreach ($artist->news as $news) {
						$news->delete();
					}
					
					$artist->banners()->delete();
					
					$artist->delete();
				}
			}
		}
		if($action == "delete"){
			if(sizeof($artists) > 0){
				foreach ($artists as $single_artist) {
					$artist = Artist::withoutGlobalScope(ActiveScope::class)->find($single_artist);
					$artist->tags()->detach();
					$artist->videos()->forceDelete();
					foreach ($artist->musics as $music) {
						$music->banners()->forceDelete();
					}
					$artist->musics()->forceDelete();
					$artist->albums()->forceDelete();
					$artist->news()->forceDelete();
					$artist->banners()->forceDelete();
					$artist->forceDelete();
				}
			}
		}
		return array("status"	=> 1);
	}
	
	public function storeBulkImport(Request $request)
	{
		if($request->hasfile('csv_import')){
			$validation =$request->validate([
	            'csv_import' => 'mimes:csv,txt',
	        ]);
			if (!is_array($validation)) {
	            return redirect()->back()->withErrors($validation->errors());
	        }
			$path = $request->file('csv_import')->getRealPath();
    		$data = array_map('str_getcsv', file($path));
			$header = array_shift($data);
			$csv = array();
			$filtered_array = array_filter($data, function ($element) { return (trim($element[0]) != ''); } ); 
			foreach ($filtered_array as $key => $value) {
				$csv[] = array_combine($header, $value);
			}
			$countries = DB::table('countries')->get()->toArray();
			return view('admin.artists.import',compact('csv','countries'));
		}
		$validator = Validator::make($request->all(),[
			'artist.*.name' => 'required'
		]);
		if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
		foreach ($request->artist as $key => $artist) {
			if($request->hasfile('artist.'.$key.'.image')) {
				/*$image = $request->file('artist.'.$key.'.image');
				$imageName = time().'.'.$image->getClientOriginalExtension();
				$image->move(public_path('images/artists'), $imageName);*/	
				$image = $request->file('artist.'.$key.'.image');
				$imageName = time().'.'.$image->getClientOriginalExtension();
				$extension = $image->getClientOriginalExtension();
				$thumbname = basename($imageName,'.'.$extension);
				$thumb = new ImageResize($image);
				Storage::disk('s3')->put('Artists/'.$imageName, file_get_contents($image->getRealPath()));
				$thumb->resizeToWidth(160);
				Storage::disk('s3')->put('Artists/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
			}else{
				$imageName = "";
			}
			$checkArtist = Artist::where('name', '=', $artist['name'])->first();
			if ($checkArtist === null) {
				$artist = Artist::create([
						'name' 			=> $artist['name'],
						'image'	 		=> $imageName,
						'dob'	 		=> $artist['dob']?date('Y-m-d',strtotime($artist['dob'])):null,
						'is_featured'	=> isset($artist['isFeatured'])?:0,
						'bio'			=> $artist['bio']?:'',
						'status'			=> 1,
						'country'		=> isset($artist['country']) ? $artist['country'] : ''
				]);
				$imageName = '';
				$artist_tags = array_filter(explode(',',$request->input('artist.'.$key.'.artist_tags')));
				$tagIds = array();
				foreach ($artist_tags as $tagname) {
					$tagname = trim($tagname); 
					$tag = Tag::firstOrCreate([
								'name'	=> $tagname,
								'status'=> 1
							]);
					$tagIds[]= $tag->tag_id?:$tag->id;
				}
				$artist->tags()->attach($tagIds);
			}
		}
		return redirect('admin/artists')->with('message', 'Artists imported successfully!');
	}
	
	public function createArtist(Request $request){
		$validation =$request->validate([
            'name' 	=> 'required|unique:artists|string|max:255',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);
		
        if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
        	if($request->hasfile('image')) {
				/*$image = $request->file('image');
				$imageName = time().'.'.$image->getClientOriginalExtension();
				$image->move(public_path('images/artists'), $imageName);	*/
				$image = $request->file('image');
				$imageName = time().'.'.$image->getClientOriginalExtension();
				$extension = $image->getClientOriginalExtension();
				$thumbname = basename($imageName,'.'.$extension);
				$thumb = new ImageResize($image);
				Storage::disk('s3')->put('Artists/'.$imageName, file_get_contents($image->getRealPath()));
				$thumb->resizeToWidth(160);
				Storage::disk('s3')->put('Artists/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
			}else{
				$imageName = "";
			}
			$artist = Artist::create([
						'name' 			=> $request->name,
						'image'	 		=> $imageName,
						'dob'	 		=> $request->input('dob')?date('Y-m-d',strtotime($request->input('dob'))):null,
						'is_featured'	=> $request->input('isFeatured')?:0,
						'bio'			=> $request->input('bio')?:'',
						'country'		=> $request->input('country')?:'',
						'status'	=> 1
					]);
			$artist_tags = explode(',',$request->artist_tags);
			$tagIds = array();
			foreach ($artist_tags as $tagname) {
				$tagname = trim($tagname); 
				$tag = Tag::firstOrCreate([
							'name'	=> $tagname,
							'status'=> 1
						]);
				$tagIds[]= $tag->tag_id?:$tag->id;
			}
				$artist->tags()->attach($tagIds);
			return redirect('admin/artist/view/'.$artist->id)->with('message', 'Artist created successfully!');
		}
	}
	public function getArtists(Request $request){
		$search = $request->input( 'search' );
		$start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        
        $order = $request->input ( 'order' );
		
		$column = 'updated_at';
		$dir = 'DESC';
		if(is_array($order) && sizeof($order) > 0){
			if(isset($order[0]['column']) && isset($order[0]['dir'])){
				switch ($order[0]['column']) {
				    case "2":
				        $column = "name";
						$dir = $order[0]['dir'];
				        break;
				    default:
				        $column = "updated_at";
						$dir = "DESC";
				}
			}
		}
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
            $artists = Artist::withoutGlobalScope(ActiveScope::class)->select('name','image','id','updated_at','bio','created_at')->where(function ($query) use ($searchTerm) {
															$query->where('name', 'like', "%".$searchTerm."%")
															->where('status', 1);
														})
												->orderBy($column, $dir)
												->skip($start)->take($length)
												 ->get();
           $allartist = Artist::withoutGlobalScope(ActiveScope::class)->select('name','image','id','updated_at','bio','created_at')->where(function ($query) use ($searchTerm) {
															$query->where('name', 'like', "%".$searchTerm."%")
															->where('status', 1);
														})
												->orderBy('updated_at','DESC')
												 ->count();
        }else{
			$artists = Artist::withoutGlobalScope(ActiveScope::class)->select('name','image','id','updated_at','bio','created_at')->where('status', 1)->orderBy($column, $dir)->skip($start)->take($length)->get();
			$allartist = Artist::withoutGlobalScope(ActiveScope::class)->select('name','image','id','updated_at','bio','created_at')->where('status', 1)->orderBy('updated_at','DESC')->count();
		}
		
        // Here are the parameters sent from client for paging 
        
		
		$data = array ();
        foreach ( $artists as $artist ) {
            $nestedData = array ();
            $nestedData ['name'] = $artist->name;
			$nestedData['albums']= collect($artist->albums)->pluck('title')->all();
            $nestedData ['image'] = $artist->image;
            $nestedData ['created_at'] = $artist->created_at->format('d / m / Y');
            $nestedData ['bio']['text'] = $artist->bio;
			$nestedData ['bio']['id']	= $artist->id;	
			$nestedData ['id'] = $artist->id; 
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $allartist ), // total number of records
                "recordsFiltered" => intval ( $allartist ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}

	public function getTrashedArtists(Request $request){
		$search = $request->input( 'search' );
		$start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
            $artists = Artist::withoutGlobalScope(ActiveScope::class)->onlyTrashed()->select('name','image','id','updated_at','bio','created_at')->where(function ($query) use ($searchTerm) {
															$query->where('name', 'like', "%".$searchTerm."%")
															->where('status', 1);
														})
												->orderBy('updated_at','DESC')
												->skip($start)->take($length)
												 ->get();
           $allartists = Artist::withoutGlobalScope(ActiveScope::class)->onlyTrashed()->select('name','image','id','updated_at','bio','created_at')->where(function ($query) use ($searchTerm) {
															$query->where('name', 'like', "%".$searchTerm."%")
															->where('status', 1);
														})
												->orderBy('updated_at','DESC')
												 ->count();
        }else{
			$artists = Artist::withoutGlobalScope(ActiveScope::class)->onlyTrashed()->select('name','image','id','updated_at','bio','created_at')->where('status', 1)->orderBy('updated_at','DESC')->skip($start)->take($length)->get();
			$allartists = Artist::withoutGlobalScope(ActiveScope::class)->onlyTrashed()->select('name','image','id','updated_at','bio','created_at')->where('status', 1)->orderBy('updated_at','DESC')->count();
		}
		
        // Here are the parameters sent from client for paging 
        
		
		$data = array ();
        foreach ( $artists as $artist ) {
            $nestedData = array ();
            $nestedData ['name'] = $artist->name;
			$nestedData['albums']= collect($artist->albums()->withTrashed()->get())->pluck('title')->all();
            $nestedData ['image'] = $artist->image;
            $nestedData ['created_at'] = $artist->created_at->format('d / m / Y');
            $nestedData ['bio']['text'] = $artist->bio;
			$nestedData ['bio']['id']	= $artist->id;	
			$nestedData ['id'] = $artist->id; 
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $allartists ), // total number of records
                "recordsFiltered" => intval ( $allartists ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}

	public function restoreArtist($id)
	{
		$artist = Artist::withoutGlobalScope(ActiveScope::class)->onlyTrashed()
                ->where('id', $id)->first();
		$artist->restore();
		
		foreach ($artist->musics()->withTrashed()->get() as $music) {
			$music->restore();
			$music->banners()->restore();
		}
		
		foreach ($artist->videos()->withTrashed()->get() as $video) {
			$video->restore();
		}
		
		foreach ($artist->albums()->withTrashed()->get() as $album) {
			$album->restore();
		}
		
		foreach ($artist->news()->withTrashed()->get() as $news) {
			$news->restore();
		}
		
		$artist->banners()->restore();
		
		return redirect()->action('Admin\ArtistController@index')->with('message', 'Artist and related data restored Successfully!');
	}
	
	public function editArtist($id){
		$artist = Artist::withoutGlobalScope(ActiveScope::class)->find($id);	
		$countries = DB::table('countries')->get()->toArray();
		return view('admin.artists.edit',compact('artist','countries'));
	}
	public function editArtistData(Request $request){
		$validation =$request->validate([
            'name' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);
		if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
        	
		$artist = Artist::withoutGlobalScope(ActiveScope::class)->find($request->id);	
        
		if($request->hasfile('image')) 
		{
			/*$image = $request->file('image');
			$imageName = time().'.'.$image->getClientOriginalExtension();
        	$image->move(public_path('images/artists'), $imageName);
			$image = 'images/artists/'.$imageName;*/
			$image = $request->file('image');
			$imageName = time().'.'.$image->getClientOriginalExtension();
			$extension = $image->getClientOriginalExtension();
			$thumbname = basename($imageName,'.'.$extension);
			$thumb = new ImageResize($image);
			Storage::disk('s3')->put('Artists/'.$imageName, file_get_contents($image->getRealPath()));
			$thumb->resizeToWidth(160);
			Storage::disk('s3')->put('Artists/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
		}else{
			$imageName = $artist->getOriginal('image');
		} 
		 $artist_data = array(
					'name'	=> $request->name,
					'image'	=> $imageName,
					'dob'	=> $request->dob,
					'bio'	=> $request->input('bio')?:'',
			'is_featured'	=> $request->input('isFeatured')?:0,
			'country'		=> $request->country?:'',	
				);

		$artist->update($artist_data);
		
		$artist_tags = array_filter(explode(',',trim($request->artist_tags)));
		$tagIds = array();
		if(count($artist_tags)>0){
			foreach ($artist_tags as $tagname) {
				$tagname = trim($tagname); 
				$tag = Tag::firstOrCreate([
							'name'	=> $tagname,
							'status'=> 1
						]);
				$tagIds[]= $tag->tag_id?:$tag->id;
			}
			$artist->tags()->sync($tagIds);
		}
		}
		return redirect()->back()->with('message', 'Artist updated!'); 
	}
	public function deleteArtistData($id){
		$artist = Artist::withoutGlobalScope(ActiveScope::class)->find($id);
		
		foreach ($artist->musics as $music) {
			$music->delete();
			$music->banners()->delete();
		}
		
		foreach ($artist->videos as $video) {
			$video->delete();
		}
		
		foreach ($artist->albums as $album) {
			$album->delete();
		}
		
		foreach ($artist->news as $news) {
			$news->delete();
		}
		
		$artist->banners()->delete();
		
		$artist->delete();
		
		return redirect()->back()->with('message', 'Artist has been moved trash.'); 
	}
	
	public function deletePermanentArtistData($id){
		$artist = Artist::withoutGlobalScope(ActiveScope::class)->onlyTrashed()
                ->where('id', $id)->first();
		$artist->tags()->detach();
		$artist->videos()->forceDelete();
		foreach ($artist->musics as $music) {
			$music->banners()->forceDelete();
		}
		$artist->musics()->forceDelete();
		$artist->albums()->forceDelete();
		$artist->news()->forceDelete();
		$artist->banners()->forceDelete();
		$artist->forceDelete();
		/*
		 * 
		$artist->videos()->detach();
		$artist->musics()->detach();
		$artist->albums()->detach();
		 */
		return redirect()->action('Admin\ArtistController@index')->with('message', 'Artist and related data removed Permanently!'); 
	}
	
	
	
}
