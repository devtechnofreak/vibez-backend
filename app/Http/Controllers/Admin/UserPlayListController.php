<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Playlist;
use App\User;
use App\Music;
use \Gumlet\ImageResize;
use Illuminate\Support\Facades\Storage;
use DB, Notification;
use App\Notifications\PushNotification;
use Auth;


class UserPlayListController extends Controller
{
    public function index(){
		return view('admin.userplaylists.index');
	}
	
	public function edit($id){
		$playlist = Playlist::find($id);
		
		$musics = DB::table('musics')
					->leftJoin('music_artists', 'musics.id', '=', 'music_artists.music_id')
					->leftJoin('artists', 'music_artists.artist_id', '=', 'artists.id')
					->select(
					    'musics.id AS id',
					    'musics.title AS title',
					    'artists.name as artist'
					)
					->get();
		$featured_artist  = DB::table('playlist_musics AS A')
					->leftJoin('music_artists as B', 'A.music_id', '=', 'B.music_id')
					->leftJoin('artists as C', 'B.artist_id', '=', 'C.id')
					->select('C.name as artist_name')
					->where('A.playlist_id','=',$id)
					->groupBy('B.artist_id')
					->orderBy('C.name','asc')
					->get();			
		return view('admin.userplaylists.edit',compact('playlist','musics','featured_artist'));
	}
	
	public function getPlaylists(Request $request){
		$user_id = $request->user_id;
		$search = $request->input( 'search' );
		 $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
       
        $order = $request->input ( 'order' );
		
		/*$column = 'id';
		$dir = 'DESC';*/
		$column = 'order_id';
		$dir = 'ASC';
		if(is_array($order) && sizeof($order) > 0){
			if(isset($order[0]['column']) && isset($order[0]['dir'])){
				switch ($order[0]['column']) {
				    case "3":
				        $column = "title";
						$dir = $order[0]['dir'];
				        break;
				    default:
				        $column = "order_id";
						$dir = "ASC";
				}
			}
		}
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
 			$playlists = PlayList::where('title','like',"%".$searchTerm."%")
 						->where('is_admin','!=','1')
 						->orderBy($column,$dir)
 						->skip($start)->take($length)
 						->get();
			$allplaylists = PlayList::where('title','like',"%".$searchTerm."%")->where('user_id',$user_id)->count();
           
        }else{
				$playlists = PlayList::where('is_admin','!=','1')->orderBy($column,$dir)->skip($start)->take($length)->get();
				$allplaylists = PlayList::where('is_admin','!=','1')->count();
		}
		
		$totalData = $allplaylists;            //Total record
        $totalFiltered = $playlists->count();      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
       
		
		$data = array ();
        foreach ( $playlists as $playlist ) {
            $nestedData = array ();
            $nestedData ['user_name'] = $playlist->user?$playlist->user->name:'admin';
			$nestedData ['title'] = $playlist->title;
			$nestedData ['image'] = $playlist->thumb;
			$nestedData ['description'] = $playlist->description;
			$nestedData ['musics'] = collect($playlist->musics)->pluck('title')->all();
			$nestedData ['id'] = $playlist->id; 
			$nestedData ['DT_RowId'] = $playlist->id; 
            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalData ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}


	public function addtomain($id){
		$fynd_playlist = Playlist::find($id);
		$fynd_playlist->is_admin = '1';
		$fynd_playlist->order_id = '0';
		$fynd_playlist->save();
		return redirect()->back()->with('message', 'Playlist added successfully!'); 
        
	}

	
}
