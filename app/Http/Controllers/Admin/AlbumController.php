<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Artist;
use App\Album;
use App\Tag;
use App\User;
use Mail;
use DB,Notification;
use App\MusicMeta;
use App\Notifications\PushNotification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Filesystem\Filesystem;
use Pbmedia\LaravelFFMpeg\FFMpegFacade as FFMpeg;
use App\Jobs\AudioBitrateGenerate;
use App\Jobs\AudioUpload;
use App\Jobs\BulkAlbumAudioUploadJob;
use App\Jobs\BulkAlbumAudioUploadDone;
use \Gumlet\ImageResize;
use Illuminate\Notifications\Notifiable;
use App\Notifications\SendAlbumCronjobResult;
use Illuminate\Support\Facades\Log;

class AlbumController extends Controller
{

	public function add($id=null){
		if($id){
			$artist = Artist::find($id);
			$all_artists = Artist::all();
			$artist_names = $artist->name;
			return view('admin.albums.add',compact('artist','all_artists','artist_names')); 
		}
		$artists = Artist::where('status',1)->orderBy('name')->get();
		return view('admin.albums.add',compact('artists')); 
	}
	
	public function edit($id)
	{
		$album = Album::find($id);
		$artists = Artist::all();
		return view('admin.albums.edit',compact('album','artists'));		
	}
	
	public function viewBulkImport($id=null){
		if($id){
			 $artist_id = $id;
			return view('admin.albums.import',compact('artist_id')); 
		}
		return view('admin.albums.import'); 
	}
	public function reorderMusics(Request $request){
		$id = $request->album;
		$music_ids = $request->music;
		$album = Album::find($id);
		$x = 1;
		foreach ($music_ids as $single_music) {
			$getMusic = DB::table('album_musics')
            ->where('album_id', $id)
			->where('music_id', $single_music)
            ->update(['sort_order' => $x]); 
			$x++;
		}
	}
	public function sendPushNotification(Request $request){
		$album_id = $request->album_id;
		$album = Album::find($album_id);
		$artist = $album->artists()->first();
		$notificationtype = $request->notificationtype;
		if($notificationtype == 'custom'){
			$album_title = $request->album_title;
			$album_description = $request->album_description;
		}
		else{
			$album_title = "Music Update: ".$artist->name.' - '.$album->title;
			$album_description = $album->description;
		}
		if(isset($album_id) && $album_id != ""){
			
			
			$users = $artist->followers->pluck('id')->toArray();
			if(is_array($users) && sizeof($users) > 0){
				$selectedUser = User::whereNotIn('id', $users)->get();
			}else{
				$selectedUser = User::all();
			}
			
			//$selectedUser = User::whereIn('id', ['480','9','366'])->get();
			$album_pay_arr = array(
				'id'			=> $album->id,
				'title'			=> $album->title,
				'image'		  	=> $album->db_image,
				'description' 	=> $album_description,
				'user_name'		=> $album->user_name,
				'thumb'			=> $album->db_thumb_image,
			);
			$message = array();
			$message['title'] = 'Vibez';
			$message['message'] = $album_title;
			$message['click_action'] = 'vibez.album.new';
			/*$message['album'] = $album->setAppends(['user_name'])->toArray();*/
			$message['album'] = $album_pay_arr;
			
			Notification::send($selectedUser, (new PushNotification($message))->onQueue('audio'));
			return redirect()->back()->with('message','Pushnotification sent successfully!'); 
		}
	}
	public function bulkTrashAlbumAction(Request $request){
		$action = $request->action;
		$albums = $request->albums;
		
		if($action == "restore"){
			if(sizeof($albums) > 0){
				foreach ($albums as $single_albums) {
					$album = Album::onlyTrashed()
			                ->where('id', $single_albums)->first();
					$album->restore();
					foreach ($album->musics()->withTrashed()->get() as $music) {
						$music->restore();
						$music->banners()->restore();
					}
					$album->banners()->restore();
				}
			}
		}
		if($action == "delete"){
			if(sizeof($albums) > 0){
				foreach ($albums as $single_albums) {
					$album = Album::onlyTrashed()
                			->where('id', $single_albums)->first();
					$deleted_musics = array();
					foreach ($album->musics as $music) {
						$deleted_musics[] = $music->id;
						$this->deleteMusicFiles($music);
						$music->banners()->forceDelete();
					}
					foreach ($album->artists as $key => $artist) {
						$artist->musics()->detach($deleted_musics);
					}
					$album->musics()->forceDelete();	
					$album->banners()->forceDelete();	
					$album->artists()->detach();	
					$album->tags()->detach();	
					$album->forceDelete();
				}
			}
		}
		return array("status"	=> 1);
	}
	public function bulkAlbumAction(Request $request){
		$action = $request->action;
		$albums = $request->albums;
		
		if($action == "trash"){
			if(sizeof($albums) > 0){
				foreach ($albums as $single_albums) {
					$album = Album::find($single_albums);
					foreach ($album->musics as $music) {
						$music->delete();
						$music->banners()->delete();
					}
					$album->banners()->delete();
					$album->delete();
				}
			}
		}
		if($action == "delete"){
			if(sizeof($albums) > 0){
				foreach ($albums as $single_albums) {
					$album = Album::find($single_albums);
					$deleted_musics = array();
					foreach ($album->musics as $music) {
						$deleted_musics[] = $music->id;
						$this->deleteMusicFiles($music);
						$music->banners()->forceDelete();
					}
					foreach ($album->artists as $key => $artist) {
						$artist->musics()->detach($deleted_musics);
					}
					$album->musics()->forceDelete();	
					$album->banners()->forceDelete();	
					$album->artists()->detach();	
					$album->tags()->detach();	
					$album->forceDelete();
				}
			}
		}
		return array("status"	=> 1);
	}
	public function albumImportCronjobManually(){
		echo "I AM GHERE";
		exec('wget https://www.thevibez.net/cron/albumimportcron');
		exit;
	}

	public function albumImportCronjob(){

		$folders = glob("/home/admin/web/thevibez.net/public_html/public/import/music/*", GLOB_ONLYDIR );
		$errors_album = array();
		foreach ($folders as $folder) {
			$foldername = substr($folder, strrpos($folder, '/') + 1);	
			
			$subdir = "/home/admin/web/thevibez.net/public_html/public/import/music/".$foldername;
			$csv = glob($subdir . "/*.csv");
			$musics = glob($subdir . "/*.mp3");
			$getImages = glob($subdir . "/*.jpg");

			if(sizeof($csv) > 0 && sizeof($musics) > 0 && sizeof($getImages) > 0){
				$file = fopen($csv[0],"r");
				$csvData = array();
				while(! feof($file)){
					$csvData[]	= fgetcsv($file);
				}
				fclose($file);
				$csvData = array_filter($csvData);
				if(sizeof($csvData) > 0 && sizeof($csvData[1]) == 5)
				{
					$title 			= $csvData[1][0];
					$description 	= $csvData[1][1];
					$tags 			= $csvData[1][2];
					$artist 		= $csvData[1][3];
					$release_year   = $csvData[1][4];
					$checkIfAlbumExist = Album::where('title',$title)->count();
					$checkifArtistExist = Artist::where('name','LIKE',$artist)->get()->toArray();
					
					if($checkIfAlbumExist == 0){
						$checkIfAlbumArtistExist[0] = '';
					}
					else
					{
						$newalbum = Album::where('title','like',$title)->first();
						$checkIfAlbumArtistExist = collect($newalbum->artists)->pluck('name')->all();		
					}

					if(($checkIfAlbumExist == 0 || $checkIfAlbumArtistExist[0] != $artist) && sizeof($checkifArtistExist))
					{
						if(is_array($getImages) && sizeof($getImages) > 0)
						{
							$album_image_name = substr($getImages[0], strrpos($getImages[0], '/') + 1);	
							$image = $getImages[0];
							$random_string = md5(rand(1, 100000000));
							$imageName = $random_string.'.jpg';
							$extension = 'jpg';
							$thumb = new ImageResize($getImages[0]);
							Storage::disk('s3')->put('albums/'.$imageName, file_get_contents($image));
							$thumb->resizeToWidth(300);
							Storage::disk('s3')->put('albums/'.$random_string.'_thumb.'.$extension, $thumb->getImageAsString());
							$imageName = 'albums/'.$imageName;
						}else{
							$imageName ='';
						}	
						
						$createdAlbum = Album::create([
								'title'			=>$title,
								'description'	=>$description,
								'image'			=>$imageName,
								'album_status'	=> 0,
								'release_year'	=> isset($release_year) ? $release_year : "",

								]);
						$createdAlbum->artists()->attach($checkifArtistExist[0]['id']);
						
						$album_tags = array_filter(explode(',',$tags));

						$tagIds = array();
						foreach ($album_tags as $tagname) {
							$tagname = trim($tagname); 
							$tag = Tag::firstOrCreate([
										'name'	=> $tagname,
										'status'=> 1
									]);
							$tagIds[]= $tag->tag_id?:$tag->id;
						}
						$createdAlbum->tags()->attach($tagIds);
						
						$x = 0;
						foreach ($musics as $music) {
							$music_name  = substr($music, strrpos($music, '/') + 1);
							
							$mytrackid = substr($music_name, 0, 2);
							if (is_numeric($mytrackid))
							{
								$album_sort_order = $mytrackid;
							}
							else
							{
								$album_sort_order = 00;
							}

							$ffprobe = \FFMpeg\FFProbe::create();
							
							$tags = $ffprobe->format($music)->get('tags');
							if(array_key_exists('title',$tags) && array_key_exists('artist',$tags) && array_key_exists('album_artist',$tags) && array_key_exists('album',$tags) && array_key_exists('track',$tags))
							{
								$meta_music_name = $tags['title'];
								$meta_artist = $tags['artist'];
								$meta_album_artist = $tags['album_artist'];
								$meta_album = $tags['album'];
								$meta_track = $tags['track'];
							}	
							else
							{

								$tags = $album_tags;
								$meta_music_name = strstr(strstr($music_name, '.mp3',true),' ');
								$meta_artist = $artist;
								$meta_album_artist = $artist;
								$meta_album = $title;
								$meta_track = (int) substr($music_name, 0, 2);
							}

							
							$duration = $ffprobe->format($music)->get('duration');
							$folder = substr($music_name,0,1);
							$filePath = 'musics/'.strtolower($folder)."/";
							
							$total_music = sizeof($musics) - 1;
							if($x == $total_music){
								BulkAlbumAudioUploadJob::withChain([
								    new AudioBitrateGenerate($filePath,$music_name,128),
								    new AudioBitrateGenerate($filePath,$music_name,190),
								    new AudioBitrateGenerate($filePath,$music_name,256),
								    new AudioBitrateGenerate($filePath,$music_name,320),
								    new BulkAlbumAudioUploadDone($createdAlbum->id,$foldername),
								])->dispatch($filePath,$foldername,$music_name,$tags)->allOnQueue('audio');	
							}else{
								BulkAlbumAudioUploadJob::withChain([
								    new AudioBitrateGenerate($filePath,$music_name,128),
								    new AudioBitrateGenerate($filePath,$music_name,190),
								    new AudioBitrateGenerate($filePath,$music_name,256),
								    new AudioBitrateGenerate($filePath,$music_name,320),
								])->dispatch($filePath,$foldername,$music_name,$tags)->allOnQueue('audio');
							}
							
								$filePath .= $music_name;
								
							$image = $createdAlbum->getOriginal('image');
							$musicObj = $createdAlbum->musics()->create(['title'=>$meta_music_name,
																  'status'=>1,
																  'date'=>date('Y-m-d'),
																  'image'=>$image,
																  'audio'=>$filePath,
																  'duration'=>$duration,
																  'is_new'=> 0,
																  'is_trending'=>0	
																]);

							DB::table('album_musics')->where('music_id', $musicObj->id)->update(array('sort_order' => $album_sort_order));

							/*$metadata = array(
										'music_id'		=> $musicObj->id,
										'title'			=> (isset($tags['title'])) ? $tags['title'] : "",
										'artist'		=> (isset($tags['artist'])) ? $tags['artist'] : "",
										'album_artist'	=> (isset($tags['album_artist'])) ? $tags['album_artist'] : "",
										'album'			=> (isset($tags['album'])) ? $tags['album'] : "",
										'genre'			=> (isset($tags['genre'])) ? $tags['genre'] : "",
										'track'			=> (isset($tags['track'])) ? $tags['track'] : "",
										'account_id'	=> (isset($tags['account_id'])) ? $tags['account_id'] : ""
									);*/
							$metadata = array(
										'music_id'		=> $musicObj->id,
										'title'			=> (isset($meta_music_name)) ? $meta_music_name : "",
										'artist'		=> (isset($meta_artist)) ? $meta_artist : "",
										'album_artist'	=> (isset($meta_album_artist)) ? $meta_album_artist : "",
										'album'			=> (isset($meta_album)) ? $meta_album : "",
										'genre'			=> "",
										'track'			=> (isset($meta_track)) ? $meta_track : "",
										'account_id'	=> ""
									);		
							MusicMeta::create($metadata);
									
							foreach ($createdAlbum->artists as $key => $artist) {
								$artist->musics()->attach($musicObj->id);
							}
							$musicObj->tags()->sync($tagIds);
							$x++;
						}
					}else{
						$errors_album[] = array(
											'folder'	=> $foldername,
											'message'	=> "Album name already exist!"
											);

						if(isset($foldername) && $foldername != ""){
							$directory = "/home/admin/web/thevibez.net/public_html/public/import/music/".$foldername;
							$musics = glob($directory . "/*.mp3");
						 	foreach($musics as $music){
							 	unlink($music); 
							}
							$csv = glob($directory . "/*.csv");
							foreach($csv as $single_csv){
							  unlink($single_csv); 
							}
							$images = glob($directory . "/*.jpg");
							foreach($images as $image){
							  unlink($image); 
							}
							rmdir($directory);
						}


					}
				}else{
					$errors_album[] = array(
									'folder'	=> $foldername,
									'message'	=> "either name,description,tags,artist is not exist in csv"
									);						
				}
				
			}else{
				$errors_album[] = array(
									'folder'	=> $foldername,
									'message'	=> "either .csv or .jpg or .mp3 is not exist in folder!"
									);
			}
			
		}
		

		
		if(sizeof($errors_album) > 0){
			$content = $errors_album;
			$filename = time().".txt";
			$fp = fopen("/home/admin/web/thevibez.net/public_html/public/import/music/".$filename,"wb");
			foreach ($content as $single_line) {
				 fwrite($fp, $single_line['folder']."  -  ".$single_line['message'].PHP_EOL);
			}
			fclose($fp);
			$data = array('filepath'	=> "/home/admin/web/thevibez.net/public_html/public/import/music/".$filename);
			Mail::send('emails.welcome', compact('data'), function ($message) use($data){
			    $message->from('admin@thevibez.net', 'Vibez');
				$message->attach($data['filepath']);
			    $message->to('otoide@ostech.com.ng')->subject('Vibez( Cronjob for album import)');
			});
		}
	}

	public function storeBulkImport(Request $request)
	{
		if($request->hasfile('csv_import')){
			$artists = Artist::all();
			$validation =$request->validate([
	            'csv_import' => 'mimes:csv,txt',
	        ]);
			if (!is_array($validation)) {
	            return redirect()->back()->withErrors($validation->errors());
	        }
			$path = $request->file('csv_import')->getRealPath();
    		$data = array_map('str_getcsv', file($path));
			$header = array_shift($data);
			$csv = array();
			$filtered_array = array_filter($data, function ($element) { return (trim($element[0]) != ''); } );
			foreach ($filtered_array as $key => $value) {
				$csv[] = array_combine($header, $value);
			}
			if($request->has('artist_id')){
				$artist_id = $request->artist_id;
				return view('admin.albums.import',compact('csv','artists','artist_id'));
			}
			return view('admin.albums.import',compact('csv','artists'));
		}
		
		$validator = Validator::make($request->all(),[
			'album.*.title' => 'required|unique:albums,title',
			'album.*.artist' => 'required'
		]);
		if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
		
		foreach ($request->album as $key => $album) {
			if(isset($album['folder'])){
				$directory = "/home/admin/web/thevibez.net/public_html/public/import/music/".$album['folder'];
				$musics = glob($directory . "/*.mp3");
				$getImages = glob($directory . "/*.jpg");
				if(is_array($getImages) && sizeof($getImages) > 0){
					$album_image_name = substr($getImages[0], strrpos($getImages[0], '/') + 1);	
					$image = $getImages[0];
					$random_string = md5(rand(1, 100000000));
					$imageName = $random_string.'.jpg';
					$extension = 'jpg';
					$thumb = new ImageResize($getImages[0]);
					Storage::disk('s3')->put('albums/'.$imageName, file_get_contents($image));
					$thumb->resizeToWidth(300);
					Storage::disk('s3')->put('albums/'.$random_string.'_thumb.'.$extension, $thumb->getImageAsString());
					$imageName = 'albums/'.$imageName;
				}else{
					$imageName ='';
				}
				
				$createdAlbum = Album::create([
								'title'=>$album['title'],
								'description'=>$album['description']?:'',
								'image'=>$imageName,
								]);
				$createdAlbum->artists()->attach($album['artist']);
				
				$album_tags = array_filter(explode(',',$album['tags']));
				$tagIds = array();
				foreach ($album_tags as $tagname) {
					$tagname = trim($tagname); 
					$tag = Tag::firstOrCreate([
								'name'	=> $tagname,
								'status'=> 1
							]);
					$tagIds[]= $tag->tag_id?:$tag->id;
				}
				$createdAlbum->tags()->attach($tagIds);
				
				foreach ($musics as $music) {
					$music_name  = substr($music, strrpos($music, '/') + 1);

					$mytrackid = substr($music_name, 0, 2);
					if (is_numeric($mytrackid))
					{
						$album_sort_order = $mytrackid;
					}
					else
					{
						$album_sort_order = 00;
					}

					$ffprobe = \FFMpeg\FFProbe::create();
					$tags = $ffprobe->format($music)->get('tags');
					$duration = $ffprobe->format($music)->get('duration');
					$folder = substr($music_name,0,1);
					$filePath = 'musics/'.strtolower($folder)."/";
					
					BulkAlbumAudioUploadJob::withChain([
						    new AudioBitrateGenerate($filePath,$music_name,128),
						    new AudioBitrateGenerate($filePath,$music_name,190),
						    new AudioBitrateGenerate($filePath,$music_name,256),
						    new AudioBitrateGenerate($filePath,$music_name,320),
						])->dispatch($filePath,$album['folder'],$music_name,$tags)->allOnQueue('audio');
						$filePath .= $music_name;
						
					$image = $createdAlbum->getOriginal('image');
					$musicObj = $createdAlbum->musics()->create(['title'=>$tags['title'],
														  'status'=>1,
														  'date'=>date('Y-m-d'),
														  'image'=>$image,
														  'audio'=>$filePath,
														  'duration'=>$duration,
														  'is_new'=> 0,
														  'is_trending'=>0	
														]);

					DB::table('album_musics')->where('music_id', $musicObj->id)->update(array('sort_order' => $album_sort_order));
					
					$metadata = array(
								'music_id'		=> $musicObj->id,
								'title'			=> (isset($tags['title'])) ? $tags['title'] : "",
								'artist'		=> (isset($tags['artist'])) ? $tags['artist'] : "",
								'album_artist'	=> (isset($tags['album_artist'])) ? $tags['album_artist'] : "",
								'album'			=> (isset($tags['album'])) ? $tags['album'] : "",
								'genre'			=> (isset($tags['genre'])) ? $tags['genre'] : "",
								'track'			=> (isset($tags['track'])) ? $tags['track'] : "",
								'account_id'	=> (isset($tags['account_id'])) ? $tags['account_id'] : ""
							);
					MusicMeta::create($metadata);
							
					foreach ($createdAlbum->artists as $key => $artist) {
						$artist->musics()->attach($musicObj->id);
					}
					$musicObj->tags()->sync($tagIds);
				} 
				
			}
			
			/*
			if($request->hasfile('album.'.$key.'.image')) {
				$image = $request->file('album.'.$key.'.image');
				$imageName = time().'.'.$image->getClientOriginalExtension();
				$extension = $image->getClientOriginalExtension();
				$thumbname = basename($imageName,'.'.$extension);
				
				$thumb = new ImageResize($image);
				Storage::disk('s3')->put('albums/'.$imageName, file_get_contents($image->getRealPath()));
				$thumb->resizeToWidth(300);
				Storage::disk('s3')->put('albums/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
				$imageName = 'albums/'.$imageName;
			} else {
				$imageName ='';
			}
			$album = Album::create([
								'title'=>$album['title'],
								'description'=>$album['description']?:'',
								'image'=>$imageName,
								]);
			$album->artists()->attach($album['artist']);
			
			$album_tags = array_filter(explode(',',$album['tags']));
			$tagIds = array();
			foreach ($album_tags as $tagname) {
				$tagname = trim($tagname); 
				$tag = Tag::firstOrCreate([
							'name'	=> $tagname,
							'status'=> 1
						]);
				$tagIds[]= $tag->tag_id?:$tag->id;
			}
			$album->tags()->attach($tagIds);
			
			*/
		}
		return redirect('admin/albums')->with('message', 'Albums imported successfully!');
	}

	public function redirect(Request $request)
	{
		return redirect('admin/album/add/'.$request->artist);		
	}
	
    public function index(){
    	return view('admin.albums.index');
	}

    public function trash(){
		return view('admin.albums.trash')->withErrors(array('Removing Album will remove all album related data like songs, banners etc'));
	}
	
	public function addMusics(){
		$albums = Album::get();
		return view('admin.albums.musics',compact('albums')); 
	}
	
	public function importAudioFiles(Request $request){
		if($request->hasfile('file')) {
			$file = $request->file('file');
			$fileName = $file->getClientOriginalName();
			$file->move(public_path('import/music'), $fileName);	
		}
		return response()->json(['status'=>true]);
	} 
	public function deleteAudioFiles(Request $request)
	{
   		try{
			Storage::disk('uploads')->delete($request->filename);
		}catch(\Exception $e){
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		
		return response()->json(['status'=>true]);
	}
	
	public function getAudioMeta(Request $request)
	{   try{
			$ffprobe = \FFMpeg\FFProbe::create();
			$tags = $ffprobe->format(public_path('import/music/'.$request->filename))->get('tags');
		}catch(\Exception $e){
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
		
		return response()->json(['status'=>true,'tags'=>$tags]);
	}
	
	public function importAlbumMusics(Request $request)
	{
		$validation =$request->validate([
            'album_id' => 'required|integer',
            'musics' => 'required|array',
            //'musics.*.image'=>'required_without:musics.*.setalbumimage',
        ]);
		if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
        	$album = Album::find($request->album_id);
			$s3 = Storage::disk('s3');
			if(count($request->musics)){
				foreach ($request->musics as $key => $music) {
					if(file_exists(public_path('import/music/'.$music['filename']))){
						try{
						$ffprobe = \FFMpeg\FFProbe::create();
						$duration = $ffprobe->format(public_path('import/music/'.$music['filename']))->get('duration');
						}catch(Exception $e){
							return redirect()->back()->with('message','Unable to upload audio files.');
						} 
						$folder = substr($music['filename'],0,1);
						$filePath = 'musics/'.strtolower($folder)."/";
						/*
						$bitrates = array(128,190,256,320);
						AudioUpload::dispatch($filePath,$music['filename'],$music['meta'])->onQueue('audio');
						foreach ($bitrates as $bits) {
							AudioBitrateGenerate::dispatch($filePath,$music['filename'],(int)$bits)->onQueue('audio')->delay(5);
						}
						 */ 
						AudioUpload::withChain([
						    new AudioBitrateGenerate($filePath,$music['filename'],128),
						    new AudioBitrateGenerate($filePath,$music['filename'],190),
						    new AudioBitrateGenerate($filePath,$music['filename'],256),
						    new AudioBitrateGenerate($filePath,$music['filename'],320),
						])->dispatch($filePath,$music['filename'],$music['meta'])->allOnQueue('audio');
						$filePath .= $music['filename'];
							
						/*if(isset($music['setalbumimage'])){
							$image = $album->getOriginal('image');
						}else if($request->hasfile('musics.'.$key.'.image')){
							$file = $request->file('musics.'.$key.'.image');
							$imageName = time().'.'.$file->getClientOriginalExtension();
							$extension = $file->getClientOriginalExtension();
							$thumbname = basename($imageName,'.'.$extension);
							$thumb = new ImageResize($file);
							Storage::disk('s3')->put('musics/'.strtolower($folder).'/'.$imageName, file_get_contents($file->getRealPath()));
							$thumb->resizeToWidth(300);
							Storage::disk('s3')->put('musics/'.strtolower($folder).'/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
							$image = 'musics/'.strtolower($folder).'/'.$imageName;
						}else {
							$image ='';
						}
						*/ 
						$image = $album->getOriginal('image');
						$musicObj = $album->musics()->create(['title'=>$music['title'],
															  'status'=>1,
															  'date'=>date('Y-m-d'),
															  'image'=>$image,
															  'audio'=>$filePath,
															  'duration'=>$duration,
															  'is_new'=> 0,
															  'is_trending'=>isset($music['istrending'])?1:0	
															]);
						
						$metadata = array(
									'music_id'		=> $musicObj->id,
									'title'			=> $music['meta']['title'],
									'artist'		=> $music['meta']['artist'],
									'album_artist'	=> $music['meta']['album_artist'],
									'album'			=> $music['meta']['album'],
									'genre'			=> $music['meta']['genre'],
									'track'			=> $music['meta']['track'],
									'account_id'	=> $music['meta']['account_id']
								);
						MusicMeta::create($metadata);
								
						foreach ($album->artists as $key => $artist) {
							$artist->musics()->attach($musicObj->id);
						}
						$music_tags = array_filter(explode(',',$music['tags']));
						$tagIds = array();
						foreach ($music_tags as $tagname) {
							$tagname = trim($tagname); 
							$tag = Tag::firstOrCreate([
										'name'	=> $tagname,
										'status'=> 1
									]);
							$tagIds[]= $tag->tag_id?:$tag->id;
						}
						$musicObj->tags()->sync($tagIds);
					}
				}
				$artist = $album->artists()->first();
				$album_pay_arr = array(
					'id'			=> $album->id,
					'title'			=> $album->title,
					'image'		  	=> $album->db_image,
					'description' 	=> $album->description,
					'user_name'		=> $album->user_name,
					'thumb'			=> $album->db_thumb_image,
				);
				if($request->template == "update"){
					$notification_msg = "Music Update: ".$artist->name.' - '.$album->title;
				}else{
					$notification_msg = "Music Update: ".$artist->name.' - '.$album->title;
				}
				$message['title'] = 'Vibez';
				$message['message'] = $notification_msg;
				$message['click_action'] = 'vibez.album.new';
				$message['album'] = $album_pay_arr;
				
				//$musics = $album->with('musics')->first();
				$users = $artist->followers;
				
				
				Notification::send($users, (new PushNotification($message))->onQueue('audio')); 
				$artists = Artist::where('status',1)->get();
				return redirect('admin/album/add')->with(['artists'=>$artists,'message'=>'Album has been added successfully.']); 
				
			}
		}
	}
	
	public function store(Request $request)
	{
		$validation =$request->validate([
            'title' => 'required|string|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'artist_id' => 'required',
        ]);
		
		if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{

        	$related_artist = "";	
			if($request->featured_artist_id && $request->featured_artist_id != ""){
				$related_artist = implode(',', $request->featured_artist_id);
			}

			if($request->hasfile('image')) {
				$image = $request->file('image');
				$imageName = time().'.'.$image->getClientOriginalExtension();
				$extension = $image->getClientOriginalExtension();
				$thumbname = basename($imageName,'.'.$extension);
				
				$thumb = new ImageResize($image);
				Storage::disk('s3')->put('albums/'.$imageName, file_get_contents($image->getRealPath()));
				$thumb->resizeToWidth(300);
				Storage::disk('s3')->put('albums/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
				$imageName = 'albums/'.$imageName;
			} else {
				$imageName ='';
			}

			if($request->release_year !='')
			{
				$release_year = $request->release_year;
			}
			else
			{
				$release_year = '';
			}


			$album = Album::create([
								'title'=>$request->title,
								'description'=>$request->input('description')?:'',
								'image'=>$imageName,
								'release_year' => $release_year,
								'related_artist'  => $related_artist,
								]);
			$album->artists()->attach($request->artist_id);
			
			$album_tags = array_filter(explode(',',$request->input('tags')));
			$tagIds = array();
			foreach ($album_tags as $tagname) {
				$tagname = trim($tagname); 
				$tag = Tag::firstOrCreate([
							'name'	=> $tagname,
							'status'=> 1
						]);
				$tagIds[]= $tag->tag_id?:$tag->id;
			}
			$album->tags()->attach($tagIds);
			
			return view('admin.albums.musics')->with(['album_id'=>$album->id]);
		}
        	
	}

	public function update(Request $request)
	{
		$validation =$request->validate([
            'title' => 'required|string|max:255',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'album_id' => 'required',
        ]);
		if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
        	$album = Album::find($request->album_id);
			if($request->hasfile('image')) {
				$image = $request->file('image');
				$imageName = time().'.'.$image->getClientOriginalExtension();
				$extension = $image->getClientOriginalExtension();
				$thumbname = basename($imageName,'.'.$extension);
				
				$thumb = new ImageResize($image);
				Storage::disk('s3')->put('albums/'.$imageName, file_get_contents($image->getRealPath()));
				$thumb->resizeToWidth(300);
				Storage::disk('s3')->put('albums/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
				$imageName ='albums/'.$imageName;
			} else {
				$imageName = $album->getOriginal('image');
			}


			$related_artist = "";	
			DB::table('artist_albums')->where('album_id', '=', $request->album_id)->where('is_main_artist', '=', '0')->delete();
			if($request->featured_artist_id && $request->featured_artist_id != ""){
				$related_artist = implode(',', $request->featured_artist_id);
				foreach ($request->featured_artist_id as $key => $value) {
					$ins_array[] = 
						array(
							'album_id' 	=> $request->album_id, 
							'artist_id' =>$value,
							'is_main_artist'=>'0',
							'created_at'=>date('Y-m-d H:i:s'),
							'updated_at'=>date('Y-m-d H:i:s')
						);
				}
				DB::table('artist_albums')->insert($ins_array);
			
			}

			if($request->release_year !='')
			{
				$release_year = $request->release_year;
			}
			else
			{
				$release_year = '';
			}

			$album->update([
						'title'=>$request->title,
						'description'=>$request->input('description'),
						'image'=>$imageName,
						'release_year' => $release_year,
						'related_artist'=>$related_artist,
						]);
			$album_tags = array_filter(explode(',',trim($request->album_tags)));
			$tagIds = array();
			foreach ($album_tags as $tagname) {
				$tagname = trim($tagname); 
				$tag = Tag::firstOrCreate([
							'name'	=> $tagname,
							'status'=> 1
						]);
				$tagIds[]= $tag->tag_id?:$tag->id;
			}
			$album->tags()->sync($tagIds);
			/*
			foreach ($album->artists as $key => $artist) {
				foreach ($album->musics as $key => $music) {
					$artist->musics()->detach($music->id);
					$artist->musics()->attach($music->id);
				}
			}
			 */ 
			$artist = $album->artists()->first(); 
			$album_pay_arr = array(
				'id'			=> $album->id,
				'title'			=> $album->title,
				'image'		  	=> $album->db_image,
				'description' 	=> $album->description,
				'user_name'		=> $album->user_name,
				'thumb'			=> $album->db_thumb_image,
			);
			$message['title'] = 'Vibez';
			$message['message'] = "Music Update: ".$artist->name.' - '.$album->title;
			$message['click_action'] = 'vibez.album.update';
			$message['album'] = $album_pay_arr;
			 
			//$musics = $album->with('musics')->first();
			$users = $artist->followers;
			//Notification::send($users, (new PushNotification($message))->onQueue('audio')); 
		}
		if($request->has('add_music')){
			return view('admin.albums.musics')->with(['album_id'=>$album->id]);
		}
		
		return redirect('admin/artist/view/'.$album->artists[0]->id)->with('message','Album has been updated successfully');
				
	}

	public function getAlbums(Request $request){
		$search = $request->input( 'search' );
		  $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        $order = $request->input ( 'order' );
        $column = 'id';
		$dir = 'DESC';
		if(is_array($order) && sizeof($order) > 0){
			if(isset($order[0]['column']) && isset($order[0]['dir'])){
				switch ($order[0]['column']) {
				    case "2":
				        $column = "title";
						$dir = $order[0]['dir'];
				        break;
				    default:
				        $column = "id";
						$dir = "DESC";
				}
			}
		}
		if (isset($search['value']) && $search['value'] != "") {
			$searchTerm = $search['value'];
            $albums = Album::select('title','image','id','created_at')->where(function ($query) use ($searchTerm) {
															$query->where('title', 'like', "%".$searchTerm."%")->where('album_status',1);
														})
												->orderBy($column, $dir)
												->skip($start)->take($length)
												 ->get();
			$allalbums = Album::select('title','image','id','created_at')->where(function ($query) use ($searchTerm) {
															$query->where('title', 'like', "%".$searchTerm."%");
														})
												->orderBy('id', 'DESC')
												->count();
        }else{
			$albums = Album::select('title','image','id','created_at')->where('album_status',1)->orderBy($column, $dir)->skip($start)->take($length)->get();
			$allalbums = Album::select('title','image','id','created_at')->orderBy('id', 'DESC')->count();
		}
		
        // Here are the parameters sent from client for paging 
      
		
		$data = array ();
        foreach ( $albums as $album ) {
            $nestedData = array ();
            $nestedData ['name'] = $album->title;
			$nestedData ['artist']= collect($album->artists)->pluck('name')->all();
            $nestedData ['image'] = $album->thumb;
            $nestedData ['tags'] = collect($album->tags)->pluck('name')->all();
			$nestedData ['count'] = $album->musics->count();
			$nestedData ['created_at'] = $album->created_at->format('d / m / Y');
			$nestedData ['id'] = $album->id; 
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $allalbums ), // total number of records
                "recordsFiltered" => intval ( $allalbums ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}

	public function getTrashedAlbums(Request $request){
		$search = $request->input( 'search' );
		$start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
            $albums = Album::onlyTrashed()->select('title','image','id','created_at')->where(function ($query) use ($searchTerm) {
															$query->where('title', 'like', "%".$searchTerm."%");
														})
												->orderBy('id', 'DESC')
												->skip($start)->take($length)
												 ->get();
			$allalbum  = Album::onlyTrashed()->select('title','image','id','created_at')->where(function ($query) use ($searchTerm) {
															$query->where('title', 'like', "%".$searchTerm."%");
														})
												->orderBy('id', 'DESC')
												->count();
           
        }else{
			$albums = Album::onlyTrashed()->select('title','image','id','created_at')->orderBy('id', 'DESC')->skip($start)->take($length)->get();
			$allalbum = Album::onlyTrashed()->select('title','image','id','created_at')->count();
		}
		
        // Here are the parameters sent from client for paging 
        
		
		$data = array ();
        foreach ( $albums as $album ) {
            $nestedData = array ();
            $nestedData ['name'] = $album->title;
			$nestedData ['artist']= collect($album->artists)->pluck('name')->all();
            $nestedData ['image'] = $album->thumb;
            $nestedData ['tags'] = collect($album->tags)->pluck('name')->all();
			$nestedData ['count'] = $album->musics()->withTrashed()->count();
			$nestedData ['created_at'] = $album->created_at->format('d / m / Y');
			$nestedData ['id'] = $album->id; 
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $allalbum ), // total number of records
                "recordsFiltered" => intval ( $allalbum ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}

	public function restoreAlbum($id)
	{
		$album = Album::onlyTrashed()
                ->where('id', $id)->first();
		$album->restore();
		foreach ($album->musics()->withTrashed()->get() as $music) {
			$music->restore();
			$music->banners()->restore();
		}
		$album->banners()->restore();
		return redirect()->action('Admin\AlbumController@index')->with('message', 'Album and related data restored Successfully!');
	}
	
	public function deletePermanentAlbumData($id)
	{
		$album = Album::onlyTrashed()
                ->where('id', $id)->first();
		$deleted_musics = array();
		foreach ($album->musics as $music) {
			$deleted_musics[] = $music->id;
			$this->deleteMusicFiles($music);
			$music->banners()->forceDelete();
		}
		foreach ($album->artists as $key => $artist) {
			$artist->musics()->detach($deleted_musics);
		}
		$album->musics()->forceDelete();	
		$album->banners()->forceDelete();	
		$album->artists()->detach();	
		$album->tags()->detach();	
		$album->forceDelete();
		return redirect()->back()->with('message','Album has been deleted successfully');
	}	
	
	public function delete($id)
	{
		$album = Album::find($id);
		foreach ($album->musics as $music) {
			$music->delete();
			$music->banners()->delete();
		}
		$album->banners()->delete();
		$album->delete();
		return redirect()->back()->with('message','Album has been moved to trash successfully');
	}
	
	protected function deleteMusicFiles(Music $music)
	{
		$imagePath = pathinfo($music->getOriginal('image'));
		
		$filePath = pathinfo($music->getOriginal('audio'));
		if (strpos($imagePath['dirname'], 'albums') === false) {
			//Storage::disk('s3')->delete($music->getOriginal('image'));
			//Storage::disk('s3')->delete($imagePath['dirname'].'/'.$imagePath['filename'].'_thumb.'.$imagePath['extension']);
		}
	    //------------------
	     
	    /*Storage::disk('s3')->delete($music->getOriginal('audio'));
		Storage::disk('s3')->delete($filePath['dirname'].'/'.$filePath['filename'].'_128.mp3');
		Storage::disk('s3')->delete($filePath['dirname'].'/'.$filePath['filename'].'_190.mp3');
		Storage::disk('s3')->delete($filePath['dirname'].'/'.$filePath['filename'].'_256.mp3');
		Storage::disk('s3')->delete($filePath['dirname'].'/'.$filePath['filename'].'_320.mp3');*/
		
		$music->tags()->detach();
		$music->playlist()->detach();
		$music->artists()->detach();
		$music->album()->detach();
				
		$music->delete(); 		
	}
	//------------------------------------------------
}
