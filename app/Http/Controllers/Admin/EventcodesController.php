<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\EventCodes;
use App\User;

class EventcodesController extends Controller
{
	public function add($id=null){
		return view('admin.event_codes.add'); 
	}
	public function index(){
		return view('admin/event_codes/index');
	}
	public function readCsv(Request $request){
		if($request->hasfile('csv_import')){
			$validation =$request->validate([
	            'csv_import' => 'mimes:csv,txt',
	        ]);
			if (!is_array($validation)) {
	            return redirect()->back()->withErrors($validation->errors());
	        }
			$path = $request->file('csv_import')->getRealPath();
    		$data = array_map('str_getcsv', file($path));
			$header = array_shift($data);
			$csv = array();
			$filtered_array = array_filter($data, function ($element) { return (trim($element[0]) != ''); } );
			foreach ($filtered_array as $key => $value) {
				$csv[] = array_combine($header, $value);
			}
			
			return view('admin.event_codes.add',compact('csv'));
		}
		
		
		$customMessages = [
	        'required' 	=> 'Please insert codes',
	        'unique'	=> 'Code already Exist',
	    ];
			
		$validator = Validator::make($request->all(),[
			'eventcodes.*.codes' => 'required|unique:event_codes,code'
		],$customMessages);
		if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
		foreach ($request->eventcodes as $key => $code) {
			$data = array(
						'code'	=> $code['codes']
					);
			EventCodes::create($data);
		}
		return redirect('admin/eventcodes')->with('message', 'Codes imported successfully!');
	}
	
	public function getEventCodes(Request $request){ 
		$search = $request->input( 'search' );
		$start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' ); 
		if (isset($search['value']) && $search['value'] != "") { 
			
			$searchTerm = $search['value'];
            $eventCodes = EventCodes::select('*')->where(function ($query) use ($searchTerm) {
															$query->where('code', 'like', "%".$searchTerm."%");
														})
												->orderBy('id', 'DESC')
												->skip($start)->take($length)
												 ->get();
			 $allEventCodes = EventCodes::select('id')->where(function ($query) use ($searchTerm) {
															$query->where('code', 'like', "%".$searchTerm."%");
														})
												->orderBy('id', 'DESC')
												->count();
           
        }else{
			$eventCodes = EventCodes::select('*')->orderBy('id', 'DESC')->skip($start)->take($length)->get();
			$allEventCodes = EventCodes::select('id')->count();
		}
        // Here are the parameters sent from client for paging 
          //  Get length record from start
		
		$data = array ();
        foreach ( $eventCodes as $eventCode ) {
            $nestedData = array ();
			if(isset($eventCode->user_id) && $eventCode->user_id != 0){
				$user = User::find($eventCode->user_id);
				$name=$user->name;
				$email=$user->name;
				$image=$user->image;
			}else{
				$name="";
				$email="";
				$image="";
			}
            $nestedData ['code'] = $eventCode->code;
			$nestedData ['name']= $name;
			$nestedData ['email']= $email;
			$nestedData ['image']= $image;
			$nestedData ['date'] = ($eventCode->created_at != "") ? date('d-m-Y',strtotime($eventCode->created_at)) : "";
			$nestedData ['id'] = $eventCode->id; 
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $allEventCodes ), // total number of records
                "recordsFiltered" => intval ( $allEventCodes ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}
	public function deleteEventCode($id){
		EventCodes::where('id', $id)->delete();
		return redirect()->back()->with('message', 'Event Code successfully deleted!'); 
	}
	public function bulkEventCodesAction(Request $request){
		$action = $request->action;
		$eventcodes = $request->eventcodes;
		if($action == "delete"){
			if(sizeof($eventcodes) > 0){
				foreach ($eventcodes as $single_eventcodes) {
					EventCodes::where('id', $single_eventcodes)->delete();
				}
			}
		}
		return array("status"	=> 1); 
	}
}
