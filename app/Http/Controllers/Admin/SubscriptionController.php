<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscriptions;
use DB;

class SubscriptionController extends Controller{
    //
	
	public function index(){
		return view('admin/subscriptions/index');
	}
	public function add(){
		return view('admin/subscriptions/add');
	}
	public function editPlan($id){
		if($id != "" && $id > 0){
			
			$plan_data  = Subscriptions::where('plan_id', $id)->get()->toArray();
			return view('admin/subscriptions/edit')->with('data', $plan_data);
		}else{
			return view('admin/subscriptions/index');
		}
	}
	public function editPlanData(Request $request){
		$plan_data = array(
						'name'		=> $request['name'],
						'price'		=> $request['price'],
						'duration'	=> $request['duration']
					);
		DB::table('subscription_plans')->where('plan_id', $request['id'])->update($plan_data);
		return redirect()->back()->with('message', 'Subscription Plan updated!'); 
	}
	public function getplans(Request $request){
		$search = $request->input( 'search' );
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
            $plans = Subscriptions::select('*')->where(function ($query) use ($searchTerm) {
															$query->where('name', 'like', "%".$searchTerm."%")
															->where('status', 1);
														})												 
												 ->get();
        }else{
			$plans = Subscriptions::select('*')->where('status', 1)->get();
		}
		
		$totalData = $plans->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
		
		$data = array ();
        foreach ( $plans as $plan ) {
			
            $nestedData = array ();
            $nestedData ['name'] = $plan->name;
			$nestedData ['id'] = $plan->plan_id;
			$nestedData ['price'] = $plan->price;
			$nestedData ['duration'] = $plan->duration;
			
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}
	public function createPlan(Request $request){
		$validation =$request->validate([
            'name' 		=> 'required|string|max:255',
			'price'		=> 'required',
			'duration'	=> 'required'
        ]);
        if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
			Subscriptions::create([
				'name' 		=> $request['name'],
				'price'		=> $request['price'],
				'duration'	=> $request['duration'],
				'status'	=> 1
			]);
			return redirect()->back()->with('message', 'Subscription created!');
		}
	}
	public function deletePlanData($id){
		if($id != "" && $id > 0){
			$plan_data = array('status'	=> 0);
			DB::table('subscription_plans')->where('plan_id', $id)->update($plan_data); 
			return redirect()->back()->with('message', 'Subscription Deleted!');  
		}else{
			return view('admin/tags/index');
		}
	}
}
