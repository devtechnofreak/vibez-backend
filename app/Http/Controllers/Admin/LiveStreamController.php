<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\LiveStream;
use App\User;
use App\Artist;
use \Gumlet\ImageResize;
use Illuminate\Support\Facades\Storage;
use DB, Notification;
use App\Notifications\PushNotification;

class LiveStreamController extends Controller
{
    public function index(){
		return view('admin.livestream.index');
	}	
	
	public function add(){
		
		$artists = Artist::get();
		
		return view('admin.livestream.add',compact('artists')); 
	}
	
	public function edit($id){
		$stream = LiveStream::find($id);
		$artists = Artist::get();
		return view('admin.livestream.edit',compact('stream','artists'));
	}
	
	public function create(Request $request){
		$validation = $request->validate([
			'title'	=>'required',
            'url' 	=> 'required|string|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'artist_id'=>'required',
            'start_time'=>'required'
        ]);
		
        if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }	
		
		if($request->hasfile('image')){
			$file = $request->file('image');
			$imageName = $file->getClientOriginalName();
			$extension = $file->getClientOriginalExtension();
			$thumbname = basename($imageName,'.'.$extension);
			$thumb = new ImageResize($file);
			Storage::disk('s3')->put('images/livestream/'.$imageName, file_get_contents($file->getRealPath()));
			$thumb->resizeToWidth(300);
			Storage::disk('s3')->put('images/livestream/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
			$image = 'images/livestream/'.$imageName;
		}	

			$stream = LiveStream::create([
				'title'			=> $request->title,
				'url' 		    => $request->url,
				'image'	 		=> $image,
				'description'	=> $request->input('description')?:'',
				'start_time'	=> date('Y-m-d H:i:s',strtotime($request->start_time)),
				'end_time'		=> date('Y-m-d H:i:s',strtotime($request->end_time)),
			]);
			$stream->artists()->attach($request->artist_id);
			return redirect()->back()->with('message', 'Live Stream added successfully!');
	}	

	public function update(Request $request){
		$validation =$request->validate([
			'title'	=>'required',
			'livestream_id' => 'required|integer',
            'url' 	=> 'required|string|max:255',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'artist_id'=>'required',
            'start_time'=>'required'
        ]);
		if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }
    	$stream = LiveStream::find($request->livestream_id);
		if($request->hasfile('image')){
			$file = $request->file('image');
			$imageName = $file->getClientOriginalName();
			$extension = $file->getClientOriginalExtension();
			$thumbname = basename($imageName,'.'.$extension);
			$thumb = new ImageResize($file);
			Storage::disk('s3')->put('images/livestream/'.$imageName, file_get_contents($file->getRealPath()));
			$thumb->resizeToWidth(300);
			Storage::disk('s3')->put('images/livestream/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
			$image = 'images/livestream/'.$imageName;
		}else{
			$image = $stream->getOriginal('image');
		}
		$stream_data = array(
			'title'			=> $request->title,
			'url' 		    => $request->url,
			'image'	 		=> $image,
			'description'	=> $request->input('description')?:'',
			'start_time'	=> date('Y-m-d H:i:s',strtotime($request->start_time)),
			'end_time'		=> date('Y-m-d H:i:s',strtotime($request->end_time)),
		);
		
		$stream->update($stream_data);
		$stream->artists()->sync($request->artist_id);
		return redirect()->back()->with('message', 'Live Stream updated successfully!'); 
	}

	public function getLivestreams(Request $request){
		$search = $request->input( 'search' );
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
 			$streams = LiveStream::where('url','like',"%".$searchTerm."%")->get();
           
        }else{
				$streams = LiveStream::get();
		}
		
		$totalData = $streams->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
		
		$data = array ();
        foreach ( $streams as $stream ) {
			
            $nestedData = array ();
			$nestedData ['title'] = $stream->title;
			$nestedData ['start_time'] = $stream->start_time;
			$nestedData ['image'] = $stream->thumb;
			$nestedData ['description'] = $stream->description?:'';
			$nestedData ['id'] = $stream->id; 
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}

	public function delete($id)
	{
		$stream = LiveStream::find($id);
		$stream->comments()->forceDelete();
		$stream->delete();
		
		return redirect()->back()->with('message', 'Live Stream removed successfully!');
	}
}		
	