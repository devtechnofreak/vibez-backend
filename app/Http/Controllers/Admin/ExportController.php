<?php 
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Artist;
use App\Album;
use App\Tag;
use App\User;
use Mail;
use DB,Notification;
use App\MusicMeta;

use Illuminate\Support\Facades\Log;

class ExportController extends Controller
{
    
    public function all(){        

        $artists = Artist::all();

        $video = DB::table('video_artists as A')
                ->join('views as B', 'B.viewable_id', '=', 'A.video_id')
                ->join('videos as C', 'C.id', '=', 'A.video_id')
                ->join('artists as D', 'D.id', '=', 'A.artist_id')
                ->select(array('A.artist_id','D.id','D.name', 'C.id AS newId', 'C.title'))
                ->where('A.is_main_artist','=','1')
                ->where('B.viewable_type', 'App\Video')
                ->groupBy('B.viewable_id')
                ->orderBy('A.artist_id','desc')
                ->get();    

        $music = DB::table('music_artists as A')
                ->join('views as B', 'B.viewable_id', '=', 'A.music_id')
                ->join('musics as C', 'C.id', '=', 'A.music_id')
                ->join('artists as D', 'D.id', '=', 'A.artist_id')
                ->select(array('A.artist_id','D.id','D.name', 'C.id AS newId', 'C.title'))
                ->where('B.viewable_type', 'App\Music')
                ->groupBy('B.viewable_id')
                ->orderBy('A.artist_id','desc')
                ->get();

        // echo '<pre>'; print_r($artists->toArray()); die;
        return view('admin.export.add',compact('artists', 'video', 'music'));       
    }

    public function generate(Request $request) {       

        // echo '<pre>'; print_r($_POST);
        // die;

        if ( $request->dateFilter === 'custom' ) {
            
            $validation = $request->validate([
                'date_from' => 'required',
                'date_to'   => 'required',
            ]);
            
            if (!is_array($validation))
                return redirect()->back()->withErrors($validation->errors());
        }       

        $selectedArtist = null;
        if ($request->has('artist_id')) {
            $selectedArtist = $request->artist_id;
        }

        $music_ids = $video_ids = [];
        if ($request->has('artistContentData')) {
            foreach ($request->artistContentData as $key => $value) {
                if (strpos($value, '-video') !== false) {
                    $exp_video = explode('-video', $value);
                    array_push($video_ids, $exp_video[0]);
                }
                if (strpos($value, '-music') !== false) {
                    $exp_music = explode('-music', $value);
                    array_push($music_ids, $exp_music[0]);
                }
            }
        }

        // echo '<pre>'; print_r([$music_ids, $video_ids]); die;    

        $contentRadio = null;
        if ( $request->has('contentRadio') && $request->contentRadio != '' ) {
            $contentRadio = $request->contentRadio;
            if ($contentRadio === 'music') {
                $contentRadio = 'App\Music';
            } elseif ($contentRadio === 'video') {
                $contentRadio = 'App\Video';
            } else {
                $contentRadio = 'both';
            }                
        } else {
            $contentRadio = 'both';
        }       

        $date_from = $date_to = '';

        $isDateFilterApply = true;
        if ($request->dateFilter === 'all') {
            $isDateFilterApply = false;
        } elseif ($request->dateFilter === 'this_year') {
            $date_from = date('Y').'-01-01';
            $date_to   = date('Y-m-d');
        } elseif ($request->dateFilter === 'last_year') {
            $date_from = date("Y", strtotime("-1 year")).'-01-01';
            $date_to   = date("Y", strtotime("-1 year")).'-12-31';
        } elseif ($request->dateFilter === 'last_30') {
            $date_from = date("Y-m-d", strtotime("-30 days"));
            $date_to   = date("Y-m-d");
        } elseif ($request->dateFilter === 'last_60') {
            $date_from = date("Y-m-d", strtotime("-60 days"));
            $date_to   = date("Y-m-d");
        } elseif ($request->dateFilter === 'last_90') {
            $date_from = date("Y-m-d", strtotime("-90 days"));
            $date_to   = date("Y-m-d");
        } else {
            $date_from = date('Y-m-d', strtotime($request->date_from));
            $date_to   = date('Y-m-d', strtotime($request->date_to));
        }

        // DB::enableQueryLog();

        $video = $music = collect([]);

        $video = DB::table('video_artists as A')
                ->join('views as B', 'B.viewable_id', '=', 'A.video_id')
                ->join('videos as C', 'C.id', '=', 'A.video_id')
                ->join('artists as D', 'D.id', '=', 'A.artist_id')
                ->select(array('A.artist_id','D.id','D.name','B.viewable_id','B.viewable_type','C.title',DB::raw("COUNT(B.id) as count_click"),DB::raw("sum(B.count) as sum")))
                ->where('A.is_main_artist','=','1')
                // ->whereDate('B.created_at', '>=', $date_from) 1461 1354
                // ->whereDate('B.created_at', '<=', $date_to)
                ->when($isDateFilterApply, function ($query) use ($date_from) {
                    return $query->whereDate('B.created_at', '>=', $date_from);
                })
                ->when($isDateFilterApply, function ($query) use ($date_to) {
                    return $query->whereDate('B.created_at', '<=', $date_to);
                })
                ->when($selectedArtist, function ($query, $selectedArtist) {
                    return $query->whereIn('A.artist_id', $selectedArtist);
                })
                ->when($video_ids, function ($query, $video_ids) {
                    return $query->whereIn('C.id', $video_ids);
                })
                ->when($contentRadio, function ($query, $contentRadio) {
                    return $query->where('B.viewable_type', 'App\Video');
                })
                ->groupBy('B.viewable_id')
                ->orderBy('A.artist_id','desc')
                ->get();

        $music = DB::table('music_artists as A')
                ->join('views as B', 'B.viewable_id', '=', 'A.music_id')
                ->join('musics as C', 'C.id', '=', 'A.music_id')
                ->join('artists as D', 'D.id', '=', 'A.artist_id')
                ->select(array('A.artist_id','D.id','D.name','B.viewable_id','B.viewable_type','C.title',DB::raw("COUNT(B.id) as count_click"),DB::raw("sum(B.count) as sum")))
                // ->whereBetween('B.created_at',[$date_from, $date_to])
                // ->whereDate('B.created_at', '>=', $date_from)
                // ->whereDate('B.created_at', '<=', $date_to)
                ->when($isDateFilterApply, function ($query) use ($date_from) {
                    return $query->whereDate('B.created_at', '>=', $date_from);
                })
                ->when($isDateFilterApply, function ($query) use ($date_to) {
                    return $query->whereDate('B.created_at', '<=', $date_to);
                })
                ->when($selectedArtist, function ($query, $selectedArtist) {
                    return $query->whereIn('A.artist_id', $selectedArtist);
                })
                ->when($music_ids, function ($query, $music_ids) {
                    return $query->whereIn('C.id', $music_ids);
                })
                ->when($contentRadio, function ($query, $contentRadio) {
                    return $query->where('B.viewable_type', 'App\Music');
                })
                ->groupBy('B.viewable_id')
                ->orderBy('A.artist_id','desc')
                ->get();

         // echo '<pre>'; print_r(DB::getQueryLog());
         // die;

        if (trim($request->contentRadio) === 'both') {
             // $merged = $video->merge($music);
             if ( (!empty($music_ids)) && (!empty($video_ids)) ) {
                $merged = $video->merge($music);
            } elseif (!empty($video_ids)) {
                $merged = $video->merge(collect([]));
            } elseif (!empty($music_ids)) {
                $merged = $music->merge(collect([]));
            } else {
                $merged = $video->merge($music);
            }
        } elseif (trim($request->contentRadio) === 'music') {
            $merged = $music->merge(collect([]));
        } elseif (trim($request->contentRadio) === 'video') {
            $merged = $video->merge(collect([]));
        } else {
            if ( (!empty($music_ids)) && (!empty($video_ids)) ) {
                $merged = $video->merge($music);
            } elseif (!empty($video_ids)) {
                $merged = $video->merge(collect([]));
            } elseif (!empty($music_ids)) {
                $merged = $music->merge(collect([]));
            } else {
                $merged = $video->merge($music);
            }                
        }
        
        $result = $merged->all();   
        
        // echo '<pre>'; print_r([$music_ids, $video_ids]);
        // echo '<pre>'; print_r([$video, $music, $result]); die; 

        return view('admin.export.report',compact('result'));
    }

    public function getDataByArtist(Request $request) {
        
        // echo '<pre>'; print_r($_POST); die;

        $json = ['success' => 0, 'message' => 'No data found'];

        $selectedArtist = null;
        if ($request->has('artist_ids')) {
            $selectedArtist = $request->artist_ids;
        }

        $contentType = $request->contentType;

        $video = collect([]);
        $music = collect([]);

        $optiosVal = '';

        if ( $contentType === 'both'  ) {
            $video = DB::table('video_artists as A')
                        ->join('views as B', 'B.viewable_id', '=', 'A.video_id')
                        ->join('videos as C', 'C.id', '=', 'A.video_id')
                        ->join('artists as D', 'D.id', '=', 'A.artist_id')
                        ->select(array('A.artist_id','D.id','D.name', 'C.id AS newId', 'C.title'))
                        ->where('A.is_main_artist','=','1')
                        ->when($selectedArtist, function ($query, $selectedArtist) {
                            return $query->whereIn('A.artist_id', $selectedArtist);
                        })
                        ->groupBy('B.viewable_id')
                        ->orderBy('A.artist_id','desc')
                        ->get();

            if ($video->count() > 0) {
                foreach ($video as $key => $value) {
                    $optiosVal .= '<option value="'.$value->newId.'-video">'.$value->title.' - video'.'</option>';
                }
            }              

            $music = DB::table('music_artists as A')
                    ->join('views as B', 'B.viewable_id', '=', 'A.music_id')
                    ->join('musics as C', 'C.id', '=', 'A.music_id')
                    ->join('artists as D', 'D.id', '=', 'A.artist_id')
                    ->select(array('A.artist_id','D.id','D.name', 'C.id AS newId', 'C.title'))
                    ->when($selectedArtist, function ($query, $selectedArtist) {
                        return $query->whereIn('A.artist_id', $selectedArtist);
                    })
                    ->groupBy('B.viewable_id')
                    ->orderBy('A.artist_id','desc')
                    ->get();

            if ($music->count() > 0) {
                foreach ($music as $key => $value) {
                    $optiosVal .= '<option value="'.$value->newId.'-music">'.$value->title.' - music'.'</option>';
                }
            }        

        } else if ( $contentType === 'video' ) {
            $video = DB::table('video_artists as A')
                        ->join('views as B', 'B.viewable_id', '=', 'A.video_id')
                        ->join('videos as C', 'C.id', '=', 'A.video_id')
                        ->join('artists as D', 'D.id', '=', 'A.artist_id')
                        ->select(array('A.artist_id','D.id','D.name', 'C.id AS newId', 'C.title'))
                        ->where('A.is_main_artist','=','1')
                        ->when($selectedArtist, function ($query, $selectedArtist) {
                            return $query->whereIn('A.artist_id', $selectedArtist);
                        })
                        ->groupBy('B.viewable_id')
                        ->orderBy('A.artist_id','desc')
                        ->get();

            if ($video->count() > 0) {
                foreach ($video as $key => $value) {
                    $optiosVal .= '<option value="'.$value->newId.'-video">'.$value->title.' - video'.'</option>';
                }
            }              

        } else {
            $music = DB::table('music_artists as A')
                    ->join('views as B', 'B.viewable_id', '=', 'A.music_id')
                    ->join('musics as C', 'C.id', '=', 'A.music_id')
                    ->join('artists as D', 'D.id', '=', 'A.artist_id')
                    ->select(array('A.artist_id','D.id','D.name', 'C.id AS newId', 'C.title'))
                    ->when($selectedArtist, function ($query, $selectedArtist) {
                        return $query->whereIn('A.artist_id', $selectedArtist);
                    })
                    ->groupBy('B.viewable_id')
                    ->orderBy('A.artist_id','desc')
                    ->get();

            if ($music->count() > 0) {
                foreach ($music as $key => $value) {
                    $optiosVal .= '<option value="'.$value->newId.'-music">'.$value->title.' - music'.'</option>';
                }
            }          
        }   

        $merged = $video->merge($music);
        $result = $merged->all();   
        
        // echo '<pre>'; print_r([$video, $music, $result]); die;           
        
        if (count($result) > 0) {
            $json = ['success' => 1, 'message' => 'Data fetched successfully', 'dataHtml' => $optiosVal];      
        }  

        echo json_encode($json);
    }

    public function getDateByFilter(Request $request) {
        
        $json = ['success' => 0];

        if ($request->has('filter_str')) {            
            if ($request->filter_str === 'last_month') {
                  $newdate = date("m/d/Y", strtotime("-1 months"));
            } elseif ($request->filter_str === 'last_year') {
                $newdate = date("m/d/Y", strtotime("-1 year"));
            } else {
                $newdate = '';
            }

            $json = ['success' => 1, 'newdate' => [ 'date_from' => $newdate, 'date_to' => date('m/d/Y') ]];
        }

        echo json_encode($json);
    }
    
}
?>