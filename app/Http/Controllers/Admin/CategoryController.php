<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Category;
use DB;
use \Gumlet\ImageResize;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
	public function index(){
		return view('admin/categories/index');
	}
	public function add(){
		return view('admin/categories/add');
	}
	public function create(Request $request){
		$validation =$request->validate([
            'name' => 'required|string|max:255',
            'slug' => 'required|string|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg', 
        ]);
        if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
        	if($request->hasfile('image')){
        		$file = $request->file('image');
				$imageName = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				$thumbname = basename($imageName,'.'.$extension);
				$thumb = new ImageResize($file); 
				Storage::disk('s3')->put('category/'.$imageName, file_get_contents($file->getRealPath()));
				$thumb->resizeToWidth(400);
				Storage::disk('s3')->put('category/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
				$image = 'category/'.$imageName;
			}
			Category::create([
						'name'	=> $request->name,
						'slug'	=> $request->slug,
						'is_featured'   => $request->input('is_featured')?:0,
						'image'		=> $image?:'',
						'status'	=> 1
					]);
		return redirect()->back()->with('message', 'Category created!');
		}
	}
	public function reorderCategories(Request $request){
		$category_ids = $request->catArray;
		
		$x = 1;
		foreach ($category_ids as $single_category) {
			$category = Category::find($single_category);
			$cat_data = array(
							'sort_order'	=> $x
						);
			$category->update($cat_data);
			//$getMusic = Category::where('id', $single_category)->update(['sort_order' => $x]); 
			$x++;
		}
	}
	public function getCategories(Request $request){
		$search = $request->input( 'search' );
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
            $categories = Category::select('name','image','slug','id')->where(function ($query) use ($searchTerm) {
															$query->where('name', 'like', "%".$searchTerm."%");
														})
												 ->orWhere(function ($query) use ($searchTerm) {
																$query->where('slug', 'like', "%".$searchTerm."%");
															})
												 ->orderByRaw('sort_order = 0, sort_order')->get();
           
        }else{
			$categories = Category::select('name','image','slug','id')->orderByRaw('sort_order = 0, sort_order')->get();
		}
		
		$totalData = $categories->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
		
		$data = array ();
        foreach ( $categories as $category ) {
			
            $nestedData = array ();
            $nestedData ['name'] = $category->name;
            $nestedData ['slug'] = $category->slug;
            $nestedData ['image'] = $category->thumb;
			$nestedData ['id'] = $category->id;
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}

	public function edit($id){
		$category = Category::find($id);
		return view('admin/categories/edit',compact('category'));
	}
	
	
	public function update(Request $request){
		$category = Category::find($request->category_id);
		if($request->hasfile('image')){
    		$file = $request->file('image');
			$imageName = $file->getClientOriginalName();
			$extension = $file->getClientOriginalExtension();
			$thumbname = basename($imageName,'.'.$extension);
			$thumb = new ImageResize($file); 
			Storage::disk('s3')->put('category/'.$imageName, file_get_contents($file->getRealPath()));
			$thumb->resizeToWidth(400);
			Storage::disk('s3')->put('category/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
			$image = 'category/'.$imageName;
		} else {
			$image = $category->getOriginal('image');
		}
		
		$cat_data = array(
						'name'	=> $request->name,
						'slug'	=> $request->slug,
						'is_featured'   => $request->input('is_featured')?:0,
						'image'	=> $image?:'',
					);
		$category->update($cat_data);
		return redirect()->back()->with('message', 'Category updated!'); 
	}
	public function delete($id){
		$category = Category::find($id);
		$category->videos()->delete();
		$category->delete();
		
		return redirect()->back()->with('message', 'Category Deleted!');  
	}
}
