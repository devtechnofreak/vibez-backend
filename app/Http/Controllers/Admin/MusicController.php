<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Filesystem\Filesystem;
use Pbmedia\LaravelFFMpeg\FFMpegFacade as FFMpeg;
use \Gumlet\ImageResize;
use Illuminate\Support\Facades\Storage;
use App\Music;
use App\Tag;
use App\MusicTag;
use App\MusicArtist;
use App\AlbumMusic;
use App\PlaylistMusic;
use App\Artist;
use DB;

class MusicController extends Controller
{
    public function index(){
		return view('admin.musics.index');
	}
	
	public function add(){
		
		$artists = Artist::where('status',1)->get();
		
		return view('admin.musics.add',compact('artists')); 
	}
	
	public function getMusics(Request $request) {
		$search = $request->input( 'search' );
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
            $musics = Music::select('image','title','id','created_at')->where(function ($query) use ($searchTerm) {
															$query->where('title', 'like', "%".$searchTerm."%")
															->where('musics.status', 1)
															;
														})
								->get();
           
        }else{
			$musics = Music::select('image','title','id','created_at')->where('musics.status', 1)->get();
		}
		
		$totalData = $musics->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
		
		$data = array ();
        foreach ( $musics as $music ) {
			
            $nestedData = array ();
			$nestedData ['title'] = $music->title;
			$nestedData ['uploaded'] = time_elapsed_string($music->created_at);
            $nestedData ['image'] = $music->thumb;
			$nestedData ['id'] = $music->id; 
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;

	}

	public function bulkMusicsUpdate(Request $request){
		$musics = $request->music;
		foreach ($musics as $music) {
			if(isset($music['id']) && $music['id'] != ""){
				$musicData = Music::find($music['id']);
				$musicData->title = $music['title'];
				$musicData->is_trending = isset($music['is_trending']) ? 1 : 0;
				$musicData->is_new = isset($music['is_new']) ? 1 : 0;
				$musicData->save();
			}
		}
		return redirect()->back()->with('message', 'Musics Updated!');
	}
	
	public function createMusics(Request $request){
		
		$validation =$request->validate([
            'title' 	=> 'required|string|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'audio' => 'required'
        ]);
		
		
		if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
        	$s3 = \Storage::disk('s3');
			
			//----Audio Upload---------------------
			if($request->hasfile('audio')) {
				$file = $request->file('audio');
			 	// $fileName = time() . '.' . $file->getClientOriginalExtension();
				$audioOriginalName = $file->getClientOriginalName();
				$folder = substr($audioOriginalName,0,1);
						
				$fileNameWithoutExtension = explode(".",$audioOriginalName);
				
				$filePath = 'musics/'.strtolower($folder)."/".$audioOriginalName;
				
				try {
					$s3->put($filePath, file_get_contents($file));
				}catch(Exception $e){
					echo $e->getMessage();
				}
			}
			//------------------------------
			
			//----Image Upload---------------------
			if($request->hasfile('image')) {

				$image = $request->file('image');
				$imageName = time().'.'.$image->getClientOriginalExtension();
				$extension = $image->getClientOriginalExtension();
	        	$image->move(public_path('images/musics'), $imageName);
				$imagePath = 'music/'.strtolower($folder)."/".$imageName;
				$thumb = explode('.',$imageName);
				
				$image = new ImageResize(public_path('images/musics/'.$imageName));
				$image->resizeToWidth(300);
				$image->save(public_path('images/musics/'.$thumb[0].'_thumb.'.$extension));
				
				$thumbImage = 'musics/'.strtolower($folder)."/".$thumb[0].'_thumb.'.$extension;
				$s3 = \Storage::disk('s3');
				
				try {
					$s3->put('musics/'.strtolower($folder)."/".$imageName, file_get_contents(public_path('images/musics/'.$imageName)));
					$s3->put('musics/'.strtolower($folder)."/".$thumb[0].'_thumb.'.$extension, file_get_contents(public_path('images/musics/'.$thumb[0].'_thumb.'.$extension)));
				}catch(Exception $e){
					echo $e->getMessage();
				}
				
				unlink(public_path('images/musics/'.$imageName));
				unlink(public_path('images/musics/'.$thumb[0].'_thumb.'.$extension));


			}
			//--------------------------------
			
		$lowBitrate = (new \FFMpeg\Format\Audio\Mp3)->setAudioKiloBitrate(190);
		$midBitrate = (new \FFMpeg\Format\Audio\Mp3)->setAudioKiloBitrate(256);
		$HighBitrate = (new \FFMpeg\Format\Audio\Mp3)->setAudioKiloBitrate(320);
		
		$audio = FFMpeg::fromDisk('s3')
		    ->open($filePath)
			
			->export()
		    ->toDisk('s3')
			->inFormat(new \FFMpeg\Format\Audio\Mp3)
			->save('musics/'.strtolower($folder)."/".$fileNameWithoutExtension[0].'_128.mp3')
			
		    ->export()
		    ->toDisk('s3')
			->inFormat($lowBitrate)
			->save('musics/'.strtolower($folder)."/".$fileNameWithoutExtension[0].'_190.mp3')
			
			->export()
		    ->toDisk('s3')
			->inFormat($midBitrate)
		    ->save('musics/'.strtolower($folder)."/".$fileNameWithoutExtension[0].'_256.mp3')
			
			->export()
		    ->toDisk('s3')
			->inFormat($HighBitrate)
		    ->save('musics/'.strtolower($folder)."/".$fileNameWithoutExtension[0].'_320.mp3');
				
			
			$music = Music::create([
				'title' 		=> $request['title'],
				'image'	 		=> $thumbImage,
				'date'	 		=> date('Y-m-d',strtotime($request['date'])),
				'audio'        	=> $filePath,
				'status'		=> 1
			]);
			
			$music_artists = explode(',',$request->artistsId);
			$music_tags = explode(',',$request->tagsId);
			foreach ($music_artists as $key => $value) {
				MusicArtist::firstOrCreate([
					'music_id' => $music['id'],
					'artist_id' => $value
				]);
			}

			foreach ($music_tags as $key => $value) {
				MusicTag::firstOrCreate([
					'music_id' => $music['id'],
					'tag_id' => $value
				]);
			}
			return redirect()->back()->with('message', 'Musics created!');
		}


	}


	public function delete($id)
	{
		$music = Music::where('id', $id)->first();
		
		$imagePath = pathinfo($music->getOriginal('image'));
		
		$filePath = pathinfo($music->getOriginal('audio'));
		if (strpos($imagePath['dirname'], 'albums') === false) {
			//Storage::disk('s3')->delete($music->getOriginal('image'));
			//Storage::disk('s3')->delete($imagePath['dirname'].'/'.$imagePath['filename'].'_thumb.'.$imagePath['extension']);
		}
	    //------------------
	     
	    Storage::disk('s3')->delete($music->getOriginal('audio'));
		Storage::disk('s3')->delete($filePath['dirname'].'/'.$filePath['filename'].'_128.mp3');
		Storage::disk('s3')->delete($filePath['dirname'].'/'.$filePath['filename'].'_190.mp3');
		Storage::disk('s3')->delete($filePath['dirname'].'/'.$filePath['filename'].'_256.mp3');
		Storage::disk('s3')->delete($filePath['dirname'].'/'.$filePath['filename'].'_320.mp3');
		
		$music->tags()->detach();
		$music->playlist()->detach();
		$music->artists()->detach();
		$music->album()->detach();
				
		$music->delete(); 	

		return redirect()->back()->with('message', 'Music has been removed successfully!');	
	}

	public function update(Request $request){
		$validation =$request->validate([
			'music_id' => 'required|integer',
            'title' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);
		if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
        	$music = Music::find($request->music_id);
			$filepath = pathinfo($music->audio);
			$folder = substr($filepath['filename'],0,1);
			if($request->hasfile('image')){
				$file = $request->file('image');
				$imageName = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				$thumbname = basename($imageName,'.'.$extension);
				$thumb = new ImageResize($file);
				Storage::disk('s3')->put('musics/'.strtolower($folder).'/'.$imageName, file_get_contents($file->getRealPath()));
				$thumb->resizeToWidth(300);
				Storage::disk('s3')->put('musics/'.strtolower($folder).'/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
				$image = 'musics/'.strtolower($folder).'/'.$imageName;
			}else {
				$image =$music->getOriginal('image');
			}
			$music_data = array(						
						'title'			=> $request->title,
						'image'			=> $image,
						'date'			=> $request->input('date'),
						'is_new'		=> $request->input('isNew')?:0,
						'is_trending'	=> $request->input('isTrending')?:0,
						);
			$music->update($music_data);
			$music_tags = array_filter(explode(',',trim($request->music_tags)));
			$tagIds = array();
			if(count($music_tags)>0){
				foreach ($music_tags as $tagname) {
					$tagname = trim($tagname); 
					$tag = Tag::firstOrCreate([
								'name'	=> $tagname,
								'status'=> 1
							]);
							
					$tagIds[]= $tag->tag_id?:$tag->id;
				}
			$music->tags()->sync($tagIds);
			}
		}
		return redirect()->back()->with('message', 'Music updated successfully!'); 
	}
	
	public function edit($id){
			
		$music = Music::where('id', $id)->first();
		$artists = Artist::all();
		return view('admin.musics.edit',compact('music','artists'));
	}
	
}
