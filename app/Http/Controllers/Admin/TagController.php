<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Tag;
use DB;

class TagController extends Controller
{
    public function index(){
		return view('admin/tags/index');
	}
	
	public function add(){
		return view('admin/tags/add');
	}
	
	public function create(Request $request){
		$validation =$request->validate([
            'name' => 'required|string|max:255', 
        ]);
        if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
			Tag::create([
				'name' 		=> $request->name,
				'status'	=> 1
			]);
			return redirect()->back()->with('message', 'Tag created!');
		}
	}
	
	public function createTagAjax(Request $request){
		if(isset($_POST['search']) && $_POST['search'] != ""){
			$searchTerm = $_POST['search'];
           $tag =  Tag::create([
				'name' 		=> $searchTerm,
				'status'	=> 1
			]); 
			echo json_encode($tag->id);
		}
		exit;
	} 
	
	public function getTagsAjax(Request $request){
		
		if(isset($_POST['search']) && $_POST['search'] != ""){
			$searchTerm = $_POST['search'];
            $tags = Tag::select('name','tag_id')->where(function ($query) use ($searchTerm) {
															$query->where('name', 'like', "%".$searchTerm."%")
															->where('status', 1);
														})												 
												 ->skip(0)->take(10)->get()->toArray();
			echo json_encode($tags);
		}
		exit;
	}
	
	public function getTags(Request $request){
		$search = $request->input( 'search' );
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
            $tags = Tag::select('name','tag_id')->where(function ($query) use ($searchTerm) {
															$query->where('name', 'like', "%".$searchTerm."%");
														})												 
												 ->get();
           
        }else{
			$tags = Tag::select('name','tag_id')->get();
		}
		
		$totalData = $tags->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
		
		$data = array ();
        foreach ( $tags as $tag ) {
			
            $nestedData = array ();
            $nestedData ['name'] = $tag->name;
			$nestedData ['id'] = $tag->tag_id;
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}

	public function edit($id){
		$tag  = Tag::where('tag_id',$id)->first();
		return view('admin/tags/edit',compact('tag'));
	}
	
	public function update(Request $request){
		$tag = Tag::where('tag_id',$id)->first($request->tag_id);
		$tag->update(array(
						'name'	=> $request->name,
					));
		return redirect()->back()->with('message', 'Tag updated!'); 
	}
	
	public function delete($id){
		$tag = Tag::where('tag_id',$id)->first();
		$tag->albums()->detach();
		$tag->artists()->detach();
		$tag->videos()->detach();
		$tag->musics()->detach();
		$tag->remove();
		return redirect()->back()->with('message', 'Tag Removed Successfully!');  
	}
}
