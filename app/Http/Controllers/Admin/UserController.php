<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;
use App\userMeta;
use DB;
use App\UserManualPayment;
use App\UserSubscription;
use App\UserSubscriptionHistory;
use App\UserHelp;
use App\UserHelpFeedback;
use Illuminate\Validation\Rule;
class UserController extends Controller
{
    public function index(){
		return view('admin/users/index');
	}
	
	public function add(){
		return view('admin/users/add');
	}
	
	public function manualPaymentIndex(){
		return view('admin/manual-payment/index');
	}
	
	public function createUser(Request $request){
		$validation = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed',
            'vibez_id'=>'required'
        ]);
        if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
			User::create([
				'name' 		=> $request->name,
				'email' 	=> $request->email,
				'password' 	=> Hash::make($request->password),
				'role_id'	=> 2,
				'status'	=> 1,
				'vibez_id'	=> $request->input('vibez_id')
			]);
			return redirect()->back()->with('message', 'User registered successfully!');
		}
    }
	public function bulkSuspendUserAction(Request $request){
		$action = $request->action;
		$users = $request->users;
		
		if($action == "restore"){
			if(sizeof($users) > 0){
				foreach ($users as $single_users) {
					$user = User::where('id', $single_users)->first();
					$data = array(
						'user_suspend' 	=> 0 
		            );
					$user->update($data);
				}
			}
		}
		if($action == "delete"){
			if(sizeof($users) > 0){
				foreach ($users as $single_users) {
					$user = User::where('id', $single_users)->first();	
					$user->playlists()->forceDelete();	
					$user->token()->delete();	
					$user->comments()->forceDelete();
					$user->keywords()->delete();
					$user->watchedData()->delete();
					$user->forceDelete();
				}
			}
		}
		return array("status"	=> 1);
	}
	public function bulkTrashUserAction(Request $request){
		$action = $request->action;
		$users = $request->users;
		
		if($action == "restore"){
			if(sizeof($users) > 0){
				foreach ($users as $single_users) {
					$user = User::onlyTrashed()
			                ->where('id', $single_users)->first();
					$user->playlists()->restore();
					$user->comments()->restore();
					$user->restore();
				}
			}
		}
		if($action == "delete"){
			if(sizeof($users) > 0){
				foreach ($users as $single_users) {
					$user = User::onlyTrashed()
			                ->where('id', $single_users)->first();	
					$user->playlists()->forceDelete();	
					$user->token()->delete();	
					$user->comments()->forceDelete();
					$user->keywords()->delete();
					$user->watchedData()->delete();
					$user->forceDelete();
				}
			}
		}
		return array("status"	=> 1);
	}
	public function suspendUser(Request $request){
		
		if(isset($request->user_id) && $request->user_id != ""){
			$userid = $request->user_id;
			$user = User::find($userid);
			$data = array(
				'user_suspend' 	=> 1 
            );
			$user->update($data);
			return redirect('admin/users')->with('message', 'User has been suspended.');
		}
		
	}
	public function bulkUserAction(Request $request){
		$action = $request->action;
		$users = $request->users;
		
		if($action == "trash"){
			if(sizeof($users) > 0){
				foreach ($users as $single_users) {
					$user = User::find($single_users);	
					$user->playlists()->delete();
					$user->comments()->delete();
					$user->delete();
				}
			}
		}
		if($action == "delete"){
			if(sizeof($users) > 0){
				foreach ($users as $single_users) {
					$user = User::find($single_users);	
					$user->playlists()->forceDelete();	
					$user->token()->delete();	
					$user->comments()->forceDelete();
					$user->keywords()->delete();
					$user->watchedData()->delete();
					// $user->likes()->forceDelete();
					// $user->followings()->forceDelete();
					$user->forceDelete();
				}
			}
		}
		return array("status"	=> 1);
	}
	public function editUser($id){
			$userdata  = User::find($id);
			$user_subscription_history = UserSubscriptionHistory::where('user_id',$id)->orderBy('id', 'DESC')->get()->toArray();
			$subscription_info = UserSubscription::where('user_id',$id)->get()->toArray();
			
			$data = array(
						'data'						=> $userdata,
						'user_subscription_history' => $user_subscription_history,
						'subscription_info'			=> $subscription_info
					);
			return view('admin/users/edit')->with('data', $data);
	}
	
	public function editUserData(Request $request){
        $validation = $request->validate([
            'name' => 'required|max:255',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'dob' => 'required',
            'user_id' => 'required',
            'gender' => ['required',Rule::in(['Male', 'Female'])],
        ]);
        if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }
		
		$user = User::find($request->user_id);
        if($request->hasfile('image')) {
        	$image = $request->file('image');
        	$imageName = time().'.'.$image->getClientOriginalExtension();
        	 $image->move(public_path('images/users'), $imageName);
			 $image = 'images/users/'.$imageName;
        } else {
        	$image = $user->getOriginal('image');
        }
		
    	$data = array(
				'name' 			=> $request->name, 
				'password' 		=> $request->input('password')?Hash::make($request->input('password')):$user->getOriginal('password'),
				'dob' 			=> date('Y-m-d',strtotime($request->dob)),
				'image' 		=> $image,
				'gender' 		=> $request->gender,
				'phone'			=> $request->input('phone'), 
            );
		$user->update($data);
		return redirect()->back()->with('message', 'User updated successfully!');
	}

	public function deleteUser($id)
	{
		$user = User::find($id);	
		$user->playlists()->delete();
		$user->comments()->delete();
		$user->delete();
		
		return redirect()->back()->with('message', 'User has been moved trash.'); 
	} 
	public function manualPaymentEdit($id){
		$userdata  = UserManualPayment::select('user_manual_payment.*','users.name as name','users.email as email','users.image as image')
										->join("users","users.id","=","user_manual_payment.user_id")
										->where('user_manual_payment.id',$id)
										->get()->toArray();
		
		if(sizeof($userdata) > 0){
			$user_subscription_history = UserSubscriptionHistory::where('user_id',$userdata[0]['user_id'])->orderBy('id', 'DESC')->get()->toArray();
			
			return view('admin/manual-payment/edit',compact('userdata', 'user_subscription_history'));	
		}else{
			return view('admin/manual-payment/index');
		}	 
	}
	public function manualPaymentList(Request $request){
		$search = $request->input( 'search' );
		 $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );
		if (isset($search['value']) && $search['value'] != "") {
			$searchTerm = $search['value'];
			$users = UserManualPayment::select('user_manual_payment.*','users.name as name','users.email as email','users.image as image')
										->join("users","users.id","=","user_manual_payment.user_id")
										->where(function ($query) use ($searchTerm) {
															$query->where('users.name', 'like', "%".$searchTerm."%");
														})
												 ->orWhere(function ($query) use ($searchTerm) {
																$query->where('users.email', 'like', "%".$searchTerm."%");
															})
												
												->orderBy('id', 'DESC')
												->skip($start)->take($length)
												 ->get()->toArray();
			$allusers = UserManualPayment::select('user_manual_payment.*','users.name as name','users.email as email','users.image as image')
										->join("users","users.id","=","user_manual_payment.user_id")
										->where(function ($query) use ($searchTerm) {
															$query->where('users.name', 'like', "%".$searchTerm."%");
														})
												 ->orWhere(function ($query) use ($searchTerm) {
																$query->where('users.email', 'like', "%".$searchTerm."%");
															})
												 ->count();
		}else{
			$users = UserManualPayment::select('user_manual_payment.*','users.name as name','users.email as email','users.image as image')
										->join("users","users.id","=","user_manual_payment.user_id")
										->orderBy('id', 'DESC')->skip($start)->take($length)->get()->toArray();
			$allusers = UserManualPayment::select('user_manual_payment.*','users.name as name','users.email as email','users.image as image')
										->join("users","users.id","=","user_manual_payment.user_id")
										->count();
		}
		
		
		$totalData = $allusers;            //Total record
        $totalFiltered = $allusers;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
          //  Get length record from start
		$data = array ();
        foreach ( $users as $user ) {
			
            $nestedData = array ();
            $nestedData ['name'] 				= $user['name'];
            $nestedData ['email'] 				= $user['email'];
			$nestedData ['image'] 				= $user['image'];
			$nestedData ['date_paid'] 			= $user['date_paid'];
			$nestedData ['bank_name'] 			= $user['bank_name'];
			$nestedData ['bank_account_number'] = $user['bank_account_number'];
			$nestedData ['id'] 					= $user['id'];
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $allusers ), // total number of records
                "recordsFiltered" => intval ( sizeof($users) ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}
	
	
	// public function getUserData(Request $request){
	// 	$search = $request->input( 'search' );
	// 	 $start = $request->input ( 'start' );           // Skip first start records
 //        $length = $request->input ( 'length' );
	// 	$order = $request->input ( 'order' );
		
	// 	$column = 'id';
	// 	$dir = 'DESC';
	// 	if(is_array($order) && sizeof($order) > 0){
	// 		if(isset($order[0]['column']) && isset($order[0]['dir'])){
	// 			switch ($order[0]['column']) {
	// 			    case "2":
	// 			        $column = "name";
	// 					$dir = $order[0]['dir'];
	// 			        break;
	// 			    case "3":
	// 			        $column = "vibez_id";
	// 					$dir = $order[0]['dir'];
	// 			        break;
	// 			    case "4":
	// 			        $column = "email";
	// 					$dir = $order[0]['dir'];
	// 			        break;
	// 				case "5":
	// 			        $column = "created_at";
	// 					$dir = $order[0]['dir'];
	// 			        break;	
	// 			    default:
	// 			        $column = "id";
	// 					$dir = "DESC";
	// 			}
	// 		}
	// 	}
	// 	if (isset($search['value']) && $search['value'] != "") {
			
	// 		$searchTerm = $search['value'];
 //            $users = User::select('name','email','id','vibez_id','image','created_at')->where(function ($query) use ($searchTerm) {
	// 														$query->where('role_id', 2)
	// 														->where('name', 'like', "%".$searchTerm."%")
	// 														->where('user_suspend',0);
	// 													})
	// 											 ->orWhere(function ($query) use ($searchTerm) {
	// 															$query->where('role_id', 2)
	// 															->where('email', 'like', "%".$searchTerm."%")
	// 															->where('user_suspend',0);
	// 														})
	// 											 ->orWhere(function ($query) use ($searchTerm) {
	// 															$query->where('role_id', 2)
	// 															->where('vibez_id', 'like', "%".$searchTerm."%")
	// 															->where('user_suspend',0);
	// 														})
	// 											->orderBy($column, $dir)
	// 											->skip($start)->take($length)
	// 											 ->get();

	// 		$allusers = User::select('name','email','id','vibez_id','image','created_at')->where(function ($query) use ($searchTerm) {
	// 														$query->where('role_id', 2)
	// 														->where('name', 'like', "%".$searchTerm."%")
	// 														->where('user_suspend',0);
	// 													})
	// 											 ->orWhere(function ($query) use ($searchTerm) {
	// 															$query->where('role_id', 2)
	// 															->where('email', 'like', "%".$searchTerm."%")
	// 															->where('user_suspend',0);
	// 														})
	// 											 ->count();
           
 //        }else{
			
        	
	// 		$users = User::select('name','email','id','vibez_id','image','created_at')->where('role_id',2)->where('user_suspend',0)->orderBy($column, $dir)->skip($start)->take($length)->get();
	// 		//$user_with = User::with('enddate')->orderBy('id','DESC')->get()->toArray();
	// 		//echo "<pre>";
	// 		//print_r(User::with('enddate')->get());
	// 		//print_r($user_with);
	// 		//echo "</pre>";
	// 		//exit;


	// 		/*$users = DB::table('users AS A')
	// 					->leftJoin('user_subscriptions', 'A.id', '=', 'user_subscriptions.user_id')
	// 					->select('A.name','A.email','A.id','A.vibez_id','A.image','A.created_at','user_subscriptions.plan','user_subscriptions.end_date')
	// 					->where('A.role_id',2)
	// 					->where('A.user_suspend',0)
	// 					->orderBy($column, $dir)
	// 					->skip($start)
	// 					->take($length)
	// 					->get();*/

			

			

	// 		$allusers = User::select('name','email','id','vibez_id','image','created_at')->where('role_id',2)->count();
	// 	}
		
		
	// 	$totalData = $allusers;            //Total record
 //        $totalFiltered = $users->count();      // No filter at first so we can assign like this
 //        // Here are the parameters sent from client for paging 
 //          //  Get length record from start
		
	// 	$data = array ();
 //        foreach ( $users as $user ) {

        	
 //        	$user_id = $user->id;
	// 		/*echo "<pre>";
	// 		print_r($user_id);
	// 		echo "</pre>";*/
	// 		$subscription_info = UserSubscription::where('user_id',$user_id)->get()->toArray();
	// 		/*echo "<pre>";
	// 		print_r($subscription_info);
	// 		echo "</pre>";*/

	// 		if($user->image == '')
	// 		{
	// 			$userimg  = 'https://www.thevibez.net/public/assets/img/favicon.png';
	// 		}
	// 		else
	// 		{
	// 			$userimg = $user->image;
	// 		}

	// 			switch ($subscription_info[0]['plan']) {
	// 			case 'Trial Period':
	// 				$plan_type  = "Trial";
	// 			break;

	// 			case 'yearly subscription':
	// 				$plan_type  = "Yearly";
	// 			break;

	// 			case 'monthly subscription':
	// 				$plan_type  = "Monthly";
	// 			break;
				
	// 			default:
	// 				$plan_type = "-----";
	// 			break;
	// 			}

			
 //            $nestedData = array ();
 //            $nestedData ['name'] = $user->name;
 //            $nestedData ['email'] = $user->email;
	// 		$nestedData ['image'] = $userimg;
	// 		$nestedData ['vibez_id'] = $user->vibez_id;
	// 		$nestedData ['end_date'] = date('d / m / Y',strtotime($subscription_info[0]['end_date']));
	// 		$nestedData ['plan_type'] = $plan_type;
	// 		$nestedData ['created_at'] = $user->created_at->format('d / m / Y');
	// 		$nestedData ['id'] = $user->id;
 //            $data [] = $nestedData; 
 //        }

 //        /*
 //        * This below structure is required by Datatables
 //        */ 
 //        $tableContent = array (
 //                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
 //                "recordsTotal" => intval ( $totalData ), // total number of records
 //                "recordsFiltered" => intval ( $totalData ), // total number of records after searching, if there is no searching then totalFiltered = totalData
 //                "data" => $data
 //        );
 //        return $tableContent;
		
	// }

	public function getUserData(Request $request){
		$search = $request->input( 'search' );
		 $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );
		$order = $request->input ( 'order' );
		
		$column = 'id';
		$dir = 'DESC';
		if(is_array($order) && sizeof($order) > 0){
			if(isset($order[0]['column']) && isset($order[0]['dir'])){
				switch ($order[0]['column']) {
				    case "2":
				        $column = "name";
						$dir = $order[0]['dir'];
				        break;
				    case "3":
				        $column = "vibez_id";
						$dir = $order[0]['dir'];
				        break;
				    case "4":
				        $column = "email";
						$dir = $order[0]['dir'];
				        break;
					case "5":
				        $column = "created_at";
						$dir = $order[0]['dir'];
				        break;	
				    default:
				        $column = "id";
						$dir = "DESC";
				}
			}
		}

		$isJoinFilterApply = true;
	    $isFilterApply     = true;
	    $isFilterExpired   = true;
	    $isAllFilter	   = false;
	    if ($request->csv_filter === 'all') {
	    	$isFilterApply     = false;
	    	$isJoinFilterApply = false;
	    	$isFilterExpired   = false;
	    	$isAllFilter	   = true;
	    } elseif ($request->csv_filter === 'verify') {
	    	$isJoinFilterApply = false;
	    	$isFilterExpired   = false;
	    } elseif ($request->csv_filter === 'expired') {
	    	$isFilterApply     = false;
	    	$isJoinFilterApply = false;
	    } else {
	    	$isFilterApply     = false;
	    	$isFilterExpired   = false;
	    }

	    $csv_filter = $request->csv_filter;
	    $newrequestmodalval = $request->modalvalue;
	    if($request->modalvalue == '')
	    {
	    	$modalval = false;
	    }
	    else
	    {
			$modalval = true;
		}

		if (isset($search['value']) && $search['value'] != "") {			

		    $searchTerm = $search['value'];

			$users = DB::table('users as U')
			                ->join('user_subscriptions as US', 'U.id', '=', 'US.user_id')
			                ->select(array('U.id', 'U.name', 'U.email', 'U.vibez_id', 'U.image', 'U.created_at', 'US.plan'))
			                ->where('U.role_id', '2')
			                ->where('U.user_suspend', '0')
			                ->when($isFilterApply, function ($query) {
			                    return $query->where('U.email_verify', '1');
			                })
			               ->when($isJoinFilterApply, function ($query) use ($csv_filter,$modalval,$newrequestmodalval) {
			                	if($modalval && $newrequestmodalval == '0')
			                	{
			                		return $query->where('US.plan', 'like', "%".$csv_filter."%");	
			                	}
			                	else if($modalval && $newrequestmodalval == '1')
			                	{
			                		return $query->where('US.plan', 'like', "%".$csv_filter."%")->where('US.end_date', '>=', date('Y-m-d'));
			                	}
			                	else if($modalval && $newrequestmodalval == '2')
			                	{
			                		return $query->where('US.plan', 'like', "%".$csv_filter."%")->where('US.end_date', '<', date('Y-m-d'));
			                	}
			                	else
			                	{
			                		return $query->where('US.plan', 'like', "%".$csv_filter."%")->where('US.end_date', '>=', date('Y-m-d'));
			                	}
			                    
			                })
			                ->when($isFilterExpired, function ($query) use ($csv_filter) {
			                    return $query->whereDate('US.end_date', '<', date('Y-m-d') );
			                })
			                ->when($isAllFilter, function ($query) use ($modalval,$newrequestmodalval) {
			                    if($modalval && $newrequestmodalval == '1')
			                	{
			                		return $query->where('US.end_date', '>=', date('Y-m-d'));
			                	}
			                	else if($modalval && $newrequestmodalval == '2')
			                	{
			                		return $query->where('US.end_date', '<', date('Y-m-d'));
			                	}
			                })
			                ->where(function ($query) use ($searchTerm) {
								$query->where('U.name', 'like', "%".$searchTerm."%");
							})
							->orWhere(function ($query) use ($searchTerm) {
								$query->where('U.email', 'like', "%".$searchTerm."%");
							})
							->orWhere(function ($query) use ($searchTerm) {
								$query->where('U.vibez_id', 'like', "%".$searchTerm."%");
							})
							->orderBy($column, $dir)
							->skip($start)->take($length)
							->get();
			
			
       //      $users = User::select('name','email','id','vibez_id','image','created_at')
       //      				->where(function ($query) use ($searchTerm) {
							// 	$query->where('role_id', 2)
							// 	->where('name', 'like', "%".$searchTerm."%")
							// 	->where('user_suspend',0);
							// })
							// ->orWhere(function ($query) use ($searchTerm) {
							// 	$query->where('role_id', 2)
							// 	->where('email', 'like', "%".$searchTerm."%")
							// 	->where('user_suspend',0);
							// })
							// ->orWhere(function ($query) use ($searchTerm) {
							// 	$query->where('role_id', 2)
							// 	->where('vibez_id', 'like', "%".$searchTerm."%")
							// 	->where('user_suspend',0);
							// })
							// ->orderBy($column, $dir)
							// ->skip($start)->take($length)
							// ->get();

			$allusers = DB::table('users as U')
			                ->join('user_subscriptions as US', 'U.id', '=', 'US.user_id')
			                ->select(array('U.id', 'U.name', 'U.email', 'U.vibez_id', 'U.image', 'U.created_at', 'US.plan'))
			                ->where('U.role_id', '2')
			                ->where('U.user_suspend', '0')
			                ->when($isFilterApply, function ($query) {
			                    return $query->where('U.email_verify', '1');
			                })
			                ->when($isJoinFilterApply, function ($query) use ($csv_filter,$modalval,$newrequestmodalval) {
			                    if($modalval && $newrequestmodalval == '0')
			                	{
			                		return $query->where('US.plan', 'like', "%".$csv_filter."%");	
			                	}
			                	else if($modalval && $newrequestmodalval == '1')
			                	{
			                		return $query->where('US.plan', 'like', "%".$csv_filter."%")->where('US.end_date', '>=', date('Y-m-d'));
			                	}
			                	else if($modalval && $newrequestmodalval == '2')
			                	{
			                		return $query->where('US.plan', 'like', "%".$csv_filter."%")->where('US.end_date', '<', date('Y-m-d'));
			                	}
			                	else
			                	{
			                		return $query->where('US.plan', 'like', "%".$csv_filter."%")->where('US.end_date', '>=', date('Y-m-d'));
			                	}
			                })
			                ->when($isFilterExpired, function ($query) use ($csv_filter) {
			                    return $query->whereDate('US.end_date', '<', date('Y-m-d') );
			                })
			                ->when($isAllFilter, function ($query) use ($modalval,$newrequestmodalval) {
			                    if($modalval && $newrequestmodalval == '1')
			                	{
			                		return $query->where('US.end_date', '>=', date('Y-m-d'));
			                	}
			                	else if($modalval && $newrequestmodalval == '2')
			                	{
			                		return $query->where('US.end_date', '<', date('Y-m-d'));
			                	}
			                })
			                ->where(function ($query) use ($searchTerm) {
								$query->where('U.name', 'like', "%".$searchTerm."%");
							})
							->orWhere(function ($query) use ($searchTerm) {
								$query->where('U.email', 'like', "%".$searchTerm."%");
							})
							->orWhere(function ($query) use ($searchTerm) {
								$query->where('U.vibez_id', 'like', "%".$searchTerm."%");
							})
							->count();				

			// $allusers = User::select('name','email','id','vibez_id','image','created_at')
			// 					->where(function ($query) use ($searchTerm) {
			// 						$query->where('role_id', 2)
			// 						->where('name', 'like', "%".$searchTerm."%")
			// 						->where('user_suspend',0);
			// 					})
			// 				 	->orWhere(function ($query) use ($searchTerm) {
			// 						$query->where('role_id', 2)
			// 						->where('email', 'like', "%".$searchTerm."%")
			// 						->where('user_suspend',0);
			// 					})
			// 				 	->count();  

			
        }else{
			
        	
			// $users = User::select('name','email','id','vibez_id','image','created_at')
			// 				->where('role_id',2)
			// 				->where('user_suspend',0)
			// 				->orderBy($column, $dir)
			// 				->skip($start)
			// 				->take($length)
			// 				->get();

			$users = DB::table('users as U')
			                ->join('user_subscriptions as US', 'U.id', '=', 'US.user_id')
			                ->select(array('U.id', 'U.name', 'U.email', 'U.vibez_id', 'U.image', 'U.created_at', 'US.plan'))
			                ->where('U.role_id', '2')
			                ->where('U.user_suspend', '0')
			                ->when($isFilterApply, function ($query) {
			                    return $query->where('U.email_verify', '1');
			                })
			                ->when($isJoinFilterApply, function ($query) use ($csv_filter,$modalval,$newrequestmodalval) {
			                    if($modalval && $newrequestmodalval == '0')
			                	{
			                		return $query->where('US.plan', 'like', "%".$csv_filter."%");	
			                	}
			                	else if($modalval && $newrequestmodalval == '1')
			                	{
			                		return $query->where('US.plan', 'like', "%".$csv_filter."%")->where('US.end_date', '>=', date('Y-m-d'));
			                	}
			                	else if($modalval && $newrequestmodalval == '2')
			                	{
			                		return $query->where('US.plan', 'like', "%".$csv_filter."%")->where('US.end_date', '<', date('Y-m-d'));
			                	}
			                	else
			                	{
			                		return $query->where('US.plan', 'like', "%".$csv_filter."%")->where('US.end_date', '>=', date('Y-m-d'));
			                	}
			                })
			                ->when($isFilterExpired, function ($query) use ($csv_filter) {
			                    return $query->whereDate('US.end_date', '<', date('Y-m-d') );
			                })
			                ->when($isAllFilter, function ($query) use ($modalval,$newrequestmodalval) {
			                    if($modalval && $newrequestmodalval == '1')
			                	{
			                		return $query->where('US.end_date', '>=', date('Y-m-d'));
			                	}
			                	else if($modalval && $newrequestmodalval == '2')
			                	{
			                		return $query->where('US.end_date', '<', date('Y-m-d'));
			                	}
			                })
							->orderBy($column, $dir)
							->skip($start)->take($length)
							->get();				

			//$user_with = User::with('enddate')->orderBy('id','DESC')->get()->toArray();
			//echo "<pre>";
			//print_r(User::with('enddate')->get());
			//print_r($user_with);
			//echo "</pre>";
			//exit;


			/*$users = DB::table('users AS A')
						->leftJoin('user_subscriptions', 'A.id', '=', 'user_subscriptions.user_id')
						->select('A.name','A.email','A.id','A.vibez_id','A.image','A.created_at','user_subscriptions.plan','user_subscriptions.end_date')
						->where('A.role_id',2)
						->where('A.user_suspend',0)
						->orderBy($column, $dir)
						->skip($start)
						->take($length)
						->get();*/

			// $allusers = User::select('name','email','id','vibez_id','image','created_at')->where('role_id',2)->count();
			$allusers = DB::table('users as U')
			                ->join('user_subscriptions as US', 'U.id', '=', 'US.user_id')
			                ->select(array('U.id', 'U.name', 'U.email', 'U.vibez_id', 'U.image', 'U.created_at', 'US.plan'))
			                ->where('U.role_id', '2')
			                ->where('U.user_suspend', '0')
			                ->when($isFilterApply, function ($query) {
			                    return $query->where('U.email_verify', '1');
			                })
			                ->when($isJoinFilterApply, function ($query) use ($csv_filter,$modalval,$newrequestmodalval) {
			                    if($modalval && $newrequestmodalval == '0')
			                	{
			                		return $query->where('US.plan', 'like', "%".$csv_filter."%");	
			                	}
			                	else if($modalval && $newrequestmodalval == '1')
			                	{
			                		return $query->where('US.plan', 'like', "%".$csv_filter."%")->where('US.end_date', '>=', date('Y-m-d'));
			                	}
			                	else if($modalval && $newrequestmodalval == '2')
			                	{
			                		return $query->where('US.plan', 'like', "%".$csv_filter."%")->where('US.end_date', '<', date('Y-m-d'));
			                	}
			                	else
			                	{
			                		return $query->where('US.plan', 'like', "%".$csv_filter."%")->where('US.end_date', '>=', date('Y-m-d'));
			                	}
			                })
			                ->when($isFilterExpired, function ($query) use ($csv_filter) {
			                    return $query->whereDate('US.end_date', '<', date('Y-m-d') );
			                })
			                ->when($isAllFilter, function ($query) use ($modalval,$newrequestmodalval) {
			                    if($modalval && $newrequestmodalval == '1')
			                	{
			                		return $query->where('US.end_date', '>=', date('Y-m-d'));
			                	}
			                	else if($modalval && $newrequestmodalval == '2')
			                	{
			                		return $query->where('US.end_date', '<', date('Y-m-d'));
			                	}
			                })
							->count();
		}
		
		
		$totalData = $allusers;            //Total record
        $totalFiltered = $users->count();      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
          //  Get length record from start
		
        // echo '<pre>'; print_r($users); die;

		$data = array ();
        foreach ( $users as $user ) {

        	
        	$user_id = $user->id;
			/*echo "<pre>";
			print_r($user_id);
			echo "</pre>";*/
			$subscription_info = UserSubscription::where('user_id',$user_id)->get()->toArray();
			/*echo "<pre>";
			print_r($subscription_info);
			echo "</pre>";*/

			if($user->image == '')
			{
				$userimg  = 'https://www.thevibez.net/public/assets/img/favicon.png';
			}
			else
			{
				$userimg = url($user->image);
			}

				switch ($subscription_info[0]['plan']) {
				case 'Trial Period':
					$plan_type  = "Trial";
				break;

				case 'yearly subscription':
					$plan_type  = "Yearly";
				break;

				case 'monthly subscription':
					$plan_type  = "Monthly";
				break;
				
				default:
					$plan_type = "-----";
				break;
				}

			
            $nestedData = array ();
            $nestedData ['name'] = $user->name;
            $nestedData ['email'] = $user->email;
			$nestedData ['image'] = $userimg;
			$nestedData ['vibez_id'] = $user->vibez_id;
			$nestedData ['end_date'] = date('d / m / Y',strtotime($subscription_info[0]['end_date']));
			$nestedData ['plan_type'] = $plan_type;
			// $nestedData ['created_at'] = $user->created_at->format('d / m / Y');
			$nestedData ['created_at'] = date_format(date_create($user->created_at), 'd / m / Y');
			$nestedData ['id'] = $user->id;
            $data [] = $nestedData; 
        }

        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalData ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
		
	}


	public function trash(){
		return view('admin.users.trash');
	}
	public function suspended(){
		return view('admin.users.suspended');
	}
	public function helpIndex(){
		return view('admin.users.help');
	}

	public function UserHelpReply(Request $request){

		$validation = $request->validate([
            'status' => 'required',
            'feedback' => 'required',
            'help_id' => 'required',
        ]);
        if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }
		
		$data = array(
				'help_id' 		=> $request->help_id, 
				'feedback'		=> $request->feedback,
				'feedback_from'	=> 'Vibez',
				'status'        => $request->status 
            );
		UserHelpFeedback::create($data);
		
		$help = UserHelp::find($request->help_id);
		
		$help->status = $request->status;
		$help->save();

		$data['mail']    = $request->email;
        $data['name']    = $request->name;
        $data['subject'] = 'Help Feedback';
        $data['message'] = $request->feedback;
        
        \Mail::send('emails.help_feedback',['user' => $data], function ($message) use($data){
            $message->from('otoide@ostech.com.ng', 'Vibez Support');
            $message->subject('Vibez '.$data['subject']);
            $message->to($data['mail'], $data['name']);
        });
        
		return redirect()->back()->with('message', 'Feedback updates successfully!');
	}
	public function restoreUsers($id)
	{
		$user = User::onlyTrashed()
                ->where('id', $id)->first();
		$user->playlists()->restore();
		$user->comments()->restore();
		$user->restore();
		
		return redirect()->action('Admin\UserController@index')->with('message', 'User and related data restored Successfully!');
	}
	
	public function suspendRestoreUsers($id){
		$user = User::where('id', $id)->first();
		$data = array(
			'user_suspend' 	=> 0 
        );
		$user->update($data);
		return redirect()->action('Admin\UserController@index')->with('message', 'User and related data restored Successfully!');
	}
	
	public function UserHelpEdit($id){
		$userhelpdate  = UserHelp::select('user_help.*','users.name as name','users.email as email','users.image as image', 'users.vibez_id')
										->join("users","users.id","=","user_help.user_id")
										->where('user_help.id',$id)
										->get()->toArray();
		
		if(sizeof($userhelpdate) > 0){
			$user_help_feedback = UserHelpFeedback::where('help_id',$userhelpdate[0]['id'])->orderBy('id', 'DESC')->get()->toArray();
			
			return view('admin/users/edithelp',compact('userhelpdate', 'user_help_feedback'));	
		}else{
			return view('admin/users/index');
		}	
	}
	
	public function deletePermanentUserData($id)
	{
		$user = User::onlyTrashed()
                ->where('id', $id)->first();
		
		$user->playlists()->forceDelete();	
		$user->token()->delete();	
		$user->comments()->forceDelete();
		$user->keywords()->delete();
		$user->watchedData()->delete();
		// $user->likes()->forceDelete();
		// $user->followings()->forceDelete();
		$user->forceDelete();
		return redirect()->back()->with('message','User has been deleted successfully');
	
	}
	
	public function getUserHelpList(Request $request){
		$search = $request->input( 'search' );
		$start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' ); 
		if (isset($search['value']) && $search['value'] != "") {
			$searchTerm = $search['value'];
			$users = UserHelp::select('user_help.*','users.name as name','users.email as email','users.image as image')
										->join("users","users.id","=","user_help.user_id")
										->where(function ($query) use ($searchTerm) {
															$query->where('users.name', 'like', "%".$searchTerm."%");
														})
												 ->orWhere(function ($query) use ($searchTerm) {
																$query->where('users.email', 'like', "%".$searchTerm."%");
															})
												
												->orderBy('id', 'DESC')
												->skip($start)->take($length)
												 ->get()->toArray();
			$allusers = UserHelp::select('user_help.*','users.name as name','users.email as email','users.image as image')
										->join("users","users.id","=","user_help.user_id")
										->where(function ($query) use ($searchTerm) {
															$query->where('users.name', 'like', "%".$searchTerm."%");
														})
												 ->orWhere(function ($query) use ($searchTerm) {
																$query->where('users.email', 'like', "%".$searchTerm."%");
															})
												 ->count();
		}else{
			$users = UserHelp::select('user_help.*','users.name as name','users.email as email','users.image as image')
										->join("users","users.id","=","user_help.user_id")
										->orderBy('id', 'DESC')->skip($start)->take($length)->get()->toArray();
			$allusers = UserHelp::select('user_help.*','users.name as name','users.email as email','users.image as image')
										->join("users","users.id","=","user_help.user_id")
										->count();
		}
		
		
		$totalData = $allusers;            //Total record
        $totalFiltered = $allusers;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
          //  Get length record from start
		$data = array ();
		$status = array(
					'0'	=> 'pending',
					'1'	=> 'Solved',
					'2'	=> 'Closed'
				);
        foreach ( $users as $user ) {
			
            $nestedData = array ();
            $nestedData ['name'] 				= $user['name'];
            $nestedData ['email'] 				= $user['email'];
			$nestedData ['image'] 				= $user['image'];
			$nestedData ['help'] 				= $user['help'];
			$nestedData ['created_at'] 			= $user['created_at'];
			$nestedData ['status'] 				= $status[$user['status']];
			$nestedData ['id'] 					= $user['id'];
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $allusers ), // total number of records
                "recordsFiltered" => intval ( sizeof($users) ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}
	
	public function getSuspendedUsers(Request $request){
		$search = $request->input( 'search' );
		$start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' ); 
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
            $users = User::select('name','email','id','vibez_id','image','created_at')->where(function ($query) use ($searchTerm) {
															$query->where('role_id', 2)
															->where('name', 'like', "%".$searchTerm."%")
															->where('user_suspend',1);
														})
												 ->orWhere(function ($query) use ($searchTerm) {
																$query->where('role_id', 2)
																->where('email', 'like', "%".$searchTerm."%")
																->where('user_suspend',1);
															})
												->orderBy('id', 'DESC')
												->skip($start)->take($length)
												 ->get();
			 $allusers = User::select('name','email','id','vibez_id','image','created_at')->where(function ($query) use ($searchTerm) {
															$query->where('role_id', 2)
															->where('name', 'like', "%".$searchTerm."%")
															->where('user_suspend',1);
														})
												 ->orWhere(function ($query) use ($searchTerm) {
																$query->where('role_id', 2)
																->where('email', 'like', "%".$searchTerm."%")
																->where('user_suspend',1);
															})
												->count();
           
        }else{
			$users = User::select('name','email','id','vibez_id','image','created_at')
					->where('role_id',2)
					->where('user_suspend',1)
					->orderBy('id', 'DESC')
					->skip($start)->take($length)->get();
			$allusers = User::select('name','email','id','vibez_id','image','created_at')->where('role_id',2)->where('user_suspend',1)->count();
		}
		
		
		$totalData = $allusers;            //Total record
        $totalFiltered = $users->count();      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
          //  Get length record from start
		
		$data = array ();
        foreach ( $users as $user ) {
			
        	$user_id = $user->id;
			$subscription_info = UserSubscription::where('user_id',$user_id)->get()->toArray();
			
			if($user->image == '')
			{
				$userimg  = 'https://www.thevibez.net/public/assets/img/favicon.png';
			}
			else
			{
				$userimg = $user->image;
			}

			switch ($subscription_info[0]['plan']) {
				case 'Trial Period':
					$plan_type  = "Trial";
				break;

				case 'yearly subscription':
					$plan_type  = "Yearly";
				break;

				case 'monthly subscription':
					$plan_type  = "Monthly";
				break;
				
				default:
					$plan_type = "-----";
				break;
				}

            $nestedData = array ();
            $nestedData ['name'] = $user->name;
            $nestedData ['email'] = $user->email;
			$nestedData ['image'] = $userimg;
			$nestedData ['vibez_id'] = $user->vibez_id;
			$nestedData ['end_date'] = date('d / m / Y',strtotime($subscription_info[0]['end_date']));
			$nestedData ['plan_type'] = $plan_type;
			$nestedData ['created_at'] = $user->created_at->format('d / m / Y');
			$nestedData ['id'] = $user->id;
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalData ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}
	
	public function getTrashUsers(Request $request){
		$search = $request->input( 'search' );
		$start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' ); 
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
            $users = User::onlyTrashed()->select('name','email','id','vibez_id','image','created_at')->where(function ($query) use ($searchTerm) {
															$query->where('role_id', 2)
															->where('name', 'like', "%".$searchTerm."%");
														})
												 ->orWhere(function ($query) use ($searchTerm) {
																$query->where('role_id', 2)
																->where('email', 'like', "%".$searchTerm."%");
															})
												->orderBy('id', 'DESC')
												->skip($start)->take($length)
												 ->get();
			 $allusers = User::onlyTrashed()->select('name','email','id','vibez_id','image','created_at')->where(function ($query) use ($searchTerm) {
															$query->where('role_id', 2)
															->where('name', 'like', "%".$searchTerm."%");
														})
												 ->orWhere(function ($query) use ($searchTerm) {
																$query->where('role_id', 2)
																->where('email', 'like', "%".$searchTerm."%");
															})
												->count();
           
        }else{
			$users = User::onlyTrashed()->select('name','email','id','vibez_id','image','created_at')
					->where('role_id',2)
					->orderBy('id', 'DESC')
					->skip($start)->take($length)->get();
			$allusers = User::onlyTrashed()->select('name','email','id','vibez_id','image','created_at')->where('role_id',2)->count();
		}
		
		
		$totalData = $allusers;            //Total record
        $totalFiltered = $users->count();      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
          //  Get length record from start
		
		$data = array ();
        foreach ( $users as $user ) {
			
            $user_id = $user->id;
			$subscription_info = UserSubscription::where('user_id',$user_id)->get()->toArray();
			
			if($user->image == '')
			{
				$userimg  = 'https://www.thevibez.net/public/assets/img/favicon.png';
			}
			else
			{
				$userimg = $user->image;
			}

			switch ($subscription_info[0]['plan']) {
				case 'Trial Period':
					$plan_type  = "Trial";
				break;

				case 'yearly subscription':
					$plan_type  = "Yearly";
				break;

				case 'monthly subscription':
					$plan_type  = "Monthly";
				break;
				
				default:
					$plan_type = "-----";
				break;
				}


            $nestedData = array ();
            $nestedData ['name'] = $user->name;
            $nestedData ['email'] = $user->email;
			$nestedData ['image'] = $userimg;
			$nestedData ['vibez_id'] = $user->vibez_id;
			$nestedData ['end_date'] = date('d / m / Y',strtotime($subscription_info[0]['end_date']));
			$nestedData ['plan_type'] = $plan_type;
			$nestedData ['created_at'] = $user->created_at->format('d / m / Y');
			$nestedData ['id'] = $user->id;
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalData ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}

	public function exportUserDataToCsv(Request $request) {

	    $headers = array(
	        "Content-type" => "text/csv",
	        "Content-Disposition" => "attachment; filename=users-".time().'-'.$request->csv_filter.".csv",
	        "Pragma" => "no-cache",
	        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
	        "Expires" => "0"
	    );

	    $isJoinFilterApply = true;
	    $isFilterApply     = true;
	    $isFilterExpired   = true;
	    $isAllFilter	   = false;
	    if ($request->csv_filter === 'all') {
	    	$isFilterApply     = false;
	    	$isJoinFilterApply = false;
	    	$isFilterExpired   = false;
	    	$isAllFilter	   = true;
	    } elseif ($request->csv_filter === 'verify') {
	    	$isJoinFilterApply = false;
	    	$isFilterExpired   = false;
	    } elseif ($request->csv_filter === 'expired') {
	    	$isFilterApply     = false;
	    	$isJoinFilterApply = false;
	    } else {
	    	$isFilterApply     = false;
	    	$isFilterExpired   = false;
	    }

	    $csv_filter = $request->csv_filter;
	    $newrequestmodalval = $request->modalvalue;
	    if($request->modalvalue == '')
	    {
	    	$modalval = false;
	    }
	    else
	    {
			$modalval = true;
		}

	    // DB::enableQueryLog();

	    $reviews = DB::table('users as U')
                ->join('user_subscriptions as US', 'U.id', '=', 'US.user_id')
                ->select(array('U.id','U.name','U.email','U.vibez_id', 'U.created_at', 'US.plan'))
                ->where('U.role_id', '2')
                ->where('U.user_suspend', '0')
                ->when($isFilterApply, function ($query) {
                    return $query->where('U.email_verify', '1');
                })
                ->when($isJoinFilterApply, function ($query) use ($csv_filter,$modalval,$newrequestmodalval) {
                    if($modalval && $newrequestmodalval == '0')
                	{
                		return $query->where('US.plan', 'like', "%".$csv_filter."%");	
                	}
                	else if($modalval && $newrequestmodalval == '1')
                	{
                		return $query->where('US.plan', 'like', "%".$csv_filter."%")->where('US.end_date', '>=', date('Y-m-d'));
                	}
                	else if($modalval && $newrequestmodalval == '2')
                	{
                		return $query->where('US.plan', 'like', "%".$csv_filter."%")->where('US.end_date', '<', date('Y-m-d'));
                	}
                	else
                	{
                		return $query->where('US.plan', 'like', "%".$csv_filter."%")->where('US.end_date', '>=', date('Y-m-d'));
                	}
                })
                ->when($isFilterExpired, function ($query) use ($csv_filter) {
                    return $query->whereDate('US.end_date', '<', date('Y-m-d') );
                })
                ->when($isAllFilter, function ($query) use ($modalval,$newrequestmodalval) {
                    if($modalval && $newrequestmodalval == '1')
                	{
                		return $query->where('US.end_date', '>=', date('Y-m-d'));
                	}
                	else if($modalval && $newrequestmodalval == '2')
                	{
                		return $query->where('US.end_date', '<', date('Y-m-d'));
                	}
                })
                ->orderBy('U.id','desc')
                ->get();

        // echo '<pre>'; print_r(DB::getQueryLog()); die;
	    
	    $columns = array('#', 'Name', 'Email', 'Date');

	    $callback = function() use ($reviews, $columns) {
	        $file = fopen('php://output', 'w');
	        fputcsv($file, $columns);

	        foreach($reviews as $key => $review) {
	            fputcsv($file, array(++$key, $review->name, $review->email, date_format(date_create($review->created_at), 'd-m-Y') ) );
	        }
	        fclose($file);
	    };

	    return response()->stream($callback, 200, $headers);
	}
}
