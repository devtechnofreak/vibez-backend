<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Promocodes;
use App\Artist;

class PromocodeController extends Controller
{
	public function add($id=null){
		//$artists = Artist::whereIn('status',[0,1])->get();
		$artists = \DB::table('artists')->whereIn('status', array(0, 1))->get();
		return view('admin.promocodes.add',compact('artists')); 
	}
	public function editPromocode($id){
		$artists = Artist::all();
		$promocode = Promocodes::find($id);
		return view('admin.promocodes.edit',compact('artists','promocode'));
	}
	public function create(Request $request){
		$validation =$request->validate([
            'code' 			=> 'required|string|max:255|unique:promocodes',
            'artist_id' 	=> 'required',
            'start_date'	=> 'required',
        ]);
		if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
			
			$news = Promocodes::create([
				'code'			=> $request->code,
				'artist_id'		=> $request->artist_id,
				'start_date'	=> $request->start_date ? date('Y-m-d',strtotime($request->start_date)) : '',
				'end_date'		=> $request->end_date ? date('Y-m-d',strtotime($request->end_date)) : '',
			]);
			return redirect()->back()->with('message', 'Promocode created successfully!');
		}
	}
	public function updatePromocode(Request $request){
		$validation =$request->validate([
            'code' 			=> 'required',
            'artist_id' 	=> 'required',
            'start_date'	=> 'required',
            'promocode_id'	=> 'required'
        ]);
		if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
			$Promocode = Promocodes::find($request->promocode_id);
			
			$Promocode_data = array(
					'code'			=> $request->code,
					'artist_id'		=> $request->artist_id,
					'start_date'	=> $request->start_date ? date('Y-m-d',strtotime($request->start_date)) : '',
					'end_date'		=> $request->end_date ? date('Y-m-d',strtotime($request->end_date)) : '',
				);
			$Promocode->update($Promocode_data);
			return redirect()->back()->with('message', 'Promocode created successfully!');
		}
	}
	public function index(){
		return view('admin.promocodes.index');
	}
	public function getPromocode(Request $request){ 
		$search = $request->input( 'search' );
		$start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' ); 
		if (isset($search['value']) && $search['value'] != "") { 
			
			$searchTerm = $search['value'];
            $promocodes = Promocodes::select('*')->where(function ($query) use ($searchTerm) {
															$query->where('code', 'like', "%".$searchTerm."%");
														})
												->orderBy('id', 'DESC')
												->skip($start)->take($length)
												 ->get();
			 $allpromocodes = Promocodes::select('id')->where(function ($query) use ($searchTerm) {
															$query->where('code', 'like', "%".$searchTerm."%");
														})
												->orderBy('id', 'DESC')
												->count();
           
        }else{
			$promocodes = Promocodes::select('*')->orderBy('id', 'DESC')->skip($start)->take($length)->get();
			$allpromocodes = Promocodes::select('id')->count();
		}
		
        // Here are the parameters sent from client for paging 
          //  Get length record from start
		
		$data = array ();
        foreach ( $promocodes as $promocode ) {
        	//$artist_name = Artist::where('id',$promocode->artist_id)->first();
        	$artist_name= \DB::table('artists')->where('id', $promocode->artist_id)->first();
        	//$artist_name = Artist::where('id',$promocode->artist_id)->first();
            $nestedData = array ();
            $nestedData ['code'] = $promocode->code;
			$nestedData ['artist']= $artist_name->name;
			$nestedData ['start_date'] = ($promocode->start_date != "") ? date('d-m-Y',strtotime($promocode->start_date)) : "";
			$nestedData ['end_date'] = ($promocode->end_date != "") ? date('d-m-Y',strtotime($promocode->end_date)) : "";
			$nestedData ['id'] = $promocode->id; 
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $allpromocodes ), // total number of records
                "recordsFiltered" => intval ( $allpromocodes ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}
	public function deletePromocode($id){
		Promocodes::where('id', $id)->delete();
		return redirect()->back()->with('message', 'Promocode successfully deleted!');
	}
	public function bulkPromocodeAction(Request $request){
		$action = $request->action;
		$promocodes = $request->promocode;
		if($action == "delete"){
			if(sizeof($promocodes) > 0){
				foreach ($promocodes as $single_promocodes) {
					Promocodes::where('id', $single_promocodes)->delete();
				}
			}
		}
		return array("status"	=> 1); 
	}
}
