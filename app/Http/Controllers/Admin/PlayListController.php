<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Playlist;
use App\User;
use App\Music;
use \Gumlet\ImageResize;
use Illuminate\Support\Facades\Storage;
use DB, Notification;
use App\Notifications\PushNotification;
use Auth;


class PlayListController extends Controller
{
    public function index(){
		return view('admin.playlists.index');
	}
	
    public function trash(){
		return view('admin.playlists.trash');
	}	
	
	public function add(){
		$users = User::get();
		
		/*$musics = Music::get();*/

		$musics = DB::table('musics')
					->leftJoin('music_artists', 'musics.id', '=', 'music_artists.music_id')
					->leftJoin('artists', 'music_artists.artist_id', '=', 'artists.id')
					->select(
					    'musics.id AS id',
					    'musics.title AS title',
					    'artists.name as artist'
					)
					->get();

		/*$musics = Music::select(DB::raw('musics.id AS id','musics.title AS title','music_meta.artist AS artist'))->leftJoin('music_meta','musics.id','=','music_meta.music_id')->get();*/

		return view('admin.playlists.add',compact('users','musics')); 
	}

	public function updateOrder(){
		
		$position = $_POST['position'];
		$i=1;
		foreach($position as $k=>$v){
		    $playlist = Playlist::find($v['uniq_id']);
			$playlist_data = array(
					'order_id' 		=> $v['position'],
			);
			
			$playlist->update($playlist_data);
		}
	}

	public function edit($id){
		$playlist = Playlist::find($id);
		
		/*$musics = Music::all();*/

		$musics = DB::table('musics')
					->leftJoin('music_artists', 'musics.id', '=', 'music_artists.music_id')
					->leftJoin('artists', 'music_artists.artist_id', '=', 'artists.id')
					->select(
					    'musics.id AS id',
					    'musics.title AS title',
					    'artists.name as artist'
					)
					->get();
		$featured_artist  = DB::table('playlist_musics AS A')
					->leftJoin('music_artists as B', 'A.music_id', '=', 'B.music_id')
					->leftJoin('artists as C', 'B.artist_id', '=', 'C.id')
					->select('C.name as artist_name')
					->where('A.playlist_id','=',$id)
					->groupBy('B.artist_id')
					->orderBy('C.name','asc')
					->get();			
		return view('admin.playlists.edit',compact('playlist','musics','featured_artist'));
	}
	public function reorderMusics(Request $request){
		$id = $request->playlist;
		$music_ids = $request->music;
		$x = 1;
		foreach ($music_ids as $single_music) {
			$getMusic = DB::table('playlist_musics')
            ->where('playlist_id', $id)
			->where('music_id', $single_music)
            ->update(['position' => $x]); 
			$x++;
		}  
	}	
	public function create(Request $request){
		$validation = $request->validate([
            'title' 	=> 'required|string|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'music_id'=>'required|array'
        ]);
		
        if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
			if($request->hasfile('image')){
				$file = $request->file('image');
				$imageName = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				$thumbname = basename($imageName,'.'.$extension);
				$thumb = new ImageResize($file);
				Storage::disk('s3')->put('images/playlists/'.$imageName, file_get_contents($file->getRealPath()));
				$thumb->resizeToWidth(300);
				Storage::disk('s3')->put('images/playlists/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
				$image = 'images/playlists/'.$imageName;
			}
			
			$playlist = Playlist::create([
				'title' 		=> $request->title,
				'image'	 		=> $image?:'',
				'description'	=> $request->input('description')?:'',
				'user_id'		=> 1,
			]);


			// $playlist_id = $playlist_created->id;
			$playlist_id = $playlist->id;

			$fynd_playlist = Playlist::find($playlist_id);
			$fynd_playlist->order_id = $playlist_id;
			$fynd_playlist->save();
			
			$playlist->musics()->attach($request->music_id);

			return redirect()->back()->with('message', 'Playlist created successfully!');
		}
	}

	public function bulkTrashPlaylistAction(Request $request){
		$action = $request->action;
		$playlists = $request->playlists;
		
		if($action == "restore"){
			if(sizeof($playlists) > 0){
				foreach ($playlists as $single_playlist) {
					$playlist = Playlist::onlyTrashed()
			                ->where('id', $single_playlist)->first();
					$playlist->banners()->restore();
					$playlist->restore();
				}
			}
		}
		if($action == "delete"){
			if(sizeof($playlists) > 0){ 
				foreach ($playlists as $single_playlist) {
					$playlist = Playlist::onlyTrashed()
               					 ->where('id', $single_playlist)->first();
					if($playlist->getOriginal('image')):
						$path = pathinfo($playlist->getOriginal('image'));
						
						Storage::disk('s3')->delete($playlist->getOriginal('image'));
						Storage::disk('s3')->delete($path['dirname'].'/'.$path['filename'].'_thumb.'.$path['extension']);
					endif;
					$playlist->musics()->detach();
					$playlist->banners()->forceDelete();
					$playlist->forceDelete();
				}
			}
		}
		return array("status"	=> 1);
	}
	
	public function bulkPlaylistAction(Request $request){
		$action = $request->action;
		$playlists = $request->playlists;
		
		if($action == "trash"){
			if(sizeof($playlists) > 0){
				foreach ($playlists as $single_playlist) {
					$playlist = Playlist::find($single_playlist);
					$playlist->banners()->delete();		
					$playlist->delete();
				}
			}
		}
		if($action == "delete"){
			if(sizeof($playlists) > 0){ 
				foreach ($playlists as $single_playlist) {
					$playlist = Playlist::find($single_playlist);
					if($playlist->getOriginal('image')):
						$path = pathinfo($playlist->getOriginal('image'));
						
						Storage::disk('s3')->delete($playlist->getOriginal('image'));
						Storage::disk('s3')->delete($path['dirname'].'/'.$path['filename'].'_thumb.'.$path['extension']);
					endif;
					$playlist->musics()->detach();
					$playlist->banners()->forceDelete();
					$playlist->forceDelete();
				}
			}
		}
		return array("status"	=> 1);
	}

	public function update(Request $request){
		$validation =$request->validate([
			'playlist_id' => 'required|integer',
            'title' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'music_id'=>'required|array'
        ]);
		if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{



        	$playlist = Playlist::find($request->playlist_id);
			
			if($request->hasfile('image')){
				$file = $request->file('image');
				$imageName = time().'.'.$file->getClientOriginalExtension();
				$extension = $file->getClientOriginalExtension();
				$thumbname = basename($imageName,'.'.$extension);
				$thumb = new ImageResize($file);
				Storage::disk('s3')->put('images/playlists/'.$imageName, file_get_contents($file->getRealPath()));
				$thumb->resizeToWidth(300);
				Storage::disk('s3')->put('images/playlists/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
				$image = 'images/playlists/'.$imageName;
			}else{
				$image = $playlist->getOriginal('image');
			}
			
			$playlist_data = array(
				'title'	=> $request->title,
				'image'	=> $image,
				'description'=>$request->input('description')?:'',
				'updated_at' => date('Y-m-d H:i:s'),
				'is_pushed'   => $request->input('is_pushed')?:0,
			);
			

			$old_music_data = collect($playlist->musics)->pluck('id')->all();
			
			
			$new_music = $request->music_id;
			sort($old_music_data);
			sort($new_music);
			$flag = 1;
			if($new_music != $old_music_data){
				$flag = 2;
			}

			$playlist->update($playlist_data);
			$playlist->musics()->sync($request->music_id);

			$playlist_pay_arr = array(
				'id'			=> $playlist->id,
				'user_id'		=> $playlist->user_id,
				'title'			=> $playlist->title,
				'image'			=> $playlist->db_image,		 
				'description' 	=> $playlist->description,
				'thumb'			=> $playlist->db_thumb_image,
				'user_name' 	=> $playlist->user_name,
			);

			$message['title'] = 'Vibez';
			$message['message'] = "Playlist updated: '".$request->title."'";
			$message['click_action'] = 'vibez.playlist.update';
			$message['playlist'] = $playlist_pay_arr;

			$users = $playlist->followers;
			if(($flag == 2) && (!$request->is_pushed)){
				Notification::send($users, (new PushNotification($message))->onQueue('audio'));
			}
		}			
		return redirect()->back()->with('message', 'Playlist updated!'); 
	}


	public function sendPushNotification(Request $request){

		$playlist_id = $request->playlist_id;
		$playlist = Playlist::find($playlist_id);
		$notificationtype = $request->notificationtype;
		if($notificationtype == 'custom'){
			$playlist_title = $request->playlist_title;
			$playlist_description = $request->playlist_description;
		}
		else{
			$playlist_title = 'Playlist updated: '.$playlist->title;
			$playlist_description = $playlist->description;
		}
		if(isset($playlist_id) && $playlist_id != ""){
			
			$selectedUser = User::all();
			//$selectedUser = User::whereIn('id', ['480','9','366'])->get();
			$playlist_pay_arr = array(
				'id'			=> $playlist->id,
				'user_id'		=> $playlist->user_id,
				'title'			=> $playlist->title,
				'image'			=> $playlist->db_image,		 
				'description' 	=> $playlist->description,
				'thumb'			=> $playlist->db_thumb_image,
				'user_name' 	=> $playlist->user_name,
			);
			$message = array();
			$message['title'] = 'Vibez';
			$message['message'] = $playlist_title;
			$message['click_action'] = 'vibez.playlist.update'; 
			$message['playlist'] = $playlist_pay_arr;
			Notification::send($selectedUser, (new PushNotification($message))->onQueue('audio'));
			return redirect()->back()->with('message','Pushnotification sent successfully!');
		}
	}
	
	public function delete($id){
		$playlist = Playlist::find($id);
		$playlist->banners()->delete();		
		$playlist->delete();
			 
		return redirect()->back()->with('message', 'Playlist has been moved to trash successfully!');  
	}
	
	public function restorePlaylist($id)
	{
		$playlist = Playlist::onlyTrashed()
                ->where('id', $id)->first();
		$playlist->banners()->restore();
		$playlist->restore();

		return redirect()->action('Admin\PlayListController@index')->with('message', 'Playlist and related data restored Successfully!');
	}
	
	public function deletePermanentPlaylistData($id)
	{
		$playlist = Playlist::onlyTrashed()
                ->where('id', $id)->first();
		if($playlist->getOriginal('image')):
			$path = pathinfo($playlist->getOriginal('image'));
			
			Storage::disk('s3')->delete($playlist->getOriginal('image'));
			Storage::disk('s3')->delete($path['dirname'].'/'.$path['filename'].'_thumb.'.$path['extension']);
		endif;
		$playlist->musics()->detach();
		$playlist->banners()->forceDelete();
		$playlist->forceDelete();
		return redirect()->back()->with('message','Playlist has been deleted successfully');
	}
	
	public function getPlaylists(Request $request){
		$user_id = $request->user_id;
		$search = $request->input( 'search' );
		 $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
       
        $order = $request->input ( 'order' );
		
		/*$column = 'id';
		$dir = 'DESC';*/
		$column = 'order_id';
		$dir = 'ASC';
		if(is_array($order) && sizeof($order) > 0){
			if(isset($order[0]['column']) && isset($order[0]['dir'])){
				switch ($order[0]['column']) {
				    case "3":
				        $column = "title";
						$dir = $order[0]['dir'];
				        break;
				    default:
				        $column = "order_id";
						$dir = "ASC";
				}
			}
		}
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
 			$playlists = PlayList::where('title','like',"%".$searchTerm."%")
 						->where('is_admin','1')
 						->orderBy($column,$dir)
 						->skip($start)->take($length)
 						->get();
			$allplaylists = PlayList::where('title','like',"%".$searchTerm."%")->where('user_id',$user_id)->count();
           
        }else{
				$playlists = PlayList::where('is_admin','1')->orderBy($column,$dir)->skip($start)->take($length)->get();
				$allplaylists = PlayList::where('is_admin','1')->count();
		}
		
		$totalData = $allplaylists;            //Total record
        $totalFiltered = $playlists->count();      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
       
		
		$data = array ();
        foreach ( $playlists as $playlist ) {
            $nestedData = array ();
            $nestedData ['user_name'] = $playlist->user?$playlist->user->name:'admin';
			$nestedData ['title'] = $playlist->title;
			$nestedData ['image'] = $playlist->thumb;
			$nestedData ['description'] = $playlist->description;
			$nestedData ['musics'] = collect($playlist->musics)->pluck('title')->all();
			$nestedData ['id'] = $playlist->id; 
			$nestedData ['DT_RowId'] = $playlist->id; 
			$nestedData ['user_id'] = $playlist->user_id; 
            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalData ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}

	public function getTrashedPlaylists(Request $request){
		$search = $request->input( 'search' );
		$start = $request->input ( 'start' );   
		$user_id = $request->user_id;        // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
 			$playlists = PlayList::onlyTrashed()
 						->where('title','like',"%".$searchTerm."%")
 						->where('user_id',$user_id)
 						->orderBy('id', 'DESC')
 						->skip($start)->take($length)
 						->get();
           $allplaylists = PlayList::onlyTrashed()
 						->where('title','like',"%".$searchTerm."%")
 						->where('user_id',$user_id)
						->count();
        }else{
				$playlists = PlayList::onlyTrashed()->where('user_id',$user_id)->orderBy('id', 'DESC')->skip($start)->take($length)->get();
				$allplaylists= PlayList::onlyTrashed()->where('user_id',$user_id)->count();
		}
		
		$totalData = $playlists->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        
		
		$data = array ();
        foreach ( $playlists as $playlist ) {
			
            $nestedData = array ();
            $nestedData ['user_name'] = $playlist->user?$playlist->user->name:'admin';
			$nestedData ['title'] = $playlist->title;
			$nestedData ['image'] = $playlist->thumb;
			$nestedData ['description'] = $playlist->description;
			$nestedData ['musics'] = collect($playlist->musics)->pluck('title')->all();
			$nestedData ['id'] = $playlist->id; 
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $allplaylists ), // total number of records
                "recordsFiltered" => intval ( $allplaylists ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
	}

	public function removefromhome($id){
		$fynd_playlist = Playlist::find($id);
		$fynd_playlist->is_admin = '0';
		$fynd_playlist->save();
		return redirect()->back()->with('message', 'Playlist removed successfully!'); 
        
	}
}
