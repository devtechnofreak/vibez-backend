<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;
use App\Report;
use App\userMeta;
use DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;
class ReportController extends Controller
{
    public function index(){
		return view('admin/reports/index');
	}
	
    public function edit($id){
		return view('admin/reports/edit',compact('id'));
	}
	
	public function update(Request $request){
        $validation = $request->validate([
            'description' => 'required',
            'report_id' => 'required',
        ]);
        if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }
		
		$report = Report::find($request->report_id);
		
    	$data = array(
				'feedback' 			=> $request->description, 
            );
		$report->update($data);
		$data = array('mail'=>$report->reportedUser->email,'name'=>$report->reportedUser->name,'message'=>$request->description,'id'=>$report->id);
		try {
		 Mail::send([],[], function ($message) use($data){
		            $message->from('noreply@thevibez.com','Vibez');
		            $message->subject('Feedback for report #'.$data['id']);
		            $message->to($data['mail'], $data['name']);
					$message->setBody($data['message'], 'text/html');
		 });
		}catch(\Exception $e){
			return redirect()->back()->withErrors([$e->getMessage()]);
		}
		return redirect()->back()->with('message', 'Feedback has been send successfully!');
	}

	public function getReports(Request $request){
		
		$reports = Report::with(['reportingUser','reportedUser'])->orderBy('created_at','DESC')->get();
		
		$totalFiltered = $totalData = $reports->count();            //Total record
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
		
		$data = array ();
        foreach ( $reports as $report ) {
			
            $nestedData = array ();
            $nestedData ['from_user'] = $report->reportingUser->name;
            $nestedData ['to_user'] = $report->reportedUser->name;
			$nestedData ['description'] = $report->description;
			$nestedData ['feedback'] = $report->feedback;
			$nestedData ['created_at'] = $report->created_at->format('d / m / Y');
			$nestedData ['id'] = $report->id;
            $data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;
		
	}
}
