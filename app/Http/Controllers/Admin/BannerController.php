<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Banner;
use App\Music;
use App\Video;
use App\News;
use App\Playlist;
use App\Artist;
use App\Album;
use App\LiveStream;
use Illuminate\Support\Facades\Storage;
use \Gumlet\ImageResize;
use DB;

use App\BannerView;

class BannerController extends Controller
{
    public function index(){
		return view('admin.banners.index');
	}
	
    public function trash(){
		return view('admin.banners.trash');
	}	
	
	public function add(){
		return view('admin.banners.add'); 
	}

	public function updateOrder(){
		
		$position = $_POST['position'];
		$i=1;
		foreach($position as $k=>$v){
		    $banner = Banner::find($v['uniq_id']);
			$banners_data = array(
					'order_id' 		=> $v['position'],
			);
			
			$banner->update($banners_data);
		}
	}
	
	public function getBanners(Request $request) {
		$search = $request->input( 'search' );
		 $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        $order = $request->input ( 'order' );
        $column = 'order_id';
		$dir = 'ASC';
		if(is_array($order) && sizeof($order) > 0){
			if(isset($order[0]['column']) && isset($order[0]['dir'])){
				switch ($order[0]['column']) {
				    case "2":
				        $column = "title";
						$dir = $order[0]['dir'];
				        break;
					 case "3":
				        $column = "type";
						$dir = $order[0]['dir'];
				        break;
				    default:
				        $column = "order_id";
						$dir = "ASC";
				}
			}
		}
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
            $banners = Banner::select('title','image','type','id','bannerable_id','bannerable_type','typeid')->where(function ($query) use ($searchTerm) {
									$query->where('title', 'like', "%".$searchTerm."%");
								})
								->orderBy($column,$dir)
								->skip($start)->take($length)
								->get();
            $allbanner = Banner::select('title','image','type','id','bannerable_id','bannerable_type','typeid')->where(function ($query) use ($searchTerm) {
									$query->where('title', 'like', "%".$searchTerm."%");
								})
								->count();
        }else{
			$banners = Banner::select('title','image','type','id','bannerable_id','bannerable_type','typeid')->orderBy($column,$dir)->skip($start)->take($length)->get();
			$allbanner = Banner::select('title','image','type','id','bannerable_id','bannerable_type','typeid')->count();
		}
		
		$totalData = $allbanner;            //Total record
        $totalFiltered = $banners->count();      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
       
		
		$data = array ();


        foreach ( $banners as $banner ) {

        	$nestedData = array ();
			$nestedData ['title'] = $banner->title;
            $nestedData ['image'] = $banner->thumb;
			$nestedData ['type'] = $banner->type;
			/*$nestedData ['item']['name'] = isset($banner->bannerable->name)?$banner->bannerable->name:$banner->bannerable->title;*/
			$nestedData ['item']['name'] = isset($banner->bannerable->name)?$banner->bannerable->name:$banner->title;
			$nestedData ['item']['type'] = $banner->type;
			$nestedData ['item']['bannerable_id'] = $banner->bannerable_id;
			$nestedData ['id'] = $banner->id; 
			$nestedData ['cnt'] = BannerView::where('banner_id', $banner->id)->count(); 
			$nestedData ['DT_RowId'] = $banner->id; 
			$data [] = $nestedData; 
        }

        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalData ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;

	}

	public function bulkTrashBannersAction(Request $request){
		$action = $request->action; 
		$banners = $request->banners;
		if($action == "restore"){
			if(sizeof($banners) > 0){
				foreach ($banners as $single_banner) {
					$banner = Banner::onlyTrashed()
				                ->where('id', $single_banner)->first();
						$banner->restore();
				}
			}
		}
		if($action == "delete"){
			if(sizeof($banners) > 0){
				foreach ($banners as $single_banner) {
					$banner = Banner::onlyTrashed()
						        ->where('id', $single_banner)->first();
					if($banner->getOriginal('image')):
						$imagePath = pathinfo($banner->getOriginal('image'));	
						Storage::disk('s3')->delete($banner->getOriginal('image'));
						Storage::disk('s3')->delete($imagePath['dirname'].'/'.$imagePath['filename'].'_thumb.'.$imagePath['extension']);
					endif;
					
					$banner->forceDelete();
				}
			}
		}
		return array("status"	=> 1);
	}

	public function bulkBannersAction(Request $request){
		$action = $request->action;
		$banners = $request->banners;
		
		if($action == "trash"){
			if(sizeof($banners) > 0){
				foreach ($banners as $single_banner) {
					$banner = Banner::find($single_banner);
					$banner->delete(); 
				}
			}
		}
		if($action == "delete"){
			if(sizeof($banners) > 0){
				foreach ($banners as $single_banner) {
					$banner = Banner::find($single_banner);
					if($banner->getOriginal('image')):
						$imagePath = pathinfo($banner->getOriginal('image'));	
						Storage::disk('s3')->delete($banner->getOriginal('image'));
						Storage::disk('s3')->delete($imagePath['dirname'].'/'.$imagePath['filename'].'_thumb.'.$imagePath['extension']);
					endif;
					$banner->forceDelete();
				}
			}
		}
		return array("status"	=> 1);
	}

	public function getTrashedBanners(Request $request) {
		$search = $request->input( 'search' );
		if (isset($search['value']) && $search['value'] != "") {
			
			$searchTerm = $search['value'];
            $banners = Banner::onlyTrashed()->select('title','image','type','id','bannerable_id','bannerable_type','typeid')->where(function ($query) use ($searchTerm) {
									$query->where('title', 'like', "%".$searchTerm."%");
								})
								->get();
           
        }else{
			$banners = Banner::onlyTrashed()->select('title','image','type','id','bannerable_id','bannerable_type','typeid')->get();
		}
		
		$totalData = $banners->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
		
		$data = array ();


        foreach ( $banners as $banner ) {
            $nestedData = array ();
			$nestedData ['title'] = $banner->title;
            $nestedData ['image'] = $banner->thumb;
			$nestedData ['type'] = $banner->type;
			
			$nestedData ['id'] = $banner->id; 
			$data [] = $nestedData; 
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        return $tableContent;

	}
	
	public function create(Request $request){
		
		$validation =$request->validate([
            'title'=> 'required|string|max:255',
            'image'=> 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'type' => 'required',
            'item' => 'required',
        ]);
		
        if (!is_array($validation)) {
            return redirect()->back()->withErrors($validation->errors());
        }else{
			if($request->hasfile('image')) {
				$image = $request->file('image');
				$imageName = $image->getClientOriginalName();
				$extension = $image->getClientOriginalExtension();
				$thumbname = basename($imageName,'.'.$extension);
				$thumb = new ImageResize($image);
				Storage::disk('s3')->put('banners/'.$imageName, file_get_contents($image->getRealPath()));
				$thumb->scale(50);
				Storage::disk('s3')->put('banners/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());

				$original_thumb = new ImageResize($image);
				$original_thumb->resizeToWidth(600);
				Storage::disk('s3')->put('banners/'.$thumbname.'_600.'.$extension, $original_thumb->getImageAsString());

			} else {
				$imageName ='';
			}
			$banner_created = Banner::create([
				'title' 		=> $request->title,
				'image'	 		=> 'banners/'.$imageName,
				'type'			=> $request->type,
				'bannerable_type'=> 'App\\'.$request->type,
				'typeid'		=> $request->item,
				'bannerable_id'=>$request->item,
			]);

			$banner_id = $banner_created->id;

			$fynd_banner = Banner::find($banner_id);
			$fynd_banner->order_id = $banner_id;
			$fynd_banner->save();
			
			return redirect()->back()->with('message', 'Banner created!');
		}
	}
	
	public function  edit($id){
		$banner = Banner::find($id);
	
		return view('admin.banners.edit',compact('banner'));
	}
	
	public function update(Request $request){
		
		$validation =$request->validate([
		'banner_id'=> 'required|integer',
            'title'=> 'required|string|max:255',
            'image'=> 'image|mimes:jpeg,png,jpg,gif,svg',
            'type' => 'required',
            'item' => 'required',
        ]);
		$banner = Banner::find($request->banner_id);
		if($request->hasfile('image')) {
			$image = $request->file('image');
			$imageName = $image->getClientOriginalName();
			$extension = $image->getClientOriginalExtension();
			$thumbname = basename($imageName,'.'.$extension);
			$thumb = new ImageResize($image);
			Storage::disk('s3')->put('banners/'.$imageName, file_get_contents($image->getRealPath()));
			$thumb->scale(50);
			Storage::disk('s3')->put('banners/'.$thumbname.'_thumb.'.$extension, $thumb->getImageAsString());
			
			$original_thumb = new ImageResize($image);
			$original_thumb->resizeToWidth(600);
			Storage::disk('s3')->put('banners/'.$thumbname.'_600.'.$extension, $original_thumb->getImageAsString());
			
			$imageName = 'banners/'.$imageName;

		} else {
			$imageName = $banner->getOriginal('image');
		}		
		$banners_data = array(
				'title' 		=> $request->title,
				'image'	 		=> $imageName,
				'type'			=> $request->type,
				'bannerable_type'=> 'App\\'.$request->type,
				'typeid'		=> $request->item,
				'bannerable_id'=>$request->item,
		);
		
		$banner->update($banners_data);
		
		return redirect()->back()->with('message', 'News updated!'); 
	}

	public function delete($id)
	{
		$banner = Banner::find($id);
		$banner->delete(); 
		
		return redirect()->back()->with('message', 'Banners Deleted!');  
	}
	
	public function deletePermanentBannersData($id)
	{
		$banner = Banner::onlyTrashed()
        ->where('id', $id)->first();
		if($banner->getOriginal('image')):
			$imagePath = pathinfo($banner->getOriginal('image'));	
			Storage::disk('s3')->delete($banner->getOriginal('image'));
			Storage::disk('s3')->delete($imagePath['dirname'].'/'.$imagePath['filename'].'_thumb.'.$imagePath['extension']);
		endif;
		
		$banner->forceDelete();
		return redirect()->back()->with('message', $banner->title.' has been deleted permanently.');
	} 
	
	public function restoreBanner($id)
	{
		$banner = Banner::onlyTrashed()
                ->where('id', $id)->first();
		$banner->restore();
		return redirect()->action('Admin\BannerController@index')->with('message', 'Banner and related data restored Successfully!');
	}	
	
	public function getBannerItems(Request $request){
		
		$item_type = $request->input('type');
		if($item_type != "" || $item_type != NULL){
			switch ($item_type) {
			    case "Album":
			        $data = DB::table('albums')->select('title','id','image')->get()->toArray();
			        break;
			    case "Artist":
			        $data = DB::table('artists')->select('name as title','id','image')->get()->toArray();
			        break;
			    case "Playlist":
			        $data = DB::table('playlists')->select('title','id','image', 'user_id')->get()->toArray();
			        if ($data) {
			        	foreach ($data as $key => $value) {
			        		$data[$key] = $value;
			        		$user = \App\User::find($value->user_id);
			        		if (!is_null($user)) {
			        			$data[$key]->title = $value->title.' - '.$user->name;
			        		}
			        	}
			        }
			        break;
				case "Music":
			        $data = DB::table('musics')->select('title','id','image')->get()->toArray();
			        break;
				case "News":
			        $data = DB::table('news')->select('title','id','image')->get()->toArray(); 
			        break;
				case "Video":
			        $data = DB::table('videos')->select('title','id','image')->get()->toArray(); 
			        break;
			    case "LiveStream":
			        $data = DB::table('live_streams')->select('title','id','image')->get()->toArray(); 
			        break;
			    default:
			       $data = DB::table('videos')->select('title','id','image')->get()->toArray();
			}
			return response()->json(['data' => $data,'status' => 1]);
		}else{ 
			return response()->json(['data' => [],'status' => 0]);
		}
	}

}
