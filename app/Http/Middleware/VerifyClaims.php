<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use App\User;

class VerifyClaims
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$user = User::find(Auth::user()->id);
		if($user->device_id != JWTAuth::payload()->get('device_id')){
			return response()->json(['message'=>'User already logged in from other device.','code'=>403]);
		}

        return $next($request);
    }
}