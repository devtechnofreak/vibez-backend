<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check() && Auth::guard($guard)->user()->role_id != 1) {
        	Auth::guard()->logout();
			$request->session()->invalidate();
            return redirect('/admin/login')->withErrors('message','Unauthorized Access');
        }

        return $next($request);
    }
}
