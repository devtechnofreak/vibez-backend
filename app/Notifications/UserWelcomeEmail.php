<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserWelcomeEmail extends Notification
{
    //Places this task to a queue if its enabled
    use Queueable;

    //Token handler
    public $vibez_id;

    public function __construct($vibez_id)
    {
        $this->vibez_id = $vibez_id;
    }

    //Notifications sent via email
    public function via($notifiable)
    {
        return ['mail'];
    }

    //Content of email sent to the Seller
    public function toMail($notifiable)
    {
    	$today=date('Y-m-d');
		$event=date('2018-12-16');
		$img_src = '<img src="https://www.thevibez.net/assets/img/vibez_subscription.jpg" style="width: 100px;">';


    	if($event > $today) {
		    return (new MailMessage)
			->subject('Welcome to the Vibez Family!')
			->greeting('Hello '.$this->vibez_id.',')
			->line('Welcome to the Vibez Family!')
	        ->line('Our sole purpose is to bring you the best music content from the entire African continent. New music, videos, breaking news, live events and lots more!')
	        ->line('We hope you enjoy the Vibez experience and we look forward to seeing you interact with other Vibez members in the comments sections and even via the inbuilt private messaging platform.')
			->line("We would also love to hear from you about features you'd like to see on Vibez. Feel free to shoot us a mail: ")
			->line('info@vibezapp.net');
			
		}else{
			return (new MailMessage)
			->subject('Welcome to the Vibez Family!')
			->greeting('Hello '.$this->vibez_id.',')
			->line('Welcome to the Vibez Family!')
			->line('Our sole purpose is to bring you the best music content from the entire African continent. New music, videos, breaking news, live events, giveaways and lots more!')
	        ->line('We hope you enjoy the Vibez experience and we look forward to seeing you interact with other Vibez members in the comments sections and even via the inbuilt private messaging platform.')
	        ->line("We would also love to hear from you about features you'd like to see on Vibez. Feel free to shoot us a mail:")
	        ->line("info@vibezapp.net");	
		}
    }
}