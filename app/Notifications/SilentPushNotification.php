<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Log;
use App\Jobs\SendFirebaseSilentNotificationQueue;

class SilentPushNotification extends Notification implements ShouldQueue
{
    use Queueable;
	
	protected $_data;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
    	Log::info('PUSH NOTIFICATION CONSTRUCTOR');
        $this->_data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
     
    
    public function via($notifiable)
    {
		SendFirebaseSilentNotificationQueue::dispatch($notifiable->fcm_token,$this->_data,$notifiable->device_type)->onQueue('audio');
        return false;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->_data;
    }
	
	public function toFcm($notifiable) {
	
	    $message = new \Benwilkins\FCM\FcmMessage();
		if($notifiable->device_type == 'android'){
		    $message->contentAvailable(true)
		    ->content([
		    ])->data([
		        'data' => $this->_data // Optional
		    ])->priority(\Benwilkins\FCM\FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.			
		} else {
		    $message->contentAvailable(true)
		    ->content([
		    ])->data([  
		    	'data' => $this->_data
		    ])->priority(\Benwilkins\FCM\FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.			
		}
	    
	    return $message;
	}
}
