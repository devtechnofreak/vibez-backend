<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SendAlbumCronjobResult extends Notification
{
    //Places this task to a queue if its enabled
    use Queueable;

    //Token handler
    public $token;

    public function __construct($filename)
    {
        $this->filename = $filename;
    }

    //Notifications sent via email
    public function via($notifiable)
    {
        return ['mail'];
    }

    //Content of email sent to the Seller
    public function toMail($notifiable)
    {
    	 echo "I AM HERER";
		 exit;
		if(isset($this->filename) && $this->filename != ""){
			$notifiable->email = 'milanpatel2811@gmail.com';
       		$url = url('/invoice/import/music/'.$this->filename);
			return (new MailMessage)
	                ->greeting('Hello!')
	                ->line('One of your invoices has been paid!')
	                ->action('View Invoice', $url)
	                ->line('Thank you for using our application!');	
		}
	    
    }

}