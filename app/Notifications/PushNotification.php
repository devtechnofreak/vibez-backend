<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
//use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Log;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use App\Jobs\SendFirebaseNotification;
use FCM;

class PushNotification extends Notification
{
    use Queueable;
	
	protected $_data;

    protected $sentNoti = false;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data) {
        $this->_data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if($this->_data['click_action'] == "vibez.message.new"){
            SendFirebaseNotification::dispatch($notifiable->fcm_token,$this->_data,$notifiable->device_type)->onQueue('audio'); 
            return false;
        }
        //$this->sendPushNotification($notifiable->fcm_token,$this->_data,$notifiable->device_type);
        SendFirebaseNotification::dispatch($notifiable->fcm_token,$this->_data,$notifiable->device_type)->onQueue('audio');
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable) {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable) {
        return $this->_data;
    }
	
	public function toFcm($notifiable) {
        return false;
	    $message = new \Benwilkins\FCM\FcmMessage();
		if($notifiable->device_type == 'android'){
            Log::info('PUSH NOTIFICATION : 86');
		    $message->content([
		    ])->data([
		        'data' => $this->_data // Optional
		    ])->priority(\Benwilkins\FCM\FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.			
		} else {
			Log::info('PUSH NOTIFICATION : 92');
		    $message->content([
		        'title'        => $this->_data['title'], 
		        'body'         => $this->_data['message'], 
		        'sound' => "default",
		    ])->data([
		        'data' => $this->_data // Optional
		    ])->priority(\Benwilkins\FCM\FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.			
		} 
	    return $message;
	}
}
