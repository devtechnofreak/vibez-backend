<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	protected $fillable = ['conversation_id', 'from_user','message','read_flag'];
	
	protected static function boot()
  	{
    	static::creating(function (Message $model) {
        	$model->conversation->touch();
    	});
  	}
	
	public function conversation()
	{
    	return $this->belongsTo(Conversation::class);
	}
}
