<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shortcode extends Model
{

	protected $table = 'shortcode';
    
    protected $fillable = [
        'news_id','type', 'text', 'code', 'created_at','updated_at'
    ];
}
