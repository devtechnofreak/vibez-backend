<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\CommentableTrait;
use JamesMills\Watchable\Traits\Watchable;

class LiveStream extends Model
{
	use CommentableTrait, Watchable;
	
	protected $fillable = ['url','image','description','start_time','title','end_time'];
	
	protected $appends = array('thumb','view_count', 'view_link');
	
	protected $with = array('artists');
	
	public function artists()
	{
		return $this->belongsToMany('App\Artist','live_stream_artists')->withTimestamps();
	}
	
	public function getImageAttribute($value)
	{
		return $value?getCloudFrontPrivateUrl($value):'';
	}
	
	public function getThumbAttribute()
	{
		$filePath = pathinfo($this->getOriginal('image'));
		if($this->getOriginal('image')){
			return getCloudFrontPrivateUrl($filePath['dirname'].'/'.$filePath['filename'].'_thumb.'.$filePath['extension']);
		} else {
			return '';
		}		
	}

	public function getViewLinkAttribute()
    {
    	$title = urlencode($this->title);
    	$entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
    	$replacements = array('', '', "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
    	$new_title = str_replace($entities, $replacements, $title);
        return url('share/LiveStream/'.$new_title.'/'.$this->id);
    }
	
	public function getViewCountAttribute()
	{
		return count($this->collectWatchers());
	}
}
