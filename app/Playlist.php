<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
use Auth;
use Overtrue\LaravelFollow\Traits\CanBeFollowed;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;


class Playlist extends Model
{
	use SoftDeletes, CanBeLiked, CanBeFollowed, SearchableTrait;
	
	protected $dates = ['deleted_at'];
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'playlists.title' => 10,
           // 'playlists.description' => 2,
        ],
        'groupBy'=>'playlists.id'
    ];
	
    //protected $with = array('musics');
	protected $appends = array('thumb','music_count','user_name','is_liked','is_followed','follow_count','updated_ago','view_link','db_image','db_thumb_image');
    protected $fillable = [
        'title','description','image','user_id','is_admin','is_pushed','created_at','updated_at','deleted_at','order_id'
    ];
	public function musics(){
		return $this->belongsToMany('App\Music','playlist_musics')->orderBy('playlist_musics.position','ASC')->withTimestamps();
	}
	
	public function user(){
		return $this->belongsTo('App\User');
	}
	
	public function getImageAttribute($value)
	{
		return $value?getCloudFrontPrivateUrl($value):'';
	}
	
    public function banners()
    {
        return $this->morphMany('App\Banner', 'bannerable');
    }
	
	public function getUpdatedAtAttribute($value)
	{
		//return time_elapsed_string($value);
		return $value;
	}
	
	public function getUpdatedAgoAttribute()
	{
		return time_elapsed_string($this->updated_at);
	}
	
	public function getThumbAttribute()
	{
		$filePath = pathinfo($this->getOriginal('image'));
		if($this->getOriginal('image')){
			return getCloudFrontPrivateUrl($filePath['dirname'].'/'.$filePath['filename'].'_thumb.'.$filePath['extension']);
		} else {
			return '';
		}
		
	}
	
	public function getMusicCountAttribute()
	{
		return $this->belongsToMany('App\Music','playlist_musics')->get()->count();
	}
	
	public function getUserNameAttribute()
	{
		if($this->user){
			return  'By '.$this->user->name;
		}
		return  '';
	}
	
    public function getIsLikedAttribute()
    {
        return Auth::user()->hasLiked($this);
    }
	
    public function getIsFollowedAttribute()
    {
        return $this->isFollowedBy(Auth::user());
    }
	
    public function getFollowCountAttribute()
    {
        return $this->followers()->count();
    }
	
    public function getShareLinkAttribute()
    {
        return \Share::load(url('share/Playlist/'.$this->id),$this->title)->services('facebook', 'gplus', 'twitter','whatsapp');
    }
    public function getViewLinkAttribute()
    {
    	$title = urlencode($this->title);
        return url('share/Playlist/'.$title.'/'.$this->id);
    }

    public function getDbImageAttribute()
    {
    	return env('AWS_URL').$this->getOriginal('image');
    }

    public function getDbThumbImageAttribute()
    {
    	$filePath = pathinfo($this->getOriginal('image'));
		if($this->getOriginal('image')){
			return env('AWS_URL').($filePath['dirname'].'/'.$filePath['filename'].'_thumb.'.$filePath['extension']);
		} else {
			return '';
		}
    }
}
