<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $fillable = ['user1', 'user2'];
	
	public function messages()
	{
    	return $this->hasMany('App\Message', 'conversation_id', 'id')->orderBy('created_at','DESC');
	}
	
	public static function getConversation($id)
	{
		return Conversation::whereIn('user1',array($id,auth()->id()))->whereIn('user2',array($id,auth()->id()))->first();
	}
	
	public function markAsRead()
	{
		if($this->messages)
		$this->messages()->update(['read_flag'=>1]);
	}
}
