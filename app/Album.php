<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
use Overtrue\LaravelFollow\Traits\CanBeFollowed;
use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Album extends Model
{
	use SoftDeletes, SearchableTrait, CanBeLiked, CanBeFollowed; 
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'albums.title' => 10,
            'tags.name' => 5,
           // 'albums.description' => 2,
        ],
        'joins' => [
        'album_tags'=>['albums.id','album_tags.album_id'],
            'tags' => ['album_tags.tag_id','tags.tag_id'],
        ],
        'groupBy'=>'albums.id'
    ];
	
	protected $dates = ['deleted_at'];
   // protected $with = array('musics');
	protected $appends = array('thumb','music_count','is_liked','user_name','is_followed','follow_count','view_link','db_image','db_thumb_image');
    protected $fillable = [
        'title','description','image','album_status','release_year','related_artist'
    ];
	public function musics(){
		return $this->belongsToMany('App\Music','album_musics')->orderByRaw('sort_order = 0, sort_order')->withTimestamps();
	}
	
	public function artists(){
		return $this->belongsToMany('App\Artist','artist_albums')->where('is_main_artist','=','1')->withTimestamps();
	}
	
    public function banners()
    {
        return $this->morphMany('App\Banner', 'bannerable');
    }
	
	public function tags(){
		return $this->belongsToMany('App\Tag','album_tags','album_id','tag_id','id','tag_id')->withTimestamps();
	}
	
	public function getImageAttribute($value)
	{
		return getAwsPrivateUrl($value);
	}
	
	public function getUpdatedAtAttribute($value)
	{
		return time_elapsed_string($value);
	}
	public function getUserNameAttribute()
	{
		$artist = $this->belongsToMany('App\Artist','artist_albums')->first();
		if($artist){
			return  'By '.$artist->name;
		}
		return  '';
	}	
	public function getThumbAttribute()
	{
		$image = explode('.',$this->getOriginal('image'));
		return getAwsPrivateUrl('albums/'.basename($this->getOriginal('image'),'.'.end($image)).'_thumb.'.end($image));
	}
	
    public function getIsFollowedAttribute()
    {
        return $this->isFollowedBy(Auth::user());
    }
	
    public function getFollowCountAttribute()
    {
        return $this->followers()->count();
    }
		
	public function getMusicCountAttribute()
	{
		return $this->belongsToMany('App\Music','album_musics')->count();
	}
	
    public function getIsLikedAttribute()
    {
        return Auth::user()->hasLiked($this);
    }
    public function getViewLinkAttribute()
    {
    	$title = urlencode($this->title);
    	$entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
    	$replacements = array('', '', "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
    	$new_title = str_replace($entities, $replacements, $title);
        return url('share/Album/'.$new_title.'/'.$this->id);
    }

    public function getDbImageAttribute()
    {
    	return env('AWS_URL').$this->getOriginal('image');
    }

    public function getDbThumbImageAttribute()
    {
    	$filePath = pathinfo($this->getOriginal('image'));
		if($this->getOriginal('image')){
			return env('AWS_URL').($filePath['dirname'].'/'.$filePath['filename'].'_thumb.'.$filePath['extension']);
		} else {
			return '';
		}
    }
	
}
