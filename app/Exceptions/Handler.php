<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
	    \Illuminate\Auth\AuthenticationException::class,
	    \Illuminate\Auth\Access\AuthorizationException::class,
	    \Symfony\Component\HttpKernel\Exception\HttpException::class,
	    \Illuminate\Database\Eloquent\ModelNotFoundException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
    	//return parent::render($request, $exception); 
        if ($request->is('api/*')) {
			if($exception instanceof UnauthorizedHttpException) {
				$status = 403;
			}else{
				$status = 401;
			}
	    	return response()->json([
	          'code' => $status,
	          'message' => $exception->getMessage()
	        ]);
		} else {
            
            if($this->isHttpException($exception)){
                switch ($exception->getStatusCode())
                {
                    case 404:
                        return redirect()->route('404');
                    default: 
                        return $this->renderHttpException($exception);
                    break;
                }
            }else{
                return parent::render($request, $exception);
            }
            
		}
	}
}
