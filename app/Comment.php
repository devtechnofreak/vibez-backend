<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
	use CanBeLiked, SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $dates = ['deleted_at'];
    protected $fillable = ['user_id', 'body'];
	
	protected $with = ['author'];
	
	protected $hidden =['commentable_id','commentable_type'];
	protected $appends = array('is_liked','total_likes');

    /**
     * Relationship: author
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Relationship: commentable models
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function commentable()
    {
        return $this->morphTo();
    }
	
    public function getIsLikedAttribute()
    {
        return Auth::user()->hasLiked($this);
    }
	
    public function getTotalLikesAttribute()
    {
        return $this->likers()->count();
    }
	
	public function getCreatedAtAttribute($value)
	{
		return time_elapsed_string($value);
	}
}
