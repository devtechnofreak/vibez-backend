<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Overtrue\LaravelFollow\Traits\CanBeFollowed;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
use Auth;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\ActiveScope;

class Artist extends Model
{
	use SoftDeletes, SearchableTrait, CanBeFollowed, CanBeLiked; 
	
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'artists.name' => 10,
    		'tags.name' => 5,
        ],
        'joins' => [
        'artist_tags'=>['artists.id','artist_tags.artist_id'],
            'tags' => ['artist_tags.tag_id','tags.tag_id'],
        ],
        'groupBy'=>'artists.id'
    ];
	protected $dates = ['deleted_at'];
    //protected $with = array('tags');
    protected $fillable = [
        'name', 'image','dob', 'status','is_featured','bio','country'
    ];
	protected $appends =['is_followed','follow_count','is_liked','country_flag','thumb'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope);
    }

	public function getImageAttribute($value)
	{
		//return env('APP_URL').$value;
		if($this->getOriginal('image')!='')
		{
			$path = pathinfo($this->getOriginal('image'));	
			return getCloudFrontPrivateUrl('Artists'.'/'.$path['filename'].'.'.$path['extension']);
		}
		else
		{
			return "";
		}
	}

	public function getThumbAttribute()
	{
		if($this->getOriginal('image')!='')
		{
			$path = pathinfo($this->getOriginal('image'));	
			return getCloudFrontPrivateUrl('Artists'.'/'.$path['filename'].'_thumb.'.$path['extension']);
		}
		else
		{
			return "";
		}
	}
	
    public function banners()
    {
        return $this->morphMany('App\Banner', 'bannerable');
    }
	
	public function albums(){
		return $this->belongsToMany('App\Album','artist_albums')->withTimestamps();
	}
	
	public function musics(){
		return $this->belongsToMany('App\Music','music_artists')->withTimestamps();
	}
	public function getLatestAlbums(){
		//return $this->belongsToMany('App\Album','artist_albums')->OrderBy('updated_at','DESC')->paginate(10);
		return $this->belongsToMany('App\Album','artist_albums')->where('is_main_artist','=','1')->OrderBy('updated_at','DESC')->paginate(10);
	}
	public function getTrendingMusics(){
		return $this->belongsToMany('App\Music','music_artists')->OrderBy('updated_at','DESC')->paginate(10);
	}
	
	public function getLatestVideos(){
		return $this->belongsToMany('App\Video','video_artists')->OrderBy('updated_at','DESC')->paginate(10);
	}
	
	public function getLatestNews(){
		return $this->belongsToMany('App\News','artist_news')->OrderBy('updated_at','DESC')->paginate(5);
	}
	
	public function getLivestreams(){
		//return $this->hasMany('App\LiveStream')->OrderBy('updated_at','DESC')->paginate(10); 
		return $this->belongsToMany('App\LiveStream','live_stream_artists')->where('end_time','>=',date('Y-m-d').' 00:00:00')->OrderBy('updated_at','DESC')->paginate(10);
	}
	
	public function videos(){
		return $this->belongsToMany('App\Video','video_artists')->withTimestamps();
	}
	
	public function news(){
		return $this->belongsToMany('App\News','artist_news')->withTimestamps();
	}
	
	public function livestreams(){
		return $this->hasMany('App\LiveStream');
	}
	
	public function tags(){
		return $this->belongsToMany('App\Tag','artist_tags','artist_id','tag_id','id','tag_id')->withTimestamps();
	}

    public function getIsFollowedAttribute()
    {
        return $this->isFollowedBy(Auth::user());
    }

    public function getFollowCountAttribute()
    {
        return $this->followers()->count();
    }

    public function getCountryFlagAttribute()
	{
		if($this->country == '')
		{
			return "";
		}
		else
		{
			$mycountry = str_replace(" ","%20",$this->country);
			$url = env('APP_URL').'images/flags/png/'.$mycountry.'.png';
			return $url;	
		}
		

	}
	
    public function getIsLikedAttribute()
    {
    	if (Auth::check()) {
  			return Auth::user()->hasLiked($this);	
  		}else{
  			return false;
		}
        
    }
}
