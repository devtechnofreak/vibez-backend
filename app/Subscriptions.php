<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriptions extends Model
{
	protected $table = 'subscription_plans';
    //
	protected $primarykey = 'id';
	protected $fillable = [
        'name','price','duration','status'
    ];
}
