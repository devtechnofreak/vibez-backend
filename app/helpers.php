<?php
 
if (!function_exists('getAwsPrivateUrl')) {
    /**
     * Returns a human readable file size
     *
     * @param integer $bytes
     * Bytes contains the size of the bytes to convert
     *
     * @param integer $decimals
     * Number of decimal places to be returned
     *
     * @return string a string in human readable format
     *
     * */
    function getAwsPrivateUrl($value)
    {
		$path = pathinfo($value);

		if(\Cache::has('aws_'.$path['dirname'].$path['filename'])){
			return \Cache::get('aws_'.$path['dirname'].$path['filename']);
		}
		$adapter = \Storage::disk('s3')->getDriver()->getAdapter(); 
		// $exist = $adapter->getClient()->doesObjectExist($adapter->getBucket(),$adapter->getPathPrefix().$value);
		// if($exist){
	    $command = $adapter->getClient()->getCommand('GetObject', [
	        'Bucket' => $adapter->getBucket(),
	        'Key'    => $adapter->getPathPrefix().$value,
	    ]);
		$url = (string)$adapter->getClient()->createPresignedRequest($command, '+1 day')->getUri();
		$expiresAt = now()->addHours(23);
		\Cache::put('aws_'.$path['dirname'].$path['filename'], $url, $expiresAt);
 		return $url;
		// }else{
			// return '';
		// }
    }
}
if (!function_exists('getCloudFrontPrivateUrl')) {
    /**
     * Returns a human readable file size
     *
     * @param integer $bytes
     * Bytes contains the size of the bytes to convert
     *
     * @param integer $decimals
     * Number of decimal places to be returned
     *
     * @return string a string in human readable format
     *
     * */
    function getCloudFrontPrivateUrl($value)
    {
		$path = pathinfo($value);

		/*if(\Cache::has('cloudfront_'.$path['dirname'].$path['filename'])){
			return \Cache::get('cloudfront_'.$path['dirname'].$path['filename']);
		}*/
		$url = CloudFrontUrlSigner::sign(env('CLOUD_URL').$value);
		$expiresAt = now()->addHours(23);
		\Cache::put('cloudfront_'.$path['dirname'].$path['filename'], $url, $expiresAt);
 		return $url;
    }
}
if (!function_exists('time_elapsed_string')) {
	function time_elapsed_string($datetime, $full = false) {
	    $now = new DateTime;
	    $ago = new DateTime($datetime);
	    $diff = $now->diff($ago);
	
	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;
	
	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        's' => 'second',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }
	
	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
}
if (!function_exists('getAppUrl')) {
	function getAppUrl()
	{
		return env('APP_URL');
	}
}