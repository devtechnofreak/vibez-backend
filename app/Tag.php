<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    protected $table = 'tags'; 
	protected $primaryKey  = 'tag_id';
	protected $fillable = [
        'name', 'status'
    ];

    public function musics()
    {
        return $this->belongsToMany('App\Music','music_tags','tag_id','music_id','tag_id','id')->withTimestamps();
    }
	
    public function albums()
    {
        return $this->belongsToMany('App\Album','album_tags','tag_id','album_id','tag_id','id')->withTimestamps();
    }
	
    public function artists()
    {
        return $this->belongsToMany('App\Artist','artist_tags','tag_id','artist_id','tag_id','id')->withTimestamps();
    }
	
    public function videos()
    {
        return $this->belongsToMany('App\Video','video_tags','tag_id','video_id','tag_id','id')->withTimestamps();
    }
	
    public function remove()
    {
        return $this->delete();
    }
}
