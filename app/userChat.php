<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userChat extends Model
{
    //
    protected $table = 'user_chat';
	protected $fillable = array(
        'from_id','to_id'
    );
}
