<?php

namespace App;

use Overtrue\LaravelFollow\Follow;
use Illuminate\Database\Eloquent\Relations\MorphToMany;


class Block extends Follow
{
	const RELATION_BLOCK = 'block';	
    const RELATION_TYPES = [
        'likes' => 'like',
        'likers' => 'like',
        'fans' => 'like',
        'followings' => 'follow',
        'followers' => 'follow',
        'favoriters' => 'favorite',
        'favorites' => 'favorite',
        'bookmarkers' => 'bookmark',
        'bookmarks' => 'bookmark',
        'subscriptions' => 'subscribe',
        'subscribers' => 'subscribe',
        'upvotes' => 'upvote',
        'upvoters' => 'upvote',
        'downvotes' => 'downvote',
        'downvoters' => 'downvote',
        'blocks'=>'block',
        'blockers'=>'block',
    ];	
	

    /**
     * @param \Illuminate\Database\Eloquent\Relations\MorphToMany $relation
     *
     * @throws \Exception
     *
     * @return array
     */
    protected static function getRelationTypeFromRelation(MorphToMany $relation)
    {
        if (!\array_key_exists($relation->getRelationName(), self::RELATION_TYPES)) {
            throw new \Exception('Invalid relation definition.');
        }

        return self::RELATION_TYPES[$relation->getRelationName()];
    }
}	