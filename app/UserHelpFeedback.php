<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHelpFeedback extends Model
{
    protected $table = 'help_feedback';
     protected $fillable = [
        'help_id','feedback','feedback_from', 'status'
    ];
}
