<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHelp extends Model
{
    //
     protected $table = 'user_help';
     protected $fillable = [
        'user_id','help','status'
    ];
	public function feedback()
    {
        return $this->hasOne('App\UserHelpFeedback','id','help_id'); 
    }
}
