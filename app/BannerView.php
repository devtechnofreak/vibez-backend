<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerView extends Model
{
    protected $fillable = ['banner_id', 'user_id', 'bannerable_id', 'bannerable_type'];
}
