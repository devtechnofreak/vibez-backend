<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];
	protected $appends = array('thumb','db_image','db_thumb_image');
    protected $fillable = [
        'title', 'image', 'type','typeid','bannerable_id','bannerable_type','order_id'
    ];

	public function getImageAttribute($value)
	{
		//return getCloudFrontPrivateUrl($value);
		$filePath = pathinfo($this->getOriginal('image'));
		return getCloudFrontPrivateUrl($filePath['dirname'].'/'.$filePath['filename'].'_600.'.$filePath['extension']);
	}
	public function getThumbAttribute()
	{
		$filePath = pathinfo($this->getOriginal('image'));
		return getCloudFrontPrivateUrl($filePath['dirname'].'/'.$filePath['filename'].'_thumb.'.$filePath['extension']);
	}
    public function bannerable()
    {
        return $this->morphTo(null,'bannerable_type','bannerable_id');
    }

    public function getDbImageAttribute()
    {
    	$imageurl = env('AWS_URL').$this->getOriginal('image');
    	$newurl = str_replace(" ","%20", $imageurl);
    	return $newurl;
    }

    public function getDbThumbImageAttribute()
    {
    	$filePath = pathinfo($this->getOriginal('image'));
		if($this->getOriginal('image')){
			$thumb_url = env('AWS_URL').($filePath['dirname'].'/'.$filePath['filename'].'_thumb.'.$filePath['extension']);
			$newthumburl = str_replace(" ","%20", $thumb_url);
			return $newthumburl;
		} else {
			return '';
		}
    }
}
