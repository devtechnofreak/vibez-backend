<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
use Auth;

class Category extends Model
{
    use CanBeLiked;
    protected $table = 'categories';
	protected $fillable = [
        'name', 'slug', 'status','sort_order','image','is_featured'
    ];
	protected $appends = array('thumb','total_subscriber','is_subscribed');
	
    public function videos()
    {
        return $this->hasMany('App\Video')->orderBy('created_at','DESC');
    }
	
    public function getVideosPageAttribute()
    {
        return $this->videos()->paginate(12);
    }

    public function getImageAttribute($value)
    {
        if($value!='')
        {
            return getCloudFrontPrivateUrl($value);    
        }
        else
        {
            return '';
        }
        
    }

    public function getThumbAttribute()
    {
        if($this->getOriginal('image') !='')
        {
            $path = pathinfo($this->getOriginal('image'));
            return getCloudFrontPrivateUrl($path['dirname'].'/'.$path['filename'].'_thumb.'.$path['extension']);
        }
        else
        {
            return '';
        }
        
    }

    public function getTotalSubscriberAttribute()
    {
        return $this->likers()->count();
    }

    public function getIsSubscribedAttribute()
    {
        if (Auth::check()) {
            return Auth::user()->hasLiked($this);       
        }else{
            return false;
        }
        
    }
}
