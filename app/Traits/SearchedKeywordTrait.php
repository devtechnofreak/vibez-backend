<?php

namespace App\Traits;

use App\SearchKeyword;
use Illuminate\Database\Eloquent\Model;
use Auth;

trait SearchedKeywordTrait
{
    /**
     * Return all comments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany('\App\SearchKeyword', 'searchable');
    }

    /**
     * Add comment a new comment.
     *
     * @param  string $body
     * @param  Model  $user
     * @return bool
     */
    public function addKeyword($query)
    {
        $keyword = SearchKeyword::firstOrNew(['keyword'=> $query,'user_id'=>Auth::user()->id]);
		$keyword->touch();

        return $keyword->save();
    }
}