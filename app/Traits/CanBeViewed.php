<?php

namespace App\Traits;

use App\View;
use Illuminate\Database\Eloquent\Model;
use DB;

trait CanBeViewed
{
    /**
     * Return all comments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function viewers()
    {
        return $this->morphMany(View::class, 'viewable');
    }

    /**
     * Add comment a new comment.
     *
     * @param  string $body
     * @param  Model  $user
     * @return bool
     */
    public function addView($user_id = null)
    {
        $view = $this->viewers()->firstOrNew([
            'user_id' => $user_id ?? auth()->id(),
        ],['count'=>DB::raw('count + 1')]);
		$view->save();
		$view->touch();
    }
	
	public function getTotalViews()
	{
		return $this->morphMany(View::class, 'viewable')->sum('count');
	}
}