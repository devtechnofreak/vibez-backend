<?php

namespace App\Traits;

use App\Block;

use DB;

/**
 * Trait CanBeBlocked.
 */
trait CanBeBlocked
{
    /**
     * Check if user is isBlockedBy by given user.
     *
     * @param int $user
     *
     * @return bool
     */
    public function isBlockedBy($user)
    {
        return Block::isRelationExists($this, 'blockers', $user);
    }

    /**
     * Return blockers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function blockers()
    {
        return $this->morphToMany(config('follow.user_model'), config('follow.morph_prefix'), config('follow.followable_table'))
                    ->wherePivot('relation', '=', Block::RELATION_BLOCK)
                    ->withPivot('followable_type', 'relation', 'created_at');
    }

}