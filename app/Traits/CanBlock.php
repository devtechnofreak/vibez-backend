<?php

namespace App\Traits;

use App\Block;

use DB;

/**
 * Trait CanBlock.
 */
trait CanBlock
{
    /**
     * Block an item or items.
     *
     * @param int|array|\Illuminate\Database\Eloquent\Model $targets
     * @param string                                        $class
     *
     * @return array
     *
     * @throws \Exception
     */
    public function block($targets, $class = __CLASS__)
    {
        return Block::attachRelations($this, 'blocks', $targets, $class);
    }

    /**
     * UnBlock an item or items.
     *
     * @param int|array|\Illuminate\Database\Eloquent\Model $targets
     * @param string                                        $class
     *
     * @return array
     */
    public function unblock($targets, $class = __CLASS__)
    {
        return Block::detachRelations($this, 'blocks', $targets, $class);
    }

    /**
     * Toggle Block an item or items.
     *
     * @param int|array|\Illuminate\Database\Eloquent\Model $targets
     * @param string                                        $class
     *
     * @return array
     *
     * @throws \Exception
     */
    public function toggleBlock($targets, $class = __CLASS__)
    {
        return Block::toggleRelations($this, 'blocks', $targets, $class);
    }

    /**
     * Check if user is Blocked given item.
     *
     * @param int|array|\Illuminate\Database\Eloquent\Model $target
     * @param string                                        $class
     *
     * @return bool
     */
    public function hasBlocked($target, $class = __CLASS__)
    {
        return Block::isRelationExists($this, 'blocks', $target, $class);
    }

    /**
     * Return item blocks.
     *
     * @param string $class
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function blocks($class = __CLASS__)
    {
        return $this->morphedByMany($class, config('follow.morph_prefix'), config('follow.followable_table'))
                    ->wherePivot('relation', '=', Block::RELATION_BLOCK)
                    ->withPivot('followable_type', 'relation', 'created_at');
    }
}