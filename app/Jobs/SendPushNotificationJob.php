<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class SendPushNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
     
    protected $fcm_token;
	protected $_data;
	protected $device_type;
    public function __construct($fcmToken,$data,$deviceType)
    {
		$this->fcm_token = $fcmToken;
		$this->_data = $data;
		$this->device_type = $deviceType;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
		
		if($this->device_type == "android"){
				$fields = array
					(
						'to'		=> $this->fcm_token,
						'data'	=> array(
									'data'	=> $this->_data
								)
					);	
			}else{
				
				$notification = array(
									'title' =>$this->_data['title'] , 
									'text' => $this->_data['message'], 
									'sound' => 'default'
								);
				$fields = array(
							'to' 			=> $this->fcm_token, 
							'notification' 	=> $notification,
							'data'			=> $this->_data,
							'priority'		=> 'high'
							);
			} 
			$headers = array
					(
						'Authorization: key='.config('services.fcm.key'),
						'Content-Type: application/json'
					);
				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
				$result = curl_exec($ch );
				curl_close( $ch );
				Log::debug('An informational message Notification.'); 
				Log::debug('PUSH NOTIFICATION PARAMES 1. '.$this->fcm_token);
				Log::debug('PUSH NOTIFICATION PARAMES 2. '.$this->_data);
				Log::debug('PUSH NOTIFICATION PARAMES 3. '.$this->device_type);
				Log::debug('PUSH NOTIFICATION RESULT. '.$result); 
				
    }
}
