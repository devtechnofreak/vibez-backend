<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Filesystem\Filesystem;
use Pbmedia\LaravelFFMpeg\FFMpegFacade as FFMpeg;
use Illuminate\Support\Facades\Log;
class BulkAlbumAudioUpload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
	protected $path;
	protected $name;
	protected $metas;
	protected $folder;
	
	
    public function __construct($filepath,$folder,$filename ,Array $meta_data)
    {
		$this->path = $filepath;
		$this->name = $filename;
		$this->metas = $meta_data;
		$this->folder = $folder;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
    	Log::debug('I AM HERE IN BULK FILEBNAME = '.$this->name);
		Log::debug('I AM HERE IN BULK FOLDER = '.$this->folder); 
		$audio =   FFMpeg::fromDisk('uploads')
						    ->open($this->folder."/".$this->name)
							->addFilter(function ($filters) {
						        $filters->addMetadata($this->metas);
						    })
							->export()
						    ->toDisk('s3')
							->inFormat(new \FFMpeg\Format\Audio\Mp3)
							->save($this->path.$this->name);
		
		//FFMpeg::cleanupTemporaryFiles();
    }
}
