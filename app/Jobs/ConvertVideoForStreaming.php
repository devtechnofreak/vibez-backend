<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Filesystem\Filesystem;
use Pbmedia\LaravelFFMpeg\FFMpegFacade as FFMpeg;
use FFMpeg\Format\Video\X264;
use FFMpeg\Coordinate\Dimension;

class ConvertVideoForStreaming implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 1;
	protected $path;
	protected $name;
	public $timeout = 900;
	
    public function __construct($filepath,$filename)
    {
		$this->path = $filepath;
		$this->name = $filename;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $lowBitrateFormat  = (new \FFMpeg\Format\Video\X264('aac', 'libx264'))->setKiloBitrate(500);
		$fileNameArray= explode(".",$this->name);
		$fileNameExtension= end($fileNameArray);
		$fileBaseName = basename($this->name,'.'.$fileNameExtension);
		//$midBitrateFormat  = (new \FFMpeg\Format\Video\X264('aac', 'libx264'))->setKiloBitrate(500);
		try{
		$audio = FFMpeg::fromDisk('uploads') 
						    ->open($this->name)
							
						    ->exportForHLS()
							->toDisk('public')
							->setSegmentLength(10)
						    ->addFormat($lowBitrateFormat)
							->save($this->path.$fileBaseName.'.m3u8');
		} catch(\Exception $e){
			echo $e->getMessage();
		}
		//FFMpeg::cleanupTemporaryFiles();
    }
}
