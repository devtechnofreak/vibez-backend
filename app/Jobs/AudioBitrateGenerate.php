<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Filesystem\Filesystem;
use Pbmedia\LaravelFFMpeg\FFMpegFacade as FFMpeg;
use Illuminate\Support\Facades\Log;

class AudioBitrateGenerate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
	protected $path;
	protected $name;
	protected $bits;
	
    public function __construct($filepath,$filename,$bitrate)
    {
		$this->path = $filepath;
		$this->name = $filename;
		$this->bits = (int)$bitrate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
		try {
			$bitrate = (new \FFMpeg\Format\Audio\Mp3)->setAudioKiloBitrate($this->bits);
			$fileNameArray= explode(".",$this->name);
			$fileNameExtension= end($fileNameArray);
			$fileBaseName = basename($this->name,'.'.$fileNameExtension);
			$audio = FFMpeg::fromDisk('s3')
							    ->open($this->path.$this->name)
								->export()
							    ->toDisk('s3')
								->inFormat($bitrate)
								->save($this->path.$fileBaseName.'_'.$this->bits.'.mp3');
			Log::debug("In Audio Try");
			Log::debug("Audio Path = ".$this->path);
			Log::debug("Audio Bits = ".$this->bits);
			Log::debug("Audio Name = ".$this->name);
			Log::debug("FilenameArray = ".$fileNameExtension);					
		} catch (Exception $e) {
				Log::debug("In Audio Catch");
				Log::debug("Audio Path = ".$this->path);
				Log::debug("Audio Name = ".$this->name);
				Log::debug("Audio Bits = ".$this->bits);
				Log::debug($e->getMessage());
		}
		
		//FFMpeg::cleanupTemporaryFiles();
    }
}
