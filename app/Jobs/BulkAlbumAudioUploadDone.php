<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Filesystem\Filesystem;
use Pbmedia\LaravelFFMpeg\FFMpegFacade as FFMpeg;
use Illuminate\Support\Facades\Log;
use App\Album;
class BulkAlbumAudioUploadDone implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
	protected $album_id;
	protected $foldername;
	
	
    public function __construct($id,$folder_name)
    {
		$this->album_id = $id;
		$this->foldername = $folder_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
    	Log::debug('I AM HERE IN BULK ALBUM CRONJOB ID = '.$this->album_id);
		$album = Album::find($this->album_id);
		$album->album_status = 1;
		$album->save();
		if(isset($this->foldername) && $this->foldername != ""){
			$directory = "/home/admin/web/thevibez.net/public_html/public/import/music/".$this->foldername;
			$musics = glob($directory . "/*.mp3");
		 	foreach($musics as $music)
			{
			 	unlink($music); 
			}
			$csv = glob($directory . "/*.csv");
			foreach($csv as $single_csv)
			{
			  unlink($single_csv); 
			}
			$images = glob($directory . "/*.jpg");
			foreach($images as $image)
			{
			  unlink($image); 
			}
			rmdir($directory);
		}
    }
}
