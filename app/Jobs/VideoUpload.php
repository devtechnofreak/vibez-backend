<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Filesystem\Filesystem;
use Pbmedia\LaravelFFMpeg\FFMpegFacade as FFMpeg;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
class VideoUpload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 1;
	protected $path;
	protected $name;
	public $timeout = 900;
	
    public function __construct($filepath,$filename)
    {
		$this->path = $filepath;
		$this->name = $filename;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
    	Log::debug('I AM HERE VIDEO UPLOAD PATH = '.$this->path);
    	Log::debug('I AM HERE VIDEO UPLOAD NAME = '.$this->name);
		try{
		$disk = Storage::disk('s3');
		$disk->put($this->path.$this->name, fopen(public_path('import/music').'/'.$this->name, 'r+'), 'public');
		/*
		$audio = FFMpeg::fromDisk('uploads')
						    ->open($this->name)
							
							->export()
							->toDisk('s3')
							->inFormat(new \FFMpeg\Format\Video\X264('aac', 'libx264'))
							->save($this->path.$this->name);
		 * 
		 */
		} catch(\Exception $e){
			echo $e->getMessage();
		}
		//FFMpeg::cleanupTemporaryFiles();
    }
}
