<?php
namespace App\Jobs;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Filesystem\Filesystem;
use Pbmedia\LaravelFFMpeg\FFMpegFacade as FFMpeg;
use Illuminate\Http\Request;
use App\Video;
use DB,Notification;
use App\Notifications\PushNotification;
use Illuminate\Support\Facades\Log;

class CreateNewVideoNew implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
   public $tries = 1;
	protected $data;
	public $timeout = 180;
	
    public function __construct($request)
    {
		$this->data = $request;
    }


	protected function process() {
       
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
    		Log::debug('An informational message HANDLE NEW 46.');
    		Log::debug('An informational message HANDLE NEW 48.'.$this->data['is_featured']);
    		
			$video = Video::updateOrCreate(['title' => $this->data['title'],'url'=> $this->data['url']],[
				'image'	 		=> $this->data['image'],
				'description'	=> $this->data['description'],
				'date_released'	=> $this->data['date_released'],
				'duration'		=> $this->data['duration'],
				'category_id'	=> (int)$this->data['category_id'],
				'is_featured'	=> $this->data['is_featured'],
				'related_artist'=> $this->data['related_artist'],
			]);
			$video->artists()->attach($this->data['artist_id']);

			/* Featured Artist Logic HERE */ 
			if($this->data['related_artist'] && $this->data['related_artist'] != ""){
				$related_arr = explode(",",$this->data['related_artist']);
				foreach ($related_arr as $key => $value) {
					$ins_array[] = 
						array(
							'video_id' 			=> $video->id, 
							'artist_id' 		=> $value,
							'is_main_artist'	=> '0',
							'created_at'		=> date('Y-m-d H:i:s'),
							'updated_at'		=> date('Y-m-d H:i:s')
						);
				}
				DB::table('video_artists')->insert($ins_array);
			}
			/* Featured Artist Logic HERE END */ 

			$unlink_image = substr($this->data['image'], strrpos($this->data['image'], '/') + 1);
    		$unlink_video = substr($this->data['url'], strrpos($this->data['url'], '/') + 1);

    		unlink(public_path('import/music/'.$unlink_image));
    		unlink(public_path('import/music/'.$unlink_video));

			if($this->data['tag_id']!='')
			{
				$video->tags()->attach($this->data['tag_id']);	
			}
			
			$video_url = $this->data['url'];
			$video_name = substr($video_url, strrpos($video_url, '/') + 1);
			$filepath = public_path('import/music'.$video_name);

			Storage::disk('uploads')->delete($video_name);
			$artist = $video->artists()->first(); 

			$video_pay_arr = array(
				'id'			=> $video->id,
				'title'			=> $video->title,
				'url'			=> $video->db_url,
				'image'			=> $video->db_image,
				'description'	=> $video->description,
				'thumb'			=> $video->db_thumb_image,
				'stream_url'	=> $video->stream_url,
			);

			$message['title'] = 'Vibez';
			$message['message'] = 'Video Update: '.$artist->name.' - '.$video->title;
			$message['click_action'] = 'vibez.video.new';
			$message['video'] = $video_pay_arr;
			
			$pathinfo = pathinfo($this->data['url']);
			if($this->data['url']){
				//return getCloudFrontPrivateUrl('HLS/'.$pathinfo['filename'].'/'.$pathinfo['filename'].'-master-playlist.m3u8');
				//return env('APP_URL')."storage/".$pathinfo['dirname']."/".$pathinfo['filename'].'.m3u8';
				$message['video']['stream_url'] = 'https://s3.amazonaws.com/vibezapp/HLS/'.$pathinfo['filename'].'/'.$pathinfo['filename'].'-master-playlist.m3u8';
			}else {
				$message['video']['stream_url'] = '';
			} 
			
			$users = $artist->followers;
			Notification::send($users, (new PushNotification($message))->onQueue('audio'));
    }
}
?>