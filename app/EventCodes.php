<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventCodes extends Model
{
    //
     protected $table = 'event_codes';
     protected $fillable = [
        'user_id','code'
    ];
}
