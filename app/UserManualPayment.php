<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserManualPayment extends Model
{
    //
    protected $table = 'user_manual_payment';
     protected $fillable = [
        'user_id','date_paid','bank_name','bank_account_number','status','transaction_id'
    ];
	public function users()
    {
        return $this->hasOne('App\User','id','user_id'); 
    }
}
