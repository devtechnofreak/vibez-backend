<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{
    //
     protected $table = 'user_subscriptions';
     protected $fillable = [
        'user_id','receipt','plan','start_date','end_date','user_status','promocode','subscription_amount','subscription_platform','transaction_id','currency','subscription_gateway'
    ];
	public function promocodes()
    {
        return $this->hasOne('App\Promocodes','id','promocode'); 
    }

}
