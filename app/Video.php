<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\CommentableTrait;
use App\Traits\CanBeViewed;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
use Auth;
use Nicolaslopezj\Searchable\SearchableTrait;
use JamesMills\Watchable\Traits\Watchable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model
{
	use SoftDeletes, CommentableTrait, CanBeLiked, CanBeViewed, SearchableTrait, Watchable;
	
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'videos.title' => 10,
        ],
        'joins' => [
        'video_tags'=>['videos.id','video_tags.video_id'],
            'tags' => ['video_tags.tag_id','tags.tag_id'],
        ],
        'joins' => [
        'video_artists'=>['videos.id','video_artists.video_id'],
            'artists' => ['video_artists.artist_id','artists.id'],
        ],
        'groupBy'=>'videos.id'
    ];
	protected $dates = ['deleted_at'];
	protected $table = 'videos';
    protected $with = array('artists','related_artist');
	protected $appends = array('thumb','is_liked','total_comments','view_count','stream_url','view_link','db_image','db_thumb_image','db_url', 'total_likes','channel_name');
    protected $fillable = array(
        'title', 'url','image', 'date_released','duration','category_id','description','is_featured','related_artist'
    ); 

    public function getTotalLikesAttribute()
    {
        return $this->likers()->count();
    }
	
    public function artists()
    {
        return $this->belongsToMany('App\Artist','video_artists')->where('is_main_artist','=','1')->withTimestamps();
    }

    public function related_artist()
    {
        return $this->belongsToMany('App\Artist','video_artists')->where('is_main_artist','=','0')->withTimestamps();
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
	
    public function banners()
    {
        return $this->morphMany('App\Banner', 'bannerable');
    }

    public function getChannelNameAttribute($value)
    {
        return $this->belongsTo('App\Category','category_id','id')->pluck('name')->first();
    }	
	
	public function getImageAttribute($value)
	{
		return getCloudFrontPrivateUrl($value);
	}
	public function getUserNameAttribute()
	{
		if($this->user){
			return  'By '.$this->user->name;
		}
		return  '';
	}
	
    public function tags()
    {
        return $this->belongsToMany('App\Tag','video_tags','video_id','tag_id','id','tag_id')->withTimestamps();
    }
	
	public function getThumbAttribute()
	{
		$filePath = pathinfo($this->getOriginal('image'));
		return getCloudFrontPrivateUrl($filePath['dirname'].'/'.$filePath['filename'].'_thumb.'.$filePath['extension']);
	}
	
	public function getUrlAttribute($value)
	{
        $url = getCloudFrontPrivateUrl($value);
		$url = str_replace(".jpg",".mp4",$url); 
		return $url;
	}

	public function getDateReleasedAttribute($value)
	{
		return date('d-m-Y',strtotime($value));
	}
	
    public function getTotalCommentsAttribute()
    {
        return count($this->comments()->get()->filter(function($item) {
							    return $item->author && $item->author->is_blocked === false;
							 }));
    }
	
    public function getIsLikedAttribute()
    {
    	if (Auth::check()) {
    		return Auth::user()->hasLiked($this);		
    	}else{
    		return false;
    	}
    	
    }
    public function getStreamUrlAttribute()
    {
    	$pathinfo = pathinfo($this->getOriginal('url'));
		if($this->getOriginal('url')){
			//return getCloudFrontPrivateUrl('HLS/'.$pathinfo['filename'].'/'.$pathinfo['filename'].'-master-playlist.m3u8');
			//return env('APP_URL')."storage/".$pathinfo['dirname']."/".$pathinfo['filename'].'.m3u8';
			return 'https://s3.amazonaws.com/vibezapp/HLS/'.$pathinfo['filename'].'/'.$pathinfo['filename'].'-master-playlist.m3u8';
		}else {
			return '';
		}
        
    }
	
    public function getViewCountAttribute()
    {
        return $this->getTotalViews()?(int)$this->getTotalViews():0;
    }
    public function getShareLinkAttribute()
    {
        return \Share::load(url('share/Video/'.$this->title.'/'.$this->id),$this->title)->services('facebook', 'gplus', 'twitter','whatsapp');
    }
	
    public function getViewLinkAttribute()
    {
    	$title = urlencode($this->title);
        return url('share/Video/'.$title.'/'.$this->id);
    }

    public function getDbImageAttribute()
    {
        return env('AWS_URL').$this->getOriginal('image');
    }

    public function getDbThumbImageAttribute()
    {
        $filePath = pathinfo($this->getOriginal('image'));
        if($this->getOriginal('image')){
            return env('AWS_URL').($filePath['dirname'].'/'.$filePath['filename'].'_thumb.'.$filePath['extension']);
        } else {
            return '';
        }
    }

    public function getDbUrlAttribute()
    {
        return env('AWS_URL').$this->getOriginal('url');
    }
}
