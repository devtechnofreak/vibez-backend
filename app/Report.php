<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = ['report_from','report_to','description','feedback'];
	
    public function reportingUser(){
    	
    	return $this->belongsTo('App\User','report_from');	
    }


    public function reportedUser(){
    	
    	return $this->belongsTo('App\User','report_to');
    }
}
