<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
//use CyrildeWit\EloquentViewable\Viewable;
use Auth;
//use CyrildeWit\EloquentViewable\ViewTracker;
use Nicolaslopezj\Searchable\SearchableTrait;
use JamesMills\Watchable\Traits\Watchable;
use App\Traits\SearchedKeywordTrait;
use App\Traits\CanBeViewed;
use Illuminate\Database\Eloquent\SoftDeletes;

class Music extends Model
{
	use SoftDeletes, CanBeLiked, Watchable, CanBeViewed, SearchedKeywordTrait;
	use SearchableTrait {
		scopeSearch as protected traitScopeSearch;
	}
	
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'musics.title' => 10,
            'tags.name' => 5,
        ],
        'joins' => [
        'music_tags'=>['musics.id','music_tags.music_id'],
            'tags' => ['music_tags.tag_id','tags.tag_id'],
        ],
        'groupBy'=>'musics.id'
    ];
	
	protected $dates = ['deleted_at'];
    protected $with = array('artists');
	protected $appends = array('albums','albums_id','playlists','playlist_id','thumb','is_liked','bitrates','tag_data','view_link','is_added_library', 'minute_show');
    protected $fillable = [
        'title','image', 'date','status','audio','duration','is_new','is_trending'
    ];

    public function getMinuteShowAttribute() {
        return str_pad(floor($this->duration/60),2,"0",STR_PAD_LEFT). ":" .str_pad(($this->duration % 60),2,"0",STR_PAD_LEFT);
    }
	
    public function artists()
    {
        return $this->belongsToMany('App\Artist','music_artists')->withTimestamps();
    }
	
    public function getArtistDataAttribute()
    {
    	return $this->belongsToMany('App\Artist','music_artists')->select('name','bio')->get();
    }
	
    public function playlist()
    {
        return $this->belongsToMany('App\Playlist','playlist_musics')->withTimestamps();
    }

    public function album()
    {
        return $this->belongsToMany('App\Album','album_musics')->withTimestamps();
    }
	
    public function getAlbumsAttribute()
    {
		$albums = $this->belongsToMany('App\Album','album_musics')->get()->pluck('title')->all();
    	return implode(', ',$albums);
    }

    public function getAlbumsIdAttribute()
    {
        $albums_id = $this->belongsToMany('App\Album','album_musics')->get()->pluck('id')->all();
        return implode(', ',$albums_id);
    }

    public function getPlaylistsAttribute()
    {
    	$playlists = $this->belongsToMany('App\Playlist','playlist_musics')->get()->pluck('title')->all();
    	return implode(', ',$playlists);
    }

    public function getPlaylistIdAttribute()
    {
        $playlists_id = $this->belongsToMany('App\Playlist','playlist_musics')->get()->pluck('id')->all();
        return implode(', ',$playlists_id);
    }

	public function tags()
    {
        return $this->belongsToMany('App\Tag','music_tags','music_id','tag_id','id','tag_id')->withTimestamps();
    }	
    public function getTagDataAttribute()
    {
        return implode(', ',$this->belongsToMany('App\Tag','music_tags','music_id','tag_id','id','tag_id')->withTimestamps()->get()->pluck('name')->all());
    }
	public function getThumbAttribute()
	{
		$filePath = pathinfo($this->getOriginal('image'));
		return getCloudFrontPrivateUrl($filePath['dirname'].'/'.$filePath['filename'].'_thumb.'.$filePath['extension']);
	}
	
    public function banners()
    {
        return $this->morphMany('App\Banner', 'bannerable');
    }	
	
	public function getImageAttribute($value)
	{
		return getCloudFrontPrivateUrl($value);
	}
	
	public function getAudioAttribute($value)
	{
		$filePath = pathinfo($this->getOriginal('audio'));
		return getCloudFrontPrivateUrl($filePath['dirname'].'/'.$filePath['filename'].'_128.mp3');
	}
	
	public function getBitratesAttribute()
	{
		$filePath = pathinfo($this->getOriginal('audio'));
		$array = array(
						'128'=>getCloudFrontPrivateUrl($filePath['dirname'].'/'.$filePath['filename'].'_128.mp3'),
						'190'=>getCloudFrontPrivateUrl($filePath['dirname'].'/'.$filePath['filename'].'_190.mp3'),
						'256'=>getCloudFrontPrivateUrl($filePath['dirname'].'/'.$filePath['filename'].'_256.mp3'),
						'320'=>getCloudFrontPrivateUrl($filePath['dirname'].'/'.$filePath['filename'].'_320.mp3')
						);
       return $array;
	}
	
    public function getIsLikedAttribute()
    {
        return Auth::user()->hasLiked($this);
    }
	
    public function getShareLinkAttribute()
    {
        return \Share::load(url('share/Music/'.$this->title.'/'.$this->id),$this->title)->services('facebook', 'gplus', 'twitter','whatsapp');
    }
	
    public function getViewLinkAttribute()
    {
		$album = $this->belongsToMany('App\Album','album_musics')->first();
		$title = urlencode($this->title);
        return url('share/Music/'.$title.'/'.$this->id.'?'.$album->id);
    }
	
	public function scopeSearch($query, $search, $threshold = null, $entireText = false, $entireTextOnly = false)
	{
		$this->addKeyword($search);
		return $this->traitScopeSearch($query,$search,$threshold,$entireText,$entireTextOnly);
	}
	
    public function getViewCountAttribute()
    {
        return $this->getTotalViews()?(int)$this->getTotalViews():0;
    }
	
    public function getIsAddedLibraryAttribute()
    {
        return $this->belongsToMany('App\User','libraries')->where('users.id',auth()->user()->id)->exists();
    }
	
}
