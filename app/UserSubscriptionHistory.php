<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSubscriptionHistory extends Model
{
    //
     protected $table = 'user_subscription_histories';
     protected $fillable = [
        'user_id','receipt','plan','subscription_platform','subscription_amount','promocode','date','promocode','transaction_id','currency','subscription_gateway'
    ];
}
