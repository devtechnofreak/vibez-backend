<?php
/*header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Authorization, Content-Type');*/
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::post('upload-audio', 'Admin\AudioUploadController@audioUpload')->name('audio.upload');
// Password Reset Routes...
/*Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
 * 
 */

header("Access-Control-Allow-Origin: *");
header('Access-Control-Request-Headers: Content-Type');
header('Access-Control-Allow-Headers: Origin, Content-Type,Authorization');

Route::group(['namespace' => 'Api'], function(){
	Route::post('register', 'AuthController@register');
	Route::post('login', 'AuthController@login');
	Route::post('socialLogin', 'AuthController@socialLogin');
	Route::post('socialSignup', 'AuthController@socialSignup');
	Route::post('recover-password', 'AuthController@recoverPassword');
	Route::post('updatePassword','AuthController@updatePassword');
	Route::post('sendPlaystoreApi','AuthController@sendPlaystoreApi');
	//Route::get('share/{class}/{id}','AuthController@share');
	
	Route::get('manual_payment/info/{email}','AuthController@manualPaymentInfo');
	Route::get('manual_payment/insert/{email}','AuthController@manualPaymentInsert');
	Route::get('manual_payment/payment_gateway/{email}','AuthController@manualPaymentGateway');
	Route::post('manual_payment/submit','AuthController@manualPaymentSubmit');
	
	Route::get('check_app_version','AuthController@checkAppVersion');
	
/*
|--------------------------------------------------------------------------
| Page Routes
|--------------------------------------------------------------------------
*/		
	Route::post('page', 'AuthController@getPage');
	Route::get('logout', 'AuthController@logout')->middleware('jwt.auth','throttle:200,1');
	Route::get('forcelogout', 'AuthController@forceLogout')->middleware('jwt.auth','throttle:200,1');
	Route::group(['middleware' => ['jwt.auth','verify.claims']], function() {
/*
|--------------------------------------------------------------------------
| General Routes
|--------------------------------------------------------------------------
*/ 
		Route::post('changePassword','AuthController@changePassword');
		Route::post('deleteUser','AuthController@deleteUser');
		Route::post('profile/update','AuthController@updateProfile');
		Route::post('chat/invite','AuthController@sendChatInvitation');
		Route::post('chat/accept','AuthController@acceptChatInvitation');
		Route::post('refreshFcm', 'AuthController@refreshFcm');
		Route::post('changeDesign', 'AuthController@changeThemeDesign');
		Route::post('verify_email_otp','AuthController@verifyEmailOTP');
		Route::get('resend_email_verification', 'AuthController@resendEmailVerification');
		Route::post('checkPromocode', 'AuthController@checkPromocode');
		
		Route::post('addSubscription', 'AuthController@addSubscription');
		
		Route::post('addSubscriptionPayStack', 'AuthController@addSubscriptionPayStack');
		Route::post('addSubscriptionPayStack_TestMode', 'AuthController@addSubscriptionPayStack_TestMode');
		
		Route::post('cancelSubscriptionPayStack', 'AuthController@cancelSubscriptionPayStack');

		Route::post('subscriptionTransactionInstiallize', 'AuthController@subscriptionTransactionInstiallize');		

		Route::get('checkSubscription', 'AuthController@checkSubscription');
		Route::get('checkSubscriptionBySiddhesh', 'AuthController@checkSubscriptionBySiddhesh');
		Route::get('getSubscription', 'AuthController@getSubscription');
		
		Route::get('cancelSubscription', 'AuthController@cancelSubscription');
		
		Route::post('help/add','AuthController@addUserHelp');
		
		
		Route::get('help/getlist','AuthController@getUserHelp');
		
		Route::post('help/addFeedback','AuthController@addFeedback');

		Route::post('updatestausinapp','AuthController@UpdateStausInApp');		
		
/*
|--------------------------------------------------------------------------
| Search Routes
|--------------------------------------------------------------------------
*/
		Route::get('search/{query}', 'AuthController@searchAll');
		Route::get('search/{type}/{query}', 'AuthController@searchByType');
		Route::get('searched-keywords','AuthController@searchedKeywords');
		Route::post('letusknow','AuthController@searchresult');
/*
|--------------------------------------------------------------------------
| Notification Routes
|--------------------------------------------------------------------------
*/
		Route::get('notifications/all', 'AuthController@getAllNotifications');
        Route::get('notifications/unread', 'AuthController@getAllUnreadNotifications');
		Route::get('notifications/mark_read', 'AuthController@markAsRead');
		Route::get('notifications/remove/{id}', 'AuthController@removeNotification');
		Route::get('notifications/removeall', 'AuthController@removeAllNotification');
		Route::post('message/new','AuthController@sendMessage');
		Route::get('message/read', 'AuthController@readAllMessage');
/*
|--------------------------------------------------------------------------
| Banner Routes
|--------------------------------------------------------------------------
*/		
		Route::get('banners', 'BannerController@getBanner');
		Route::post('banners/watch', 'BannerController@bannerWatch');
/*
|--------------------------------------------------------------------------
| Livestream Routes
|--------------------------------------------------------------------------
*/
		Route::post('livestream/comment/add','LiveStreamController@addComment');
		Route::get('livestream_detail/{id}','LiveStreamController@getLivestreamDetail');
		Route::post('livestream/play','LiveStreamController@playLivestream');
/*
|--------------------------------------------------------------------------
| Album Routes
|--------------------------------------------------------------------------
*/		
		Route::post('album/like','AuthController@likeAlbum');
		Route::post('album/unlike','AuthController@unlikeAlbum');
		Route::post('album/follow','AuthController@followAlbum');
		Route::post('album/unfollow','AuthController@unfollowAlbum');
		Route::post('single-album', 'PlaylistController@getSingleAlbum');
/*
|--------------------------------------------------------------------------
| Artist Routes
|--------------------------------------------------------------------------
*/		
		Route::post('artist/follow','ArtistController@followArtist');
		Route::post('artist/unfollow','ArtistController@unfollowArtist');
		Route::post('artist/profile','ArtistController@getArtistInfo');
		Route::get('artist/featured', 'ArtistController@getFeaturedArtists');
		Route::get('artist/more', 'ArtistController@getArtistsMore');
		Route::post('artist/like','ArtistController@likeArtist');
		Route::post('artist/unlike','ArtistController@unlikeArtist');
		Route::get('artist/all','ArtistController@getAllArtists');
		Route::post('artist/bulk/follow','ArtistController@bulkFollowArtists');
/*
|--------------------------------------------------------------------------
| Library Routes
|--------------------------------------------------------------------------
*/		
		Route::post('library/track/add', 'MusicController@addTracksLibrary');
		Route::post('library/track/remove', 'MusicController@removeTracksLibrary');
		Route::get('library', 'MusicController@getLibrarySongs');
/*
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
*/		
		Route::post('user/follow','AuthController@followUser');
		Route::post('user/unfollow','AuthController@unfollowUser');
		Route::post('user/block','AuthController@blockUser');
		Route::post('user/unblock','AuthController@unblockUser');
		Route::post('user/profile','AuthController@getUserInfo');
		Route::post('user/report','AuthController@reportUser');
		Route::post('user/followers','AuthController@getUserFollowers');
		Route::post('user/followings','AuthController@getUserFollowings');
		Route::get('user/chat_list','AuthController@getChatList');
		Route::get('user/hasBlockedCurrent/{id}','AuthController@hasBlockedCurrent');
		Route::get('blocked/users','AuthController@getBlockedUser');
/*
|--------------------------------------------------------------------------
| Music Routes
|--------------------------------------------------------------------------
*/		
		Route::get('musics', 'MusicController@getMusic');
		Route::get('trending-musics', 'MusicController@getTrendingMusicNew');
		Route::get('trending-musics-new', 'MusicController@getTrendingMusicNew');
		Route::get('latest-musics', 'MusicController@getLatestMusic');
		Route::post('music/like','MusicController@likeMusic');
		Route::post('music/unlike','MusicController@unlikeMusic');
		Route::get('recent/added/musics','MusicController@getRecentlyAddedMusic');
		Route::get('recent/played/musics','MusicController@getRecentlyPlayedMusic');
		Route::post('music/play', 'MusicController@playMusic');
/*
|--------------------------------------------------------------------------
| Playlist Routes
|--------------------------------------------------------------------------
*/			
		Route::get('favourites/{type?}', 'PlaylistController@getFavourites');
		Route::get('user/playlists', 'PlaylistController@getUserPlaylist');
		Route::get('recent/playlists', 'PlaylistController@getRecentlyUpdatedPlaylist');
		Route::get('play-lists', 'PlaylistController@getPlaylist');
		Route::post('single-playlist', 'PlaylistController@getSinglePlaylist');
		Route::post('playlist/create', 'PlaylistController@create');
		Route::post('playlist/update', 'PlaylistController@update');
		Route::post('playlist/remove-track', 'PlaylistController@removeTrack');
		Route::post('playlist/add-tracks', 'PlaylistController@addTracks');
		Route::get('playlist/delete/{id}', 'PlaylistController@delete');
		Route::post('playlist/like','PlaylistController@likePlaylist');
		Route::post('playlist/unlike','PlaylistController@unlikePlaylist');
		Route::post('playlist/follow','PlaylistController@followPlaylist');
		Route::post('playlist/unfollow','PlaylistController@unfollowPlaylist');
		Route::post('playlist/change-order','PlaylistController@changeOrderMusics');
/*
|--------------------------------------------------------------------------
| Comment Routes
|--------------------------------------------------------------------------
*/	
		Route::post('comment/like','NewsController@likeComment');
		Route::post('comment/unlike','NewsController@unlikeComment');
		Route::post('comment/remove','NewsController@removeComment');
/*
|--------------------------------------------------------------------------
| News Routes
|--------------------------------------------------------------------------
*/			
    	Route::get('news','NewsController@getNews');
		Route::get('home_news','NewsController@getHomeNews');
		Route::get('news_banner','NewsController@getNewsBanners');
		Route::post('news/comment/add','NewsController@addComment');
		Route::post('news/like','NewsController@likeNews');
		Route::post('news/unlike','NewsController@unlikeNews');
		Route::get('news_detail/{id}','NewsController@getNewsDetail');
/*
|--------------------------------------------------------------------------
| Video Routes
|--------------------------------------------------------------------------
*/			
		Route::get('videos','VideoController@getVideos');
		Route::get('video-categories','VideoController@getVideoCategories');
		Route::get('video-category/{id}','VideoController@getSingleVideoCategory');
		Route::post('video/comment/add','VideoController@addComment');
		Route::get('video_detail/{id}','VideoController@getVideoDetail');
		Route::get('video/{id}','VideoController@getVideoDetail');
		Route::post('video/play','VideoController@playVideo');
		Route::post('video/unlike','VideoController@unlikeVideo');
		Route::post('video/like','VideoController@likeVideo');
/*
|--------------------------------------------------------------------------
| Channel Routes
|--------------------------------------------------------------------------
*/
		Route::post('channel/unsubscribe','VideoController@unlikeCategory');
		Route::post('channel/subscribe','VideoController@likeCategory');
		Route::get('channel/featured', 'VideoController@getFeaturedVideoCategories');
		
	});
});
