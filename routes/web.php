<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('404', ['as' => '404', 'uses' => 'ErrorController@notfound']);

Route::get('/privacy-policy', 'PageController@index');
Route::get('/terms-of-use', 'PageController@terms');
Route::get('/support', 'SupportController@index');
Route::post('/support_mail', 'SupportController@mail')->name('support_mail');
Route::post('/support_popup_mail', 'SupportController@popup_mail')->name('support_popup_mail');

// Auth::routes();
Route::get('share/{class}/{title}/{id}','Api\AuthController@share');
Route::get('admin/logout','Auth\LoginController@logout')->name('admin.logout');
Route::get('checkSubscription','Api\AuthController@checkSubscriptionCronjob');
Route::get('checkSubscriptionPaystack','Api\AuthController@checkSubscriptionCronjobPaystack');
Route::get('cron/albumimportcron','Admin\AlbumController@albumImportCronjob');
Route::get('run_album_cron_manually','Admin\AlbumController@albumImportCronjobManually');
Route::get('checkRcIos', 'Api\AuthController@checkRcIos');

Route::group(['middleware' => ['guest'],'namespace' => 'Auth', 'prefix' => 'admin'], function(){
	//Login Routes...
	Route::get('home', 'HomeController@index')->name('home');
	Route::get('login','LoginController@showLoginForm')->name('login');
	Route::post('login','LoginController@login')->name('admin.login');
	// Registration Routes...
	Route::get('register', 'RegisterController@showRegistrationForm')->name('admin.register');
	Route::post('register', 'RegisterController@register');

	// Forgotpassword Routes...
	Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
	Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
	Route::post('password/reset', 'ResetPasswordController@reset');
});
Route::get('news/regenerate/thumbnail', 'Admin\NewsController@regenerateThumbnail');
Route::group(['middleware' => ['admin:web','auth:web'],'prefix' => 'admin','namespace' => 'Admin'], function () {
	 
	Route::get('/', 'AdminController@index');

    Route::get('users', 'UserController@index');
	Route::get('user/add', 'UserController@add');
	Route::post('user/create', 'UserController@createUser');
	Route::post('user/getuserdata', 'UserController@getUserData');
	Route::get('user/edit/{id}', 'UserController@editUser');
	Route::post('user/editdata', 'UserController@editUserData');
	Route::get('user/delete/{id}', 'UserController@deleteUser');
	Route::get('user/restore/{id}', 'UserController@restoreUsers');
	Route::get('user/suspendrestore/{id}', 'UserController@suspendRestoreUsers');

	// Route for UserCsvExport...
	Route::post('user/export/csv', 'UserController@exportUserDataToCsv');
	
	Route::post('user/getTrashUsers', 'UserController@getTrashUsers');
	Route::get('users/trash', 'UserController@trash');
	Route::get('user/permanent/delete/{id}', 'UserController@deletePermanentUserData');
	Route::post('user/suspenduser', 'UserController@suspendUser');
	Route::get('users/suspended', 'UserController@suspended');
	
	Route::post('user/getSuspendedUsers', 'UserController@getSuspendedUsers');
	
	Route::post('user/bulk/action', 'UserController@bulkUserAction'); 
	Route::post('user/trash/bulk/action', 'UserController@bulkTrashUserAction');
	Route::post('user/suspend/bulk/action', 'UserController@bulkSuspendUserAction');
	
	
	
	Route::get('eventcodes', 'EventcodesController@index');
	Route::get('eventcodes/add/', 'EventcodesController@add');
	Route::post('eventcodes/add', 'EventcodesController@readCsv');
	Route::get('eventcodes/delete/{id}', 'EventcodesController@deleteEventCode');
	Route::post('eventcodes/bulk/action', 'EventcodesController@bulkEventCodesAction');
	
	Route::post('geteventcodes', 'EventcodesController@getEventCodes');
	
	
	Route::get('reports', 'ReportController@index');
	Route::post('report/all', 'ReportController@getReports');
	Route::get('report/feedback/{id}', 'ReportController@edit');
	Route::post('report/update', 'ReportController@update');
	
	
	Route::get('help', 'UserController@helpIndex');
	Route::post('help/getlist', 'UserController@getUserHelpList');
	Route::get('help/edit/{id}', 'UserController@UserHelpEdit');
	Route::post('help/reply', 'UserController@UserHelpReply');
	
	Route::get('album', 'AlbumController@index');
	Route::get('album/add/{id?}', 'AlbumController@add');
	Route::get('album/edit/{id}', 'AlbumController@edit');
	Route::get('album/delete/{id}', 'AlbumController@delete');
	Route::post('album/import/musics', 'AlbumController@importAlbumMusics');
	Route::get('album/import/bulk/{id?}', 'AlbumController@viewBulkImport');
	Route::post('album/import/bulk', 'AlbumController@storeBulkImport');
	Route::post('album/musics/reorder', 'AlbumController@reorderMusics');
	Route::get('album/musics', 'AlbumController@addMusics');
	Route::post('album/create', 'AlbumController@store');
	Route::post('album/update', 'AlbumController@update');
	Route::post('album/redirect', 'AlbumController@redirect');
	Route::post('album/getalbums', 'AlbumController@getAlbums');
	Route::get('albums', 'AlbumController@index');
	Route::get('album/restore/{id}', 'AlbumController@restoreAlbum');
	Route::post('album/getTrashAlbums', 'AlbumController@getTrashedAlbums');
	Route::get('album/trash', 'AlbumController@trash');
	Route::get('album/permanent/delete/{id}', 'AlbumController@deletePermanentAlbumData');
	
	Route::post('album/bulk/action', 'AlbumController@bulkAlbumAction');
	Route::post('album/trash/bulk/action', 'AlbumController@bulkTrashAlbumAction');
	Route::post('album/send_pushnotification', 'AlbumController@sendPushNotification');
	
	
	
	Route::get('announcement', 'AnnouncementController@index');
	Route::get('announcement/add', 'AnnouncementController@add');
	Route::post('announcement/create', 'AnnouncementController@create');
	Route::post('announcement/getaannouncement', 'AnnouncementController@getaannouncement');
	
	Route::post('announcement/bulk/action', 'AnnouncementController@bulkAnnouncementAction');
	
	Route::get('promocode', 'PromocodeController@index');
	Route::get('promocode/add', 'PromocodeController@add');
	Route::post('promocode/create', 'PromocodeController@create');
	Route::get('promocode/edit/{id}', 'PromocodeController@editPromocode');
	Route::get('promocode/delete/{id}', 'PromocodeController@deletePromocode');
	Route::post('promocode/update', 'PromocodeController@updatePromocode');
	
	Route::post('promocode/getpromocode', 'PromocodeController@getPromocode');
	Route::post('promocode/bulk/action', 'PromocodeController@bulkPromocodeAction');
	
	Route::get('manual-payment', 'UserController@manualPaymentIndex');
	Route::get('manual-payment/edit/{id}', 'UserController@manualPaymentEdit');
	Route::post('manual-payment/getlist', 'UserController@manualPaymentList'); 
	
	Route::get('artists', 'ArtistController@index');
	Route::get('artists/trash', 'ArtistController@trash');
	Route::get('artist/restore/{id}', 'ArtistController@restoreArtist');
	Route::get('artist/add', 'ArtistController@add'); 
	Route::get('artist/import/bulk', 'ArtistController@viewBulkImport');
	Route::post('artist/import/bulk', 'ArtistController@storeBulkImport');
	Route::get('artist/view/{id}', 'ArtistController@view');
	Route::post('artist/getartists', 'ArtistController@getArtists');
	Route::post('artist/getTrashArtists', 'ArtistController@getTrashedArtists');
	Route::get('artist/edit/{id}', 'ArtistController@editArtist');
	Route::post('artist/create', 'ArtistController@createArtist');
	Route::post('artist/editdata', 'ArtistController@editArtistData');
	Route::get('artist/delete/{id}', 'ArtistController@deleteArtistData');
	Route::get('artist/permanent/delete/{id}', 'ArtistController@deletePermanentArtistData');
	
	Route::post('artist/bulk/action', 'ArtistController@bulkArtistAction');
	Route::post('artist/trash/bulk/action', 'ArtistController@bulkTrashArtistAction');
	
	
	//---------Ajax route to add artist-----------------
	
	//--------------------------------------------------
	Route::get('musics', 'MusicController@index');
	Route::get('musics/add', 'MusicController@add');
	Route::post('musics/getmusics', 'MusicController@getMusics');
	Route::get('musics/edit/{id}', 'MusicController@edit');
	Route::post('musics/create', 'MusicController@createMusics');
	Route::post('musics/update', 'MusicController@update');
	
	Route::post('musics_bulk/update', 'MusicController@bulkMusicsUpdate');
	Route::get('musics/delete/{id}', 'MusicController@delete');
	//--------------Bulk Import--------------------
	// Route::get('musics/import','MusicController@getImport');
	// Route::post('musics/import','MusicController@postImport');
	//---------------------------------------------
	Route::get('live-streams', 'LiveStreamController@index');
	Route::get('live-stream/add', 'LiveStreamController@add');
	Route::post('live-stream/create', 'LiveStreamController@create');
	Route::post('live-stream/getlivestreams', 'LiveStreamController@getLivestreams');
	Route::get('live-stream/delete/{id}', 'LiveStreamController@delete');
	Route::get('live-stream/edit/{id}', 'LiveStreamController@edit');
	Route::post('live-stream/update', 'LiveStreamController@update');
	
	Route::get('pages', 'PageController@index');
	Route::get('page/add', 'PageController@add');
	Route::post('page/create', 'PageController@create');
	Route::post('page/getpages', 'PageController@getPages');
	Route::get('page/delete/{id}', 'PageController@delete');
	Route::get('page/edit/{id}', 'PageController@edit');
	Route::post('page/update', 'PageController@update');
	
	Route::get('play-lists', 'PlayListController@index');
	Route::get('play-lists/add', 'PlayListController@add');
	Route::post('play-lists/get-play-lists', 'PlayListController@getPlaylists');
	Route::get('playlist/edit/{id}', 'PlayListController@edit');
	Route::post('play-lists/create', 'PlayListController@create');
	Route::post('play-lists/editdata', 'PlayListController@update');
	Route::get('playlist/delete/{id}', 'PlayListController@delete');
	Route::get('playlist/trash', 'PlayListController@trash');
	Route::post('playlist/getTrashPlaylists', 'PlayListController@getTrashedPlaylists');
	Route::get('playlist/restore/{id}', 'PlayListController@restorePlayList');
	Route::get('playlist/permanent/delete/{id}', 'PlayListController@deletePermanentPlaylistData');
	Route::post('playlist/musics/reorder', 'PlayListController@reorderMusics');
	Route::post('playlist/bulk/action', 'PlayListController@bulkPlaylistAction'); 
	Route::post('playlist/trash/bulk/action', 'PlayListController@bulkTrashPlaylistAction');
	Route::post('playlist/noti', 'PlayListController@sendPushNotification');
	Route::post('playlist/updateOrder', 'PlayListController@updateOrder');
	Route::get('playlist/removefromhome/{id}', 'PlayListController@removefromhome');


	Route::get('user-play-lists', 'UserPlayListController@index');
	Route::post('user-play-lists/get-play-lists', 'UserPlayListController@getPlaylists');
	Route::get('user-playlist/edit/{id}', 'UserPlayListController@edit');
	Route::get('user-playlist/addtomain/{id}', 'UserPlayListController@addtomain');

	

	
	Route::post('importAudioFiles', 'AlbumController@importAudioFiles');
	Route::post('deleteAudioFiles', 'AlbumController@deleteAudioFiles');
	Route::post('getAudioMeta', 'AlbumController@getAudioMeta');
	
	Route::get('news', 'NewsController@index');
	Route::get('news/add', 'NewsController@add');
	Route::post('news/getnews', 'NewsController@getNews');
	Route::get('news/edit/{id}', 'NewsController@edit');
	Route::post('news/create', 'NewsController@create');
	Route::post('news/editdata', 'NewsController@update');
	Route::get('news/delete/{id}', 'NewsController@delete');
	Route::get('news/trash', 'NewsController@trash');
	Route::post('news/getTrashNews', 'NewsController@getTrashedNews');
	Route::get('news/restore/{id}', 'NewsController@restoreNews');
	Route::get('news/permanent/delete/{id}', 'NewsController@deletePermanentNewsData');
	Route::post('news/send_pushnotification', 'NewsController@sendPushNotification');
	Route::post('news/bulk/action', 'NewsController@bulkNewsAction'); 
	Route::post('news/trash/bulk/action', 'NewsController@bulkTrashNewsAction');


	Route::post('news/attachImage', 'NewsController@newsimages');
	
	
	Route::get('categories', 'CategoryController@index');
	Route::get('category/add', 'CategoryController@add');
	Route::post('category/reorder', 'CategoryController@reorderCategories');
	Route::post('category/getcategories', 'CategoryController@getCategories');
	Route::get('category/edit/{id}', 'CategoryController@edit');
	Route::post('category/create', 'CategoryController@create');
	Route::post('category/editdata', 'CategoryController@update');
	Route::get('category/delete/{id}', 'CategoryController@delete');

	Route::get('tags', 'TagController@index');
	Route::get('tag/add', 'TagController@add');
	Route::post('tag/gettags', 'TagController@getTags');
	Route::get('tag/edit/{id}', 'TagController@edit');
	Route::post('tag/create', 'TagController@create');
	Route::post('tag/editdata', 'TagController@update');
	Route::get('tag/delete/{id}', 'TagController@delete');
	Route::post('tag/gettagsajax', 'TagController@getTagsAjax');
	Route::post('tag/addtagsajax', 'TagController@createTagAjax'); 
	
	Route::get('subscriptions', 'SubscriptionController@index');
	Route::get('subscription/add', 'SubscriptionController@add');
	Route::post('subscription/getplans', 'SubscriptionController@getPlans');
	Route::get('subscription/edit/{id}', 'SubscriptionController@editPlan');
	Route::post('subscription/create', 'SubscriptionController@createPlan');
	Route::post('subscription/editdata', 'SubscriptionController@editPlanData');
	Route::get('subscription/delete/{id}', 'SubscriptionController@deletePlanData');
	
	Route::get('banners', 'BannerController@index');
	Route::get('banners/add', 'BannerController@add');
	Route::post('banners/get-banners', 'BannerController@getBanners');
	Route::get('banners/edit/{id}', 'BannerController@edit');
	Route::post('banners/create', 'BannerController@create');
	Route::post('banners/editdata', 'BannerController@update');
	Route::get('banners/delete/{id}', 'BannerController@delete');
	Route::post('getbanneritems', 'BannerController@getBannerItems');
	Route::get('banner/trash', 'BannerController@trash');
	Route::post('banner/getTrashBanners', 'BannerController@getTrashedBanners');
	Route::get('banner/restore/{id}', 'BannerController@restoreBanner');
	Route::get('banner/permanent/delete/{id}', 'BannerController@deletePermanentBannersData');
	Route::post('banner/updateOrder', 'BannerController@updateOrder');
	
	
	Route::post('banner/bulk/action', 'BannerController@bulkBannersAction'); 
	Route::post('banner/trash/bulk/action', 'BannerController@bulkTrashBannersAction');
	
	Route::get('videos', 'VideoController@index');
	Route::get('videos/add', 'VideoController@add');
	Route::post('videos/get-videos', 'VideoController@getVideos');
	Route::get('video/edit/{id}', 'VideoController@edit');
	Route::post('videos/create', 'VideoController@create');
	Route::post('videos/editdata', 'VideoController@update');
	Route::get('video/delete/{id}', 'VideoController@delete');
	Route::get('videos/trash', 'VideoController@trash');
	Route::post('videos/send_pushnotification', 'VideoController@sendPushNotification');

	Route::post('videos/sendPushNotificationForTest', 'VideoController@sendPushNotificationForTest');
	
	Route::post('video/bulk/action', 'VideoController@bulkVideoAction');
	Route::post('video/trash/bulk/action', 'VideoController@bulkTrashVideoAction');
	
	
	Route::post('video/getTrashVideos', 'VideoController@getTrashVideos');
	Route::get('video/restore/{id}', 'VideoController@restoreVideo');
	Route::get('video/permanent/delete/{id}', 'VideoController@deletePermanentVideoData');
	Route::post('video/thumb/generate', 'VideoController@generateThumbnail');
	Route::post('video/thumb/multiplegenerate', 'VideoController@generateThumbMultiple');
	Route::get('video/thumb/set/{id}', 'VideoController@setThumbnail');
	Route::get('video/import/bulk', 'VideoController@viewBulkImport');
	Route::post('videos/import', 'VideoController@postBulkImport');
	Route::post('importVideoFiles', 'VideoController@importVideoFiles');
	Route::post('deleteVideoFiles', 'VideoController@deleteVideoFiles');

	Route::post('news/sendPushNotification_News', 'NotificationController@sendPushNotification_News');
	Route::post('news/sendPushNotification_Album', 'NotificationController@sendPushNotification_Album');
	Route::post('news/sendPushNotification_Video', 'NotificationController@sendPushNotification_Video');
	Route::post('news/sendPushNotification_Playlist', 'NotificationController@sendPushNotification_Playlist');

	Route::post('shortcode_create', 'ShortcodeController@create');
	Route::post('shortcode_delete', 'ShortcodeController@delete');

	Route::get('export/all', 'ExportController@all');
	Route::post('export/create', 'ExportController@generate');
	Route::post('export/getDataByArtist', 'ExportController@getDataByArtist');
	Route::post('export/getDateByFilter', 'ExportController@getDateByFilter');




});