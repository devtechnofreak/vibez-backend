//$.noConflict();

jQuery(document).ready(function($) {

	//"use strict";

	[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
		new SelectFx(el);
	} );

	jQuery('.selectpicker').selectpicker;


	$('#menuToggle').on('click', function(event) {
		$('body').toggleClass('open');
	});

	$('.search-trigger').on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();
		$('.search-trigger').parent('.header-left').addClass('open');
	});

	$('.search-close').on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();
		$('.search-trigger').parent('.header-left').removeClass('open');
	});

	$(document).on('click','.items_result ul li.currentitems', function(){
		console.log("132");
		var currentTagId = $(this).attr('data_id');
		var currentTagName = $(this).text();
		$('.display_items ul').html('<li data_id="'+currentTagId+'">'+currentTagName+'<a class="close_tag">X</a></li>');
		$('.banner_type_id').val(currentTagId);
		$('.items_result').hide();
	});
/*
    $("#itemName").keyup(function() {
    	 var banner_type = jQuery('.banner_type').val();
		var csrf_token = jQuery(document).find('input[name="_token"]').val();	
		var current_val = $(this).val();
       if (current_val == "") {
           $(".add_artist .tags_result ul").empty();
       }else{ 
       	$.ajax({
       	    beforeSend: function(xhrObj){
	                xhrObj.setRequestHeader("X-CSRF-TOKEN",csrf_token);
	        },
		   url: site_url+"/admin/getbanneritems",
		   data: {
		      'search': current_val,
		      'type': banner_type
		   },	   
		   dataType: 'json',
		   success: function(data) {
		   	var data = data.data;
		   	var htmlcontent = '';
		     if(data.length > 0){
		     	for(var i=0;i<data.length;i++){ 
		     		htmlcontent+= '<li class="currentitems" data_id="'+data[i].id+'">'+data[i].title+'</li>';	
		     	}
		     	console.log(htmlcontent);
		     	$('.items_result ul').html(htmlcontent);
		     	$('.items_result').show();
		     }else{
		     	if(banner_type == ""){
		     		banner_type = "items";
		     	}
		     	htmlcontent+= '<li>No '+banner_type+' Found !</li>';
		     	$('.items_result ul').html(htmlcontent);
		     	$('.items_result').show();
		     }
		   },
		   type: 'POST'
		});
       }
	});
*/
	$(document).on('change', 'select.banner_type', function() {
		if($(this).val() !== ''){
	       	$.ajax({
	       	    beforeSend: function(xhrObj){
		                xhrObj.setRequestHeader("X-CSRF-TOKEN",jQuery(document).find('input[name="_token"]').val());
		        },
			   url: site_url+"/admin/getbanneritems",
			   data: {
			      'type': $(this).val()
			   },	   
			   dataType: 'json',
			   success: function(data) {
			   	var data = data.data;
			   	var htmlcontent = '';
			     if(data.length > 0){
			     	for(var i=0;i<data.length;i++){ 
			     		htmlcontent+= '<option value="'+data[i].id+'">'+data[i].title+'</option>';	
			     	}
			     	$('.banner_item').html(htmlcontent);
			     }
			   },
			   type: 'POST'
			});
		} else{
			$('.banner_item').html('');
		}
	});
	//console.log('here');
	if($('#dob').length >0 ){
		$('#dob').datepicker({
			dateFormat: "dd-mm-yy",
			changeYear: true,
			changeMonth: true,
			yearRange: "1920:2018",
		});		
	}
	if($('.release_date').length >0 ){
		$('.release_date').datepicker({
			dateFormat: "dd-mm-yy",
			changeYear: true,
			changeMonth: true,
			yearRange: "1970:2050",
		});
	}
	if($('.dob').length >0 ){
		$('.dob').datepicker({
			dateFormat: "dd-mm-yy",
			changeYear: true,
			changeMonth: true,
			yearRange: "1900:1998",
		});
	}

	if($('#start_time').length >0 ){
		$('#start_time').datetimepicker({
			format: "DD-MM-YYYY HH:mm:ss",
			minDate: moment.utc(),
		});
	}

	$(".add_tag_ajax").click(function(){
		var csrf_token = jQuery(document).find('input[name="_token"]').val();
		var tag_name = $('input#tags').val(); 
		
		if(tag_name != ""){
			$.ajax({
	       	    beforeSend: function(xhrObj){
		                xhrObj.setRequestHeader("X-CSRF-TOKEN",csrf_token); 
		        },
			   url: site_url+"/admin/tag/addtagsajax",
			   data: {
			      'search': tag_name
			   },		   
			   dataType: 'json',
			   success: function(data) {
			   	console.log(data);
			   	$('.display_tags ul').append('<li data_id="'+data+'">'+tag_name+'<a class="close_tag">X</a></li>');
				$('.add_tag_ajax').hide();
				$('.tags_result').hide();
			   },
			   type: 'POST'
			});
		}
	});
	
	$('.add_music_btn').click(function(){
		if($('.display_tags ul li').length > 0){
			var tags = [];
			$('.display_tags ul li').each(function(){
				var tagid 	= $(this).attr('data_id');
				var tagname = $(this).text();
				// tags.push({"id":tagid,"name" : tagname});
				tags.push(tagid);
			});	
			var jsonobj = JSON.stringify(tags);
			// $('#tags').val(jsonobj); 
			$('#tagsId').val(tags);
		}
		$('.add_artist').submit();
	});
	
	$('.update_music_btn').click(function(){
		if($('.display_tags ul li').length > 0){
			var tags = [];
			$('.display_tags ul li').each(function(){
				var tagid 	= $(this).attr('data_id');
				var tagname = $(this).text();
				// tags.push({"id":tagid,"name" : tagname});
				tags.push(tagid);
			});	
			var jsonobj = JSON.stringify(tags);
			// $('#tags').val(jsonobj); 
			$('#tagsId').val(tags);
		}
		if($('.display_artists ul li').length > 0){
			var artists = [];
			$('.display_artists ul li').each(function(){
				var artistid 	= $(this).attr('data_id');
				var artistname = $(this).text();
				// artists.push({"id":artistid,"name" : artistname});
				artists.push(artistid);
			});	
			var jsonobj = JSON.stringify(artists);
			$('#artistsId').val(artists); 
			
		}
		$('.music_edit_form').submit();
	});

	$('.update_artist_btn').click(function(){
		if($('.display_tags ul li').length > 0){
			var tags = [];
			$('.display_tags ul li').each(function(){
				var tagid 	= $(this).attr('data_id');
				var tagname 	= $(this).attr('data_name');
				tags.push({"id":tagid,"name" : tagname});
			});	
			var jsonobj = JSON.stringify(tags);
			$('.artist_tags').val(jsonobj);
		}
		$('.artist_edit_form').submit();
	});
	$('.add_playlist_btn').click(function(){
		$('.add_playlist').submit();
	});
	$('.add_tracks').click(function(){
		$('form.update_album').append('<input name="add_music" value="true" type="hidden">');
		$('.update_album').submit();
	});
	$('.add_news_btn').click(function(){
		$('.add_news').submit();
	});
	$('.add_banner_btn').click(function(){
		$('.add_banner').submit();
	});
	$('.add_video_btn').click(function(){
		$('.add_video').submit();
	})
	$('.update_banner_btn').click(function(){
		$('.banner_edit_form').submit();
	})
	$(document).on('click','.close_tag',function(){
		$(this).parent().remove();
	})
	$(document).on('click','.tags_result ul li.currenttags', function(){
		var currentTagId = $(this).attr('data_id');
		var currentTagName = $(this).text();
		$('.display_tags ul').append('<li data_id="'+currentTagId+'" data_name="'+currentTagName+'">'+currentTagName+'<a class="close_tag">X</a></li>');
		$('.add_tag_ajax').hide();
		$('.tags_result').hide();
	});
	$("#tags").keyup(function() { 
		var csrf_token = jQuery(document).find('input[name="_token"]').val();	
		var current_val = $(this).val();
		$('.home_search_section #searchform .fa-spinner').show();
       if (current_val == "") {
           $(".add_artist .tags_result ul").empty();
       }else{ 
       	$.ajax({
       	    beforeSend: function(xhrObj){
	                xhrObj.setRequestHeader("X-CSRF-TOKEN",csrf_token);
	        },
		   url: site_url+"/admin/tag/gettagsajax",
		   data: {
		      'search': current_val
		   },		   
		   dataType: 'json',
		   success: function(data) {
		   	var htmlcontent = '';
		     if(data.length > 0){
		     	for(var i=0;i<data.length;i++){
		     		htmlcontent+= '<li class="currenttags" data_id="'+data[i].tag_id+'" data_name="'+data[i].name+'">'+data[i].name+'</li>';	
		     	}
		     	console.log(htmlcontent); 
		     	$('.tags_result ul').html(htmlcontent);
		     	$('.tags_result').show();
		     	$('.add_tag_ajax').hide();
		     }else{
		     	htmlcontent+= '<li>No Tags Found !</li>';
		     	$('.tags_result ul').html(htmlcontent);
		     	$('.add_tag_ajax').show();
		     	$('.tags_result').show();
		     }
		   },
		   type: 'POST'
		});
       }
	});
	
	//------Add Artist Ajax----------------------
	$(".add_artist_ajax").click(function(){
		var csrf_token = jQuery(document).find('input[name="_token"]').val();
		var artist_name = $('input#artists').val();
		if(artist_name != ""){
			$.ajax({
	       	    beforeSend: function(xhrObj){
		                xhrObj.setRequestHeader("X-CSRF-TOKEN",csrf_token); 
		        },
			   url: site_url+"/admin/artist/addartistsajax",
			   data: {
			      'search': artist_name
			   },		   
			   dataType: 'json',
			   success: function(data) {
			   	console.log(data);
			   	$('.display_artists ul').append('<li data_id="'+data+'">'+artist_name+'<a class="close_tag">X</a></li>');
				$('.add_artist_ajax').hide();
				$('.artists_result').hide();
			   },
			   type: 'POST'
			});
		}
	});
	$('.add_artist_btn').click(function(){
		$('form.add_artist').submit();
	})
	
	$('#video').change(function()
	{
		if($(this).prop('files')[0])
		{
			$('.generate_thumbnail').removeClass('d-none');
			$('#select_thumbnail').removeClass('d-none');
		}
	});
	$(document).find('.generate_thumbnail').on('click',function(){
		var formdata = new FormData();
		var target = $(this);
		target.append('<img id="ajax_loader" src="'+site_url+'/images/ajax-loader.gif" style="width:20px;"/>');
		formdata.append("video",$('#video').prop('files')[0]);
			$.ajax({
	       	    beforeSend: function(xhrObj){
		                xhrObj.setRequestHeader("X-CSRF-TOKEN",jQuery(document).find('input[name="_token"]').val()); 
		        },
			   url: site_url+"/admin/video/thumb/generate",
			   data: formdata,		   
			   dataType: 'json',
		       contentType: false,
    		   processData: false,
			   success: function(data) {
			   	$(document).find('#ajax_loader').hide();
			   	var htmlcontent = '';
			   	htmlcontent+='<img src="'+site_url+'/'+data.image+'" class="artist_image img-thumbnail" /><input type="hidden" name="filename" value="'+data.filename+'"/>';
			   	target.parent().append(htmlcontent);
			   	$('#select_thumbnail').prop('checked', true);
			   },
			   type: 'POST'
			});		
	});
	
	$(document).on('click','.multiplegenerate',function(){
		
		var target = $(this);
		var videoname = $(this).closest('tr').find('td:eq(0)').find('input[type="hidden"]').val();
		var attrname = $(this).closest('tr').find('td:eq(0)').find('input[type="hidden"]').attr('name');
		var attrname = attrname.replace("filename", "generated_thumbnail");
		target.append('<img id="ajax_loader" src="'+site_url+'/images/ajax-loader.gif" style="width:20px;"/>');
		$.ajax({
	       	    beforeSend: function(xhrObj){
		                xhrObj.setRequestHeader("X-CSRF-TOKEN",jQuery(document).find('input[name="_token"]').val()); 
		        },
			   url: site_url+"/admin/video/thumb/multiplegenerate",
			   data: {filename	: videoname},
			   success: function(data) {
			   	console.log(data); 
			   	$(document).find('#ajax_loader').hide();
			   	var htmlcontent = '';
			   	htmlcontent+='<img src="'+site_url+'/'+data.image+'" class="artist_image img-thumbnail" /><input type="hidden" name="'+attrname+'" value="'+data.filename+'"/>';
			   	target.parent().append(htmlcontent);
			   	target.remove();
			   	$('#select_thumbnail').prop('checked', true);
			   	
			   },
			   type: 'POST'
			});		
	})
	
	$('.add_music_btn').click(function(){
		if($('.display_artists ul li').length > 0){
			var artists = [];
			$('.display_artists ul li').each(function(){
				var artistid 	= $(this).attr('data_id');
				var artistname = $(this).text();
				// artists.push({"id":artistid,"name" : artistname});
				artists.push(artistid);
			});	
			var jsonobj = JSON.stringify(artists);
			$('#artistsId').val(artists); 
			
		}
		$('.add_artist').submit();
	});
	$('.update_artist_btn').click(function(){
		if($('.display_artists ul li').length > 0){
			var artists = [];
			$('.display_artists ul li').each(function(){
				var artistid 	= $(this).attr('data_id');
				var artistname = $(this).attr('data_name');
				artists.push({"id":artistid,"name" : artistname});
			});	
			var jsonobj = JSON.stringify(artists);
			$('.artist_tags').val(jsonobj);
		}
		console.log(jsonobj);
		$('.artist_edit_form').submit();
	});
	$('.add_playlist_btn').click(function(){
		$('add_playlist').submit(); 
	}); 
	$(document).on('click','.close_tag',function(){
		$(this).parent().remove();
	})
	$(document).on('click','.artists_result ul li.currentartists', function(){
		var currentArtistId = $(this).attr('data_id');
		var currentArtistName = $(this).text();
		$('.display_artists ul').append('<li data_id="'+currentArtistId+'" data-name="'+currentArtistName+'">'+currentArtistName+'<a class="close_tag">X</a></li>');
		$('.add_artist_ajax').hide();
		$('.artists_result').hide();
	});
	$("#artists").keyup(function() { 
		var csrf_token = jQuery(document).find('input[name="_token"]').val();	
		var current_val = $(this).val();
		$('.home_search_section #searchform .fa-spinner').show();
       if (current_val == "") {
           $(".add_artist .tags_result ul").empty();
       }else{ 
       	$.ajax({
       	    beforeSend: function(xhrObj){
	                xhrObj.setRequestHeader("X-CSRF-TOKEN",csrf_token);
	        },
		   url: site_url+"/admin/artist/getartistsajax",
		   data: {
		      'search': current_val
		   },		   
		   dataType: 'json',
		   success: function(data) {
		   	var htmlcontent = '';
		     if(data.length > 0){
		     	for(var i=0;i<data.length;i++){
		     		htmlcontent+= '<li class="currentartists" data_id="'+data[i].id+'">'+data[i].name+'</li>';	
		     	}
		     	console.log(htmlcontent);
		     	$('.artists_result ul').html(htmlcontent);
		     	$('.artists_result').show();
		     	$('.add_artist_ajax').hide();
		     }else{
		     	htmlcontent+= '<li>No Artist Found !</li>';
		     	$('.artists_result ul').html(htmlcontent);
		     	$('.add_artist_ajax').show();
		     	$('.artists_result').show();
		     }
		   },
		   type: 'POST'
		});
       }
	});
	//----------------------------------------------------------
	

});